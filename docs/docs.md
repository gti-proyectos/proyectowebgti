##manifest.json
El archivo manifest.json es un archivo JSON que le permite configurar la apariencia y 
el comportamiento de inicio de una aplicación web marcada o agregada a la pantalla de 
inicio de un dispositivo. 

El manifiesto contiene una propiedad de matriz, iconos que pueden usarse para 
especificar una lista de objetos de imagen, cada uno de los cuales puede tener src, 
tamaños y propiedades de tipo que describen el icono. Se guarda en root de la app

##browserconfig.xml
Browserconfig.xml es un archivo XML que se puede usar para especificar 
iconos de mosaico para Microsoft Windows. También se coloca en la raíz del sitio web.

##favicon
Siguiendo recomendaciones de diseño y buenas prácticas se ha creado los siguientes fav icon:

Para los navegadores web:
- 16x16.ico
- 32x32.ico (Safari)
- 16x16.png
- 32x32.png

Apple Touch
- apple-touch-icon-180.png

Google y Android:
- icon-192.png

Safari pinned svg:
- safari-pinned-tab.svg

El Tile de Microsfot
- mstile-150x150.png

