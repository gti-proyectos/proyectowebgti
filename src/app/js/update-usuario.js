/**
 * Autor: Ruben Pardo
 * Fecha: 08/05/2020
 * Modelo Vista Controlador Editar un cliente
 *
 */

var ModeloModificarUsuario = {
    controlador: {},

    editarUsuario: async function (usuario) {

        try{
            console.log(JSON.stringify(usuario))
            let form = new FormData();
            form.append("modificar","true");
            form.append("data",JSON.stringify(usuario));

            await fetch('../../src/api/v1.0/usuario',{
                method: 'POST',
                body: form,
            })
                .then(respuesta => {
                    if (respuesta.status === 200) {
                        return respuesta.json();
                    } else {
                        this.controlador.errorUpdate();
                        throw Error(respuesta.statusText);

                    }
                })


            this.controlador.editarUsuarioOk(usuario);


        }catch (e) {
            console.error(e);
        }
    },
};

var VistaModificarUsuario = {

    controlador: {},
    formulario:{},
    modal:{},
    idCliente: -1,
    idUsuario: -1,


    preparar:function (idError, idCarga,idFormulario,idModal,idCloseModal) {
        this.bloqueError = document.getElementById(idError);
        this.bloqueCarga = document.getElementById(idCarga);
        this.formulario = document.getElementById(idFormulario);
        this.modal = document.getElementById(idModal);

    },
    mostrarModal: function(){
        this.bloqueError.style.display = "none";
        this.modal.style.display = "block";
    },

    esconderModal: function(){
        this.modal.style.display = "none";
        this.formulario.nombre.value =  "";
        this.formulario.correo.value =  "";
        this.formulario.rol.value =  -1;
    },

    mostrarError:function () {
        this.bloqueError.style.display = "block"

    },

    // funciones para el controlar del formulario
    prepararFormulario: function(usuario){
        // cogemos todos los inputs del formulario y le asignamos los valores del cliente
        this.formulario.nombre.value =  usuario.nombre == null ? "" : usuario.nombre;
        this.formulario.correo.value =  usuario.correo == null ? "" : usuario.correo;
        this.formulario.apellidos.value =  usuario.apellido == null ? "" : usuario.apellido;
        this.formulario.rol.value =  usuario.rolId;
    },

    getUsuarioFormulario: function(){
        let usuario = {};

        // cogemos todos los inputs del formulario y le asignamos los valores del cliente
        usuario["id"] = this.idUsuario;
        usuario["cliente"] = this.idCliente;
        usuario["nombre"] = this.formulario.nombre.value;
        usuario["apellidos"] = this.formulario.apellidos.value;
        usuario["correo"] = this.formulario.correo.value;
        // si intenta cambiarse el mismo a agricultor volver a ponerle admin
        usuario["rol"] =  this.formulario.rol.value;

        return usuario;
    },

}

var ControladorModificarUsuario = {
    modelo: ModeloModificarUsuario,
    vista: VistaModificarUsuario,

    error: false,// controla si ha habido un error en la obtencio de datos

    iniciar: function (usuario,idCliente) {
        this.modelo.controlador = this;
        this.vista.idUsuario = usuario.id;
        this.vista.idCliente = idCliente;
        this.vista.controlador = this;
        this.vista.prepararFormulario(usuario);
        this.vista.mostrarModal();
    },

    representarError: function () {
        this.vista.mostrarError();
    },

    // eventos del on sumbit y on reset del formulario de los datos
    onSumbitFormulario: function(e){
        e.preventDefault();
        this.modelo.editarUsuario(this.vista.getUsuarioFormulario())
    },
    onResetFormulario: function(e){
      e.preventDefault();
      this.vista.esconderModal();
    },

    errorUpdate: function(){
        this.vista.mostrarError();
    },
    editarUsuarioOk: function(usuario){
        // llamar al callback de la lista para actualizarlo
        this.vista.esconderModal();
        window.location.reload();
    },

}
