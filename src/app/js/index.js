/**
 * Autor: Raul
 * Fecha: 03/03/2020
 * Index.js
 * Funcionalidad del index.html
 */

// funcionalidad para mostrar o esconder el menu flotante



var scrollPos = 0;

window.onload = function(){
    if(window.pageYOffset > 667){
        let caja = document.getElementById("caja-flotante");
        caja.style.background = "rgba(255, 255, 255,1)";
    }
}

// llamar al principio para cuando se refresco
comprobarSeccionActual();

// adding scroll event
window.addEventListener('scroll',  ()=> {

    comprobarSeccionActual();


    let caja = document.getElementById("caja-flotante");

    // esconder el desplegable cuando sube
    let desplegable = document.getElementById("navbarSupportedContent");
    desplegable.classList.remove("show");

    if (window.pageYOffset <= 50) {
        // mostrar el menú siempre al cargar la página con un grado de transparencia
        caja.style.background = "rgba(255, 255, 255, 0.4)";
    } else {
        // detects new state and compares it with the new one
        if ((document.body.getBoundingClientRect()).top > scrollPos) {
            caja.style.display = "flex";
            if (window.pageYOffset <= window.innerHeight-50) {
                //si entra en la primera sección, aplicar transparencia
                caja.style.background = "rgba(255, 255, 255, 0.4)";
            } else {
                //si no está en la primera sección, fondo blanco
                caja.style.background = "rgba(255, 255, 255,1)";
            }

        } else {
            caja.style.display = "none";


        }
    }

    // Guardamos la posicion para la siguiente vuelta
    scrollPos = (document.body.getBoundingClientRect()).top;
});


function comprobarSeccionActual(){

    //CADA SECTION OCUPA 100VH por tanto cogemos el page y offset y dividimos por el 100vh
    // obtenemos en que seccion estamos


    // obtenemos el tamaño de cada seccion
    let seccionHeader = document.getElementById("ir_arriba").clientHeight;
    let seccionCarac = document.getElementById("sec-caracteristicas").clientHeight;
    let seccionRes = document.getElementById("resultados").clientHeight;


    // cada seccion empezara en la suma de px de todos los anteriores
    let caracEmpieza = seccionHeader ;
    let resEmpieza = caracEmpieza + seccionCarac;
    let nosEmpeiza = resEmpieza + seccionRes;

    // cogemos todos los items del nav
    let itemsTodos = document.getElementById("navbar-nav").children;
    // quitamos la clase "actual"
    for(let i = 0; i<itemsTodos.length; i++){
        itemsTodos[i].classList.remove("actual")
    }

    let caracteristicas = itemsTodos[0];
    let resultados = itemsTodos[2];
    let nosotros = itemsTodos[4];

    // donde esta el scroll pintamos la clase actual
    if(window.pageYOffset>=nosEmpeiza){
        console.log("estoy en nos")
        nosotros.classList.add("actual")
    }else if(window.pageYOffset>= resEmpieza){
        console.log("estoy en res")
        resultados.classList.add("actual")
    }else if(window.pageYOffset >= caracEmpieza){
        console.log("estoy en carac")
        caracteristicas.classList.add("actual")
    }

}
