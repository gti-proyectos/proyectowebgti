/**
 * Autor: Carlos Ramirez Dorado
 * Fecha: 21/05/2020
 * Modelo Vista Controlador Crear una temporada
 *
 */
var ModeloCrearTemporada = {
    controlador: {},
    crearTemporada: async function (temporada) {

        try{
            let form = new FormData();
            form.append("modificar","false");
            form.append("data",JSON.stringify(temporada));

            await fetch('../../src/api/v1.0/temporada',{
                method: 'POST',
                body: form,
            })
                .then(respuesta => {
                    if (respuesta.status === 200) {
                        return respuesta.json();
                    } else {
                        throw Error(respuesta.statusText);

                    }
                })


            this.controlador.crearTemporadaOk();


        }catch (e) {
            this.controlador.errorUpdate();
            console.error(e);
        }
    }
}
var VistaCrearTemporada = {
    bloqueError: {},
    controlador: {},
    formulario:{},
    modal:{},
    idCliente: -1,
    parcelasMultiSelectSelected:[],


    preparar:function (idError,idFormulario,idModal,idCliente) {
        this.bloqueError = document.getElementById(idError);
        this.formulario = document.getElementById(idFormulario);
        this.modal = document.getElementById(idModal);
        this.idCliente = idCliente;

        this.prepararMultiSelectParcelas();
    },

    //--------------------------------------------------------------------------
    //cogemos las parcelas que nos llegan de detalles-campo.html
    //--------------------------------------------------------------------------------
    prepararMultiSelectParcelas:function(){
        let parcelas = JSON.parse(localStorage.getItem("parcelas-detallesCampo"))[0];
        //rellenar el select de parcelas del modal de crear temporadas
        let htmlSelect = "";
        for(let i = 0;i<parcelas.length;i++){
            htmlSelect+=`<option value="${parcelas[i].id}">${parcelas[i].nombre}</option>`;
        }
        $("#select-parcela-temporada")
            .html(htmlSelect)
            .selectpicker('refresh');
    },

    mostrarModal: function(){
        this.bloqueError.style.display = "none";
        this.modal.style.display = "block";
    },

    esconderModal: function(){
        this.modal.style.display = "none";
        this.formulario.nombre.value =  "";
        this.formulario.descripcion.value =  "";
        $('#select-parcela-temporada').selectpicker("deselectAll", false).selectpicker("refresh");
    },

    mostrarError:function () {
        this.bloqueError.style.display = "block"

    },

    // -> T/F
    // comprueba que se cumplen los campos obligatorios
    // nombre y +1 parcela seleccionada
    checkFormulario: function(){

        let valid = true;
        let name = this.formulario.nombre.value;
        this.parcelasMultiSelectSelected =$("#select-parcela-temporada").val();
        if(name.trim().length<=0){
            valid = false;
        }
        if(this.parcelasMultiSelectSelected == null){
            valid = false;
        }
        return valid

    },

    getTemporadaFormulario: function(){
        let temporada = {};

        // cogemos todos los inputs del formulario y le asignamos los valores del cliente
        temporada["idCliente"] = this.idCliente;
        temporada["nombre"] = this.formulario.nombre.value;
        temporada["descripcion"] = this.formulario.descripcion.value;
        temporada["fechaInicio"] = getToday();
        temporada["parcela"] = this.parcelasMultiSelectSelected;

        return temporada;
    },

}
var ControladorCrearTemporada = {
    modelo: ModeloCrearTemporada,
    vista: VistaCrearTemporada,

    error: false,// controla si ha habido un error en la obtencio de datos

    iniciar: function () {
        this.modelo.controlador = this;
        this.vista.controlador = this;
        this.vista.mostrarModal();
    },

    representarError: function () {
        this.vista.mostrarError();
    },

    // eventos del on sumbit y on reset del formulario de los datos
    onSumbitFormulario: function(e){
        e.preventDefault();
        if(this.vista.checkFormulario()){
            this.modelo.crearTemporada(this.vista.getTemporadaFormulario())
        }else{
            this.vista.mostrarError();
        }

    },
    onResetFormulario: function(e){
        e.preventDefault();
        this.vista.esconderModal();
    },

    errorUpdate: function(){
        this.vista.mostrarError();
    },
    crearTemporadaOk: function(){
        // llamar al callback de la lista para actualizarlo
        this.vista.esconderModal();
        window.location.reload();
    },
}