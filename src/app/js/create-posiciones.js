/**
 * Autor: Ruben Pardo
 * Fecha: 10/05/2020
 * Modelo Vista Controlador Crear posiciones
 *
 */

var ModeloCrearSondas = {
    controlador: {},

    postSondas: async function (data) {

        try{
            let form = new FormData();
            form.append("modificar","false");
            form.append("data",JSON.stringify(data));


            await fetch('../../src/api/v1.0/posiciones',{
                method: 'POST',
                body: form,
            })
                .then(respuesta => {
                    if (respuesta.status === 200) {
                        return respuesta.json();
                    } else {
                        this.controlador.representarError();
                        throw Error(respuesta.statusText)
                    }
                })


            this.controlador.crearOk();


        }catch (e) {
            console.error(e);
        }
    },
};

var VistaCrearSondas = {

    bloqueError: {},
    bloqueOk: {},
    bloqueCarga:{},
    controlador: {},
    modal:{},
    idParcela: {},

    preparar:function (idError, idCarga,idFormulario,idBloqueOk,idParcela) {
        this.bloqueError = document.getElementById(idError);
        this.bloqueCarga = document.getElementById(idCarga);
        this.bloqueOk = document.getElementById(idBloqueOk);
        this.formulario = document.getElementById(idFormulario);
        this.idParcela = idParcela;
        this.esconderError();
        this.esconderOk();
    },

    mostrarError:function () {
        this.bloqueError.style.display = "block"

    },
    esconderError: function(){
        this.bloqueError.style.display = "none";
    },

    mostrarOk:function () {
        this.esconderError();
        this.bloqueOk.style.display = "block"
    },
    esconderOk:function () {
        this.bloqueOk.style.display = "none"
    },

    checkForm: function(){
        // ningun campo vacio
        let valid = true;
        let arrayLat = document.querySelectorAll("input[name='lat']");
        let arrayLng = document.querySelectorAll("input[name='lng']");
        let arrayNombres = document.querySelectorAll("input[name='nombre']");

        arrayLat.forEach(element =>{
            if(element.value.trim().length===0){
                valid = false;
            }
        });

        arrayLng.forEach(element =>{
            if(element.value.trim().length===0){
                valid = false;
            }
        });
        arrayNombres.forEach(element =>{
            if(element.value.trim().length===0){
                valid = false;
            }
        });


        return valid
    },

    getData: function(){
        let data = {posiciones:[]};
        let i = 0;
        let arrayNombres = document.querySelectorAll("input[name='nombre']");
        let arrayLat = document.querySelectorAll("input[name='lat']");
        let arrayLng = document.querySelectorAll("input[name='lng']");
        for(i=0;i<arrayLat.length;i++){
            data.posiciones.push({lat:arrayLat[i].value,lng:arrayLng[i].value,nombre:arrayNombres[i].value})
        }
        data['parcela'] = this.idParcela;
        console.log(data)
        return data;
    },



}

var ControladorCrearSondas = {
    modelo: ModeloCrearSondas,
    vista: VistaCrearSondas,

    error: false,// controla si ha habido un error en la obtencio de datos

    iniciar: function (idParcela) {
        this.modelo.controlador = this;
        this.modelo.idParcela = idParcela;
        this.vista.controlador = this;
    },

    representarError: function () {
        this.vista.mostrarError();
    },

    enviar(){
        this.vista.esconderError();
        if(this.vista.checkForm()){
            this.modelo.postSondas(this.vista.getData())
        }else{
            this.vista.mostrarError();
        }
    },
    mostrarOk:function(){
        this.vista.mostrarOk()
    },

    crearOk: function(){
        this.mostrarOk()
        setTimeout(function () {
            window.location.href = "panel-admin-tecnico-sondas.html";
        },1500)
    },
}

// control de añadir campos al formulario
$(document).ready(function(){
    var maxField = 10; //Input fields increment limitation
    var addButton = $('.add_button'); //Add button selector
    var wrapper = $('.form-element'); //Input field wrapper
    var fieldHTML = `
             <div>
                <h2>Nueva Sonda</h2>
                     <div class="pair">
                        <label>Nombre</label>
                        <input type="text" name="nombre" value="" placeholder="Nombre" required/>
                    </div>
                    <div class="pair">
                        <label>Latitud</label>
                        <input type="number" name="lat" value="" placeholder="Latitud" required/>
                    </div>
                    <div class="pair">
                        <label>Longitud</label>
                        <input type="number" name="lng" value="" placeholder="Longitud" required/>
                    </div>
                    <a href="javascript:void(0);" class="remove_button" title="Remove field">Eliminar Sonda</a>
                </div>`
        ; //New input field html
    var x = 1; //Initial field counter is 1
    $(addButton).click(function(){ //Once add button is clicked
        x++; //Increment field counter
        $(wrapper).append(fieldHTML); // Add field html
    });
    $(wrapper).on('click', '.remove_button', function(e){ //Once remove button is clicked
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter

    });
});