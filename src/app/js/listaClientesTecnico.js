/**
 * Autor: Rubén Pardo Casanova
 * GTI GRUPO 11
 * Fecha: 09/05/2020
 *
 * Modelo vista controlador que gestiona la lista de clientes para el panel tecnico
 *
 */

var ModeloListaClientesTecnico = {

    controlador: {},

    todosLosClientes:{},//contendrá todos los clientes siempre
    clientesFiltrados:{},// contendrá los clientes filtrados cuando se use el buscador o el ordenar

    paginaActual: 1,

    // al principio no estan ordenados
    // true = ASC, false = DESC
    ordenNombre: false,
    ordenCorreo: false,
    ordenTelefono: false,

    obtenerDatos: async function () {

        try{
            let clientes = await fetch('../../src/api/v1.0/cliente')
                .then(respuesta =>{
                    if(respuesta.status === 200){
                        return respuesta.json();
                    }else{
                        this.controlador.error = true;
                        throw Error(respuesta.statusText);
                    }
                });

            this.todosLosClientes = clientes;
            this.clientesFiltrados = clientes;
            this.controlador.representarDatos(this.clientesFiltrados,this.paginaActual);


        }catch (e) {
            console.error(e);
            this.controlador.representarError();
        }
    },


    // filtrar los clientes tanto por nombre correo o telefono
    filtrarPorNombreCorreoTelefono: function (textoAFiltrar) {
        // si no hay nada escrito poner todos los clientes
        if(textoAFiltrar === ""){
            this.clientesFiltrados = this.todosLosClientes;
        }else{
            this.clientesFiltrados = this.todosLosClientes.filter(cliente =>{
                if(cliente.nombre!=null && cliente.nombre.toLowerCase().includes(textoAFiltrar.toLowerCase())){
                    return cliente;
                }else if(cliente.correo!=null && cliente.correo.toLowerCase().includes(textoAFiltrar.toLowerCase())){
                    return cliente;
                }else if(cliente.telefono!=null && cliente.telefono.toLowerCase().includes(textoAFiltrar.toLowerCase())){
                    return cliente;
                }
            })


        }
        // siempre que filtremos volvamos a la pagina 1
        this.paginaActual = 1;
        this.controlador.representarDatos(this.clientesFiltrados,this.paginaActual);
    },

    // idOrden -> 1 = Nombre, 2 = Correo, 3 = telefono
    // ascendente -> true = ordenar asc, false = ordenar desc
    ordenarPor:function (idOrden) {
        //ocultamos todas las flechas
        document.getElementById('flecha-nombre-tecnico').style.display="none";
        document.getElementById('flecha-correo-tecnico').style.display="none";
        let filtro = "";
        let ordenASC = null;

        // determinar el tipo de filtro y el orden ASC o DESC
        if(idOrden===1){
            //aparece la flecha
            document.getElementById('flecha-nombre-tecnico').style.display="inline";

            filtro = "nombre"
            // ordenar por nombre
            // cade vez que se pulse de mas cambiarlo
            this.ordenNombre = !this.ordenNombre;
            ordenASC = this.ordenNombre;
        }else{
            this.ordenNombre = false;
        }
        if(idOrden === 2){
            //aparece la flecha
            document.getElementById('flecha-correo-tecnico').style.display="inline";

            filtro = "correo";
            // ordenar por correo

            // cade vez que se pulse de mas cambiarlo
            this.ordenCorreo = !this.ordenCorreo;
            ordenASC = this.ordenCorreo;
        }else{
            this.ordenCorreo = false;
        }
        // ordenamos siempre desde clientes filtrados
        // al inicio seran todos y si hay algun filtro en el buscador se
        // ordenará sobre este
        this.clientesFiltrados = this.clientesFiltrados.sort((cliente1,cliente2) =>{
            // juntar todos los nulls o arriba o abajo
            if(cliente1[filtro] == null){
                return ordenASC ? 1 : -1;
            }else if(cliente2[filtro] == null ){
                return ordenASC ? -1 : 1;
            }else {
                // ordenar
                if (cliente1[filtro].toLowerCase() > cliente2[filtro].toLowerCase()) {
                    return ordenASC ? 1 : -1;
                } else if (cliente1[filtro].toLowerCase() < cliente2[filtro].toLowerCase()) {
                    return ordenASC ? -1 : 1;
                }
            }
            return 0;
        })

        // simepre que ordenemos volvemos a la pagina 1
        this.paginaActual = 1
        this.controlador.cambiarFlechasOrden(this.ordenNombre,this.ordenCorreo);
        this.controlador.representarDatos(this.clientesFiltrados,this.paginaActual);
    },

    anteriorPagina:function () {
        this.paginaActual--;
        this.controlador.representarDatos(this.clientesFiltrados,this.paginaActual);
    },

    siguientePagina: function () {
        this.paginaActual++;
        this.controlador.representarDatos(this.clientesFiltrados,this.paginaActual);
    }

};

var VistaListaClientesTecnico = {

    controlador: {},

    bloqueLista:{}, // donde se representarán los elementos de la lista
    paginaActual: 1,// indice para controlar la paginacion
    maxElementosPorPagina: 5,
    bloqueError:{},
    bloqueCarga:{},
    bloqueBodyLista:{},

    // botones para el control de la paginacion de clientes
    btnNextPage:{},
    btnPreviousPage:{},

    // booleano que controla si se tiene que mostrar en primera instancia
    mostrar: false,


    preparar:function (idBloqueLista, idListaBody, idError, idCarga, idBtnNext, idBtnPrev,mostrar) {
        this.bloqueLista = document.getElementById(idBloqueLista);
        this.bloqueBodyLista = document.getElementById(idListaBody);
        this.bloqueError = document.getElementById(idError);
        this.bloqueCarga = document.getElementById(idCarga);
        this.btnPreviousPage = document.getElementById(idBtnPrev);
        this.btnNextPage = document.getElementById(idBtnNext);

        this.mostrar = mostrar;
    },

    // funciones para controlar cuando se muestran los bloques html
    esconderElementos: function(){
        this.bloqueLista.style.display = "none";
        this.bloqueCarga.style.display = "none";
        this.bloqueError.style.display = "none";
    },
    mostrarCarga: function () {
        if(this.mostrar) {
            this.esconderElementos();
            this.bloqueCarga.style.display = "flex"
        }
    },
    mostrarError:function () {
        this.esconderElementos();
        this.bloqueError.style.display = "flex"
    },
    mostrarLista:function () {
        if(this.mostrar) {
            this.esconderElementos();
            if(this.controlador.error){
                this.bloqueError.style.display = "block"
            }else{
                this.bloqueLista.style.display = "flex"
            }
        }
    },

    // representar elementos en el html
    representarDatos:function (clientes,paginaActual) {
        //control de paginacion
        // maxPages = length/maxElementosPorPagina redondeado hacia arriba
        let maxPages = Math.ceil(clientes.length/this.maxElementosPorPagina);
        // si estamos en la pagina 1 empezamos desde el 0 (5*(1-1))
        // si estamos en la pagina 2 empezamos desde el 5 (5*(2-1))
        let elementoInicial = this.maxElementosPorPagina * (paginaActual-1);

        this.prepararBotonesPaginador(paginaActual,maxPages);

        this.bloqueBodyLista.innerHTML = "";

        // pintar elemntos acorde al maxElementosPorPagina
        // los elementos iran desde (maxElementosPorPagina* (paginaActual-1)) hasta this.maxElementosPorPagina*paginaActual
        for(let i = elementoInicial;i<(this.maxElementosPorPagina*paginaActual);i++){
            // si en una pagina no llega al maxElementosPorPagina hay que evitar que intente pintar esos
            // elementos y no de error
           if(i<clientes.length){
               let hilera = document.createElement("tr");

               let celdaNombre = document.createElement("td");
               let celdaCorreo = document.createElement("td");
               let celdaTel = document.createElement("td");
               let celdaOperacion = document.createElement("td");
               celdaOperacion.className = "list-opereciones";

               let textoCeldaNombre = document.createTextNode(clientes[i].nombre != null ? clientes[i].nombre : "Sin asignar");
               let textoCeldaCorreo = document.createTextNode(clientes[i].correo != null ? clientes[i].correo : "Sin asignar");
               let textoCeldaTel = document.createTextNode(clientes[i].telefono != null ? clientes[i].telefono : "Sin asignar");


               celdaNombre.appendChild(textoCeldaNombre);
               celdaCorreo.appendChild(textoCeldaCorreo);
               celdaTel.appendChild(textoCeldaTel);


               // añadimos los botones a todas las listas
               let btnRead = document.createElement("button");
               btnRead.className = "btn-list-element";
               btnRead.onclick = () =>{this.controlador.onClickElementoLista(clientes[i])};

               let ul = document.createElement("ul");
               ul.className = "listaOperaciones";

               let li = document.createElement("li")
               btnRead.innerHTML = `<i class="fas fa-eye"></i><p class="label">Ver campos</p>`;
               li.appendChild(btnRead);
               ul.append(li)

               this.createDropDown(celdaOperacion,clientes[i])
               celdaOperacion.appendChild(ul);

               // añadimos los elementos a la fila
               hilera.appendChild(celdaNombre);
               hilera.appendChild(celdaCorreo);
               hilera.appendChild(celdaTel);
               hilera.appendChild(celdaOperacion)


               // añadimos la fila al tbody
               this.bloqueBodyLista.appendChild(hilera);
           }


        }


    },

    createDropDown:function(celdaOperacion,cliente) {
        let btnRead = document.createElement("button");
        btnRead.className = "btn-list-element";
        btnRead.onclick = () =>{this.controlador.onClickElementoLista(cliente)};


        let liDrop = document.createElement("div")
        btnRead.innerHTML = `<i class="fas fa-eye"></i><p>Ver detalles</p>`
        liDrop.appendChild(btnRead);

        // añadimos el dropdown button que se mostrará en movil
        celdaOperacion.innerHTML = `<button class="btn btn-secondary dropdown-toggle tresPuntos"
                          type="button" id="dropdownMenu1" data-toggle="dropdown"
                          aria-haspopup="true" aria-expanded="false">
                   <i class="fas fa-ellipsis-v tresPuntos">
                  </button>
                  <div class="dropdown-menu" aria-labelledby="dropdownMenu1">
                    
                  </div>`
        celdaOperacion.querySelector("div.dropdown-menu").append(liDrop)
    },

    // habilitar y deshabilitar los botones que controlan la paginacion
    // para evitar errores
    prepararBotonesPaginador : function (paginaActual, maxPages) {
        this.btnNextPage.removeAttribute("disabled");
        this.btnPreviousPage.removeAttribute("disabled");

        document.getElementById("paginadorListaCliente").innerHTML = "Página "+paginaActual + " de " + maxPages;

        // si el maxPages es mayor que uno habilitar el next page
        if(maxPages>1){
            this.btnNextPage.removeAttribute("disabled");
        }
        // si el current page es igual al maxPages deshabilitar el next page
        if(paginaActual === maxPages){
            this.btnNextPage.setAttribute("disabled","disabled");
        }
        if(maxPages === 0){
            this.btnNextPage.setAttribute("disabled","disabled");
        }
        // si el current page es mayor que uno habilitar el previous page
        if(paginaActual > 1){
            this.btnPreviousPage.removeAttribute("disabled");
        }else{
            this.btnPreviousPage.setAttribute("disabled","disabled");
        }

    },

    cambiarFlechasOrden(ordenNombre, ordenCorreo) {
        document.getElementById("flecha-nombre-tecnico").className = (!ordenNombre) ? "fas fa-chevron-down" : "fas fa-chevron-up";
        document.getElementById("flecha-correo-tecnico").className = (!ordenCorreo) ? "fas fa-chevron-down" : "fas fa-chevron-up";
    },

};

var ControladorListaClientesTecnico = {
    modelo: ModeloListaClientesTecnico,
    vista: VistaListaClientesTecnico,
    controladorCreateUsuario:  null,
    error: false,// controla si ha habido un error en la obtencio de datos

    iniciar: function (controladorCreateUsuario) {
        this.modelo.controlador = this;
        this.vista.controlador = this;
        this.controladorCreateUsuario = controladorCreateUsuario;
        this.vista.mostrarCarga();
        this.modelo.obtenerDatos();
    },
    cambiarFlechasOrden(ordenNombre, ordenCorreo) {
        this.vista.cambiarFlechasOrden(ordenNombre,ordenCorreo)
    },
    representarDatos: function(datos,paginaActual){
        this.vista.representarDatos(datos,paginaActual);
        this.vista.mostrarLista();
    },

    representarError: function () {
        this.vista.mostrarError();
    },

    // eventos on click de los elementos de la lista
    // el on click y el on edit redirecciona a la siguiente pagina con un flag de editar
    // el on delete confirma la operacion y lanza la peticion al server en caso afirmativo
    onClickElementoLista:   function (elemento) {
        localStorage.setItem("listaClienteTecnico-cliente",JSON.stringify(elemento));
        location.href = "panel-admin-tecnico-campos.html";
    },



}