// ––––––––––––––––-
// parcela.js
// Fecha 16/04/2020
// ––––––––––––––––-

class Parcela {

    // - - - - - - - - - - - - - - -
    // - - - - - - - - - - - - - - -
    constructor( id, nombre, color, posiciones, sondas ) {

        this.id = id;
        this.nombre = nombre;
        this.color = color;
        this.posiciones = posiciones;
        this.sondas = sondas;
        this.seleccionado = false;
    }

    // - - - - - - - - - - - - - - -
    // - - - - - - - - - - - - - - -
    getId () {
        return this.id
    }

    // - - - - - - - - - - - - - - -
    // - - - - - - - - - - - - - - -
    getNombre () {
        return this.nombre
    }

    // - - - - - - - - - - - - - - -
    // - - - - - - - - - - - - - - -
    getPosiciones () {
        return this.posiciones
    }

    // - - - - - - - - - - - - - - -
    // - - - - - - - - - - - - - - -
    getSondas () {
        return this.sondas
    }

    // - - - - - - - - - - - - - - -
    // - - - - - - - - - - - - - - -
    getColor () {
        return this.color
    }

    // - - - - - - - - - - - - - - -
    // - - - - - - - - - - - - - - -
    isSeleccionado () {
        return this.seleccionado
    }

    // - - - - - - - - - - - - - - -
    // - - - - - - - - - - - - - - -
    setSelected(seleccionado){
        this.seleccionado = seleccionado;
    }

    setPolygon(polygon){
        this.polygon = polygon;
    }

    getPolygon(){
        return this.polygon;
    }


} // class

// - - - - - - - - - - - - - - -
// JSON,JSON -> getParcelasFromJson() -> Lista<Parcelas>
// - - - - - - - - - - - - - - -
function getParcelasFromJson(jsonParcelas,jsonSondas){
    let contador = 0;
    return jsonParcelas[0].map((parcela)=>{
        let color =  "hsl( " + getRandomColor(contador, jsonParcelas[0].length) + ", 100%, 50% )";
        let vertices = parcela.vertices.map((vertice)=>{
            return new Posicion(vertice.lat,vertice.lng);
        })
        let sondas = jsonSondas[contador].posiciones.map(sonda =>{
            return new Sonda(sonda.id,new Posicion(sonda.latitud,sonda.longitud),sonda.nombre,34,95,89,10)
        })
        contador++;
        return new Parcela(parcela.id,parcela.nombre,color,vertices,sondas);
    });
}
