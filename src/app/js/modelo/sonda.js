// ––––––––––––––––-
// sonda.js
// Fecha 16/04/2020
// ––––––––––––––––-



class Sonda {

    // - - - - - - - - - - - - - - -
    // - - - - - - - - - - - - - - -
    constructor( id, posicion, label, temp, hum,ilu, sal,fecha) {

        this.id = id;
        this.posicion = posicion;
        this.label = label;

        this.ultimaTemp = temp;
        this.ultimaHum = hum;
        this.ultimaIlu = ilu;
        this.ultimaSal = sal;

        this.ultimaFecha = fecha;

        this.seleccionado = false;
    }

    // - - - - - - - - - - - - - - -
    // - - - - - - - - - - - - - - -
    getId () {
        return this.id
    }

    // - - - - - - - - - - - - - - -
    // - - - - - - - - - - - - - - -
    getLabel () {
        return this.label
    }

    // - - - - - - - - - - - - - - -
    // - - - - - - - - - - - - - - -
    getPosicion () {
        return this.posicion
    }

    // - - - - - - - - - - - - - - -
    // - - - - - - - - - - - - - - -
    isSeleccionado () {
        return this.seleccionado
    }

    // - - - - - - - - - - - - - - -
    // - - - - - - - - - - - - - - -
    setSelected(seleccionado){
        this.seleccionado = seleccionado;
    }

    // - - - - - - - - - - - - - - -
    // - - - - - - - - - - - - - - -
    setLabel (label) {
        this.label = label;
    }

    setMarker(marker){
        this.marker = marker;
    }

    getMarker(){
        if(this.marker === undefined){
            return null
        }else{
            return this.marker;
        }

    }


    getColorWarningTemp(){
        if(this.ultimaTemp > 30){
            return '#FF0000'
        }else if(this.ultimaTemp<5){
            return '#FF0000'
        }
        return '#00FF00'
    }

    getColorWarningIlu(){
        if(this.ultimaIlu>=30 &&this.ultimaIlu<80 ){
            return '#ff8d00'
        }else if(this.ultimaIlu>=80){
            return '#00FF00'
        }
        return '#FF0000'
    }
    getColorWarningSal(){
        if(this.ultimaSal>40 &&this.ultimaSal<=70 ){
            return '#FF8D00'
        }else if(this.ultimaSal<=40){
            return '#00FF00'
        }
        return '#FF0000'
    }
    getColorWarningHum(){
        if(this.ultimaHum>=20 &&this.ultimaHum<40 ){
            return '#FF8D00'
        }else if(this.ultimaHum>=40 &&this.ultimaHum<80){
            return '#00FF00'
        }else if(this.ultimaHum>=80){
            return '#0000FF';
        }
        return '#FF0000'
    }

} // class
