// ––––––––––––––––-
// posicion.js
// Fecha 16/04/2020
// ––––––––––––––––-
class Posicion {

    // - - - - - - - - - - - - - - -
    // - - - - - - - - - - - - - - -
    constructor( lat, lng ) {
        this.lat = lat
        this.lng = lng
    }

    // - - - - - - - - - - - - - - -
    // - - - - - - - - - - - - - - -
    getLat () {
        return this.lat
    }

    // - - - - - - - - - - - - - - -
    // - - - - - - - - - - - - - - -
    getLng () {
        return this.lng
    }

} // class
