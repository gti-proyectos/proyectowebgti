/**
 * Autor: Rubén Pardo Casanova
 * GTI GRUPO 11
 * Fecha: 27/02/2020
 *
 * restringir acceso
 * script que, añadido a cualquier página, haga petición a GET /sesion/ y si no
 * recibe código 200 redirija al login.
 */
var ModeloAcceso = {
    controlador: {},
    datos: [],
    cargar: function () {
        //Hacemos una peticion get-sesion, si nos devuelve ok redirijimos devolvemos el usuario
        fetch('../../src/api/v1.0/sesion').then((respuesta) => {
            if (respuesta.status !== 200) {
                location.href = this.controlador.vista.urlLogin;//vuelve a app, se redireccionará al login
            } else {
                return respuesta.json();
            }
        }).then((json) => {

            // si el id de rol del usuario no esta entre los permitidos le redireccionamos al login
            if (!this.controlador.vista.rolesPermitidos.includes(json['rol']['id'])) {
                location.href = this.controlador.vista.urlLogin;//vuelve a app, se redireccionará al login
            } else {

                // esta dentra de los roles permitidos
                // pero si es id 1 debemos comprobar que es un admin general
                if(document.URL.includes("admin-general") && json['cliente'] != null){
                    // si es admin pero tiene cliente no tiene acceso
                    location.href = this.controlador.vista.urlLogin;
                }


                this.datos = json;
                this.controlador.representar(json);
            }
        });
    }
};

var VistaAcceso = {
    urlLogin: "",
    rolesPermitidos: {},
    preparar: function (url, roles) {
        this.urlLogin = url;
        this.rolesPermitidos = roles;
    },
    representar: function (datos) {
        //document.body.style.display = "block";//mostrar el body
    }
};

var ControladorAcceso = {
    modelo: ModeloAcceso,
    vista: VistaAcceso,
    iniciar: function () {
        this.modelo.controlador = this;
        this.modelo.cargar();
    },
    representar: function () {
        this.vista.representar(this.modelo.datos);
    }
};


