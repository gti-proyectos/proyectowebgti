/**
 * Autor Carlos
 * Logica de carga de los MVC de panel-admin-detalles-usuario.html
 */

let usuario = JSON.parse(localStorage.getItem("listaUsuarioCliente-usuario"));
let user =  JSON.parse(localStorage.getItem("user"));
let cliente = JSON.parse(localStorage.getItem("listaCliente-cliente"));
// ponemos el nombre del usuario en el titulo
document.getElementById("titulo").innerText = user['nombre'];

if(cliente.id === user.cliente){
    // esta logeado el  usuario admin
    let liAdmin = document.getElementById("li-admin-jerarquia")
    let ol = document.getElementById("ol-jerarquia")
    ol.removeChild(liAdmin);
}

document.getElementById("titulo").innerHTML = usuario.nombre;

document.getElementById("jq-nombre-cliente").innerText = cliente.nombre
document.getElementById("jq-nombre-usuario").innerText = usuario.nombre

// tener en cuenta que los admins no tienen campos asignados
if(usuario.rolId != 1){
    // mostrar los campos asignados
    VistaListaCamposUsuario.preparar("lista-campos","lista-campos-body","error","carga","btnNextCampos","btnPrevCampos",usuario,"radio-group");
    // le pasamos al controlador de la lista los controladores de crear y modicar de campos
    ControladorListaCamposUsuario.iniciar(usuario);

}else{
    // los admins no tienen campos
}








// para que cuando refresque no entre en modo editar
let userTemp = usuario;
userTemp.modificar = false;
localStorage.setItem("listaUsuarioCliente-usuario",JSON.stringify(userTemp))
