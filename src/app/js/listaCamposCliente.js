/**
 * Autor: Rubén Pardo Casanova
 * GTI GRUPO 11
 * Fecha: 07/05/2020
 *
 * Modelo vista controlador que gestiona la lista de los campos de un cliente
 *
 */

var ModeloListaCampos = {

    controlador: {},
    idCliente: -1,

    todosLosCampos:{},//contendrá todos los clientes siempre
    camposFiltrados:{},// contendrá los clientes filtrados cuando se use el buscador o el ordenar

    paginaActual: 1,

    // al principio no estan ordenados
    // true = ASC, false = DESC
    ordenNombre: false,
    ordenDescripcion: false,
    ordenFecha: false,

    obtenerDatos: async function () {

        try{
            this.todosLosCampos = await fetch('../../src/api/v1.0/campo?idCliente=' + this.idCliente)
                .then(respuesta => {
                    if (respuesta.status === 200) {
                        return respuesta.json();
                    } else {
                        this.controlador.error = true;
                        throw Error(respuesta.statusText);
                    }
                })// juntamos los dos arrays en uno
            this.camposFiltrados = this.todosLosCampos;
            console.log(this.controlador)
            this.controlador.representarDatos(this.camposFiltrados,this.paginaActual);


        }catch (e) {
            console.error(e);
            this.controlador.representarError();
        }
    },
    borrarElemento: async function (idCliente) {

        try{

            await fetch('../../src/api/v1.0/campo', {
                method: 'DELETE', // or 'PUT'
                body: JSON.stringify({id:idCliente}) // data can be `string` or {object}!

            }).then(res => res.json())
                .catch(error => {throw Error(error)})
                .then(response => console.log('Success:', response));



            // si sale bien borramos el elemento en local y nos evitamos el tener que refrescar
            // encontramos el elemento
            let campoABorrar = this.todosLosCampos.find(e=>{
                if(e.id === idCliente){
                    return e;
                }
            });
            if(campoABorrar!=null){
                // lo borramos del array local
                this.todosLosCampos.splice(this.todosLosCampos.indexOf(campoABorrar),1);
            }

            // volvemos a la pagina principal y pintamos de nuevo los datos
            this.paginaActual = 1;
            this.controlador.representarDatos(this.camposFiltrados,this.paginaActual);

        }catch (e) {
            console.error(e);
            this.controlador.representarError();
        }
    },

    // filtrar los clientes tanto por nombre correo o telefono
    filtrarPorNombreDescripcion: function (textoAFiltrar) {
        // si no hay nada escrito poner todos los clientes
        if(textoAFiltrar === ""){
            this.camposFiltrados = this.todosLosCampos;
        }else{
            this.camposFiltrados = this.todosLosCampos.filter(campo =>{
                if(campo.nombre!=null && campo.nombre.toLowerCase().includes(textoAFiltrar.toLowerCase())){
                    return campo;
                }else if(campo.descripcion!=null &&  campo.descripcion.toLowerCase().includes(textoAFiltrar.toLowerCase())){
                    return campo;
                }
            })


        }
        // siempre que filtremos volvamos a la pagina 1
        this.paginaActual = 1;
        this.controlador.representarDatos(this.camposFiltrados,this.paginaActual);
    },

    // idOrden -> 1 = Nombre, 2 = Correo, 3 = telefono
    // ascendente -> true = ordenar asc, false = ordenar desc
    ordenarPor:function (idOrden) {
        //ocultamos todas las flechas
        document.getElementById('flecha-nombre-campo').style.display="none";
        document.getElementById('flecha-desc-campo').style.display="none";
        document.getElementById('flecha-fecha-campo').style.display="none";

        let filtro = "";
        let ordenASC = null;

        // determinar el tipo de filtro y el orden ASC o DESC
        if(idOrden===1){
            //aparece la flecha
            document.getElementById('flecha-nombre-campo').style.display="inline";

            filtro = "nombre"
            // ordenar por nombre
            if(this.ordenNombre == null){
                // la primera vez ordenar ascendentemente
                this.ordenNombre = true;
            }else{
                // cade vez que se pulse de mas cambiarlo
                this.ordenNombre = !this.ordenNombre;
            }
            ordenASC = this.ordenNombre;
        }else{
            this.ordenNombre = false;
        }
        if(idOrden === 2){
            //aparece la flecha
            document.getElementById('flecha-desc-campo').style.display="inline";

            filtro = "descripcion";
            // ordenar por correo
            if(this.ordenDescripcion == null){
                // la primera vez ordenar ascendentemente
                this.ordenDescripcion = true;
            }else{
                // cade vez que se pulse de mas cambiarlo
                this.ordenDescripcion = !this.ordenDescripcion;
            }
            ordenASC = this.ordenDescripcion;
        }else{
            this.ordenDescripcion = false;
        }
        if(idOrden === 3){
            //aparece la flecha
            document.getElementById('flecha-fecha-campo').style.display="inline";

            filtro = "fechaCreacion";
            // ordenar por correo
            if(this.ordenFecha == null){
                // la primera vez ordenar ascendentemente
                this.ordenFecha = true;
            }else{
                // cade vez que se pulse de mas cambiarlo
                this.ordenFecha = !this.ordenFecha;
            }
            ordenASC = this.ordenFecha;
        }else{
            this.ordenFecha = false;
        }
        // ordenamos siempre desde clientes filtrados
        // al inicio seran todos y si hay algun filtro en el buscador se
        // ordenará sobre este
        this.camposFiltrados = this.camposFiltrados.sort((campo1, campo2) =>{
            // juntar todos los nulls o arriba o abajo
            if(campo1[filtro] == null){
                return ordenASC ? 1 : -1;
            }else if(campo2[filtro] == null ){
                return ordenASC ? -1 : 1;
            }else{
                // ordenar
                if(campo1[filtro].toLowerCase() > campo2[filtro].toLowerCase()){
                    return ordenASC ? 1 : -1;
                }else if(campo1[filtro].toLowerCase() < campo2[filtro].toLowerCase()){
                    return ordenASC ? -1 : 1;
                }
            }


            return 0;
        })

        // simepre que ordenemos volvemos a la pagina 1
        this.paginaActual = 1
        this.controlador.cambiarFlechasOrden(this.ordenNombre,this.ordenDescripcion,this.ordenFecha);
        this.controlador.representarDatos(this.camposFiltrados,this.paginaActual);
    },

    anteriorPagina:function () {
        this.paginaActual--;
        this.controlador.representarDatos(this.camposFiltrados,this.paginaActual);
    },

    siguientePagina: function () {
        this.paginaActual++;
        this.controlador.representarDatos(this.camposFiltrados,this.paginaActual);
    }

};

var VistaListaCampos = {

    controlador: {},
    cliente:{},

    bloqueLista:{}, // donde se representarán los elementos de la lista
    paginaActual: 1,// indice para controlar la paginacion
    maxElementosPorPagina: 5,
    bloqueError:{},
    bloqueCarga:{},
    bloqueBodyLista:{},

    // botones para el control de la paginacion de clientes
    btnNextPage:{},
    btnPreviousPage:{},

    // booleano que controla si se tiene que mostrar en primera instancia
    mostrar: false,

    preparar:function (idBloqueLista, idListaBody, idError, idCarga, idBtnNext, idBtnPrev,mostrar,cliente) {
        this.bloqueLista = document.getElementById(idBloqueLista);
        this.bloqueBodyLista = document.getElementById(idListaBody);
        this.bloqueError = document.getElementById(idError);
        this.bloqueCarga = document.getElementById(idCarga);
        this.btnPreviousPage = document.getElementById(idBtnPrev);
        this.btnNextPage = document.getElementById(idBtnNext);
        this.cliente =cliente;
        this.mostrar = mostrar;
    },

    // funciones para controlar cuando se muestran los bloques html
    esconderElementos: function(){

        this.bloqueLista.style.display = "none";
        this.bloqueCarga.style.display = "none";
        this.bloqueError.style.display = "none";
    },
    mostrarCarga: function () {
        if(this.mostrar) {
            this.esconderElementos();
            this.bloqueCarga.style.display = "flex"
        }
    },
    mostrarError:function () {
        this.esconderElementos();
        this.bloqueError.style.display = "flex"

    },
    mostrarLista:function () {
        if(this.mostrar) {
            this.esconderElementos();
            if(this.controlador.error){
                this.bloqueError.style.display = "flex"
            }else{
                this.bloqueLista.style.display = "flex"
            }
        }
    },

    // representar elementos en el html
    representarDatos:function (campos,paginaActual) {
        //control de paginacion
        // maxPages = length/maxElementosPorPagina redondeado hacia arriba
        let maxPages = Math.ceil(campos.length/this.maxElementosPorPagina);
        // si estamos en la pagina 1 empezamos desde el 0 (5*(1-1))
        // si estamos en la pagina 2 empezamos desde el 5 (5*(2-1))
        let elementoInicial = this.maxElementosPorPagina * (paginaActual-1);

        this.prepararBotonesPaginador(paginaActual,maxPages);

        this.bloqueBodyLista.innerHTML = "";

        // pintar elemntos acorde al maxElementosPorPagina
        // los elementos iran desde (maxElementosPorPagina* (paginaActual-1)) hasta this.maxElementosPorPagina*paginaActual
        for(let i = elementoInicial;i<(this.maxElementosPorPagina*paginaActual);i++){
            // si en una pagina no llega al maxElementosPorPagina hay que evitar que intente pintar esos
            // elementos y no de error
            if(i<campos.length){
                let hilera = document.createElement("tr");

                let celdaNombre = document.createElement("td");
                let celdaDescripcion = document.createElement("td");
                let celdaFecha = document.createElement("td");
                let celdaOperacion = document.createElement("td");
                celdaOperacion.className = "list-opereciones";

                let textoCeldaNombre = document.createTextNode(campos[i].nombre != null ? campos[i].nombre : "Sin asignar");
                let textoCeldaDesc = document.createTextNode(campos[i].descripcion != null ? campos[i].descripcion : "Sin asignar");
                let textoCeldaFecha = document.createTextNode(campos[i].fechaCreacion != null ? campos[i].fechaCreacion : "Sin asignar");

                celdaNombre.appendChild(textoCeldaNombre);
                celdaNombre.className = "th-dos-items";
                celdaDescripcion.appendChild(textoCeldaDesc);
                celdaDescripcion.className = "th-dos-items";
                celdaFecha.appendChild(textoCeldaFecha);
                celdaFecha.className = "th-dos-items";

                let btnEdit = document.createElement("button");
                btnEdit.className = "btn-list-element";
                btnEdit.onclick = () =>{this.controlador.onEditElementoLista(campos[i])};

                let btnRemove = document.createElement("button");
                btnRemove.className = "btn-list-element";
                btnRemove.onclick = () =>{this.controlador.onRemoveElementoLista(campos[i])};


                let ul = document.createElement("ul");
                ul.className = "listaOperaciones";
                ul.classList.add("th-dos-items")


                let li2 = document.createElement("li")
                btnEdit.innerHTML = `<i class="fas fa-pen-square"></i><p class="label">Modificar</p>`
                li2.appendChild(btnEdit);



                let li3 = document.createElement("li")
                btnRemove.innerHTML = `<i class="far fa-trash-alt"></i><p class="label">Eliminar</p>`
                li3.appendChild(btnRemove);

                ul.appendChild(li2)
                ul.appendChild(li3)


                // añadimos el dropdown button que se mostrará en movil
                this.createDropDown(celdaOperacion,campos[i])
                // añadimos la lista que se mostrará a partir de tablet
                celdaOperacion.appendChild(ul)

                // añadimos los elementos a la fila
                hilera.appendChild(celdaNombre);
                hilera.appendChild(celdaDescripcion);
                hilera.appendChild(celdaFecha);
                hilera.appendChild(celdaOperacion)


                // añadimos la fila al tbody
                this.bloqueBodyLista.appendChild(hilera);

            }


        }


    },

    createDropDown:function(celdaOperacion,campo) {

        let btnEdit = document.createElement("button");
        btnEdit.className = "btn-list-element";
        btnEdit.onclick = () =>{this.controlador.onEditElementoLista(campo)};

        let btnRemove = document.createElement("button");
        btnRemove.className = "btn-list-element";
        btnRemove.onclick = () =>{this.controlador.onRemoveElementoLista(campo)};

        let li2Drop = document.createElement("div")
        btnEdit.innerHTML = `<i class="fas fa-pen-square"></i><p>Modificar</p>`
        li2Drop.appendChild(btnEdit);
        let li3Drop = document.createElement("div")
        btnRemove.innerHTML = `<i class="far fa-trash-alt"></i><p>Eliminar</p>`
        li3Drop.appendChild(btnRemove);
        // añadimos el dropdown button que se mostrará en movil
        celdaOperacion.innerHTML = `<button class="btn btn-secondary dropdown-toggle tresPuntos"
                          type="button" id="dropdownMenu1" data-toggle="dropdown"
                          aria-haspopup="true" aria-expanded="false">
                   <i class="fas fa-ellipsis-v tresPuntos">
                  </button>
                  <div class="dropdown-menu" aria-labelledby="dropdownMenu1">
                    
                  </div>`
        celdaOperacion.querySelector("div.dropdown-menu").append(li2Drop)
        celdaOperacion.querySelector("div.dropdown-menu").append(li3Drop)
    },
    // habilitar y deshabilitar los botones que controlan la paginacion
    // para evitar errores
    prepararBotonesPaginador : function (paginaActual, maxPages) {
        this.btnNextPage.removeAttribute("disabled");
        this.btnPreviousPage.removeAttribute("disabled");
        document.getElementById("paginadorListaCampo").innerHTML = "Página "+paginaActual + " de " + maxPages;
        // si el maxPages es mayor que uno habilitar el next page
        if(maxPages>1){
            this.btnNextPage.removeAttribute("disabled");
        }
        // si el current page es igual al maxPages deshabilitar el next page
        if(paginaActual === maxPages){
            this.btnNextPage.setAttribute("disabled","disabled");
        }
        if(maxPages === 0){
            this.btnNextPage.setAttribute("disabled","disabled");
        }
        // si el current page es mayor que uno habilitar el previous page
        if(paginaActual > 1){
            this.btnPreviousPage.removeAttribute("disabled");
        }else{
            this.btnPreviousPage.setAttribute("disabled","disabled");
        }

    },


    cambiarFlechasOrden(ordenNombre, ordenDescripcion, ordenFecha) {
        document.getElementById("flecha-nombre-campo").className = (!ordenNombre) ? "fas fa-chevron-down" : "fas fa-chevron-up";
        document.getElementById("flecha-desc-campo").className = (!ordenDescripcion) ? "fas fa-chevron-down" : "fas fa-chevron-up";
        document.getElementById("flecha-fecha-campo").className = (!ordenFecha) ? "fas fa-chevron-down" : "fas fa-chevron-up";

    },
};

var ControladorListaCampos = {
    modelo: ModeloListaCampos,
    vista: VistaListaCampos,
    controladorCreateUsuario: null,
    controladorModificarCampo: null,

    error: false,// controla si ha habido un error en la obtencio de datos

    iniciar: function (idCliente,controladorCreateUsuario,controladorModificarCampo) {
        this.modelo.controlador = this;
        this.modelo.idCliente = idCliente;
        this.vista.controlador = this;
        this.vista.mostrarCarga();
        this.modelo.obtenerDatos();
        this.controladorCreateUsuario = controladorCreateUsuario;
        this.controladorModificarCampo = controladorModificarCampo;
    },

    cambiarFlechasOrden(ordenNombre, ordenDesc, ordenFecha) {
        this.vista.cambiarFlechasOrden(ordenNombre,ordenDesc, ordenFecha)
    },

    representarDatos: function(datos,paginaActual){
        this.vista.representarDatos(datos,paginaActual);
        this.vista.mostrarLista();
    },
    mostrarTabla: function(){
        this.vista.mostrarLista();
    },

    representarError: function () {
        this.vista.mostrarError();
    },


    onEditElementoLista: function (elemento) {
        this.controladorModificarCampo.iniciar(this.modelo.idCliente,elemento)
    },
    onCreateCampo: function () {
        this.controladorCreateUsuario.iniciar(this.modelo.idCliente)
    },
    onRemoveElementoLista: function (elemento) {
        // confimar borrado
        if(confirm("Se borrará para siempre el campo "+elemento.nombre+"\n¿Estás seguro?")){
            this.vista.mostrarCarga();
            this.modelo.borrarElemento(elemento.id);
        }

    },

}