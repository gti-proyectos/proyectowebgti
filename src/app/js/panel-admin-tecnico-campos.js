/**
 * Autor Marc
 * Logica de carga de los MVC de panel-admin-tecnico-campos.html
 *
 */

let user =  JSON.parse(localStorage.getItem("user"));
// ponemos el nombre del usuario en el titulo
document.getElementById("titulo").innerText = user['nombre'];


// cogemos el cliente pasado por localstorage desde la lista de clientes
let cliente = JSON.parse(localStorage.getItem("listaClienteTecnico-cliente"));

VistaListaCamposTecnico.preparar("lista-campos","lista-campos-body","error","carga","btnNextCampos","btnPrevCampos",true,cliente);
// le pasamos al controlador de la lista los controladores de crear y modicar de campos
ControladorListaCamposTecnico.iniciar(cliente.id,);

