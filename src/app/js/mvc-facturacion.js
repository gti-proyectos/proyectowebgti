/**
 * Autor: Aitor Benítez Estruch
 * Fecha: 25/05/2020
 * Modelo Vista Controlador facturas
 *
 */


var modeloFacturacion = {
    controlador: {},
    facturas: [],
    facturasFiltrado: [],
    paginaActual: 1,
    textoEscritoEnBuscador: "",

    // al principio no estan ordenados
    // true = ASC, false = DESC
    ordenParcela: false,
    ordenFecha: false,
    ordenBalance: false,

    cargar: async function (idTemporada) {
        try {
            //Obtención de las temporadas registradas por el cliente
            this.facturas = await fetch('../../src/api/v1.0/facturacion?idTemporada=' + idTemporada).then((res) => {
                if (res.status !== 200) {
                    // ha habido un error mostrar error:
                    this.controlador.error = true;
                    throw Error(res.statusText);
                    //this.controlador.representarError();
                } else {
                    // han llegado los datos, devolverlos en formato json
                    return res.json();
                }
            })
            let facturasHistorial = [...this.facturas];// hacemos una copia del array para evitar problemas de race conditions

            this.facturasFiltrado = this.facturas;
            this.controlador.calcularBalanceTemporada(this.facturas);
            await this.controlador.representar(this.facturas, 1)
            await this.controlador.representarGraficas(facturasHistorial);

        } catch (e) {
            console.log(e);
            this.controlador.representarError();
        }


    },


    borrarElemento: async function (idFactura) {

        try {

            await fetch('../../src/api/v1.0/facturacion', {
                method: 'DELETE',
                body: JSON.stringify({id: idFactura})
            }).then(res => res.json())
                .catch(error => {
                    throw Error(error)
                })
                .then(response => console.log('Success:', response));


            window.location.href = "detalles-temporada.html"

        } catch (e) {
            console.error(e);
            this.controlador.representarError();
        }
    },

    // idOrden -> 1 = Nombre, 2 = Correo, 3 = telefono
    // ascendente -> true = ordenar asc, false = ordenar desc
    ordenarPor:function (idOrden) {
        let filtro = "";
        let ordenASC = null;
//ocultamos todas las flechas
        document.getElementById('flecha-nombre-temp').style.display="none";
        document.getElementById('flecha-fecha-temp').style.display="none";
        document.getElementById('flecha-balance-temp').style.display="none";
        // determinar el tipo de filtro y el orden ASC o DESC
        if(idOrden===1){
            document.getElementById('flecha-nombre-temp').style.display="inline";

            filtro = "parcela"
            // cada vez que se pulse de mas cambiarlo
            this.ordenParcela = !this.ordenParcela;
            ordenASC = this.ordenParcela;
        }else{
            this.ordenParcela = false;
        }
        if(idOrden === 2){
            document.getElementById('flecha-fecha-temp').style.display="inline";

            filtro = "fecha";
            // cada vez que se pulse de mas cambiarlo
            this.ordenFecha = !this.ordenFecha;
            ordenASC = this.ordenFecha;
        }else{
            this.ordenFecha = false;
        }
        //Falta balance (irá donde está comentado)
        if(idOrden===3) {
            document.getElementById('flecha-balance-temp').style.display="inline";

            filtro = "cantidad";
            // cada vez que se pulse de mas cambiarlo
            this.ordenBalance = !this.ordenBalance;
            ordenASC = this.ordenBalance;
        }
        else{
            this.ordenBalance = false;
        }

        if(idOrden !== 3){
            this.facturasFiltrado = this.facturasFiltrado.sort((temporada1,temporada2) =>{
                // juntar todos los nulls o arriba o abajo
                if(temporada1[filtro] == null){
                    return ordenASC ? -1 : 1;
                }else if(temporada2[filtro] == null ){
                    return ordenASC ? 1 : -1;
                }else {
                    // ordenar
                    if (temporada1[filtro].toLowerCase() > temporada2[filtro].toLowerCase()) {
                        return ordenASC ? 1 : -1;
                    } else if (temporada1[filtro].toLowerCase() < temporada2[filtro].toLowerCase()) {
                        return ordenASC ? -1 : 1;
                    }
                }
                return 0;
            })
        }
        else{
            this.facturasFiltrado = this.facturasFiltrado.sort((factura1,factura) =>{
                // juntar todos los nulls o arriba o abajo
                if(factura1[filtro] == null){
                    return ordenASC ? -1 : 1;
                }else if(factura[filtro] == null ){
                    return ordenASC ? 1 : -1;
                }else {
                    let cantidad1 = factura1.perdida === 1 ? parseInt("-"+factura1[filtro]) : parseInt(factura1[filtro])
                    let cantidad2 = factura.perdida === 1 ? parseInt("-"+factura[filtro]) : parseInt(factura[filtro])
                    // ordenar
                    if (cantidad1 > cantidad2) {
                        return ordenASC ? 1 : -1;
                    } else if (cantidad1 < cantidad2) {
                        return ordenASC ? -1 : 1;
                    }
                }
                return 0;
            })
        }

        // simepre que ordenemos volvemos a la pagina 1
        this.paginaActual = 1
        this.controlador.cambiarFlechasOrden(this.ordenParcela,this.ordenFecha,this.ordenBalance);
        this.controlador.representar(this.facturasFiltrado,this.paginaActual);
    },


    anteriorPagina:function () {
        this.paginaActual--;
        this.controlador.representar(this.facturasFiltrado,this.paginaActual);
    },

    siguientePagina: function () {
        this.paginaActual++;
        this.controlador.representar(this.facturasFiltrado,this.paginaActual);
    }
}

var vistaFacturacion = {
    controlador: {},

    bloqueLista:{}, // donde se representarán los elementos de la lista
    paginaActual: 1,// indice para controlar la paginacion
    maxElementosPorPagina: 5,
    bloqueError:{},
    bloqueCarga:{},
    bloqueBodyLista:{},


    // botones para el control de la paginacion de clientes
    btnNextPage:{},
    btnPreviousPage:{},
    mostrar: false,


    graficaBalance:null,
    graficaBeneficios:null,
    graficaGastos:null,


    preparar:function (idBloqueLista, idListaBody, idError, idCarga, idBtnNext, idBtnPrev, mostrar) {
        this.bloqueLista = document.getElementById(idBloqueLista);
        this.bloqueBodyLista = document.getElementById(idListaBody);
        this.bloqueError = document.getElementById(idError);
        this.bloqueCarga = document.getElementById(idCarga);
        this.btnPreviousPage = document.getElementById(idBtnPrev);
        this.btnNextPage = document.getElementById(idBtnNext);

        this.mostrar= mostrar;


    },

    // funciones para controlar cuando se muestran los bloques html
    esconderElementos: function(){
        this.bloqueLista.style.display = "none";
        this.bloqueCarga.style.display = "none";
        this.bloqueError.style.display = "none";
    },
    mostrarCarga: function () {
        if(this.mostrar) {
            this.esconderElementos();
            this.bloqueCarga.style.display = "flex"
        }
    },
    mostrarError:function () {
        this.esconderElementos();
        this.bloqueError.style.display = "flex"
    },
    mostrarLista:function () {
        if(this.mostrar) {
            this.esconderElementos();
            if(this.controlador.error){
                this.bloqueError.style.display = "block"
            }else{
                this.bloqueLista.style.display = "flex"
            }
        }else{
            this.bloqueLista.style.display = "none"
        }
    },

    // pinta en el html el balance total
    representarBalanceTotal(balanceTotal, beneficioTotal, gastoTotal){
        // poner el balance total en la pantalla
        let balanceHTMl = document.getElementById("balance-total");

        document.getElementById("beneficios-totales").innerText = beneficioTotal + " €";
        document.getElementById("gastos-totales").innerText = gastoTotal + " €";

        if(balanceTotal != null){
            balanceHTMl.innerHTML = balanceTotal+" €";
            if(balanceTotal > 0){
                // balance positivo
                balanceHTMl.className = ("balance-positivo")
            }else{
                // balance positivo
                balanceHTMl.className = ("balance-negativo")
            }
        }else{
            balanceHTMl.innerHTML = "Sin ingresos";
        }


    }, 

    // representar elementos en el html
    representarHistorial:function (facturas,paginaActual) {
        //control de paginacion
        // maxPages = length/maxElementosPorPagina redondeado hacia arriba
        let maxPages = Math.ceil(facturas.length/this.maxElementosPorPagina);
        // si estamos en la pagina 1 empezamos desde el 0 (5*(1-1))
        // si estamos en la pagina 2 empezamos desde el 5 (5*(2-1))
        let elementoInicial = this.maxElementosPorPagina * (paginaActual-1);

        this.prepararBotonesPaginador(paginaActual,maxPages);

        this.bloqueBodyLista.innerHTML = "";

        let balanceTotal = 0;

        // pintar elemntos acorde al maxElementosPorPagina
        // los elementos iran desde (maxElementosPorPagina* (paginaActual-1)) hasta this.maxElementosPorPagina*paginaActual
        for(let i = elementoInicial;i<(this.maxElementosPorPagina*paginaActual);i++) {
            // si en una pagina no llega al maxElementosPorPagina hay que evitar que intente pintar esos
            // elementos y no de error
            if (i < facturas.length) {

                let hilera = document.createElement("tr");

                let celdaParcela = document.createElement("td");
                let celdaFecha = document.createElement("td");
                let celdaCantidad = document.createElement("td");
                let celdaOperacion = document.createElement("td");
                celdaOperacion.className = "list-opereciones";

                let textoCeldaParcela = document.createTextNode(facturas[i].parcela != null ? facturas[i].parcela : "Sin asignar");
                let textoCeldaFecha = document.createTextNode(facturas[i].fecha != null ? facturas[i].fecha : "Sin asignar");

                let textoCeldaCantidad;
                if (facturas[i].perdida === 1) {
                    textoCeldaCantidad = document.createTextNode(facturas[i].cantidad != null ? -facturas[i].cantidad + " €" : "Sin ingresos");
                }
                else {
                    textoCeldaCantidad = document.createTextNode(facturas[i].cantidad != null ? facturas[i].cantidad + " €" : "Sin ingresos");
                }

                if (facturas[i].cantidad != null) {
                    // sumamos el balance al total
                    if (facturas[i].perdida === 1) {
                        balanceTotal -= parseInt(facturas[i].cantidad)
                    } else {
                        balanceTotal += parseInt(facturas[i].cantidad)
                    }

                    // balance de la temporada positivo
                    // añadimos la clase css
                    if (facturas[i].perdida === 0) {
                        celdaCantidad.classList.add("balance-positivo")
                    } else {
                        // balance de la temporada negativo
                        celdaCantidad.classList.add("balance-negativo")
                    }

                }

                celdaParcela.appendChild(textoCeldaParcela);
                celdaFecha.appendChild(textoCeldaFecha);
                celdaCantidad.appendChild(textoCeldaCantidad);


                // añadimos los botones a todas las listas

                let btnRemove = document.createElement("button");
                btnRemove.className = "btn-list-element";
                btnRemove.onclick = () => {this.controlador.onRemoveElementoLista(facturas[i])};


                let ul = document.createElement("ul");
                ul.className = "listaOperaciones";

                let li = document.createElement("li");
                btnRemove.innerHTML = `<i class="far fa-trash-alt"></i><p class="label">Borrar</p>`
                li.appendChild(btnRemove);


                ul.appendChild(li)

                // añadimos la lista que se mostrará a partir de tablet
                celdaOperacion.appendChild(ul)

                // añadimos los elementos a la fila
                hilera.appendChild(celdaParcela);
                hilera.appendChild(celdaFecha);
                hilera.appendChild(celdaCantidad);
                hilera.appendChild(celdaOperacion)

                celdaParcela.onclick = () =>{this.controlador.onClickElemento(facturas[i])};
                celdaFecha.onclick = () =>{this.controlador.onClickElemento(facturas[i])};
                celdaCantidad.onclick = () =>{this.controlador.onClickElemento(facturas[i])};

                // añadimos la fila al tbody
                this.bloqueBodyLista.appendChild(hilera);

            }
        }
    },

    //representar graficas de las temporada
    representarGraficas:function(facturaciones){
        if(facturaciones.length !== 0){
            document.getElementById("container-graficas").style.display = "flex";
            this.representarGraficasCirculares(facturaciones)
            this.representarBalanceNetoPorMes(facturaciones);
        }else{
            // representar graficas vacías
            document.getElementById("container-graficas").style.display = "none";
            document.getElementById("graficas-vacias").style.display = "block";
        }

    },//()
    representarGraficasCirculares(facturacion){
        //elementos graficos
        let arrayLabelBen = [];
        let arrayLabelGas = [];
        let arrayBackgroundColors = [];
        let arrayBackgroundColorsPerdida = [];

        let perdidas = [];
        let beneficios = [];
        let parcelaAux = "";// parcela actual, empieza en la posicion cero
        //objeto solo con idParcela y cantidad

        let arrayResult = []; //
        let contParcela = -1;//contador parcela
        //ordenamos las facturas por parcela
        facturacion.sort(function (a,b) {
            if (a.parcela < b.parcela) return -1;
            else if (a.parcela > b.parcela) return 1;
            return 0;
        });

        facturacion.forEach(factura=>{
            if(factura.parcela !== parcelaAux) {


                arrayLabelBen.push(factura.parcela);
                arrayLabelGas.push(factura.parcela);

                contParcela++;
                parcelaAux = factura.parcela
                beneficios[contParcela] = 0;
                perdidas[contParcela] = 0;
                if (factura.perdida == 1) {
                    perdidas[contParcela] += parseInt(factura.cantidad);
                }
                else {
                    beneficios[contParcela] += parseInt(factura.cantidad);
                }

            }
            else {
                if (factura.perdida == 1) {
                    perdidas[contParcela] += parseInt(factura.cantidad);
                }
                else {
                    beneficios[contParcela] += parseInt(factura.cantidad);
                }
            }
        })

        // añadimos las cantidades a los labels
        let cont = 0;
        arrayLabelBen = arrayLabelBen.map(label=>{
            // aprovechamos para generar los colors, este bucle tiene las mismas iteraciones que parcelas
            arrayBackgroundColors.push("hsl( " + getRandomColor(cont, arrayLabelBen.length) + ", 100%, 50% )")
            return label +": "+ beneficios[cont++] + " €";
        })
        cont = 0;
        arrayLabelGas = arrayLabelGas.map(label=>{
            arrayBackgroundColorsPerdida.push("hsl( " + getRandomColor(cont, arrayLabelGas.length) + ", 100%, 50% )")
            return label +": "+ perdidas[cont++] + " €";
        })

        let options = {

            legend: {
                display:false,
                position: 'top',
                align: 'center',
                labels: {
                    fontSize:16,
                    width:5
                }
            },

            title: {
                display: true,
                position: 'top',
                align: 'start',
                fontSize: 16,
                fontStyle: 'bold',
                fontcolor: '#d7d7d7',
                padding: 8,
            },
            elements: {
                center: {
                    text: '',
                    color: '#FF6384', // Default is #000000
                    fontStyle: 'sans-serif', // Default is Arial
                    sidePadding: 30, // Default is 20 (as a percentage)
                    minFontSize: 8, // Default is 20 (in px), set to false and text will not wrap.
                    lineHeight: 25 // Default is 25 (in px), used for when text wraps
                }
            },
            tooltips: {
                // mostrar el porcentaje en el tooltip
                callbacks: {
                    label: function(tooltipItem, data) {
                        var allData = data.datasets[tooltipItem.datasetIndex].data;
                        var tooltipLabel = data.labels[tooltipItem.index];
                        var tooltipData = allData[tooltipItem.index];
                        var total = 0;
                        for (var i in allData) {
                            total += allData[i];
                        }
                        var tooltipPercentage = Math.round((tooltipData / total) * 100);
                        return ' (' + tooltipPercentage + '%) ' + tooltipLabel;
                    }
                }
            },
            maintainAspectRatio: false,
            scaleBeginAtZero: false,
            responsive: true,
            cutoutPercentage: 70,
        };

        // graficas beneficios
        let ctx = document.getElementById('grafica-beneficios');
        this.graficaBeneficios = new Chart(ctx, {
            type: 'doughnut',
            data: {
                datasets: [{data: beneficios,backgroundColor: arrayBackgroundColors,}],
                // These labels appear in the legend and in the tooltips when hovering different arcs
                labels: arrayLabelBen
            },
            options: options
        });
        // calculo del total para ponerlo en el texto central
        let total = 0;
        beneficios.forEach(e=>{total+=e})
        this.graficaBeneficios.options.title.text = "Beneficios por parcela";
        this.graficaBeneficios.options.elements.center.text = total+" €";
        this.graficaBeneficios.options.elements.center.color = "#46b63c";

        // grafica gastos
        let ct = document.getElementById('grafica-gastos');
        this.graficaGastos = new Chart(ct, {
            type: 'doughnut',
            data: {
                datasets: [{data: perdidas,backgroundColor: arrayBackgroundColorsPerdida,}],
                // These labels appear in the legend and in the tooltips when hovering different arcs
                labels: arrayLabelGas
            },
            options: options
        });

        total = 0;
        perdidas.forEach(e=>{total+=e;})
        this.graficaGastos.options.title.text = "Gastos por parcela";
        this.graficaGastos.options.elements.center.text = total+" €";
        this.graficaGastos.options.elements.center.color = "#b73a3a";

    },//()

    // temporadas -> ()
    // pinta las gráfica temporadas en el canvas id = "grafica-balance"
    representarBalanceNetoPorMes: function(facturaciones){

        // juntar las facturas por mes
        // y por mes tener un balance total

        // primero ordenar por mes
        facturaciones = facturaciones.sort((f1,f2) =>{
            if(f1.fecha > f2.fecha){
                return 1;
            }else if(f1.fecha < f2.fecha){
                return -1;
            }
            return 0;
        });

        // coger el primer mes para luego pivotar sobre él
        // cuando cambie el mes habrá que crear una nueva posición en el
        // array resultado
        let mesAux = facturaciones[0].fecha.split("-")[1];
        let contMes = 0;
        // añadimos el balance neto del mes
        let data = [];
        // añadimos el mes
        let labels = [];
        labels.push(getStringMesFromNumber(mesAux))
        data[contMes] = 0;

        let dataBackgroundColors = [];// array que contendrá los colores de cada barra del mes

        facturaciones.forEach(facturacion=>{
            // coger el mes
            let mes = facturacion.fecha.split("-")[1];

            if(mesAux !== mes){
                // aumentar el contador del mes
                mesAux = mes;
                contMes++
                // si es diferente
                labels.push(getStringMesFromNumber(mes))

                let cantidad = 0;
                if(facturacion.perdida === 1){
                    // es perdida
                    cantidad = parseInt("-"+facturacion.cantidad)
                }else{
                    // es beneficio
                    cantidad = parseInt(facturacion.cantidad)
                }
                data[contMes] = 0;
                data[contMes] += cantidad;


            }else{
                let cantidad;
                if(facturacion.perdida === 1){
                    // es perdida
                    cantidad = parseInt("-"+facturacion.cantidad)
                }else{
                    // es beneficio
                    cantidad = parseInt(facturacion.cantidad)
                }
                data[contMes] += cantidad;
            }

            // antes de cambiar de mes
            // comprobamos si el anterior era beneficio o gasto
            // y añadimos el color de la barra del mes
            dataBackgroundColors[contMes] = (data[contMes] > 0 ? '#46B63C' : '#B63C3C');

        })

        let datosGrafica = {
            labels: labels,
            datasets: [{
                data: data,
                backgroundColor: dataBackgroundColors,
                barPercentage: 0.4
            }]
        };
        let ctx = document.getElementById('grafica-balance');
        ctx.fillStyle = "#FFF"
        let options = {
            bezierCurve: false,
            connectNullData: true,
            spanGaps: true,
            scales: {
                yAxes: [{
                    ticks: {
                    },
                    scaleLabel: {
                        display: true,
                        labelString: "Balance neto (€)"
                    },
                    gridLines: {
                        color: [],
                        zeroLineWidth: 1,
                        backgroundColor: "#fff",
                    }
                }],
            },
            maintainAspectRatio: false,
            legend: null,
            title: {
                display: true,
                position: 'top',
                align: 'start',
                fontSize: 16,
                fontStyle: 'bold',
                fontcolor: '#d7d7d7',
                padding: 8,
                text:"Balance neto mensual"
            },
            tooltips: {
                backgroundColor: '#fff',
                titleFontColor: '#000',
                titleAlign: 'center',
                bodyFontColor: '#333',
                borderColor: '#666',
                borderWidth: 1,
            },
            scaleBeginAtZero: false,
            responsive: true,
        };
        this.graficaBalance = new Chart(ctx, {
            type: 'bar',
            data: datosGrafica,
            options: options,
        });


    },//()


    // habilitar y deshabilitar los botones que controlan la paginacion
    // para evitar errores
    prepararBotonesPaginador : function (paginaActual, maxPages) {
        this.btnNextPage.removeAttribute("disabled");
        this.btnPreviousPage.removeAttribute("disabled");

        document.getElementById("paginadorListaCampo").innerHTML = "Página "+paginaActual + " de " + maxPages;

        // si el maxPages es mayor que uno habilitar el next page
        if(maxPages>1){
            this.btnNextPage.removeAttribute("disabled");
        }else{
            this.btnNextPage.setAttribute("disabled","disabled");
        }
        // si el current page es igual al maxPages deshabilitar el next page
        if(paginaActual === maxPages){
            this.btnNextPage.setAttribute("disabled","disabled");
        }
        // si el current page es mayor que uno habilitar el previous page
        if(paginaActual > 1){
            this.btnPreviousPage.removeAttribute("disabled");
        }else{
            this.btnPreviousPage.setAttribute("disabled","disabled");
        }

    },
    //Falta balance (irá donde está comentado)
    cambiarFlechasOrden(ordenParcela, ordenFecha, ordenBalance) {
        document.getElementById("flecha-nombre-temp").className = (!ordenParcela) ? "fas fa-chevron-down" : "fas fa-chevron-up";
        document.getElementById("flecha-fecha-temp").className = (!ordenFecha) ? "fas fa-chevron-down" : "fas fa-chevron-up";
        document.getElementById("flecha-balance-temp").className = (!ordenBalance) ? "fas fa-chevron-down" : "fas fa-chevron-up";
    },

}

var controladorFacturacion = {
    modelo: modeloFacturacion,
    vista: vistaFacturacion,
    controladorFinalizarTemporada: null,

    iniciar: function (/*controladorFinalizarTemporada,*/ idTemporada) {
        this.modelo.controlador = this;
        this.vista.controlador = this;
        this.vista.esconderElementos();
        this.vista.mostrarCarga();
        this.modelo.cargar(idTemporada);


        //this.controladorFinalizarTemporada = controladorFinalizarTemporada;
    },

    // calcula el balance total de todas las temporadas
    calcularBalanceTemporada: function(facturas){

        let balanceTotal = 0;
        let beneficioTotal = 0;
        let gastoTotal = 0;

        facturas.forEach(factura=>{
            if(factura.cantidad!=null){
                let balanceTemp = 0;
                (factura.perdida ? balanceTemp = -parseInt(factura.cantidad) : balanceTemp = parseInt(factura.cantidad))
                balanceTotal+= balanceTemp;
                if(balanceTemp > 0){
                    beneficioTotal+= balanceTemp;
                }else{
                    gastoTotal+= balanceTemp;
                }
            }
        })

        this.vista.representarBalanceTotal(balanceTotal,beneficioTotal,gastoTotal);

    },
    //Falta balance (irá donde está comentado)
    cambiarFlechasOrden(ordenParcela, ordenFecha, ordenBalance) {
        this.vista.cambiarFlechasOrden(ordenParcela,ordenFecha,ordenBalance)
    },
    representar : function(facturas, paginaActual){
        this.vista.representarHistorial(facturas, paginaActual);
        this.vista.mostrarLista();
    },

    representarGraficas : function(facturaciones){
        this.vista.representarGraficas(facturaciones);
    },

    representarError: function () {
        this.vista.mostrarError();

    },

    tablaFacturasTemporada: function(facturas){
        this.vista.tablaFacturasTemporada(facturas)
    },


    // Recibe una temporada, le coloca en local Storage y carga detalles-temporada
    onClickElemento: function(elemento){
        document.getElementById('concepto').textContent = elemento.concepto;
        document.getElementById("parcela-factura-modal").textContent = elemento.parcela;
        document.getElementById('fecha-factura-modal').textContent = elemento.fecha;
        document.getElementById('total-factura-modal').textContent = elemento.perdida === 1
            ?  -elemento.cantidad +"€"
            :  elemento.cantidad +"€";

        this.mostrarModal()

    },

    mostrarModal: function(){
        let modal = document.getElementById('modal-detalles-factura')
        modal.style.display = "block"
    },

    cerrarElemento: function(){
        let modal = document.getElementById('modal-detalles-factura')
        modal.style.display = "none"
    },

    // llama al controlador que gestiona el finalizar temporada
    onFinalizarTemporada: function(elemento){
        this.controladorFinalizarTemporada.iniciar(elemento.id)
    },

    onRemoveElementoLista: function (elemento) {
        // confimar borrado
        if(confirm("Se borrará para siempre la factura "+elemento.concepto+"\n¿Estás seguro?")){
            this.vista.mostrarCarga();
            this.modelo.borrarElemento(elemento.id);
        }
    },
    esconderOMostrarLabelsSegunPulsado: function(idElemento, esconder) {
        //coger id
        // coger grafica correspondiente
        // si es esonder, quitar labels
        // si es mostrar, poner labels
        if(idElemento === "grafica-balance"){
            if(esconder){
                // esconder balance
                this.vista.graficaBalance.options.scales.yAxes[0].ticks.display = false;
                this.vista.graficaBalance.options.scales.yAxes[0].scaleLabel.display = false;
                this.vista.graficaBalance.options.scales.xAxes[0].ticks.display = false;
                this.vista.graficaBalance.update();
            }else{
                this.vista.graficaBalance.options.scales.yAxes[0].ticks.display = true;
                this.vista.graficaBalance.options.scales.yAxes[0].scaleLabel.display = true;
                this.vista.graficaBalance.options.scales.xAxes[0].ticks.display = true;
                this.vista.graficaBalance.update();
            }
        }else if(idElemento === "grafica-gastos"){
            if(esconder){
                // console.log("esconder beneficios")
                this.vista.graficaGastos.options.legend.display = false;
                this.vista.graficaGastos.update();
            }else{
                // console.log("mostrar beneficios")
                this.vista.graficaGastos.options.legend.display = true;
                this.vista.graficaGastos.update();
            }
        }else if(idElemento === "grafica-beneficios"){
            if(esconder){
                // console.log("esconder beneficios")
                this.vista.graficaBeneficios.options.legend.display = false;
                this.vista.graficaBeneficios.update();
            }else{
                // console.log("mostrar beneficios")
                this.vista.graficaBeneficios.options.legend.display = true;
                this.vista.graficaBeneficios.update();
            }
        }
    },

    onclickPDF(){
        localStorage.setItem("lista-facturas", JSON.stringify(this.modelo.facturas))
        window.open("pdf.html")
    }


};









