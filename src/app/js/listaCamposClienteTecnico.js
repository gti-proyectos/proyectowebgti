/**
 * Autor: Rubén Pardo Casanova
 * GTI GRUPO 11
 * Fecha: 09/05/2020
 *
 * Modelo vista controlador que gestiona la lista de los campos de un cliente para el panel del tecnico
 *
 */

var ModeloListaCamposTecnico = {

    controlador: {},
    idCliente: -1,

    todosLosCampos:{},//contendrá todos los clientes siempre
    camposFiltrados:{},// contendrá los clientes filtrados cuando se use el buscador o el ordenar

    paginaActual: 1,

    // al principio no estan ordenados
    // true = ASC, false = DESC
    ordenNombre: false,
    ordenDescripcion: false,
    ordenFecha: false,

    obtenerDatos: async function () {


        try{
            this.todosLosCampos = await fetch('../../src/api/v1.0/campo?idCliente=' + this.idCliente)
                .then(respuesta => {
                    if (respuesta.status === 200) {
                        return respuesta.json();
                    } else {
                        this.controlador.error = true;
                        throw Error(respuesta.statusText);
                    }
                })// juntamos los dos arrays en uno
            this.camposFiltrados = this.todosLosCampos;
            console.log(this.controlador)
            this.controlador.representarDatos(this.camposFiltrados,this.paginaActual);


        }catch (e) {
            console.error(e);
            this.controlador.representarError();
        }
    },


    // filtrar los clientes tanto por nombre correo o telefono
    filtrarPorNombreDescripcion: function (textoAFiltrar) {
        // si no hay nada escrito poner todos los clientes
        if(textoAFiltrar === ""){
            this.camposFiltrados = this.todosLosCampos;
        }else{
            this.camposFiltrados = this.todosLosCampos.filter(campo =>{
                if(campo.nombre!=null && campo.nombre.toLowerCase().includes(textoAFiltrar.toLowerCase())){
                    return campo;
                }else if(campo.descripcion!=null &&  campo.descripcion.toLowerCase().includes(textoAFiltrar.toLowerCase())){
                    return campo;
                }
            })


        }
        // siempre que filtremos volvamos a la pagina 1
        this.paginaActual = 1;
        this.controlador.representarDatos(this.camposFiltrados,this.paginaActual);
    },

    // idOrden -> 1 = Nombre, 2 = Correo, 3 = telefono
    // ascendente -> true = ordenar asc, false = ordenar desc
    ordenarPor:function (idOrden) {
        //ocultamos todas las flechas
        document.getElementById('flecha-nombre-campo').style.display="none";
        document.getElementById('flecha-descripcion-campo').style.display="none";
        document.getElementById('flecha-fecha-campo').style.display="none";
        let filtro = "";
        let ordenASC = null;

        // determinar el tipo de filtro y el orden ASC o DESC
        if(idOrden===1){
            //aparece la flecha
            document.getElementById('flecha-nombre-campo').style.display="inline";

            filtro = "nombre"
            // ordenar por nombre
            if(this.ordenNombre == null){
                // la primera vez ordenar ascendentemente
                this.ordenNombre = true;
            }else{
                // cade vez que se pulse de mas cambiarlo
                this.ordenNombre = !this.ordenNombre;
            }
            ordenASC = this.ordenNombre;
        }else{
            this.ordenNombre = false;
        }
        if(idOrden === 2){
            //aparece la flecha
            document.getElementById('flecha-descripcion-campo').style.display="inline";

            filtro = "descripcion";
            // ordenar por correo
            if(this.ordenDescripcion == null){
                // la primera vez ordenar ascendentemente
                this.ordenDescripcion = true;
            }else{
                // cade vez que se pulse de mas cambiarlo
                this.ordenDescripcion = !this.ordenDescripcion;
            }
            ordenASC = this.ordenDescripcion;
        }else{
            this.ordenDescripcion = false;
        }
        if(idOrden === 3){
            //aparece la flecha
            document.getElementById('flecha-fecha-campo').style.display="inline";

            filtro = "fechaCreacion";
            // ordenar por correo
            if(this.ordenFecha == null){
                // la primera vez ordenar ascendentemente
                this.ordenFecha = true;
            }else{
                // cade vez que se pulse de mas cambiarlo
                this.ordenFecha = !this.ordenFecha;
            }
            ordenASC = this.ordenFecha;
        }else{
            this.ordenFecha = false;
        }
        // ordenamos siempre desde clientes filtrados
        // al inicio seran todos y si hay algun filtro en el buscador se
        // ordenará sobre este
        this.camposFiltrados = this.camposFiltrados.sort((campo1, campo2) =>{
            // juntar todos los nulls o arriba o abajo
            if(campo1[filtro] == null){
                return ordenASC ? 1 : -1;
            }else if(campo2[filtro] == null ){
                return ordenASC ? -1 : 1;
            }else{
                // ordenar
                if(campo1[filtro].toLowerCase() > campo2[filtro].toLowerCase()){
                    return ordenASC ? 1 : -1;
                }else if(campo1[filtro].toLowerCase() < campo2[filtro].toLowerCase()){
                    return ordenASC ? -1 : 1;
                }
            }


            return 0;
        })

        // simepre que ordenemos volvemos a la pagina 1
        this.paginaActual = 1;
        this.controlador.cambiarFlechasOrden(this.ordenNombre,this.ordenDescripcion,this.ordenFecha)
        this.controlador.representarDatos(this.camposFiltrados,this.paginaActual);
    },

    anteriorPagina:function () {
        this.paginaActual--;
        this.controlador.representarDatos(this.camposFiltrados,this.paginaActual);
    },

    siguientePagina: function () {
        this.paginaActual++;
        this.controlador.representarDatos(this.camposFiltrados,this.paginaActual);
    }

};

var VistaListaCamposTecnico = {

    controlador: {},
    cliente:{},

    bloqueLista:{}, // donde se representarán los elementos de la lista
    paginaActual: 1,// indice para controlar la paginacion
    maxElementosPorPagina: 5,
    bloqueError:{},
    bloqueCarga:{},
    bloqueBodyLista:{},

    // botones para el control de la paginacion de clientes
    btnNextPage:{},
    btnPreviousPage:{},

    // booleano que controla si se tiene que mostrar en primera instancia
    mostrar: false,

    preparar:function (idBloqueLista, idListaBody, idError, idCarga, idBtnNext, idBtnPrev,mostrar,cliente) {
        this.bloqueLista = document.getElementById(idBloqueLista);
        this.bloqueBodyLista = document.getElementById(idListaBody);
        this.bloqueError = document.getElementById(idError);
        this.bloqueCarga = document.getElementById(idCarga);
        this.btnPreviousPage = document.getElementById(idBtnPrev);
        this.btnNextPage = document.getElementById(idBtnNext);
        this.cliente =cliente;
        this.mostrar = mostrar;
    },

    // funciones para controlar cuando se muestran los bloques html
    esconderElementos: function(){

        this.bloqueLista.style.display = "none";
        this.bloqueCarga.style.display = "none";
        this.bloqueError.style.display = "none";
    },
    mostrarCarga: function () {
        if(this.mostrar) {
            this.esconderElementos();
            this.bloqueCarga.style.display = "flex"
        }
    },
    mostrarError:function () {
        this.esconderElementos();
        this.bloqueError.style.display = "flex"

    },
    mostrarLista:function () {
        if(this.mostrar) {
            this.esconderElementos();
            if(this.controlador.error){
                this.bloqueError.style.display = "flex"
            }else{
                this.bloqueLista.style.display = "flex"
            }
        }
    },

    // representar elementos en el html
    representarDatos:function (campos,paginaActual) {
        //control de paginacion
        // maxPages = length/maxElementosPorPagina redondeado hacia arriba
        let maxPages = Math.ceil(campos.length/this.maxElementosPorPagina);
        // si estamos en la pagina 1 empezamos desde el 0 (5*(1-1))
        // si estamos en la pagina 2 empezamos desde el 5 (5*(2-1))
        let elementoInicial = this.maxElementosPorPagina * (paginaActual-1);

        this.prepararBotonesPaginador(paginaActual,maxPages);

        this.bloqueBodyLista.innerHTML = "";

        // pintar elemntos acorde al maxElementosPorPagina
        // los elementos iran desde (maxElementosPorPagina* (paginaActual-1)) hasta this.maxElementosPorPagina*paginaActual
        for(let i = elementoInicial;i<(this.maxElementosPorPagina*paginaActual);i++){
            // si en una pagina no llega al maxElementosPorPagina hay que evitar que intente pintar esos
            // elementos y no de error
            if(i<campos.length){
                let hilera = document.createElement("tr");

                let celdaNombre = document.createElement("td");
                let celdaDescripcion = document.createElement("td");
                let celdaFecha = document.createElement("td");
                let celdaOperacion = document.createElement("td");
                celdaOperacion.className = "list-opereciones";

                let textoCeldaNombre = document.createTextNode(campos[i].nombre != null ? campos[i].nombre : "Sin asignar");
                let textoCeldaDesc = document.createTextNode(campos[i].descripcion != null ? campos[i].descripcion : "Sin asignar");
                let textoCeldaFecha = document.createTextNode(campos[i].fechaCreacion != null ? campos[i].fechaCreacion : "Sin asignar");

                celdaNombre.appendChild(textoCeldaNombre);
                celdaDescripcion.appendChild(textoCeldaDesc);
                celdaFecha.appendChild(textoCeldaFecha);



                let btnRead = document.createElement("button");
                btnRead.className = "btn-list-element";
                btnRead.onclick = () =>{this.controlador.onClickElemento(campos[i])};

                let ul = document.createElement("ul");
                ul.className = "listaOperaciones";

                let li = document.createElement("li")
                btnRead.innerHTML = `<i class="fas fa-eye"></i><p class="label">Ver parcelas</p>`;
                li.appendChild(btnRead);
                ul.append(li)

                this.createDropDown(celdaOperacion,campos[i])
                celdaOperacion.appendChild(ul);

                // añadimos los elementos a la fila
                hilera.appendChild(celdaNombre);
                hilera.appendChild(celdaDescripcion);
                hilera.appendChild(celdaFecha);
                hilera.appendChild(celdaOperacion)


                // añadimos la fila al tbody
                this.bloqueBodyLista.appendChild(hilera);
            }


        }


    },

    createDropDown:function(celdaOperacion,campo) {
        let btnRead = document.createElement("button");
        btnRead.className = "btn-list-element";
        btnRead.onclick = () =>{this.controlador.onClickElemento(campo)};

        let liDrop = document.createElement("div")
        btnRead.innerHTML = `<i class="fas fa-eye"></i><p>Ver parcelas</p>`
        liDrop.appendChild(btnRead);

        // añadimos el dropdown button que se mostrará en movil
        celdaOperacion.innerHTML = `<button class="btn btn-secondary dropdown-toggle tresPuntos"
                          type="button" id="dropdownMenu1" data-toggle="dropdown"
                          aria-haspopup="true" aria-expanded="false">
                   <i class="fas fa-ellipsis-v tresPuntos">
                  </button>
                  <div class="dropdown-menu" aria-labelledby="dropdownMenu1">
                    
                  </div>`
        celdaOperacion.querySelector("div.dropdown-menu").append(liDrop)
    },


    // habilitar y deshabilitar los botones que controlan la paginacion
    // para evitar errores
    prepararBotonesPaginador : function (paginaActual, maxPages) {
        this.btnNextPage.removeAttribute("disabled");
        this.btnPreviousPage.removeAttribute("disabled");
        document.getElementById("paginadorListaCampo").innerHTML = "Página "+paginaActual + " de " + maxPages;

        // si el maxPages es mayor que uno habilitar el next page
        if(maxPages>1){
            this.btnNextPage.removeAttribute("disabled");
        }
        // si el current page es igual al maxPages deshabilitar el next page
        if(paginaActual === maxPages){
            this.btnNextPage.setAttribute("disabled","disabled");
        }
        if(maxPages === 0){
            this.btnNextPage.setAttribute("disabled","disabled");
        }
        // si el current page es mayor que uno habilitar el previous page
        if(paginaActual > 1){
            this.btnPreviousPage.removeAttribute("disabled");
        }else{
            this.btnPreviousPage.setAttribute("disabled","disabled");
        }

    },

    cambiarFlechasOrden(ordenNombre, ordenDesc, ordenFecha) {
        document.getElementById("flecha-nombre-campo").className = (!ordenNombre) ? "fas fa-chevron-down" : "fas fa-chevron-up";
        document.getElementById("flecha-descripcion-campo").className = (!ordenDesc) ? "fas fa-chevron-down" : "fas fa-chevron-up";
        document.getElementById("flecha-fecha-campo").className = (!ordenFecha) ? "fas fa-chevron-down" : "fas fa-chevron-up";

    },

};

var ControladorListaCamposTecnico = {
    modelo: ModeloListaCamposTecnico,
    vista: VistaListaCamposTecnico,
    controladorCreateUsuario: null,
    controladorModificarCampo: null,

    error: false,// controla si ha habido un error en la obtencio de datos

    iniciar: function (idCliente,controladorCreateUsuario,controladorModificarCampo) {
        this.modelo.controlador = this;
        this.modelo.idCliente = idCliente;
        this.vista.controlador = this;
        this.vista.mostrarCarga();
        this.modelo.obtenerDatos();
        this.controladorCreateUsuario = controladorCreateUsuario;
        this.controladorModificarCampo = controladorModificarCampo;
    },

    cambiarFlechasOrden(ordenNombre, ordenDesc, ordenFecha ) {
        this.vista.cambiarFlechasOrden(ordenNombre,ordenDesc, ordenFecha)
    },

    representarDatos: function(datos,paginaActual){
        this.vista.representarDatos(datos,paginaActual);
        this.vista.mostrarLista();
    },


    representarError: function () {
        this.vista.mostrarError();
    },


    onClickElemento: function (elemento) {
        localStorage.setItem("listaClienteTecnico-campo",JSON.stringify(elemento));
        location.href = "panel-admin-tecnico-parcelas.html";
    },


}