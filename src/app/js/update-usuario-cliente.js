/**
 * Autor: Ruben Pardo
 * Fecha: 09/05/2020
 * Modelo Vista Controlador Editar un usuario de un cliente
 *
 */

var ModeloModificarUsuarioCliente = {
    controlador: {},
    editarCliente: async function (usuario) {
        try{
            let form = new FormData();
            form.append("modificar","true");
            form.append("data",JSON.stringify(usuario));

            await fetch('../../src/api/v1.0/usuario',{
                method: 'POST',
                body: form,
            })
                .then(respuesta => {
                    if (respuesta.status === 200) {
                        return respuesta.json();
                    } else {
                        this.controlador.errorEditarCliente();
                        throw Error(respuesta.statusText);
                    }
                })// juntamos los dos arrays en uno


            this.controlador.editarClienteOk();


        }catch (e) {
            console.error(e);
        }
    },
};

var VistaModificarUsuarioCliente = {

    controlador: {},
    usuario:{},
    usuarioAdmin:{},
    formulario:{},

    preparar:function (usuarioAdmin,idError, idCarga,usuario,idFormulario) {
        this.bloqueError = document.getElementById(idError);
        this.bloqueCarga = document.getElementById(idCarga);
        this.formulario = document.getElementById(idFormulario);
        this.usuario = usuario;
        this.usuarioAdmin = usuarioAdmin;//para prevenir que no se modifique a si mismo cambiando de rol a agricultor
    },
    mostrarModal: function(){
        this.bloqueError.style.display = "none";
        this.modal.style.display = "block";
    },

    esconderModal: function(){
        this.modal.style.display = "none";
        this.formulario.nombre.value =  "";
        this.formulario.correo.value =  "";
    },
    mostrarError:function () {
        this.bloqueError.style.display = "block"
    },
    // funciones para el controlar del formulario
    prepararFormulario: function(usuario){
        // cogemos todos los inputs del formulario y le asignamos los valores del cliente
        this.formulario.nombre.value = usuario.nombre == null ? "":usuario.nombre;
        this.formulario.apellido.value = usuario.apellido == null ? "":usuario.apellido;
        this.formulario.correo.value = usuario.correo == null ? "":usuario.correo;
        this.formulario.rol.value = usuario.rol == null ? "":usuario.rol;
    },
    getUsuarioFormulario: function(){
        let usuario = {};
        // cogemos todos los inputs del formulario y le asignamos los valores del cliente
        usuario["id"] = this.usuario.id;
        usuario["cliente"] = this.usuario.cliente;
        usuario["nombre"] = this.formulario.nombre.value;
        usuario["apellido"] = this.formulario.apellido.value;
        usuario["correo"] = this.formulario.correo.value;
        // evitamos que se cambie a agricultor
        usuario["rol"] = this.usuario.id === this.usuarioAdmin.id ? 1 : this.formulario.rol.value;
        return usuario;
    },
}

var ControladorModificarUsuarioCliente = {
    modelo: ModeloModificarUsuarioCliente,
    vista: VistaModificarUsuarioCliente,

    error: false,// controla si ha habido un error en la obtencio de datos

    iniciar: function (usuario) {
        this.modelo.controlador = this;
        this.vista.controlador = this;
        this.vista.prepararFormulario(usuario);
        this.vista.mostrarModal();
        this.vista.usuario=usuario;
    },

    representarError: function () {
        this.vista.mostrarError();
    },

    // eventos del on sumbit y on reset del formulario de los datos
    onSumbitFormulario: function(e){
        e.preventDefault();
        this.modelo.editarCliente(this.vista.getUsuarioFormulario());

    },
    onResetFormulario: function(e){
        e.preventDefault();
        this.vista.resetFormulario();
    },
    errorEditarCliente: function(){
        this.vista.mostrarError();
        this.vista.esconderModal();
    },
    editarClienteOk: function(usuario){
        let user ={};
        user["id"] = usuario.id;
        user["cliente"] = usuario.cliente;
        user["correo"] = usuario.correo;
        user["rolId"] = usuario.rol;
        user["nombre"] = usuario.nombre;
        // recargamos la pagina con el nuevo cliente por si ha cambiado de rol
        // para esconder los si ha puesto admin o mostrarlos si ha puesto agricultor
        localStorage.setItem("listaUsuarioCliente-usuario",JSON.stringify(user));
        window.location.reload();
    },
}