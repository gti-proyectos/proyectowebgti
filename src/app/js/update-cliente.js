/**
 * Autor: Ruben Pardo
 * Fecha: 08/05/2020
 * Modelo Vista Controlador Editar un cliente
 *
 */

var ModeloModificarCliente = {
    controlador: {},
    editarCliente: async function (cliente) {
        console.log(cliente)
        console.log(JSON.stringify(cliente))
        try{
            let form = new FormData();
            form.append("modificar","true");
            form.append("data",JSON.stringify(cliente));

            await fetch('../../src/api/v1.0/cliente',{
                method: 'POST',
                body: form,
            })
                .then(respuesta => {
                    if (respuesta.status === 200) {
                        return respuesta.json();
                    } else {
                        this.controlador.errorEditarCliente();
                        throw Error(respuesta.statusText);
                    }
                })// juntamos los dos arrays en uno


            this.controlador.editarClienteOk();


        }catch (e) {
            console.error(e);
            this.controlador.mostrarError();
        }
    },
};

var VistaModificarCliente = {

    controlador: {},
    cliente:{},
    formulario:{},
    modal:{},

    preparar:function (idError, idCarga,idFormulario,idModal,idCloseModal) {
        this.bloqueError = document.getElementById(idError);
        this.bloqueCarga = document.getElementById(idCarga);
        this.formulario = document.getElementById(idFormulario);
        this.modal = document.getElementById(idModal);

    },
    mostrarModal: function(){
        this.bloqueError.style.display = "none";
        this.modal.style.display = "block";
    },

    esconderModal: function(){
        this.modal.style.display = "none";
        this.formulario.nombre.value =  "";
        this.formulario.apellidos.value =  "";
        this.formulario.correo.value =  "";
    },

    mostrarError:function () {
        this.bloqueError.style.display = "flex"

    },

    // funciones para el controlar del formulario
    prepararFormulario: function(cliente){

        // cogemos todos los inputs del formulario y le asignamos los valores del cliente
        this.formulario.nombre.value = cliente.nombre == null ? "":cliente.nombre;
        this.formulario.correo.value = cliente.correo == null ? "":cliente.correo;
        this.formulario.telefono.value = cliente.telefono == null ? "":cliente.telefono;
        this.formulario.direccion.value = cliente.direccion == null ? "":cliente.direccion;
        this.formulario.apellidos.value = cliente.apellidos == null ? "":cliente.apellidos;
    },



    getClienteFormulario: function(){
        let clienteRes = {};
        // cogemos todos los inputs del formulario y le asignamos los valores del cliente
        clienteRes["id"] = this.cliente.id;
        clienteRes["nombre"] = this.formulario.nombre.value;
        clienteRes["apellidos"] = this.formulario.apellidos.value;
        clienteRes["correo"] = this.formulario.correo.value;
        clienteRes["telefono"] = this.formulario.telefono.value;
        clienteRes["direccion"] = this.formulario.direccion.value;

        return clienteRes;
    },
}

var ControladorModificarCliente = {
    modelo: ModeloModificarCliente,
    vista: VistaModificarCliente,

    error: false,// controla si ha habido un error en la obtencio de datos

    iniciar: function (cliente) {
        this.modelo.controlador = this;
        this.modelo.idCliente = cliente.id;
        this.vista.controlador = this;
        this.vista.prepararFormulario(cliente);
        this.vista.mostrarModal();
        this.vista.cliente=cliente;
    },

    representarError: function () {
        this.vista.mostrarError();
    },

    // eventos del on sumbit y on reset del formulario de los datos
    onSumbitFormulario: function(e){
        e.preventDefault();

            this.modelo.editarCliente(this.vista.getClienteFormulario())


    },
    onResetFormulario: function(e){
        e.preventDefault();
        this.vista.esconderModal();
    },
    errorEditarCliente: function(){
        this.vista.mostrarError();
    },
    editarClienteOk: function(){
        this.vista.esconderModal();
        window.location.reload();
    },
}