/**
 * Autor: Rubén Pardo Casanova
 * GTI GRUPO 11
 * Fecha: 09/05/2020
 *
 * Modelo vista controlador que gestiona la lista de los parcelas de un campo
 *
 */

var ModeloListaParcelaTecnico = {

    controlador: {},
    idCampo: -1,

    todosLasParcelas:{},//contendrá todos los clientes siempre
    parcelasFiltrados:{},// contendrá los clientes filtrados cuando se use el buscador o el ordenar

    paginaActual: 1,

    // al principio no estan ordenados
    // true = ASC, false = DESC
    ordenNombre: false,
    ordenDescripcion: false,
    ordenFecha: false,

    obtenerDatos: async function () {
        try{
            let resultado = await fetch('../../src/api/v1.0/parcela?idCampo=' + this.idCampo)
                .then(respuesta => {
                    if (respuesta.status === 200) {
                        return respuesta.json();
                    } else {
                        this.controlador.error = true;
                        throw Error(respuesta.statusText);
                    }
                })// juntamos los dos arrays en uno
            this.todosLasParcelas = resultado[0];
            this.parcelasFiltrados = this.todosLasParcelas;
            console.log(this.controlador)
            this.controlador.representarDatos(this.parcelasFiltrados,this.paginaActual);
            cargarParcelas(this.todosLasParcelas)


        }catch (e) {
            console.error(e);
            this.controlador.representarError();
        }
    },
    borrarElemento: async function (idParcela) {
        try{

            await fetch('../../src/api/v1.0/parcela',{
                method:'DELETE',
                body: JSON.stringify({id:idParcela})
            })
                .then(respuesta => {
                    if (respuesta.status === 200) {
                        return respuesta.json();
                    } else {
                        this.controlador.error = true;
                        throw Error(respuesta.statusText);

                    }
                })
            let parcelaABorrar = this.todosLasParcelas.find(e=>{
                if(e.id === idParcela){
                    return e;
                }
            });
            window.location.reload();


        }catch (e) {
            console.error(e);
            this.controlador.representarError();
        }
    },

    // filtrar los clientes tanto por nombre correo o telefono
    filtrarPorNombreDescripcion: function (textoAFiltrar) {
        // si no hay nada escrito poner todos los clientes
        if(textoAFiltrar === ""){
            this.parcelasFiltrados = this.todosLasParcelas;
        }else{
            this.parcelasFiltrados = this.todosLasParcelas.filter(campo =>{
                if(campo.nombre!=null && campo.nombre.toLowerCase().includes(textoAFiltrar.toLowerCase())){
                    return campo;
                }else if(campo.descripcion!=null &&  campo.descripcion.toLowerCase().includes(textoAFiltrar.toLowerCase())){
                    return campo;
                }
            })


        }
        // siempre que filtremos volvamos a la pagina 1
        this.paginaActual = 1;
        this.controlador.representarDatos(this.parcelasFiltrados,this.paginaActual);
    },

    // idOrden -> 1 = Nombre, 2 = Correo, 3 = telefono
    // ascendente -> true = ordenar asc, false = ordenar desc
    ordenarPor:function (idOrden) {
//ocultamos todas las flechas
        document.getElementById('flecha-nombre-parcela').style.display="none";
        document.getElementById('flecha-descripcion-parcela').style.display="none";
        document.getElementById('flecha-fecha-parcela').style.display="none";
        let filtro = "";
        let ordenASC = null;

        // determinar el tipo de filtro y el orden ASC o DESC
        if(idOrden===1){
            //aparece la flecha
            document.getElementById('flecha-nombre-parcela').style.display="inline";
            filtro = "nombre"
            // ordenar por nombre
            if(this.ordenNombre == null){
                // la primera vez ordenar ascendentemente
                this.ordenNombre = true;
            }else{
                // cade vez que se pulse de mas cambiarlo
                this.ordenNombre = !this.ordenNombre;
            }
            ordenASC = this.ordenNombre;
        }else{
            this.ordenNombre = false;
        }
        if(idOrden === 2){
            //aparece la flecha
            document.getElementById('flecha-descripcion-parcela').style.display="inline";
            filtro = "descripcion";
            // ordenar por correo
            if(this.ordenDescripcion == null){
                // la primera vez ordenar ascendentemente
                this.ordenDescripcion = true;
            }else{
                // cade vez que se pulse de mas cambiarlo
                this.ordenDescripcion = !this.ordenDescripcion;
            }
            ordenASC = this.ordenDescripcion;
        }else{
            this.ordenDescripcion = false;
        }
        if(idOrden === 3){
            //aparece la flecha
            document.getElementById('flecha-fecha-parcela').style.display="inline";
            filtro = "fechaCreacion";
            // ordenar por correo
            if(this.ordenFecha == null){
                // la primera vez ordenar ascendentemente
                this.ordenFecha = true;
            }else{
                // cade vez que se pulse de mas cambiarlo
                this.ordenFecha = !this.ordenFecha;
            }
            ordenASC = this.ordenFecha;
        }else{
            this.ordenFecha = false;
        }
        // ordenamos siempre desde clientes filtrados
        // al inicio seran todos y si hay algun filtro en el buscador se
        // ordenará sobre este
        this.parcelasFiltrados = this.parcelasFiltrados.sort((parcela1, parcela2) =>{
            // juntar todos los nulls o arriba o abajo
            if(parcela1[filtro] == null){
                return ordenASC ? 1 : -1;
            }else if(parcela2[filtro] == null ){
                return ordenASC ? -1 : 1;
            }else{
                // ordenar
                if(parcela1[filtro].toLowerCase() > parcela2[filtro].toLowerCase()){
                    return ordenASC ? 1 : -1;
                }else if(parcela1[filtro].toLowerCase() < parcela2[filtro].toLowerCase()){
                    return ordenASC ? -1 : 1;
                }
            }


            return 0;
        })

        // simepre que ordenemos volvemos a la pagina 1
        this.paginaActual = 1;
        this.controlador.cambiarFlechasOrden(this.ordenNombre,this.ordenDescripcion,this.ordenFecha)
        this.controlador.representarDatos(this.parcelasFiltrados,this.paginaActual);
    },

    anteriorPagina:function () {
        this.paginaActual--;
        this.controlador.representarDatos(this.parcelasFiltrados,this.paginaActual);
    },

    siguientePagina: function () {
        this.paginaActual++;
        this.controlador.representarDatos(this.parcelasFiltrados,this.paginaActual);
    }

};

var VistaListaParcelaTecnico = {

    controlador: {},
    campo:{},

    bloqueLista:{}, // donde se representarán los elementos de la lista
    paginaActual: 1,// indice para controlar la paginacion
    maxElementosPorPagina: 5,
    bloqueError:{},
    bloqueCarga:{},
    bloqueBodyLista:{},

    // botones para el control de la paginacion de clientes
    btnNextPage:{},
    btnPreviousPage:{},

    // booleano que controla si se tiene que mostrar en primera instancia
    mostrar: false,

    mapa: null,

    preparar:function (idBloqueLista, idListaBody, idError, idCarga, idBtnNext, idBtnPrev,mostrar,campo) {
        this.bloqueLista = document.getElementById(idBloqueLista);
        this.bloqueBodyLista = document.getElementById(idListaBody);
        this.bloqueError = document.getElementById(idError);
        this.bloqueCarga = document.getElementById(idCarga);
        this.btnPreviousPage = document.getElementById(idBtnPrev);
        this.btnNextPage = document.getElementById(idBtnNext);
        this.campo = campo;
        this.mostrar = mostrar;

    },

    // funciones para controlar cuando se muestran los bloques html
    esconderElementos: function(){
        this.bloqueLista.style.display = "none";
        this.bloqueCarga.style.display = "none";
        this.bloqueError.style.display = "none";
    },
    mostrarCarga: function () {
        if(this.mostrar) {
            this.esconderElementos();
            this.bloqueCarga.style.display = "flex"
        }
    },
    mostrarError:function () {
        this.esconderElementos();
        this.bloqueError.style.display = "flex"

    },
    mostrarLista:function () {
        if(this.mostrar) {
            this.esconderElementos();
            if(this.controlador.error){
                this.bloqueError.style.display = "flex"
            }else{
                this.bloqueLista.style.display = "flex"
            }
        }
    },

    // representar elementos en el html
    representarDatos:function (parcelas,paginaActual) {



        //control de paginacion
        // maxPages = length/maxElementosPorPagina redondeado hacia arriba
        let maxPages = Math.ceil(parcelas.length/this.maxElementosPorPagina);
        // si estamos en la pagina 1 empezamos desde el 0 (5*(1-1))
        // si estamos en la pagina 2 empezamos desde el 5 (5*(2-1))
        let elementoInicial = this.maxElementosPorPagina * (paginaActual-1);

        this.prepararBotonesPaginador(paginaActual,maxPages);

        this.bloqueBodyLista.innerHTML = "";

        // pintar elemntos acorde al maxElementosPorPagina
        // los elementos iran desde (maxElementosPorPagina* (paginaActual-1)) hasta this.maxElementosPorPagina*paginaActual
        for(let i = elementoInicial;i<(this.maxElementosPorPagina*paginaActual);i++){
            // si en una pagina no llega al maxElementosPorPagina hay que evitar que intente pintar esos
            // elementos y no de error
            if(i<parcelas.length){
                let hilera = document.createElement("tr");

                let celdaNombre = document.createElement("td");
                let celdaDescripcion = document.createElement("td");
                let celdaFecha = document.createElement("td");
                let celdaOperacion = document.createElement("td");
                celdaOperacion.className = "list-opereciones";

                let textoCeldaNombre = document.createTextNode(parcelas[i].nombre != null ? parcelas[i].nombre : "Sin asignar");
                let textoCeldaDesc = document.createTextNode(parcelas[i].descripcion != null ? parcelas[i].descripcion : "Sin asignar");
                let textoCeldaFecha = document.createTextNode(parcelas[i].fechaCreacion != null ? parcelas[i].fechaCreacion : "Sin asignar");

                celdaNombre.appendChild(textoCeldaNombre);
                celdaDescripcion.appendChild(textoCeldaDesc);
                celdaFecha.appendChild(textoCeldaFecha);


                let btnSondas = document.createElement("button");
                btnSondas.className = "btn-list-element";
                btnSondas.onclick = () =>{this.controlador.onClickElemento(parcelas[i])};

                let btnEditar = document.createElement("button");
                btnEditar.className = "btn-list-element";
                btnEditar.onclick = () =>{this.controlador.onEditParcela(parcelas[i])};

                let btnBorrar = document.createElement("button");
                btnBorrar.className = "btn-list-element";
                btnBorrar.onclick = () =>{this.controlador.onRemoveElementoLista(parcelas[i])};


                let ul = document.createElement("ul");
                ul.className = "listaOperaciones";


                let li = document.createElement("li")
                btnSondas.innerHTML = `<i class="far fa-eye"></i><p class="label">Ver detalles</p>`
                li.appendChild(btnSondas);

                let li2 = document.createElement("li")
                btnEditar.innerHTML = `<i class="fas fa-pen-square"></i><p class="label">Modificar</p>`
                li2.appendChild(btnEditar);

                let li3 = document.createElement("li")
                btnBorrar.innerHTML = `<i class="far fa-trash-alt"></i><p class="label">Eliminar</p>`
                li3.appendChild(btnBorrar);

                ul.appendChild(li)
                ul.appendChild(li2)
                ul.appendChild(li3)


                this.createDropDown(celdaOperacion,parcelas[i]);
                celdaOperacion.appendChild(ul);

                // añadimos los elementos a la fila
                hilera.appendChild(celdaNombre);
                hilera.appendChild(celdaDescripcion);
                hilera.appendChild(celdaFecha);
                hilera.appendChild(celdaOperacion)


                // añadimos la fila al tbody
                this.bloqueBodyLista.appendChild(hilera);
            }


        }


    },


    createDropDown:function(celdaOperacion,parcela) {


        let btnSondas = document.createElement("button");
        btnSondas.className = "btn-list-element";
        btnSondas.onclick = () =>{this.controlador.onClickElemento(parcela)};

        let btnEditar = document.createElement("button");
        btnEditar.className = "btn-list-element";
        btnEditar.onclick = () =>{this.controlador.onEditParcela(parcela)};

        let btnBorrar = document.createElement("button");
        btnBorrar.className = "btn-list-element";
        btnBorrar.onclick = () =>{this.controlador.onRemoveElementoLista(parcela)};


        let liDrop = document.createElement("div")
        btnSondas.innerHTML = `<i class="fas fa-eye"></i><p>Ver detalles</p>`
        liDrop.appendChild(btnSondas);
        let li2Drop = document.createElement("div")
        btnEditar.innerHTML = `<i class="fas fa-pen-square"></i><p>Modificar</p>`
        li2Drop.appendChild(btnEditar);
        let li3Drop = document.createElement("div")
        btnBorrar.innerHTML = `<i class="far fa-trash-alt"></i><p>Eliminar</p>`
        li3Drop.appendChild(btnBorrar);

        // añadimos el dropdown button que se mostrará en movil
        celdaOperacion.innerHTML = `<button class="btn btn-secondary dropdown-toggle tresPuntos"
                          type="button" id="dropdownMenu1" data-toggle="dropdown"
                          aria-haspopup="true" aria-expanded="false">
                   <i class="fas fa-ellipsis-v tresPuntos">
                  </button>
                  <div class="dropdown-menu" aria-labelledby="dropdownMenu1">
                    
                  </div>`
        celdaOperacion.querySelector("div.dropdown-menu").append(liDrop)
        celdaOperacion.querySelector("div.dropdown-menu").append(li2Drop)
        celdaOperacion.querySelector("div.dropdown-menu").append(li3Drop)
    },

    // habilitar y deshabilitar los botones que controlan la paginacion
    // para evitar errores
    prepararBotonesPaginador : function (paginaActual, maxPages) {
        this.btnNextPage.removeAttribute("disabled");
        this.btnPreviousPage.removeAttribute("disabled");
        document.getElementById("paginadorListaParcela").innerHTML = "Página "+paginaActual + " de " + maxPages;

        // si el maxPages es mayor que uno habilitar el next page
        if(maxPages>1){
            this.btnNextPage.removeAttribute("disabled");
        }
        // si el current page es igual al maxPages deshabilitar el next page
        if(paginaActual === maxPages){
            this.btnNextPage.setAttribute("disabled","disabled");
        }
        if(maxPages === 0){
            this.btnNextPage.setAttribute("disabled","disabled");
        }
        // si el current page es mayor que uno habilitar el previous page
        if(paginaActual > 1){
            this.btnPreviousPage.removeAttribute("disabled");
        }else{
            this.btnPreviousPage.setAttribute("disabled","disabled");
        }

    },

    cambiarFlechasOrden(ordenNombre, ordenDesc, ordenFecha) {
        document.getElementById("flecha-nombre-parcela").className = (!ordenNombre) ? "fas fa-chevron-down" : "fas fa-chevron-up";
        document.getElementById("flecha-descripcion-parcela").className = (!ordenDesc) ? "fas fa-chevron-down" : "fas fa-chevron-up";
        document.getElementById("flecha-fecha-parcela").className = (!ordenFecha) ? "fas fa-chevron-down" : "fas fa-chevron-up";

    },


};

var ControladorListaParcelaTecnico = {
    modelo: ModeloListaParcelaTecnico,
    vista: VistaListaParcelaTecnico,


    error: false,// controla si ha habido un error en la obtencio de datos

    iniciar: function (idCampo) {
        this.modelo.controlador = this;
        this.modelo.idCampo = idCampo;
        this.vista.controlador = this;
        this.vista.mostrarCarga();
        this.modelo.obtenerDatos();
    },

    cambiarFlechasOrden(ordenNombre, ordenDesc, ordenFecha) {
        this.vista.cambiarFlechasOrden(ordenNombre,ordenDesc, ordenFecha)
    },

    representarDatos: function(datos,paginaActual){
        this.vista.representarDatos(datos,paginaActual);
        this.vista.mostrarLista();
    },


    representarError: function () {
        this.vista.mostrarError();
    },


    onClickElemento: function (elemento) {
        elemento["campo"] = this.vista.campo.id;
        localStorage.setItem("listaParcela-parcela",JSON.stringify(elemento));
        location.href = "panel-admin-tecnico-sondas.html";
    },
    onRemoveElementoLista:function (element) {
        if(confirm("Se borrará para siempre la parcela "+element.nombre+" y todas sus sondas\n¿Estás seguro?")){
            this.vista.mostrarCarga();
            this.modelo.borrarElemento(element.id);
        }
    },
    onEditParcela:function(elemento){
        elemento["modificar"] = true;
        elemento["campo"] = this.vista.campo.id;
        console.log(elemento)
        localStorage.setItem("listaClienteTecnico-parcela",JSON.stringify(elemento));
        location.href = "parcela.html";
    },
    onCrearParcela: function () {
        let elemento = {};
        elemento["modificar"] = false;
        elemento["campo"] = this.vista.campo.id;
        console.log(elemento)
        localStorage.setItem("listaClienteTecnico-parcela",JSON.stringify(elemento));
        location.href = "parcela.html";
    }
}