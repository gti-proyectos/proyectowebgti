/**
 * Autor Marc Castelló Martó
 * Logica de carga de los MVC de panel-admin-tecnico-parcelas.html
 *
 * Modificación
 * Autor Aitor Benítez Estruch
 * Fecha: 18/05/2020
 * Añadida acciones de mostra y ocultar mapa al panel de las parcelas
 *
 */



//------------------------------------
// mostrarMapa()
// callback que esconde o muestra el mapa
//------------------------------------
function onClickBotonMapa() {
    let mapa = document.getElementById("mapa");
    // si esta escondido
    if (mapa.classList.contains("desaparecer")) {
        mostrarMapa()
    } else {
        esconderMapa()
    }
}// ()

//----------------------------------------
// Muestra el mapa y cambia los textos del boton a ocultar
//----------------------------------------
function mostrarMapa() {
    let mapa = document.getElementById("mapa");
    let texto = document.getElementById("textoMapa");
    let imagen = document.getElementById("flecha");

    mapa.classList.add("aparecer")
    mapa.classList.remove("desaparecer")

    // cambiamos la flecha a la de bajar
    imagen.className = "fas fa-angle-up"
    // // lo mostramos
    mapa.style.display = 'block';
    // cambiamos el label del boton a por 'Ocultar Mapa'
    texto.textContent = 'Ocultar Mapa';


}// ()

//----------------------------------------
// Esconde el mapa y cambia los textos del boton a mostrar
//----------------------------------------
function esconderMapa() {
    let mapa = document.getElementById("mapa");
    let texto = document.getElementById("textoMapa");
    let imagen = document.getElementById("flecha");
    mapa.classList.add("desaparecer")
    mapa.classList.remove("aparecer")
    // // escondemos el mapa
    // mapa.style.display = 'none';
    // cambiamos el label del boton a por 'Mostrar Mapa'
    texto.textContent = 'Mostrar Mapa';
    // cambiamos la flecha a la de subir
    imagen.className = "fas fa-angle-down"
}//()


let user =  JSON.parse(localStorage.getItem("user"));
// ponemos el nombre del usuario en el titulo
document.getElementById("titulo").innerText = user['nombre'];
// cogemos el cliente pasado por localstorage desde la lista de clientes
let campo = JSON.parse(localStorage.getItem("listaClienteTecnico-campo"));
VistaListaParcelaTecnico.preparar("lista-parcela","lista-parcela-body","error","carga","btnNextParcela","btnPrevParcela",true,campo);
// le pasamos al controlador de la lista los controladores de crear y modicar de campos
ControladorListaParcelaTecnico.iniciar(campo.id);
initMap()


function initMap() {
    map = new google.maps.Map(document.getElementById('mapa'), {
        center: {lat: 38.9965055, lng: -0.1674364},
        zoom: 15,
        mapTypeId: 'hybrid', /* dibuixa tb carreteres */
        styles: [
            {
                featureType: 'poi', /* Oculta informació d'edificis*/
                stylers: [{visibility: 'off'}]
            },
            {
                featureType: 'transit',/* Oculta informació de parades de tren, autobus...*/
                stylers: [{visibility: 'off'}]
            }
        ],
        mapTypeControl: false, /* Oculta canvi de tipus de mapa*/
        streetViewControl: false,/* Oculta botó de streetview*/
        rotateControl: false, /* */
    });
    map.setTilt(0); /* Forçar a que sempre estiga en la vista de dalt*/
    ModeloListaParcelaTecnico.obtenerDatos()
}


function cargarParcelas(parcelas){
    let cont = 1;
    let total = parcelas.length
    let bounds = new google.maps.LatLngBounds();
    parcelas.forEach(function(parcela){
        console.log(parcela)
        let color = "hsl( " + getRandomColor(cont, total) + ", 100%, 50% )";
        cont ++;
        let polygon = new google.maps.Polygon({ /*DIBUIXAR POÍGONS*/
            paths: parcela.vertices, /* Especifica les línies que especifiquen el polígon*/
            strokeColor: color, /* */
            strokeOpacity: 0.8, /* */
            strokeWeight: 2, /* */
            fillColor: color, /* */
            fillOpacity: 0.35, /* */
            map: map,
            /* */
        });
        polygon.getPath().getArray().forEach(function (v) {
            bounds.extend(v);
        });
        polygon.setMap(map);
    })
    map.fitBounds(bounds)
}

