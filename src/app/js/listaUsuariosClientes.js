/**
 * Autor: Carlos Ramirez Dorado
 * GTI GRUPO 11
 * Fecha: 07/05/2020
 *
 * Modelo vista controlador que gestiona la lista de los usuarios del cliente
 *
 */

var ModeloListaUsuariosCliente = {

    controlador: {},
    user:{},
    idCliente: -1,
    todosLosUsuarios:{},//contendrá todos los clientes siempre
    usuariosFiltrados:{},// contendrá los clientes filtrados cuando se use el buscador o el ordenar

    paginaActual: 1,

    // al principio no estan ordenados
    // true = ASC, false = DESC
    ordenNombre: false,
    ordenApellido: false,
    ordenRol: false,

    obtenerDatos: async function () {

        try{
            this.todosLosUsuarios = await fetch('../../src/api/v1.0/usuario?idCliente='+this.idCliente)
                .then(respuesta =>{
                    if(respuesta.status === 200){
                        return respuesta.json();
                    }else{
                        this.controlador.error = true;
                        throw Error(respuesta.statusText);
                    }
                })


            this.usuariosFiltrados = this.todosLosUsuarios;
            this.controlador.representarDatos(this.usuariosFiltrados,this.paginaActual);


        }catch (e) {
            console.error(e);
            this.controlador.representarError();
        }
    },

    borrarElemento: async function (idCliente) {

        try{

            await fetch('../../src/api/v1.0/usuario', {
                method: 'DELETE', // or 'PUT'
                body: JSON.stringify({id:idCliente}) // data can be `string` or {object}!

            }).then(res => res.json())
                .catch(error => {throw Error(error)})
                .then(response => console.log('Success:', response));



            // si sale bien borramos el elemento en local y nos evitamos el tener que refrescar
            // encontramos el elemento
            let clienteABorrar = this.todosLosUsuarios.find(e=>{
                if(e.id === idCliente){
                    return e;
                }
            });
            if(clienteABorrar!=null){
                // lo borramos del array local
                this.todosLosUsuarios.splice(this.todosLosUsuarios.indexOf(clienteABorrar),1);
            }

            // volvemos a la pagina principal y pintamos de nuevo los datos
            this.paginaActual = 1;
            this.controlador.representarDatos(this.usuariosFiltrados,this.paginaActual);

        }catch (e) {
            console.error(e);
            this.controlador.representarError();
        }
    },

    // filtrar los clientes tanto por nombre correo o telefono
    filtrarPorNombreCorreoRol: function (textoAFiltrar) {
        // si no hay nada escrito poner todos los clientes
        if(textoAFiltrar === ""){
            this.usuariosFiltrados = this.todosLosUsuarios;
        }else{
            this.usuariosFiltrados = this.todosLosUsuarios.filter(usuario =>{
                if(usuario.nombre!=null && usuario.nombre.toLowerCase().includes(textoAFiltrar.toLowerCase())){
                    return usuario;
                }else if(usuario.correo!=null &&  usuario.correo.toLowerCase().includes(textoAFiltrar.toLowerCase())){
                    return usuario;
                }else if(usuario.rol!=null && usuario.rol.toLowerCase().includes(textoAFiltrar.toLowerCase())){
                    return usuario;
                }
            })


        }
        // siempre que filtremos volvamos a la pagina 1
        this.paginaActual = 1;
        this.controlador.representarDatos(this.usuariosFiltrados,this.paginaActual);
    },

    // idOrden -> 1 = Nombre, 2 = Correo, 3 = telefono
    // ascendente -> true = ordenar asc, false = ordenar desc
    ordenarPor:function (idOrden) {
        //ocultamos todas las flechas
        document.getElementById('flecha-nombre-usuario').style.display="none";
        document.getElementById('flecha-apellido-usuario').style.display="none";
        document.getElementById('flecha-rol-usuario').style.display="none";
        let filtro = "";
        let ordenASC = null;

        // determinar el tipo de filtro y el orden ASC o DESC
        if(idOrden===1){
            //aparece la flecha
            document.getElementById('flecha-nombre-usuario').style.display="inline";

            filtro = "nombre"
            // ordenar por nombre
            if(this.ordenNombre == null){
                // la primera vez ordenar ascendentemente
                this.ordenNombre = true;
            }else{
                // cade vez que se pulse de mas cambiarlo
                this.ordenNombre = !this.ordenNombre;
            }
            ordenASC = this.ordenNombre;
        }else{
            this.ordenNombre = false;
        }
        if(idOrden === 2){
            //aparece la flecha
            document.getElementById('flecha-apellido-usuario').style.display="inline";

            filtro = "apellidos";
            // ordenar por correo
            if(this.ordenApellido == null){
                // la primera vez ordenar ascendentemente
                this.ordenApellido = true;
            }else{
                // cade vez que se pulse de mas cambiarlo
                this.ordenApellido = !this.ordenApellido;
            }
            ordenASC = this.ordenApellido;
        }else{
            this.ordenApellido = false;
        }
        if(idOrden === 3){
            //aparece la flecha
            document.getElementById('flecha-rol-usuario').style.display="inline";

            filtro = "rol"
            // ordenar por telefono
            if(this.ordenRol == null){
                // la primera vez ordenar ascendentemente
                this.ordenRol = true;
            }else{
                // cade vez que se pulse de mas cambiarlo
                this.ordenRol = !this.ordenRol;
            }
            ordenASC = this.ordenRol;
        }else{
            this.ordenRol = false;
        }
        // ordenamos siempre desde clientes filtrados
        // al inicio seran todos y si hay algun filtro en el buscador se
        // ordenará sobre este
        this.usuariosFiltrados = this.usuariosFiltrados.sort((usuario1, usuario2) =>{
            // juntar todos los nulls o arriba o abajo
            if(usuario1[filtro] == null){
                return ordenASC ? 1 : -1;
            }else if(usuario2[filtro] == null ){
                return ordenASC ? -1 : 1;
            }else {
                // ordenar
                if (usuario1[filtro].toLowerCase() > usuario2[filtro].toLowerCase()) {
                    return ordenASC ? 1 : -1;
                } else if (usuario1[filtro].toLowerCase() < usuario2[filtro].toLowerCase()) {
                    return ordenASC ? -1 : 1;
                }
            }
            return 0;
        })

        // simepre que ordenemos volvemos a la pagina 1
        this.paginaActual = 1
        this.controlador.cambiarFlechasOrden(this.ordenNombre,this.ordenApellido,this.ordenRol);
        this.controlador.representarDatos(this.usuariosFiltrados,this.paginaActual);
    },

    anteriorPagina:function () {
        this.paginaActual--;
        this.controlador.representarDatos(this.usuariosFiltrados,this.paginaActual);
    },

    siguientePagina: function () {
        this.paginaActual++;
        this.controlador.representarDatos(this.usuariosFiltrados,this.paginaActual);
    }

};

var VistaListaUsuariosCliente = {

    controlador: {},

    bloqueLista:{}, // donde se representarán los elementos de la lista
    paginaActual: 1,// indice para controlar la paginacion
    maxElementosPorPagina: 5,
    bloqueError:{},
    bloqueCarga:{},
    bloqueBodyLista:{},

    // botones para el control de la paginacion de clientes
    btnNextPage:{},
    btnPreviousPage:{},

    // booleano que controla si se tiene que mostrar en primera instancia
    mostrar: false,


    preparar:function (idBloqueLista, idListaBody, idError, idCarga, idBtnNext, idBtnPrev,mostrar) {
        this.bloqueLista = document.getElementById(idBloqueLista);
        this.bloqueBodyLista = document.getElementById(idListaBody);
        this.bloqueError = document.getElementById(idError);
        this.bloqueCarga = document.getElementById(idCarga);
        this.btnPreviousPage = document.getElementById(idBtnPrev);
        this.btnNextPage = document.getElementById(idBtnNext);

        this.mostrar = mostrar;
    },

    cambiarFlechasOrden(ordenNombre, ordenApellido, ordenRol) {
        document.getElementById("flecha-nombre-usuario").className = (!ordenNombre) ? "fas fa-chevron-down" : "fas fa-chevron-up";
        document.getElementById("flecha-apellido-usuario").className = (!ordenApellido) ? "fas fa-chevron-down" : "fas fa-chevron-up";
        document.getElementById("flecha-rol-usuario").className = (!ordenRol) ? "fas fa-chevron-down" : "fas fa-chevron-up";
    },

    // funciones para controlar cuando se muestran los bloques html
    esconderElementos: function(){

        this.bloqueLista.style.display = "none";
        this.bloqueCarga.style.display = "none";
        this.bloqueError.style.display = "none";
    },
    mostrarCarga: function () {
        if(this.mostrar) {
            this.esconderElementos();
            this.bloqueCarga.style.display = "flex"
        }
    },
    mostrarError:function () {
        this.esconderElementos();
        this.bloqueError.style.display = "flex"

    },
    mostrarLista:function () {
        if(this.mostrar) {
            this.esconderElementos();
            if(this.controlador.error){
                this.bloqueError.style.display = "flex"
            }else{
                this.bloqueLista.style.display = "flex"
            }
        }
    },

    // representar elementos en el html
    representarDatos:function (usuarios,paginaActual) {
        //control de paginacion
        // maxPages = length/maxElementosPorPagina redondeado hacia arriba

        let maxPages = Math.ceil(usuarios.length/this.maxElementosPorPagina);
        // si estamos en la pagina 1 empezamos desde el 0 (5*(1-1))
        // si estamos en la pagina 2 empezamos desde el 5 (5*(2-1))
        let elementoInicial = this.maxElementosPorPagina * (paginaActual-1);

        this.prepararBotonesPaginador(paginaActual,maxPages);

        this.bloqueBodyLista.innerHTML = "";

        // pintar elemntos acorde al maxElementosPorPagina
        // los elementos iran desde (maxElementosPorPagina* (paginaActual-1)) hasta this.maxElementosPorPagina*paginaActual
        for(let i = elementoInicial;i<(this.maxElementosPorPagina*paginaActual);i++){
            // si en una pagina no llega al maxElementosPorPagina hay que evitar que intente pintar esos
            // elementos y no de error
            if(i<usuarios.length){
                let hilera = document.createElement("tr");

                let celdaNombre = document.createElement("td");
                let celdaApellido = document.createElement("td");
                let celdaRol = document.createElement("td");
                let celdaOperacion = document.createElement("td");
                celdaOperacion.className = "list-opereciones";

                let textoCeldaNombre = document.createTextNode(usuarios[i].nombre != null ? usuarios[i].nombre : "Sin asignar");
                let textoCeldaApellido = document.createTextNode(usuarios[i].apellidos != null ? usuarios[i].apellidos : "Sin asignar");
                let textoCeldaRol = document.createTextNode(usuarios[i].rol != null
                    ? usuarios[i].rol.charAt(0).toUpperCase() + usuarios[i].rol.slice(1)
                    : "Sin asignar");


                celdaNombre.appendChild(textoCeldaNombre);
                celdaApellido.appendChild(textoCeldaApellido);
                celdaRol.appendChild(textoCeldaRol);




                this.createDropDown(celdaOperacion,usuarios[i]);







                let ul = document.createElement("ul");
                ul.className = "listaOperaciones";

                if(usuarios[i].rolId === 3){
                    let li = document.createElement("li")
                    // añadimos los botones a todas las listas
                    let btnRead = document.createElement("button");
                    btnRead.className = "btn-list-element";
                    btnRead.onclick = () =>{this.controlador.onClickElementoLista(usuarios[i])};
                    btnRead.innerHTML = `<i class="far fa-eye"></i><p class="label">Ver detalles</p>`
                    li.appendChild(btnRead);
                    ul.appendChild(li)
                }

                let li2 = document.createElement("li")
                let btnEdit = document.createElement("button");
                btnEdit.className = "btn-list-element";
                btnEdit.onclick = () =>{this.controlador.onEditElementoLista(usuarios[i])};
                btnEdit.innerHTML = `<i class="fas fa-pen-square"></i><p class="label">Modificar</p>`
                li2.appendChild(btnEdit);
                ul.appendChild(li2)

                let li3 = document.createElement("li")
                let btnRemove = document.createElement("button");
                btnRemove.className = "btn-list-element";
                btnRemove.onclick = () =>{this.controlador.onRemoveElementoLista(usuarios[i])};
                btnRemove.innerHTML = `<i class="far fa-trash-alt"></i><p class="label">Eliminar</p>`
                li3.appendChild(btnRemove);
                ul.appendChild(li3)

                // añadimos la lista que se mostrará a partir de tablet
                celdaOperacion.appendChild(ul)

                // añadimos los elementos a la fila
                hilera.appendChild(celdaNombre);
                hilera.appendChild(celdaApellido);
                hilera.appendChild(celdaRol);
                hilera.appendChild(celdaOperacion)


                // añadimos la fila al tbody
                this.bloqueBodyLista.appendChild(hilera);
            }


        }


    },

    createDropDown:function(celdaOperacion,usuario){




        let btnEditD = document.createElement("button");
        btnEditD.className = "btn-list-element";
        btnEditD.onclick = () =>{this.controlador.onEditElementoLista(usuario)};
        let btnRemoveD = document.createElement("button");
        btnRemoveD.className = "btn-list-element";
        btnRemoveD.onclick = () =>{this.controlador.onRemoveElementoLista(usuario)};

        let li2Drop = document.createElement("div")
        btnEditD.innerHTML = `<i class="fas fa-pen-square"></i><p>Modificar</p>`
        li2Drop.appendChild(btnEditD);
        let li3Drop = document.createElement("div")
        btnRemoveD.innerHTML = `<i class="far fa-trash-alt"></i><p>Eliminar</p>`
        li3Drop.appendChild(btnRemoveD);
        // añadimos el dropdown button que se mostrará en movil
        celdaOperacion.innerHTML = `<button class="btn btn-secondary dropdown-toggle tresPuntos"
                          type="button" id="dropdownMenu1" data-toggle="dropdown"
                          aria-haspopup="true" aria-expanded="false">
                   <i class="fas fa-ellipsis-v tresPuntos">
                  </button>
                  <div class="dropdown-menu" aria-labelledby="dropdownMenu1">
                    
                  </div>`


        if(usuario.rolId === 3){
            // a los admin no ponerle el ver datalles solo a los agricultores
            let btnReadD = document.createElement("button");
            btnReadD.className = "btn-list-element";
            btnReadD.onclick = () =>{this.controlador.onClickElementoLista(usuario)};

            let liDrop = document.createElement("div")
            btnReadD.innerHTML = `<i class="far fa-eye"></i><p>Ver detalles</p>`
            liDrop.appendChild(btnReadD);

            celdaOperacion.querySelector("div.dropdown-menu").append(liDrop)
        }

        celdaOperacion.querySelector("div.dropdown-menu").append(li2Drop)
        celdaOperacion.querySelector("div.dropdown-menu").append(li3Drop)
    },

    // habilitar y deshabilitar los botones que controlan la paginacion
    // para evitar errores
    prepararBotonesPaginador : function (paginaActual, maxPages) {
        this.btnNextPage.removeAttribute("disabled");
        this.btnPreviousPage.removeAttribute("disabled");
        document.getElementById("paginadorListaUsuario").innerHTML = "Página "+paginaActual + " de " + maxPages;

        // si el maxPages es mayor que uno habilitar el next page
        if(maxPages>1){
            this.btnNextPage.removeAttribute("disabled");
        }
        // si el current page es igual al maxPages deshabilitar el next page
        if(paginaActual === maxPages){
            this.btnNextPage.setAttribute("disabled","disabled");
        }
        if(maxPages === 0){
            this.btnNextPage.setAttribute("disabled","disabled");
        }
        // si el current page es mayor que uno habilitar el previous page
        if(paginaActual > 1){
            this.btnPreviousPage.removeAttribute("disabled");
        }else{
            this.btnPreviousPage.setAttribute("disabled","disabled");
        }

    }

};

var ControladorListaUsuariosCliente = {
    modelo: ModeloListaUsuariosCliente,
    vista: VistaListaUsuariosCliente,
    controladorCreateUsuario: null,
    controladorModificarUsuario: null,
    controladorUpdatePassUsuario: null,
    error: false,// controla si ha habido un error en la obtencio de datos

    iniciar: function (usuario,idCliente,controladorCreateUsuario,controladorModificarUsuario,controladorUpdatePassUsuario) {
        this.modelo.controlador = this;
        this.vista.controlador = this;
        this.modelo.user = usuario;
        this.modelo.idCliente = idCliente;
        this.vista.mostrarCarga();
        this.modelo.obtenerDatos();
        this.controladorCreateUsuario = controladorCreateUsuario;
        this.controladorModificarUsuario = controladorModificarUsuario;
        this.controladorUpdatePassUsuario = controladorUpdatePassUsuario;

    },

    cambiarFlechasOrden(ordenNombre, ordenApellido, ordenRol) {
        this.vista.cambiarFlechasOrden(ordenNombre,ordenApellido,ordenRol)
    },

    representarDatos: function(datos,paginaActual){
        this.vista.representarDatos(datos,paginaActual);
        this.vista.mostrarLista();
    },
    mostrarTabla: function(){
        this.vista.mostrarLista();
    },

    representarError: function () {
        this.vista.mostrarError();
    },

    // eventos on click de los elementos de la lista
    // el on click y el on edit redirecciona a la siguiente pagina con un flag de editar
    // el on delete confirma la operacion y lanza la peticion al server en caso afirmativo
    onClickElementoLista:   function (elemento) {
        elemento["modificar"] = false;
        elemento["cliente"] = this.modelo.idCliente;
        localStorage.setItem("listaUsuarioCliente-usuario",JSON.stringify(elemento));
        location.href = "panel-admin-detalles-usuario.html";
    },
    onEditElementoLista: function (elemento) {
        this.controladorModificarUsuario.iniciar(elemento,this.modelo.idCliente);
        this.controladorUpdatePassUsuario.vista.usuario = elemento;
    },

    onRemoveElementoLista: function (elemento) {
        if(elemento.id !== this.modelo.user.id){
            // confimar borrado
            if(confirm("Se borrará para siempre el usuario "+elemento.nombre+"\n¿Estás seguro?")){
                this.vista.mostrarCarga();
                this.modelo.borrarElemento(elemento.id);
            }
        }else{
            alert("No puedes borrarte a tí mismo")
        }


    },

    onCreateUsuario: function () {
        this.controladorCreateUsuario.iniciar(this.modelo.idCliente)
    },

}