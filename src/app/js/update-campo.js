/**
 * Autor: Ruben Pardo
 * Fecha: 08/05/2020
 * Modelo Vista Controlador Modificar un campo
 *
 */

var ModeloModificarCampo = {
    controlador: {},

    crearUsuario: async function (usuario) {

        try{
            let form = new FormData();
            form.append("modificar","true");
            form.append("data",JSON.stringify(usuario));

            await fetch('../../src/api/v1.0/campo',{
                method: 'POST',
                body: form,
            })
                .then(respuesta => {
                    if (respuesta.status === 200) {
                        return respuesta.json();
                    } else {
                        this.controlador.errorUpdate();
                        throw Error(respuesta.statusText);

                    }
                })


            this.controlador.crearUsuarioOk();


        }catch (e) {
            console.error(e);
        }
    },
};

var VistaModifcarCampo = {

    controlador: {},
    formulario:{},
    modal:{},
    idCliente: -1,
    idCampo: -1,


    preparar:function (idError, idCarga,idFormulario,idModal,idCloseModal) {
        this.bloqueError = document.getElementById(idError);
        this.bloqueCarga = document.getElementById(idCarga);
        this.formulario = document.getElementById(idFormulario);
        this.modal = document.getElementById(idModal);

    },
    mostrarModal: function(){
        this.bloqueError.style.display = "none";
        this.modal.style.display = "block";
    },

    esconderModal: function(){
        this.modal.style.display = "none";
        this.formulario.nombre.value =  "";
        this.formulario.descripcion.value =  "";
    },

    mostrarError:function () {
        this.bloqueError.style.display = "block"

    },

    // funciones para el controlar del formulario
    prepararFormulario: function(campo){
        // cogemos todos los inputs del formulario y le asignamos los valores del cliente
        this.formulario.nombre.value =  campo.nombre == null ? "" : campo.nombre;
        this.formulario.descripcion.value =  campo.descripcion == null ? "" : campo.descripcion;

    },


    getUsuarioFormulario: function(){
        let campo = {};

        // cogemos todos los inputs del formulario y le asignamos los valores del cliente
        campo["id"] = this.idCampo;
        campo["cliente"] = this.idCliente;
        campo["nombre"] = this.formulario.nombre.value;
        campo["descripcion"] = this.formulario.descripcion.value;

        return campo;
    },

}

var ControladorModificarCampo = {
    modelo: ModeloModificarCampo,
    vista: VistaModifcarCampo,

    error: false,// controla si ha habido un error en la obtencio de datos

    iniciar: function (idCliente,campo) {
        this.modelo.controlador = this;
        this.vista.idCampo = campo.id;
        this.vista.idCliente = idCliente;
        this.vista.controlador = this;
        this.vista.prepararFormulario(campo);
        this.vista.mostrarModal();
    },

    representarError: function () {
        this.vista.mostrarError();
    },

    // eventos del on sumbit y on reset del formulario de los datos
    onSumbitFormulario: function(e){
        e.preventDefault();
        this.modelo.crearUsuario(this.vista.getUsuarioFormulario())


    },
    onResetFormulario: function(e){
        e.preventDefault();
        this.vista.esconderModal();
    },

    errorUpdate: function(){
        this.vista.mostrarError();
    },
    crearUsuarioOk: function(){
        // llamar al callback de la lista para actualizarlo
        this.vista.esconderModal();
        window.location.reload();
    },
}
