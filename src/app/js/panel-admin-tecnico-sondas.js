/**
 * Autor Marc Castelló Martí
 * Logica de carga de los MVC de panel-admin-tecnico-sondas.html
 *
 * Modificación
 * Autor Aitor Benítez Estruch
 * Fecha: 18/05/2020
 * Añadida acciones de mostra y ocultar mapa al panel de las sondas
 */



//------------------------------------
// mostrarMapa()
// callback que esconde o muestra el mapa
//------------------------------------
function onClickBotonMapa() {
    let mapa = document.getElementById("mapa");
    // si esta escondido
    if (mapa.classList.contains("desaparecer")) {
        mostrarMapa()
    } else {
        esconderMapa()
    }
}// ()

//----------------------------------------
// Muestra el mapa y cambia los textos del boton a ocultar
//----------------------------------------
function mostrarMapa() {
    let mapa = document.getElementById("mapa");
    let texto = document.getElementById("textoMapa");
    let imagen = document.getElementById("flecha");

    mapa.classList.add("aparecer")
    mapa.classList.remove("desaparecer")

    // cambiamos la flecha a la de bajar
    imagen.className = "fas fa-angle-up"
    // // lo mostramos
    mapa.style.display = 'block';
    // cambiamos el label del boton a por 'Ocultar Mapa'
    texto.textContent = 'Ocultar Mapa';


}// ()

//----------------------------------------
// Esconde el mapa y cambia los textos del boton a mostrar
//----------------------------------------
function esconderMapa() {
    let mapa = document.getElementById("mapa");
    let texto = document.getElementById("textoMapa");
    let imagen = document.getElementById("flecha");
    mapa.classList.add("desaparecer")
    mapa.classList.remove("aparecer")
    // // escondemos el mapa
    // mapa.style.display = 'none';
    // cambiamos el label del boton a por 'Mostrar Mapa'
    texto.textContent = 'Mostrar Mapa';
    // cambiamos la flecha a la de subir
    imagen.className = "fas fa-angle-down"
}//()


let user =  JSON.parse(localStorage.getItem("user"));
// ponemos el nombre del usuario en el titulo
document.getElementById("titulo").innerText = user['nombre'];

function iniciar() {

    // cogemos el cliente pasado por localstorage desde la lista de clientes
    let parcela = JSON.parse(localStorage.getItem("listaParcela-parcela"));

    VistaModificarSonda.preparar("modal-error-sonda","carga","formularioModalSondas","modal-editar-sonda","close-editar-sonda");


    VistaMapaTecnico.preparar("mapa")

    VistaListaSondas.preparar("lista-sonda","lista-sonda-body","error","carga","btnNextSonda","btnPrevSonda",true,parcela,"mapa");
    // le pasamos al controlador de la lista los controladores de crear y modicar de campos
    ControladorListaSondas.iniciar(parcela,
        ControladorModificarSonda,
        ControladorMapaTecnico,
    );
}