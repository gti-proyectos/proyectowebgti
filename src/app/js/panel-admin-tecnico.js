/**
 * Autor Ruben
 * Logica de carga de los MVC de panel-admin-tecnico.html
 */

VistaListaClientesTecnico.preparar("lista-clientes","lista-clientes-body","error","carga","btnNextClientes","btnPrevClientes",true);
ControladorListaClientesTecnico.iniciar();

let user =  JSON.parse(localStorage.getItem("user"));
// ponemos el nombre del usuario en el titulo
document.getElementById("titulo").innerText = user['nombre'];