/**
 * Autor: Raul
 * Fecha: 17/03/2020
 * Funcionalidad js de la peticion get-campo.js
 */

var ModeloCampos = {
    controlador: {},
    datosUser: {},
    todosLosCampos:{},
    camposFiltrados:{},
    filtrarCampos: function (textoAFiltrar) {
        if(textoAFiltrar === ""){
            this.camposFiltrados = this.todosLosCampos;
        }else{
            this.camposFiltrados = this.todosLosCampos.filter(campos =>{
                if(campos.nombre.toLowerCase().includes(textoAFiltrar.toLowerCase())){
                    return campos;
                }
                else if(campos.descripcion!=null && campos.descripcion.toLowerCase().includes(textoAFiltrar.toLowerCase())){
                    return campos;
                }
            })
        }

        this.controlador.vista.mostrarCampos(this.camposFiltrados, this.datosUser);

    },
    obtenerCampos: function () {

        fetch('../../src/api/v1.0/sesion').then((respuesta) => {
            if (respuesta.status !== 200) {
                location.href = "login.html"//vuelve a app, se redireccionará al login
            } else {
                return respuesta.json();
            }
        }).then((json) => {
            //guardamos los datos del user
            this.datosUser = json;
            //despues de obtener los datos del user, cogemos sus campos
            fetch('../../src/api/v1.0/campo?id=' + json['id']).then(function (respuesta) {
                return respuesta;
            }).then((res) => {
                if (res.status !== 200) {
                    // ha habido un error mostrar error:
                    this.controlador.representarError();
                } else {
                    // han llegado los datos, devolverlos en formato json
                    return res.json();
                }
            }).then((json) => {
                // no tiene campos
                if (json.length === 0) {
                    this.controlador.vista.mostrarError();
                } else {
                   this.camposFiltrados=json;
                   this.todosLosCampos=json;
                    this.controlador.vista.mostrarCampos(this.camposFiltrados, this.datosUser);
                }


            })
        })

    }
};

var VistaCampos = {

    bloqueError: {},
    bloqueCarga: {},
    contenedorCampos: {},
    contenedorCamposItems: {},
    contenedorNoCampos: {},
    bloqueTitulo: {},

    //Recoje los valores contenidos en los inputs de Usuario y contraseña, y del <div> que mostrará el error (en caso de haberlo)
    preparar: function (idTitulo, idBloqueError, idContenedorCampos, idBloqueItems, idBloqueCarga, idBloqueNoCampos) {

        this.bloqueTitulo = document.getElementById(idTitulo);


        this.bloqueError = document.getElementById(idBloqueError);
        this.bloqueCarga = document.getElementById(idBloqueCarga);
        this.contenedorCamposItems = document.getElementById(idBloqueItems);
        this.contenedorCampos = document.getElementById(idContenedorCampos);
        this.contenedorNoCampos = document.getElementById(idBloqueNoCampos);
    },
    esconderElementos: function () {
        this.bloqueError.style.display = "none";
        this.bloqueCarga.style.display = "none";
        this.contenedorCampos.style.display = "none";
        this.contenedorNoCampos.style.display = "none";
        this.contenedorCamposItems.style.display = "none";
    },

    //Muestra el <div> donde se muestra el mensaje de error.
    mostrarError: function () {
        this.esconderElementos();
        this.bloqueError.style.display = "flex"
    },
    //Muestra el <div> donde se muestra el mensaje de error.
    mostrarCarga: function () {
        this.esconderElementos();
        this.bloqueCarga.style.display = "flex"
    },
    mostrarCampos: function (campos, user) {
        this.esconderElementos();
        // PONEMOS EL NOMBRE DEL USUARIO
        this.bloqueTitulo.innerHTML = "Bienvenido, " ;
        this.bloqueTitulo.innerHTML += user.nombre;

        console.log(campos);
        /** Mostramos los bloques de contenido **/
        this.contenedorCampos.style.display = "block";
        this.contenedorCamposItems.style.display = "flex";
        this.contenedorCamposItems.innerHTML = "";

        let contadorParaLasImagenes = 1;//maximo 3

        campos.forEach((campo) => {
            let urlImagen = 'img/campo' + (contadorParaLasImagenes++) + '.jpg';

            this.contenedorCamposItems.innerHTML += `<div class="card contenido-item col-1 col-xl-3 p-0">
                    <div class="card__img"></div>
                    <a href="detalles-campo.html?uc=0&id=${campo.id}" class="card_link">
                        <div class="card__img--hover" style="background-image: url(${urlImagen})"></div>
                    </a>
                    <div class="card__info">
                        <h3 class="card__title">${campo.nombre}</h3>
                        <span class="card__desc">${campo.descripcion}</a></span>
                    </div>
                </div>`;
            if (contadorParaLasImagenes > 3) contadorParaLasImagenes = 1;


        })


    },
    mostrarNoCampos: function () {
        this.esconderElementos();
        this.contenedorNoCampos.style.display = "flex";
    }
};


var ControladorCampos = {
    modelo: ModeloCampos,
    vista: VistaCampos,
    iniciar: function () {
        this.modelo.controlador = this;
        //Asegurar que el <div> de error no se está mostrando.
        this.vista.esconderElementos();
        this.vista.mostrarCarga();
        this.modelo.obtenerCampos();
    },

    representarError: function () {
        this.vista.mostrarError();
    }
};