/**
 * Autor: Ruben Pardo
 * Fecha: 09/05/2020
 * Modelo Vista Controlador Crear o modificar una parcela
 *
 */

var ModeloCrearParcela = {
    controlador: {},

    postParcela: async function (parcela) {

        try{
            let form = new FormData();
            form.append("modificar",this.controlador.vista.parcela.modificar);
            form.append("data",JSON.stringify(parcela));


            await fetch('../../src/api/v1.0/parcela',{
                method: 'POST',
                body: form,
            })
                .then(respuesta => {
                    if (respuesta.status === 200) {
                        return respuesta.json();
                    } else {
                        this.controlador.representarError();
                        throw Error(respuesta.statusText)
                    }
                })


            this.controlador.crearUsuarioOk();


        }catch (e) {
            console.error(e);
        }
    },
};

var VistaCrearParcela = {

    bloqueError: {},
    bloqueOk: {},
    bloqueCarga:{},
    controlador: {},
    modal:{},
    idCliente: -1,
    idUsuario: -1,
    parcela: {},

    preparar:function (idError, idCarga,idFormulario,idBloqueOk,parcela) {
        this.bloqueError = document.getElementById(idError);
        this.bloqueCarga = document.getElementById(idCarga);
        this.bloqueOk = document.getElementById(idBloqueOk);
        this.formulario = document.getElementById(idFormulario);
        this.parcela = parcela;
        if(this.parcela.modificar){
            this.prepararFormulario();
        }
        this.esconderError();
        this.esconderOk();
    },

    prepararFormulario:function(){

        // añadir inputs sabiendo que hay tres puestos por defecto
        let wrapper = this.formulario.querySelector(".form-element");
        let i = 4;
        if(parcela.vertices.length > 3){
            wrapper.innerHTML += (`<div>
                <p class="vertice-cont">${i++}</p>
                <div class="pair">
                    <label>Latitud</label>
                    <input type="number" name="lat" value="" placeholder="Latitud" required/>
                </div>
                <div class="pair">
                    <label>Longitud</label>
                    <input type="number" name="lng" value=""placeholder="Longitud"  required/>
                </div>
                <a href="javascript:void(0);" class="remove_button" title="Remove field">Eliminar</a>
                </div>`)
        }

        // rellenar los inputs con las latitudes y longitudes
        let arrayLat = document.querySelectorAll("input[name='lat']");
        let arrayLng = document.querySelectorAll("input[name='lng']");
        for(let i=0;i<arrayLat.length;i++){
            arrayLat[i].value =  this.parcela.vertices[i].lat;
            arrayLng[i].value= this.parcela.vertices[i].lng;
        }

        this.formulario.nombre.value = this.parcela.nombre;
        this.formulario.descripcion.value = this.parcela.descripcion;
    },

    mostrarError:function () {
        this.bloqueError.style.display = "block"

    },
    esconderError: function(){
      this.bloqueError.style.display = "none";
    },

    mostrarOk:function () {
        this.esconderError();
        this.bloqueOk.style.display = "block"
    },
    esconderOk:function () {
        this.bloqueOk.style.display = "none"
    },

    checkForm: function(){
        let valid = true;
        let arrayLat = document.querySelectorAll("input[name='lat']");
        let arrayLng = document.querySelectorAll("input[name='lng']");

        arrayLat.forEach(element =>{
            if(element.value.trim().length===0){
                valid = false;
            }
        });

        arrayLng.forEach(element =>{
            if(element.value.trim().length===0){
                valid = false;
            }
        });

        if(this.formulario.nombre.value.trim().length === 0){
            valid = false;
        }

        return valid
    },

    getData: function(){
        let data = {vertices:[]};
        let i = 0;
        let arrayLat = document.querySelectorAll("input[name='lat']");
        let arrayLng = document.querySelectorAll("input[name='lng']");
        for(i=0;i<arrayLat.length;i++){
            data.vertices.push({lat:arrayLat[i].value,lng:arrayLng[i].value})
        }

        data['nombre'] = this.formulario.nombre.value;
        data['descripcion'] = this.formulario.descripcion.value;
        data['campo'] = this.parcela.campo;
        data['id'] = this.parcela.id;
        return data;
    },



}

var ControladorCrearParcela = {
    modelo: ModeloCrearParcela,
    vista: VistaCrearParcela,

    error: false,// controla si ha habido un error en la obtencio de datos

    iniciar: function () {
        this.modelo.controlador = this;
        this.vista.controlador = this;
    },

    representarError: function () {
        this.vista.mostrarError();
    },

    enviar(){

        if(this.vista.checkForm()){
            this.modelo.postParcela(this.vista.getData())
        }else{
            this.representarError();
        }
    },
    mostrarOk:function(){
      this.vista.mostrarOk()
    },

    crearUsuarioOk: function(){
        this.mostrarOk()
        setTimeout(function () {
            window.location.href = "panel-admin-tecnico-parcelas.html";
        },1500)
    },
}

// control de añadir campos al formulario
$(document).ready(function(){
    var x = 3; //Initial field counter is 1
    var addButton = $('.add_button'); //Add button selector
    var wrapper = $('.form-element'); //Input field wrapper
    var fieldHTML = ``; //New input field html

    $(addButton).click(function(){ //Once add button is clicked
        x = document.getElementsByClassName("vertice-cont").length+1;
        $(wrapper).append(`<div>
                    <p class="vertice-cont">${x}</p>
                    <div class="pair">
                        <label>Latitud</label>
                        <input type="number" name="lat" value="" placeholder="Latitud" required/>
                    </div>
                    <div class="pair">
                        <label>Longitud</label>
                        <input type="number" name="lng" value="" placeholder="Longitud" required/>
                    </div>
                    <a href="javascript:void(0);" class="remove_button" title="Remove field">Eliminar Vertice</a>
                </div>`); // Add field html

    });
    $(wrapper).on('click', '.remove_button', function(e){ //Once remove button is clicked
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
        // repintamos los indices de los elementos
        let i = 1;
        for(const it of  document.getElementsByClassName("vertice-cont")){
            it.innerHTML = i++;
        }

    });
});