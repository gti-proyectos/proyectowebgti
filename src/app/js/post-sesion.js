//Función que se ejecuta en el onsubmit del formulario en login.html
function enviar(evento) {
    //impedir que se ejecute la accion que por defecto tiene preparada en este caso es enviar el formulario y recargar la pagina
    evento.preventDefault();
    ControladorLogin.iniciar()
};

var ModeloLogin = {
    controlador: {},
    login: function () {
        //Añadir los datos de usuario y contraseña
        let formulario = new FormData();
        formulario.append(VistaLogin.usuario.name, VistaLogin.usuario.value);
        formulario.append(VistaLogin.contrasenya.name, SHA1(VistaLogin.contrasenya.value));

        //Petición post-sesion. Si devuelve ok (200), redirige a la aplicación.
        fetch('../../src/api/v1.0/sesion', {
            method: 'POST',
            body: formulario,
        }).then((res) => {
            if (res.status !== 200) {
                this.controlador.representarError();
            } else {

                // hacemos un get sesion del usuario
                fetch('../../src/api/v1.0/sesion').then((respuesta) => {
                    if (respuesta.status !== 200) {
                        location.href = "login.html"//vuelve a app, se redireccionará al login
                    } else {
                        return respuesta.json();
                    }
                }).then((json) => {
                    let cliente = json['cliente'];
                    let rol = json['rol']['id'];
                    // saber cuantos campos tiene asignados
                    let idCampo = json['idCampo'];
                    window.localStorage.setItem("user",JSON.stringify(json));

                    if(rol == 1){
                        // es admin
                        if(cliente==null){
                            // admin global
                            location.href = "panel-admin-general.html"
                        }else{
                            // admin cliente
                            location.href = "panel-admin-cliente.html?cliente="+json['id']
                        }
                    }else if(rol == 2){
                        // es tecnico
                        location.href = "panel-admin-tecnico.html"
                    }else{
                        // es agricultor
                        if(idCampo == null){
                            location.href = "panel-usuario-campo.html"
                        }
                        else{
                            location.href = "detalles-campo.html?uc=1&id="+json['idCampo']
                        }
                    }
                })

            }
        })
    }
};

var VistaLogin = {
    usuario: {},
    contrasenya: {},
    bloqueError: {},

    //Recoje los valores contenidos en los inputs de Usuario y contraseña, y del <div> que mostrará el error (en caso de haberlo)
    preparar: function (idBloque, idUser, idPass) {
        document.getElementById("carga-login").style.display = "none";
        this.bloqueError = document.getElementById(idBloque);
        this.usuario = document.getElementById(idUser);
        this.contrasenya = document.getElementById(idPass);
    },
    //"Quita" el div donde se muestra el mensaje de error al recargar.
    quitarErrores: function () {
        this.bloqueError.style.display = "none"
    },
    //Muestra el <div> donde se muestra el mensaje de error.
    representar: function () {
        this.bloqueError.style.display = "block"
        document.getElementById("carga-login").style.display = "none";
        document.getElementById("boton-login").style.cursor = "pointer";
        document.getElementById("letras-login").style.display = "block";
    },

    mostrarCarga: function () {
        document.getElementById("boton-login").style.cursor = "wait";
        document.getElementById("carga-login").style.display = "block";
        document.getElementById("letras-login").style.display = "none";
    }
};

var ControladorLogin = {
    modelo: ModeloLogin,
    vista: VistaLogin,
    iniciar: function () {
        this.modelo.controlador = this;
        //Asegurar que el <div> de error no se está mostrando.
        this.vista.mostrarCarga();
        this.vista.quitarErrores();
        this.modelo.login();
    },
    representarError: function () {
        this.vista.representar();
    }
}