/**
 Autor: Rubén Pardo
 Fecha: 26/05/2020
 Descripcion: MVC de modificar temporada

 */
var ModeloModificarTemporada = {
    controlador: {},
    updateTemporada: async function (temporada) {

        try{
            let form = new FormData();
            form.append("modificar","true");
            form.append("data",JSON.stringify(temporada));

            await fetch('../../src/api/v1.0/temporada',{
                method: 'POST',
                body: form,
            })
                .then(respuesta => {
                    if (respuesta.status !== 200) {
                        throw Error(respuesta.statusText);
                    } else {

                    }
                })


            this.controlador.updateTemporadaOk();


        }catch (e) {
            this.controlador.errorUpdate();
            console.error(e);
        }
    }
}
var VistaModificarTemporada = {
    btnEditar: {},
    idBotonEditar:{},
    idBotonGuardar:{},
    idBotonCancelar:{},
    bloqueBotonesCancelarGuardar: {},
    bloqueErrorTitulo: {},
    tempporada: {},
    textAreaTitulo: {},
    h2Titulo: {},
    textAreaDesc: {},
    textoDesc: {},
    bloqueError: {},


    preparar:function (idBotonEditar,idBotonGuardar,idBotonCancelar,idBloqueCancelarGuardar,
                       idErrorTitulo,temporada,idTextAreaTitulo,idH2Titulo,
                       idTextAreaDescripcion,idParrafoDesc,idError) {
        this.btnEditar = document.getElementById(idBotonEditar);
        this.idBotonEditar = idBotonEditar;
        this.idBotonCancelar = idBotonCancelar;
        this.idBotonGuardar = idBotonGuardar;
        this.bloqueBotonesCancelarGuardar = document.getElementById(idBloqueCancelarGuardar);
        this.bloqueErrorTitulo = document.getElementById(idErrorTitulo);
        this.textAreaTitulo = document.getElementById(idTextAreaTitulo);
        this.h2Titulo = document.getElementById(idH2Titulo);
        this.textAreaDesc = document.getElementById(idTextAreaDescripcion);
        this.textoDesc = document.getElementById(idParrafoDesc);
        this.bloqueError = document.getElementById(idError);
        this.temporada = temporada;

    },


    mostrarError:function () {
        this.bloqueError.style.display = "block"
    },

    mostrarErrorValidacion:function () {
        this.bloqueErrorTitulo.style.display = "block"
    },

    // -> T/F
    // comprueba que se cumplen los campos obligatorios
    // nombre
    checkFormulario: function(){
       return this.textAreaTitulo.value.trim().length >= 0;

    },

    getTemporadaFormulario: function(){
        let obj = {};

        // cogemos todos los inputs del formulario y le asignamos los valores del cliente
        obj["id"] = temporada.id;
        obj["nombre"] = this.textAreaTitulo.value.trim();
        obj["descripcion"] = this.textAreaDesc.value.trim().length > 0 ? this.textAreaDesc.value : null;

        return obj;
    },

    // T/F -> ()
    // si es true mostrar boton editar
    // si es false mostrar guardar y cancelar
    // siempre esonder los otros
    mostrarBotones:function(mostrarEditar) {
        if(mostrarEditar){
            //se ha pulsado editar
            this.btnEditar.style.display = "block";
            this.bloqueBotonesCancelarGuardar.style.display = "none";
        }else{
            //se ha pulsado editar
            this.btnEditar.style.display = "none";
            this.bloqueBotonesCancelarGuardar.style.display = "flex";
        }
    },// ()

    // T/F -> ()
    // si es true mostrar textArea
    // si es false mostrar el h2 y el p
    // siempre esonder los otros
    mostrarEditText:function(mostrar) {
        if(mostrar){
            this.h2Titulo.style.display = "none";
            this.textoDesc.style.display = "none";

            this.textAreaTitulo.style.display = "block";
            this.textAreaDesc.style.display = "block";
        }else{
            this.h2Titulo.style.display = "block";
            this.textoDesc.style.display = "block";

            this.textAreaTitulo.style.display = "none";
            this.textAreaDesc.style.display = "none";
        }
    },// ()

    // modifica el objeto temporada y pone los valores en el h2 y el p
    modficarTemporada() {
        this.temporada.nombre = this.textAreaTitulo.value.trim();
        this.temporada.descripcion = this.textAreaDesc.value.trim();
        this.h2Titulo.innerHTML = this.textAreaTitulo.value.trim();
        this.textoDesc.innerHTML = this.textAreaDesc.value.trim() !== "" ? this.textAreaDesc.value.trim() : "No hay descripción";

        this.mostrarEditText(false);
        this.mostrarBotones(true)
    },

    // () -> T/F
    // true si se ha modificado algo
    hayCambios: function ()  {
        let descripcionIgual = false;
        if(this.temporada.descripcion == null && this.textAreaDesc.value.trim().length ===0){
            descripcionIgual = true;
        }else if(this.temporada.descripcion === this.textAreaDesc.value.trim()){
            descripcionIgual = true;

        }

        return ((this.temporada.nombre !== this.textAreaTitulo.value.trim()) || !descripcionIgual)
    }
}
var ControladorModificarTemporada = {
    modelo: ModeloModificarTemporada,
    vista: VistaModificarTemporada,

    error: false,// controla si ha habido un error en la obtencio de datos

    iniciar:function(){
      this.modelo.controlador = this;
      this.vista.controlador = this;
    },

    representarError: function () {
        this.vista.mostrarError();
    },

    // eventos del on sumbit y on reset del formulario de los datos
    onSumbitFormulario: function(e){
        e.preventDefault();
        if(this.vista.hayCambios()){
            if(this.vista.checkFormulario()){
                this.modelo.crearTemporada(this.vista.getTemporadaFormulario())
            }else{
                this.vista.mostrarErrorValidacion();
            }
        }else{
            // si no se ha modificado nada
            this.vista.modficarTemporada();
        }

    },

    //============================================
    // callbacks de editar temporada,
    //============================================
    // este callback lo tendrán el editar
    // guardar y cancelar de detalles de la temporada
    // -> si es editar, mostrar los botones de cancelar y guardar
    // y mostrar los edit texts
    // -> si es cancelar esconder los edit text y setear los valores
    // anteriores
    // -> si es guardar lanzar peticion a la BD
    onClickModificarTemporada:function(botonPulsado) {
        if (botonPulsado.id === this.vista.idBotonEditar) {

            // editar pulsado

            this.vista.mostrarBotones(false);

            // esconder el h2 y p y mostrar los area text
            this.vista.mostrarEditText(true)

            this.vista.textAreaTitulo.innerText = temporada.nombre;
            this.vista.textAreaDesc.innerText = temporada.descripcion;


        }
        else if (botonPulsado.id === this.vista.idBotonGuardar) {
            // se ha pulsado guardar
            if(this.vista.hayCambios()){
                if(this.vista.checkFormulario()){
                    this.modelo.updateTemporada(this.vista.getTemporadaFormulario())
                }else{
                    this.vista.mostrarError()
                }
            }else{
                this.vista.modficarTemporada();
            }

        }
        else if (botonPulsado.id === this.vista.idBotonCancelar) {
            // se ha pulsado cancelar
            this.vista.mostrarBotones(true);

            // esconder el h2 y p y mostrar los area text
            this.vista.mostrarEditText(false)

            // limpiar los text area
            this.vista.textAreaTitulo.innerText = "";
            this.vista.textAreaDesc.innerText = "";


        }
    },


    errorUpdate: function(){
        this.vista.mostrarError();
    },
    updateTemporadaOk: function(){
        // modificar el objeto temporada
        this.vista.modficarTemporada()
    },
}