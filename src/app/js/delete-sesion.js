function cerrarSesion() {
    fetch('../../src/api/v1.0/sesion', {
        method: 'DELETE',
    }).then((res) => {
        if (res.status !== 200) {
            location.href = 'login.html';
        } else {
            location.href = 'login.html';

        }
    })
}
