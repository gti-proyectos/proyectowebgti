/**
 * Autor: Marc Castello
 * Fecha: 26/05/2020
 * Modelo Vista Controlador Crear una factura
 *
 */
var ModeloCrearFactura = {
    controlador: {},
    crearFactura: async function (factura,idTemporada) {

        try{
            let form = new FormData();
            form.append("idTemporada",idTemporada);
            form.append("datos",JSON.stringify(factura));

            await fetch('../../src/api/v1.0/facturacion',{
                method: 'POST',
                body: form,
            })
                .then(respuesta => {
                    if (respuesta.status === 200) {
                        return respuesta.json();
                    } else {
                        throw Error(respuesta.statusText);
                    }
                })


            this.controlador.crearFacturaOk();


        }catch (e) {
            this.controlador.error();
            console.error(e);
        }
    }
}
var VistaCrearFactura = {
    bloqueError: {},
    controlador: {},
    formulario:{},
    modal:{},
    idTemporada: -1,


    preparar:function (idError,idFormulario,idModal,temporada) {
        this.bloqueError = document.getElementById(idError);
        this.formulario = document.getElementById(idFormulario);
        this.modal = document.getElementById(idModal);
        this.idTemporada = temporada.id;

        this.prepararSelectParcelas(temporada);
    },

    //--------------------------------------------------------------------------
    //cogemos las parcelas que nos llegan de detalles-campo.html
    //--------------------------------------------------------------------------------
    prepararSelectParcelas:function(temporada){
        let parcelas = JSON.parse(localStorage.getItem("parcelas-detallesCampo"))[0];
        //rellenar el select de parcelas del modal de crear temporadas
        let htmlSelect = "";
        for(let i = 0;i<parcelas.length;i++){
            let isValid = false;
            temporada.parcelas.forEach(idParcela=>{
                if(idParcela === parcelas[i].id){
                    isValid = true;
                    return;
                }
            })
            if(isValid){
                htmlSelect+=`<option value="${parcelas[i].id}">${parcelas[i].nombre}</option>`;
            }
        }
        document.getElementById("select-parcela-temporada").innerHTML += htmlSelect;
    },

    mostrarModal: function(){
        this.bloqueError.style.display = "none";
        this.modal.style.display = "block";
    },

    esconderModal: function(){
        this.modal.style.display = "none";
        this.formulario.nombre.value =  "";
        this.formulario.cantidad.value =  "";
        this.formulario.fecha.valueAsDate  = null;
        //  poner el select  a -1
        document.getElementById("select-parcela-temporada").value=-1;
    },

    mostrarError:function () {
        this.bloqueError.style.display = "block"

    },

    // -> T/F
    // comprueba que se cumplen los campos obligatorios
    // concepto, parcela seleccionada, cantidad sin "-" ni "+"
    // y fecha
    checkFormulario: function(){

        let valid = true;
        let name = this.formulario.nombre.value;
        let parcelaSeleccionada = document.getElementById("select-parcela-temporada").value;
        let cantidad = this.formulario.cantidad.value;

        if(name.trim().length<=0){
            valid = false;
        }
        if(parcelaSeleccionada === -1){
            valid = false;
        }

        if(cantidad<0){
            valid = false;
        }

        return valid

    },

    getTemporadaFormulario: function(){
        return {
            idParcela: document.getElementById("select-parcela-temporada").value,
            concepto: this.formulario.nombre.value,
            cantidad: this.formulario.cantidad.value,
            fecha: this.formulario.fecha.value,
            tipoFactura: document.querySelector('input[name="tipo-factura"]:checked').value
        };
    },

}
var ControladorCrearFactura = {
    modelo: ModeloCrearFactura,
    vista: VistaCrearFactura,

    error: false,// controla si ha habido un error en la obtencio de datos

    iniciar: function () {
        this.modelo.controlador = this;
        this.vista.controlador = this;
        this.vista.mostrarModal();
    },

    representarError: function () {
        this.vista.mostrarError();
    },

    // eventos del on sumbit y on reset del formulario de los datos
    onSumbitFormulario: function(e){
        e.preventDefault();
        if(this.vista.checkFormulario()){
            this.modelo.crearFactura(this.vista.getTemporadaFormulario(),this.vista.idTemporada)
        }else{
            this.vista.mostrarError();
        }

    },
    onResetFormulario: function(e){
        e.preventDefault();
        this.vista.esconderModal();
    },

    error: function(){
        this.vista.mostrarError();
    },
    crearFacturaOk: function(){
        console.log("HEY")
        // llamar al callback de la lista para actualizarlo
        this.vista.esconderModal();
        window.location.reload();
    },
}