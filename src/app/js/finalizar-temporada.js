/**
 * Autor: Ruben Pardo
 * Fecha: 23/05/2020
 * Modelo Vista Controlador Editar un cliente
 *
 */

var ModeloFinalizarTemporada = {
    controlador: {},

    finalizarTemporada: async function (data) {

        try{
            let form = new FormData();
            form.append("id",data.id);
            form.append("fechaFin",data.fechaFin);

            await fetch('../../src/api/v1.0/finalizartemporada',{
                method: 'POST',
                body: form,
            })
                .then(respuesta => {
                    if (respuesta.status === 200) {
                        return respuesta.json();
                    } else {
                        throw Error(respuesta.statusText);

                    }
                })


            this.controlador.finalizarTemporadaOk();
        }catch (e) {
            console.error(e);
            this.controlador.errorUpdate();
        }
    },
};

var VistaFinalizarTemporada = {

    controlador: {},
    formulario:{},
    modal:{},


    preparar:function (idError,idFormulario,idModal) {
        this.bloqueError = document.getElementById(idError);
        this.formulario = document.getElementById(idFormulario);
        this.modal = document.getElementById(idModal);
    },

    mostrarModal: function(){
        this.bloqueError.style.display = "none";
        this.modal.style.display = "block";
    },

    esconderModal: function(){
        this.modal.style.display = "none";
        this.bloqueError.style.display = "none";
    },

    mostrarError:function () {
        this.bloqueError.style.display = "block"

    },

};

var ControladorFinalizarTemporada = {
    modelo: ModeloFinalizarTemporada,
    vista: VistaFinalizarTemporada,
    idTemporada: -1,

    error: false,// controla si ha habido un error en la obtencio de datos

    iniciar: function (idTemporada) {
        this.modelo.controlador = this;
        this.vista.controlador = this;
        this.vista.mostrarModal();
        this.idTemporada= idTemporada;
    },

    representarError: function () {
        this.vista.mostrarError();
    },

    // eventos del on sumbit y on reset del formulario de los datos
    onSumbitFormulario: function(e){
        e.preventDefault();

        let today = new Date();

        let data = {
            id: this.idTemporada,
            fechaFin: formatDate(today)
        }

        this.modelo.finalizarTemporada(data)

    },
    onResetFormulario: function(e){
        e.preventDefault();
        this.vista.esconderModal();
    },

    errorUpdate: function(){
        this.vista.mostrarError();
    },
    finalizarTemporadaOk: function(){
        // llamar al callback de la lista para actualizarlo
        this.vista.esconderModal();
        if(window.location.href.includes("detalles")){
            // si estamos en detalles temporada volver atras para refrescar bien los datos
            window.location.href = "listado-temporadas.html"
        }else{
            // si no simplemente recargamos
            window.location.reload();
        }
    },
}
