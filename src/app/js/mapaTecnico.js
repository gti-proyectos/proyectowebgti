/**
 * Autor: Rubén Pardo Casanova
 * GTI GRUPO 11
 * Fecha: 10/05/2020
 *
 * Modelo vista controlador que gestiona el mapa a mostrar en el detalles parcela
 *
 */



var ModeloMapaTecncico = {

    controlador: {},
    idCampo: -1,
    idParcela: -1,

    todasLasSondas:{},//contendrá todos los clientes siempre
    sondasFiltradas:{},// contendrá los clientes filtrados cuando se use el buscador o el ordenar


    obtenerDatos: async function () {
        try{
            this.todasLasSondas  = await fetch('../../src/api/v1.0/posiciones?idCampo=' + this.idCampo)
                .then(respuesta => {
                    if (respuesta.status === 200) {
                        return respuesta.json();
                    } else {
                        this.controlador.error = true;
                        throw Error(respuesta.statusText);
                    }
                })


            // devuelve las posiciones por parcela
            this.todasLasSondas = this.todasLasSondas.find(parcela =>{
                if(parcela.parcela === this.idParcela){
                    return true;
                }
            });
            this.todasLasSondas = this.todasLasSondas.posiciones;
            this.sondasFiltradas = this.todasLasSondas;
            this.controlador.representarDatos(this.sondasFiltradas,this.paginaActual);


        }catch (e) {
            console.error(e);
            this.controlador.representarError();
        }
    },


};

var VistaMapaTecnico = {

    mapa:{},

    // booleano que controla si se tiene que mostrar en primera instancia
    mostrar: false,

    preparar:function (idMapa) {
        //Preparamos el mapa
        this.mapa = new google.maps.Map(document.getElementById(idMapa), {
            center: {lat: 38.9965055, lng: -0.1674364},/*Donde se centra*/
            zoom: 20,/*que nivel de zoom*/
            mapTypeId: 'hybrid',/*Tipo de mapa que se va a dibujar, hybird mapa satelite con carreteras y calles*/
            styles: [/*Permite personalizar estilos del mapa*/
                {
                    featureType: 'poi',/*Ocultar los points of interests, negocios, edificios...*/
                    stylers: [{visibility: 'off'}]
                },
                {
                    featureType: 'transit',/*Ocultar paradas de autobus, taxi..*/
                    stylers: [{visibility: 'off'}]
                }
            ],
            mapTypeControl: false,/*Denegar el cambio de tipo de mapa*/
            streetViewControl: false,/*Denegar el street view*/
            rotateControl: false,/*Denegar el rotar el mapa */
            fullscreenControl: false
        });

        this.mapa.setTilt(0);
    },

    // representar elementos en el html
    representarDatos:function (parcela,sondas) {
        let bounds = new google.maps.LatLngBounds();
        // Parcela
        let polygon = new google.maps.Polygon({
            paths: parcela.vertices,
            strokeColor: "#F00",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#F00",
            fillOpacity: 0.35,
            map: this.mapa
        });


        /*Cogemos los path de cada parcela, por cada limites extiende el bounds hasta que lo incluya*/
        polygon.getPath().getArray().forEach(function (v) {
            bounds.extend(v);
        });
        /*Hacemos que el mapa se adecue a esos limites*/
        this.mapa.fitBounds(bounds)


        // sondas
        sondas.forEach(sonda=> {
            let marker = new google.maps.Marker({
                position: {lat:sonda.latitud,lng:sonda.longitud},
                label: {
                    text: sonda.nombre,
                    color: "#000",
                    fontFamily: 'Roboto, Arial, sans-serif',
                    fontSize: '10px',
                },
                map: this.mapa,/*en que mapa queremos que se dibuje*/
            });
            marker.setIcon({
                path: google.maps.SymbolPath.CIRCLE,
                scale: 10,
                strokeColor: "#FFF",
                fillColor: "#FFF",
                fillOpacity: 1.0,
            })
        })

    },


};

var ControladorMapaTecnico = {
    modelo: ModeloMapaTecncico,
    vista: VistaMapaTecnico,


    iniciar: function (parcela,sondas) {
        this.modelo.controlador = this;
        this.vista.controlador = this;
        this.vista.parcela = parcela;
        this.vista.sondas = sondas;
        this.vista.representarDatos(parcela,sondas);
    },


    representarDatos: function(datos,paginaActual){
        this.vista.representarDatos(datos,paginaActual);
        this.vista.mostrarLista();
    },


    representarError: function () {
        this.vista.mostrarError();
    },

};