/*
---------------------------------------
    Modelo vista controlador de las graficas
    Autor: Aitor Benítez Estruch
    Fecha: 18/04/2020
--------------------------------------------
 */


//-------------------------------------------
// listener on change de tipo de medicion
//-------------------------------------------
function tipoMedicionOnChange(tipoMedicion) {
    ModeloGraficas.tipoMedicion = tipoMedicion
    ControladroGraficas.obtenerDatos();
}

var ModeloGraficas = {
    controlador: {},
    // variables publicas que se modificarán desde el date picker
    fIni:null,
    fFin:null,
    tipoMedicion:null,
    rangoTiempo:"Selecciona rango de tiempo",


    //----------------------------
    // N -> getTextMedidaById() -> Texto
    // devuelve el texto de la medida por el id de esta
    //----------------------------
    getTextMedidaById(){
        let medida;
        switch (this.tipoMedicion) {
            case "0":
                medida =  "Temperatura";
                break;
            case "1":
                medida = "Salinidad";
                break;
            case "2":
                medida = "Iluminacion"
                break;
            case "3":
                medida = "Humedad";
                break;
            default:
                medida =  "";
        }
        return medida;
    },

    //-----------------------------------------
    // N Date,Date,[N]-> obtenerDatosGrafica()
    // obtiene los datos de mediciones
    // TipoDeMedicion: 0 -> temp, 1 -> sal, 2-> ilu, 3-> hum
    // fecha ini, fecha fin: rango de tiempo
    // posiciones: array de las diferentes posiciones que se quiere coger las mediciones
    //-----------------------------------------
    obtenerDatosGrafica: async function(tipoMedicion,sondas,fechaIni,fechaFin){
        if(sondas.length !== 0) {
            // montamos la url con idPos = "id,id,id"
            // en el server se montara un array separando los
            // id por coma
            let src = '../../src/api/v1.0/mediciones?fechaIni=' + fechaIni + '&fechaFin=' + fechaFin + '&tipoMedida=' + tipoMedicion + '&idPos=';
            for (let i = 0; i < sondas.length; i++) {

                if (i !== sondas.length - 1) {
                    src += sondas[i].id + ',';
                } else {
                    src += sondas[i].id;
                }
            }
            let error = false;
            let mediciones = await fetch(src)
                .then(respuesta => {
                    if (respuesta.status === 200) {
                        return respuesta.json()
                    } else {
                        console.log("ERROR EN GET MEDICIONES, STATUS CODE: " + respuesta.status);
                        error = true;
                        this.controlador.representarError();
                    }
                })
                .catch(e => {
                    console.log("ERROR en get mediciones: " + e)
                    this.controlador.representarError();
                    error = true;
                });
            if(!error){
                this.controlador.representarDatos(mediciones);
            }
        }

    }//()


}

var VistaGraficas = {
    bloqueCarga:{},
    bloqueError:{},
    bloqueInfo:{},
    bloqueGrafica:{},

    // ponemos la grafica como atributo para no ir creando muchos charts y evitar posibles solapamientos de anteriores
    // graficas que no se borran
    grafica: null,

    preparar: function (idCarga, idInfo, idGrafica,idError) {
        this.bloqueError = document.getElementById(idError);
        this.bloqueCarga = document.getElementById(idCarga);
        this.bloqueGrafica = document.getElementById(idGrafica);
        this.bloqueInfo = document.getElementById(idInfo);

    },


    esconderElementos: function(){
        this.bloqueInfo.style.display = "none";
        this.bloqueGrafica.style.display = "none";
        this.bloqueCarga.style.display = "none";
        this.bloqueError.style.display = "none";
    },

    mostrarCarga:function () {
        this.esconderElementos();
        this.bloqueCarga.style.display = "flex";
    },
    mostrarInfo:function () {
        this.esconderElementos();
        this.bloqueInfo.style.display = "flex";
    },
    mostrarError:function () {
        this.esconderElementos();
        this.bloqueError.style.display = "flex";
    },
    mostrarGrafica:function () {
        this.esconderElementos();
        this.bloqueGrafica.style.display = "flex";
    },


    //----------------------------------------
    // datos->mostrarGrafica()
    // recibe el JSON de get mediciones
    // trata los datos y los pinta en la grafica
    //----------------------------------------
    representarGrafica: function (datos) {

        let ejeTiempo = [];

        // creamos el objeto que contendrá los datos de la grafica
        let datosGrafica = {
            labels: [],
            datasets: [

            ]
        };
        datos.forEach(dato=>{
            // ordenamos los datos por fecha y añadimos todas las fechas en un array
            dato.mediciones = dato.mediciones.sort(function (a,b) {
                if (a.fecha < b.fecha) return -1;
                if (a.fecha > b.fecha) return 1;
                return 0;
            });

            // creamos un array que contendrá los datos de una posicion
            dato.mediciones.forEach(mesura=>{
                // ponemos todas las fechas en un array
                ejeTiempo.push(mesura.fecha);
            });
        });

        // creamos un array con las fechas no repetidas
        // serán los labels
        let fechas = [];
        ejeTiempo.forEach(function (fecha) {
            let i = fechas.indexOf(fecha);
            if(i < 0) {
                fechas.push(fecha);
            }
        });
        fechas.sort(function (a,b) {
            if (a < b) return -1;
            if (a > b) return 1;
            return 0;
        })
        // añadimos las fechas al atributo label del objeto (eje X)
        datosGrafica.labels = fechas;

        let colorCounter = 0;
        datos.forEach(dato=>{
            let data = [];
            let cont = 0;
            let contData = 0;
            let j = 0;
            // hay que poner en orden los datos para evitar que todos empiezen
            // desde el principio, es decir, puede haber posiciones que empiezen a la mitad
            // y otros antes. Por tanto vamos comparando las fechas y si no es suya ponemos null
            // hasta que encuentre la suya en el array donde estan todas las fechas ordenadas
            for(let i = 0; i<dato.mediciones.length-1;i++){

                if(dato.mediciones[i].fecha !== fechas[cont++]){
                    i--;
                    data[j++]=null;
                }else{
                    data[j++] = dato.mediciones[i].medida;
                }

            }
            let color =  "hsl( " + getRandomColor(colorCounter++, datos.length) + ", 100%, 50% )";//generamos un color random
            datosGrafica.datasets.push({
                label: dato.nombrePosicion,
                data : data,
                fill: false,
                backgroundColor: color,
                borderColor: color,
                lineTension: 0.1,
                pointStyle: 'point',
                pointRadius: 2,
            })
        })

        this.mostrarGrafica();
        this.pintarGrafica(datosGrafica);

    },

    //-------------------------------------------
    // JSON, STRING -> pintarGrafica()
    // funcion que pinta en el canvas la grafica
    //-------------------------------------------
    pintarGrafica: function (datosGrafica) {
        //pintamos la grafica
        let ctx = document.getElementById('grafica');
        let cont = 0;
        let options = {
            bezierCurve: false,
            connectNullData: true,
            spanGaps: true,
            scales: {
                yAxes: [{
                    ticks: {
                    },
                    scaleLabel: {
                        display: true,
                        labelString: ""
                    },
                    gridLines: {
                        color: [],
                        zeroLineWidth: 1,
                    }
                }],
                xAxes: [{
                    gridLines: {
                        color: null,
                        zeroLineWidth: 0,
                    }
                }]
            },
            maintainAspectRatio: false,
            legend: {
                position: 'top',
                align: 'center',
            },
            title: {
                display: true,
                fontWeight: 'bold',
                fontcolor: '#820053'
            },
            tooltips: {
                backgroundColor: '#fff',
                titleFontColor: '#000',
                titleAlign: 'center',
                bodyFontColor: '#333',
                borderColor: '#666',
                borderWidth: 1,
            },
            scaleBeginAtZero: false,
            responsive: true,
        };

        let tipoMedidaTitulo;
        if(datosGrafica.datasets.length === 0) {
            tipoMedidaTitulo =  "No hay datos";
            options.scales.xAxes.type = '';
            options.scales.xAxes.ticks = {};

        }
        else{
            tipoMedidaTitulo = ModeloGraficas.getTextMedidaById()
            // si hay datos limitamos los labels a 12 para no saturar el grafico
            options.scales.xAxes[0].type = 'time';
            options.scales.xAxes[0].ticks = {
                autoSkip: true,
                maxTicksLimit: 12
            };
            options.scales.yAxes[0].ticks = {
                maxTicksLimit: 12
            };
        }
        let rangoTiempoTitulo = (ModeloGraficas.rangoTiempo === null) ? " desde "+ModeloGraficas.fIni+" hasta "+ModeloGraficas.fFin : " - "+ModeloGraficas.rangoTiempo;

        let titulo = tipoMedidaTitulo + rangoTiempoTitulo;
        let min,max;
        //0 == Temperatura
        if (ModeloGraficas.tipoMedicion === "0") {
            min = 0;
            max = 45;
            options.scaleStartValue = -5;
        }
        else {
            min = 0;
            max = 100;

        }
        options.scales.yAxes[0].ticks.suggestedMin = min;
        options.scales.yAxes[0].ticks.suggestedMax = max;

        options.title.text = titulo;

        // Poner el tipo de medida que se representa en el eje Y, y pintar las líneas del fondo a modo de líneas de alerta
        switch (ModeloGraficas.tipoMedicion) {
            case "0":
                // TEMPERATURA
                options.scales.yAxes[0].scaleLabel.labelString = "Temperatura (°C)";
                options.scales.yAxes[0].gridLines.color = [
                    '#dddddd',
                    '#dddddd',
                    '#dddddd',
                    "#ff0010",
                    '#dddddd',
                    '#dddddd',
                    '#dddddd',
                    '#dddddd',
                    "#401cff",
                    '#dddddd',
                    '#dddddd',
                    '#dddddd',
                    '#dddddd',

                ]
                break;
            case "1":
                // salinidad
                options.scales.yAxes[0].scaleLabel.labelString = "Cantidad de sales disueltas (%)";
                options.scales.yAxes[0].gridLines.color = [
                    '#dddddd',
                    '#dddddd',
                    '#dddddd',
                    "#ff0010",
                    '#dddddd',
                    '#dddddd',
                    '#dddddd',
                    '#dddddd',
                    '#dddddd',
                    '#dddddd',
                    '#dddddd',
                ]
                break;
            case "2":
                // Iluminacion
                options.scales.yAxes[0].scaleLabel.labelString = "Cantidad de luz recibida (%)";
                options.scales.yAxes[0].gridLines.color = [
                    '#dddddd',
                    '#dddddd',
                    "#13df00",
                    '#dddddd',
                    '#dddddd',
                    '#dddddd',
                    '#dddddd',
                    "#ff0010",
                    '#dddddd',
                    '#dddddd',
                    '#dddddd',
                ]
                break;
            case "3":
                //Humedad
                options.scales.yAxes[0].scaleLabel.labelString = "Nivel de saturación del suelo (%)";
                options.scales.yAxes[0].gridLines.color = [
                    '#dddddd',
                    '#dddddd',
                    "#401cff",
                    '#dddddd',
                    '#dddddd',
                    '#dddddd',
                    '#dddddd',
                    '#dddddd',
                    "#ff0010",
                    '#dddddd',
                    '#dddddd',
                ]
                break;
            default:
                options.scales.yAxes[0].scaleLabel.labelString = "";
        }

        // si es null la creamos
        if(this.grafica === null){
            this.grafica = new Chart(ctx, {
                type: 'line',
                data: datosGrafica,
                options: options,
            });
        }else{
            //si ya se ha creado actualizamos la grafica para evitar solapamientos
            this.grafica.data = datosGrafica;
            this.grafica.options = options;
            //redraw the chart
            this.grafica.update();
        }

    },//()



}

var ControladroGraficas = {
    modelo: ModeloGraficas,
    vista: VistaGraficas,
    sondas:{},
    iniciar: function (sondasSeleccionadas) {
        this.modelo.tipoMedicion = null;
        cargarCustomMedicionSelect(true)//para evitar problemas al volver por segunda vez
        resetDatapicker();//reseteamos el datapicker
        this.modelo.controlador = this;
        this.sondas = sondasSeleccionadas;
        this.vista.mostrarInfo();
    },

    obtenerDatos:function(){
        if(this.modelo.tipoMedicion !== null && this.modelo.fFin !== null &&  this.modelo.fIni !== null ){
            this.vista.mostrarCarga();
            this.modelo.obtenerDatosGrafica(this.modelo.tipoMedicion,this.sondas,this.modelo.fIni,this.modelo.fFin );
        }
    },

    representarDatos: function(datos){
        this.vista.esconderElementos();
        this.vista.representarGrafica(datos);
    },

    representarError: function () {
        this.vista.mostrarError();
    }
}


///=====================================================
// DATA PICKER
//======================================================

$(function(){
   // fecha por defecto
    var start = moment();
    var end = moment();

    resetDatapicker = function(){
        // resetamos el tiempo
        ModeloGraficas.fFin = null;
        ModeloGraficas.fIni = null;
        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
                // filtros
                'Selecciona rango de tiempo':[null,null],
                'Últimas 8 horas':[moment().subtract(8, 'hours'),moment()],
                'Últimas 24 horas': [moment().subtract(1,'days'), moment()],
                'Últimos 7 días': [moment().subtract(7, 'days'), moment()],
                'Últimos 30 días': [moment().subtract(29, 'days'), moment()],
                'Último año': [moment().subtract(365, 'days'), moment()],
            },
            timePicker: true,
            timePicker24Hour: true,
            "locale": {
                "format": "MM/DD/YYYY",
                "separator": " - ",
                "applyLabel": "Aceptar",
                "cancelLabel": "Cancelar",
                "fromLabel": "De",
                "toLabel": "Hasta",
                "customRangeLabel": "Personalizado",
                "weekLabel": "S",
                "daysOfWeek": [
                    "D",
                    "L",
                    "M",
                    "X",
                    "J",
                    "V",
                    "S"
                ],
                "monthNames": [
                    "Enero",
                    "Febrero",
                    "Marzo",
                    "Abril",
                    "Mayo",
                    "Junio",
                    "Julio",
                    "Augosto",
                    "Septiembre",
                    "Octubre",
                    "Noviembre",
                    "Diciembre"
                ],
                "firstDay": 1
            },
        }, cbDatapicker);
        $('#reportrange span').html("Selecciona rango de tiempo");

    };
    cbDatapicker = function(start, end, label) {
        // callback
        if(label !== "Selecciona rango de tiempo"){
            $('#reportrange span').html(label);
            if(label !== "Personalizado"){
                ModeloGraficas.rangoTiempo = label;//se mostrará cuando este activo
            }else{
                ModeloGraficas.rangoTiempo = null;//si se selecciona personalizado ponemos las fechas
                $('#reportrange span').html(start.format('YYYY-MM-DD HH:MM') + ' - ' + end.format('YYYY-MM-DD HH:MM'));
            }
            // LO UNICO IMPORTANTE ES ESTE CALLBACK, LO DEMAS ES ESTILO
            ModeloGraficas.fIni = start.format('YYYY-MM-DD HH:MM');
            ModeloGraficas.fFin = end.format('YYYY-MM-DD HH:MM');

            ControladroGraficas.obtenerDatos();
        }

    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
            // filtros
            'Selecciona rango de tiempo':[null,null],
            'Últimas 8 horas':[moment().subtract(8, 'hours'),moment()],
            'Últimas 24 horas': [moment().subtract(1,'days'), moment()],
            'Últimos 7 días': [moment().subtract(7, 'days'), moment()],
            'Últimos 30 días': [moment().subtract(29, 'days'), moment()],
            'Último año': [moment().subtract(365, 'days'), moment()],
        },
        timePicker: true,
        timePicker24Hour: true,
        "locale": {
            "format": "MM/DD/YYYY",
            "separator": " - ",
            "applyLabel": "Aceptar",
            "cancelLabel": "Cancelar",
            "fromLabel": "De",
            "toLabel": "Hasta",
            "customRangeLabel": "Personalizado",
            "weekLabel": "S",
            "daysOfWeek": [
                "D",
                "L",
                "M",
                "X",
                "J",
                "V",
                "S"
            ],
            "monthNames": [
                "Enero",
                "Febrero",
                "Marzo",
                "Abril",
                "Mayo",
                "Junio",
                "Julio",
                "Augosto",
                "Septiembre",
                "Octubre",
                "Noviembre",
                "Diciembre"
            ],
            "firstDay": 1
        },
    }, cbDatapicker);

    cbDatapicker(start, end,ModeloGraficas.rangoTiempo);

});
