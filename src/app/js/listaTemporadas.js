/**
 * Autor: Aitor Benítez Estruch
 * Fecha: 21/05/2020
 * Modelo Vista Controlador Listar temporadas
 *
 */


var modeloTemporadas = {
    controlador:{},
    temporadas:[],
    temporadasFiltrado:[],
    paginaActual: 1,
    textoEscritoEnBuscador:"",

    // al principio no estan ordenados
    // true = ASC, false = DESC
    ordenNombre: false,
    ordenFechaInicio: false,
    ordenBalance: false,

    cargar: async function(){
        try{

            let usuario = JSON.parse(localStorage.getItem('user'))
            let idCampo = JSON.parse(localStorage.getItem('parcelas-idCampo'))

            if(idCampo != null && usuario !=null){
                //Obtención de las temporadas registradas por el cliente
                let temporadas = await fetch('../../src/api/v1.0/temporadas?idCliente='+ usuario['cliente']+'&idUsuario='+ usuario['id']
                    +'&idCampo='+ idCampo).then((res)=>{
                    if (res.status !== 200) {
                        // ha habido un error mostrar error:
                        this.controlador.error = true;
                        throw Error(res.statusText);
                        //this.controlador.representarError();
                    } else {
                        // han llegado los datos, devolverlos en formato json
                        return res.json();
                    }
                })

                this.temporadas = temporadas;
                this.temporadasFiltrado = temporadas;
                this.controlador.calcularBalanceTotal(temporadas);
                this.controlador.representar(this.temporadasFiltrado, this.paginaActual)
            }else{
                window.location.href = "login.html";
            }

        }catch (e) {
            console.log(e);
            this.controlador.representarError();
        }

    },


    borrarElemento: async function (idTemporada) {

        try{

            await fetch('../../src/api/v1.0/temporadas', {
                method: 'DELETE',
                body: JSON.stringify({id:idTemporada})
            }).then(res => res.json())
                .catch(error => {throw Error(error)})
                .then(response => console.log('Success:', response));


            window.location.reload();

        }catch (e) {
            console.error(e);
            this.controlador.representarError();
        }
    },

    // filtrar los clientes tanto por nombre correo o telefono
    filtrarPorNombreDescripcion: function (textoAFiltrar) {
        this.textoEscritoEnBuscador = textoAFiltrar;
        // si no hay nada escrito poner todos los clientes
        if(textoAFiltrar === ""){
            this.temporadasFiltrado = this.temporadas;
        }
        else{
            // es texo escrito en el buscador
            this.temporadasFiltrado = this.temporadas.filter(temporada =>{
                if(temporada.nombre!=null && temporada.nombre.toLowerCase().includes(textoAFiltrar.toLowerCase())){
                    return temporada;
                }else if(temporada.fechaInicio!=null && temporada.fechaInicio.toLowerCase().includes(textoAFiltrar.toLowerCase())){
                    return temporada;
                }
            })

        }
        // siempre que filtremos volvamos a la pagina 1
        this.paginaActual = 1;
        this.controlador.representar(this.temporadasFiltrado,this.paginaActual);
    },

    filtrarRadioGroup: function(texto){
        // si no hay nada escrito es que se ha deseleccionado
        if(texto === ""){
            this.temporadasFiltrado = this.temporadas;
            // si hay algo escrito en el buscador
            // filtramos por ello
            if(this.textoEscritoEnBuscador !== ""){
                this.filtrarPorNombreDescripcion(this.textoEscritoEnBuscador)
            }
        }
        else if(texto.toLowerCase() === "activas"){
            // si hay algo escrito en el buscador
            if(this.textoEscritoEnBuscador !== ""){
                this.temporadasFiltrado =  this.temporadas.filter(temporada=>{
                    // filtrar por el texto y por si esta activa
                    // tiene que estar terminada y ser anterior al dia de hoy
                    if((!temporada.terminada) && temporada.nombre!=null && temporada.nombre.toLowerCase().includes(this.textoEscritoEnBuscador.toLowerCase())){
                        return temporada;
                    }else if((!temporada.terminada)  && temporada.fechaInicio!=null && temporada.fechaInicio.toLowerCase().includes(this.textoEscritoEnBuscador.toLowerCase())){
                        return temporada;
                    }
                });
            }else{
                // si no hay nada escrito en el buscador devolver las activas
                this.temporadasFiltrado =  this.temporadas.filter(temporada=>{
                    return (!temporada.terminada);
                });
            }

        }
        else if(texto.toLowerCase() === "finalizadas"){
            // si hay algo escrito en el buscador
            if(this.textoEscritoEnBuscador !== ""){
                this.temporadasFiltrado =  this.temporadas.filter(temporada=>{
                    // filtrar por el texto y por si no esta activa
                    if(temporada.terminada && temporada.nombre!=null && temporada.nombre.toLowerCase().includes(this.textoEscritoEnBuscador.toLowerCase())){
                        return temporada;
                    }else if(temporada.terminada && temporada.fechaInicio!=null && temporada.fechaInicio.toLowerCase().includes(this.textoEscritoEnBuscador.toLowerCase())){
                        return temporada;
                    }
                });
            }else{
                // si no hay nada escrito en el buscador devolver las no activas
                this.temporadasFiltrado =  this.temporadas.filter(temporada=>{
                    return temporada.terminada;
                });
            }


        }
        // siempre que filtremos volvamos a la pagina 1
        this.paginaActual = 1;
        this.controlador.representar(this.temporadasFiltrado,this.paginaActual);
    },

    // idOrden -> 1 = Nombre, 2 = Correo, 3 = telefono
    // ascendente -> true = ordenar asc, false = ordenar desc
    ordenarPor:function (idOrden) {
        //ocultamos todas las flechas
        document.getElementById('flecha-nombre-temp').style.display="none";
        document.getElementById('flecha-balance-temp').style.display="none";
        document.getElementById('flecha-fecha-temp').style.display="none";

        let filtro = "";
        let ordenASC = null;

        // determinar el tipo de filtro y el orden ASC o DESC
        if(idOrden===1){
            //aparece la flecha
            document.getElementById('flecha-nombre-temp').style.display="inline";

            filtro = "nombre"
            // ordenar por nombre
            // cade vez que se pulse de mas cambiarlo
            this.ordenNombre = !this.ordenNombre;
            ordenASC = this.ordenNombre;
        }else{
            this.ordenNombre = false;
        }
        if(idOrden === 2){
            //aparece la flecha
            document.getElementById('flecha-fecha-temp').style.display="inline";

            filtro = "fechaInicio";
            // ordenar por correo

            // cade vez que se pulse de mas cambiarlo
            this.ordenFechaInicio = !this.ordenFechaInicio;
            ordenASC = this.ordenFechaInicio;
        }else{
            this.ordenFechaInicio = false;
        }
        //Falta balance (irá donde está comentado)
      if(idOrden===3){
          //aparece la flecha
          document.getElementById('flecha-balance-temp').style.display="inline";

          filtro = "balance"
            // ordenar por telefono
            // cade vez que se pulse de mas cambiarlo
            this.ordenBalance = !this.ordenBalance;

            ordenASC = this.ordenBalance;
        }else{
            this.ordenBalance = false;
        }


        if(idOrden !== 3){
            this.temporadasFiltrado = this.temporadasFiltrado.sort((temporada1,temporada2) =>{
                // juntar todos los nulls o arriba o abajo
                if(temporada1[filtro] == null){
                    return ordenASC ? -1 : 1;
                }else if(temporada2[filtro] == null ){
                    return ordenASC ? 1 : -1;
                }else {
                    // ordenar
                    if (temporada1[filtro].toLowerCase() > temporada2[filtro].toLowerCase()) {
                        return ordenASC ? 1 : -1;
                    } else if (temporada1[filtro].toLowerCase() < temporada2[filtro].toLowerCase()) {
                        return ordenASC ? -1 : 1;
                    }
                }
                return 0;
            })
        }
        else{

            // ordenamos siempre desde clientes filtrados
            // al inicio seran todos y si hay algun filtro en el buscador se
            // ordenará sobre este
            this.temporadasFiltrado = this.temporadasFiltrado.sort((temporada1,temporada2) =>{
                // juntar todos los nulls o arriba o abajo
                if(temporada1[filtro] == null){
                    return ordenASC ? -1 : 1;
                }else if(temporada2[filtro] == null ){
                    return ordenASC ? 1 : -1;
                }else {
                    // ordenar
                    if (parseInt(temporada1[filtro]) > parseInt(temporada2[filtro])) {
                        return ordenASC ? 1 : -1;
                    } else if (parseInt(temporada1[filtro]) < parseInt(temporada2[filtro])) {
                        return ordenASC ? -1 : 1;
                    }
                }
                return 0;
            })
        }

        // simepre que ordenemos volvemos a la pagina 1
        this.paginaActual = 1
        this.controlador.cambiarFlechasOrden(this.ordenNombre,this.ordenFechaInicio,this.ordenBalance);
        this.controlador.representar(this.temporadasFiltrado,this.paginaActual);

    },


    anteriorPagina:function () {
        this.paginaActual--;
        this.controlador.representar(this.temporadasFiltrado,this.paginaActual);
    },

    siguientePagina: function () {
        this.paginaActual++;
        this.controlador.representar(this.temporadasFiltrado,this.paginaActual);
    }
}

var vistaTemporadas = {
    controlador: {},

    bloqueLista:{}, // donde se representarán los elementos de la lista
    paginaActual: 1,// indice para controlar la paginacion
    maxElementosPorPagina: 5,
    bloqueError:{},
    bloqueCarga:{},
    bloqueBodyLista:{},

    radioGroup: null,

    // botones para el control de la paginacion de clientes
    btnNextPage:{},
    btnPreviousPage:{},
    mostrar: false,


    preparar:function (idBloqueLista, idListaBody, idError, idCarga, idBtnNext, idBtnPrev, claseRadioGroup, mostrar) {
        this.bloqueLista = document.getElementById(idBloqueLista);
        this.bloqueBodyLista = document.getElementById(idListaBody);
        this.bloqueError = document.getElementById(idError);
        this.bloqueCarga = document.getElementById(idCarga);
        this.btnPreviousPage = document.getElementById(idBtnPrev);
        this.btnNextPage = document.getElementById(idBtnNext);
        this.radioGroup =  document.getElementsByName(claseRadioGroup);
        this.mostrar= mostrar;
        this.prepararRadioGroup();

    },
    // on click a los radio group para que siempre que se pulse uno se desmarquen todos
    // asi se puede desmarcar el radio group
    prepararRadioGroup:function(){
        let booRadio;
        let x = 0;
        for(x = 0; x < this.radioGroup.length; x++) {
            this.radioGroup[x].onclick = function () {

                if (booRadio == this) {
                    this.checked = false;
                    booRadio = null;
                } else {
                    booRadio = this;
                }
                controladorTemporadas.onChangeRadioGroup(this)
            };
        }
    },
    // funciones para controlar cuando se muestran los bloques html
    esconderElementos: function(){
        this.bloqueLista.style.display = "none";
        this.bloqueCarga.style.display = "none";
        this.bloqueError.style.display = "none";
    },
    mostrarCarga: function () {
        if(this.mostrar) {
            this.esconderElementos();
            this.bloqueCarga.style.display = "flex"
        }
    },
    mostrarError:function () {
        this.esconderElementos();
        this.bloqueError.style.display = "flex"
    },
    mostrarLista:function () {
        if(this.mostrar) {
            this.esconderElementos();
            if(this.controlador.error){
                this.bloqueError.style.display = "block"
            }else{
                this.bloqueLista.style.display = "flex"
            }
        }else{
            this.bloqueLista.style.display = "none"
        }
    },

    // pinta en el html el balance total
    representarBalanceTotal(balanceTotal, beneficioTotal, gastoTotal){
        // poner el balance total en la pantalla
        let balanceHTMl = document.getElementById("balance-total");

        document.getElementById("beneficios-totales").innerText = beneficioTotal + " €";
        document.getElementById("gastos-totales").innerText = gastoTotal + " €";

        if(balanceTotal != null){
            balanceHTMl.innerHTML = balanceTotal+" €";
            if(balanceTotal > 0){
                // balance positivo
                balanceHTMl.className = ("balance-positivo")
            }else{
                // balance positivo
                balanceHTMl.className = ("balance-negativo")
            }
        }else{
            balanceHTMl.innerHTML = "Sin ingresos";
        }

    },

    // representar elementos en el html
    representarDatos:function (temporadas,paginaActual) {

        //control de paginacion
        // maxPages = length/maxElementosPorPagina redondeado hacia arriba
        let maxPages = Math.ceil(temporadas.length/this.maxElementosPorPagina);
        // si estamos en la pagina 1 empezamos desde el 0 (5*(1-1))
        // si estamos en la pagina 2 empezamos desde el 5 (5*(2-1))
        let elementoInicial = this.maxElementosPorPagina * (paginaActual-1);

        this.prepararBotonesPaginador(paginaActual,maxPages);

        this.bloqueBodyLista.innerHTML = "";

        let balanceTotal = 0;
        // pintar elemntos acorde al maxElementosPorPagina
        // los elementos iran desde (maxElementosPorPagina* (paginaActual-1)) hasta this.maxElementosPorPagina*paginaActual
        for(let i = elementoInicial;i<(this.maxElementosPorPagina*paginaActual);i++){
            // si en una pagina no llega al maxElementosPorPagina hay que evitar que intente pintar esos
            // elementos y no de error
            if(i<temporadas.length){
                let hilera = document.createElement("tr");

                let celdaNombre = document.createElement("td");
                let celdaFechaIni = document.createElement("td");
                let celdaBalance = document.createElement("td");
                let celdaOperacion = document.createElement("td");
                celdaOperacion.className = "list-opereciones";

                let textoCeldaNombre = document.createTextNode(temporadas[i].nombre != null ? temporadas[i].nombre : "Sin asignar");
                let textoCeldaFechaIni = document.createTextNode(temporadas[i].fechaInicio != null ? temporadas[i].fechaInicio : "Sin asignar");
                let textoCeldaBalance = document.createTextNode(temporadas[i].balance != null ? temporadas[i].balance+" €" : "Sin ingresos");

                if(temporadas[i].balance!=null){
                    // sumamos el balance al total
                    balanceTotal+=parseInt(temporadas[i].balance);

                    // balance de la temporada positivo
                    // añadimos la clase css
                    if(temporadas[i].balance > 0){
                        celdaBalance.classList.add("balance-positivo")
                    }else{
                        // balance de la temporada negativo
                        celdaBalance.classList.add("balance-negativo")
                    }

                }

                celdaNombre.appendChild(textoCeldaNombre);
                celdaFechaIni.appendChild(textoCeldaFechaIni);
                celdaBalance.appendChild(textoCeldaBalance);


                // añadimos los botones a todas las listas

                let btnFin = document.createElement("button");
                btnFin.className = "btn-list-element";
                btnFin.onclick = () =>{this.controlador.onFinalizarTemporada(temporadas[i])};

                let btnRemove = document.createElement("button");
                btnRemove.className = "btn-list-element";
                btnRemove.onclick = () =>{this.controlador.onRemoveElementoLista(temporadas[i])};


                let ul = document.createElement("ul");
                ul.className = "listaOperaciones";

                let li = document.createElement("li")
                btnFin.innerHTML = `<i class="far fa-calendar-times"></i><p class="label">Finalizar</p>`
                li.appendChild(btnFin);

                let li2 = document.createElement("li")
                btnRemove.innerHTML = `<i class="far fa-trash-alt"></i><p class="label">Borrar</p>`
                li2.appendChild(btnRemove);

                //Si la termporada está concluida (o no empezado) añadir clase no activa, y sólo con el icono de eliminar
                //Si no lo está, poner también el icono de finalizar temporada
                if(temporadas[i].terminada){
                    hilera.classList.add("no-activa");
                    ul.appendChild(li2)
                }
                else{
                    // temporada activa
                    ul.appendChild(li)
                    ul.appendChild(li2)

                }

                // añadimos la lista que se mostrará a partir de tablet
                celdaOperacion.appendChild(ul)

                celdaNombre.onclick = () => {this.controlador.onClickElemento(temporadas[i])}
                celdaBalance.onclick = () => {this.controlador.onClickElemento(temporadas[i])}
                celdaFechaIni.onclick = () => {this.controlador.onClickElemento(temporadas[i])}

                // añadimos los elementos a la fila
                hilera.appendChild(celdaNombre);
                hilera.appendChild(celdaFechaIni);
                hilera.appendChild(celdaBalance);
                hilera.appendChild(celdaOperacion)



                // añadimos la fila al tbody
                this.bloqueBodyLista.appendChild(hilera);
            }
        }

    },

    // habilitar y deshabilitar los botones que controlan la paginacion
    // para evitar errores
    prepararBotonesPaginador : function (paginaActual, maxPages) {
        this.btnNextPage.removeAttribute("disabled");
        this.btnPreviousPage.removeAttribute("disabled");

        document.getElementById("paginadorListaCampo").innerHTML = "Página "+paginaActual + " de " + maxPages;

        // si el maxPages es mayor que uno habilitar el next page
        if(maxPages>1){
            this.btnNextPage.removeAttribute("disabled");
        }
        // si el current page es igual al maxPages deshabilitar el next page
        if(paginaActual === maxPages){
            this.btnNextPage.setAttribute("disabled","disabled");
        }
        if(maxPages === 0){
            this.btnNextPage.setAttribute("disabled","disabled");
        }
        // si el current page es mayor que uno habilitar el previous page
        if(paginaActual > 1){
            this.btnPreviousPage.removeAttribute("disabled");
        }else{
            this.btnPreviousPage.setAttribute("disabled","disabled");
        }

    },
    //Falta balance (irá donde está comentado)
    cambiarFlechasOrden(ordenNombre, ordenFechaInicio , ordenBalance) {
        document.getElementById("flecha-nombre-temp").className = (!ordenNombre) ? "fas fa-chevron-down" : "fas fa-chevron-up";
        document.getElementById("flecha-fecha-temp").className = (!ordenFechaInicio) ? "fas fa-chevron-down" : "fas fa-chevron-up";
        document.getElementById("flecha-balance-temp").className = (!ordenBalance) ? "fas fa-chevron-down" : "fas fa-chevron-up";
    },

}

var controladorTemporadas = {
    modelo: modeloTemporadas,
    vista: vistaTemporadas,
    controladorFinalizarTemporada: null,

    iniciar: function (controladorFinalizarTemporada) {
        this.modelo.controlador = this;
        this.vista.controlador = this;
        this.vista.esconderElementos();
        this.vista.mostrarCarga();
        this.modelo.cargar();
        this.controladorFinalizarTemporada = controladorFinalizarTemporada;
    },

    // calcula el balance total de todas las temporadas
    calcularBalanceTotal: function(temporadas){

        let balanceTotal = 0;
        let beneficioTotal = 0;
        let gastoTotal = 0;

        temporadas.forEach(temporada=>{
            if(temporada.balance!=null){
                let balanceTemp = parseInt(temporada.balance);
                balanceTotal+= balanceTemp;
                if(balanceTemp > 0){
                    beneficioTotal+= balanceTemp;
                }else{
                    gastoTotal+= balanceTemp;
                }

            }
        })
        this.vista.representarBalanceTotal(balanceTotal,beneficioTotal,gastoTotal);
    },
    //Falta balance (irá donde está comentado)
    cambiarFlechasOrden(ordenNombre, ordenFechaInicio,ordenBalance) {
        this.vista.cambiarFlechasOrden(ordenNombre,ordenFechaInicio,ordenBalance)
    },
    representar : function(temporadas, paginaActual){
        this.vista.representarDatos(temporadas, paginaActual);
        this.vista.mostrarLista();
    },

    representarError: function () {
        this.vista.mostrarError();

    },


    // Recibe una temporada, le coloca en local Storage y carga detalles-temporada
    onClickElemento: function(elemento){
        localStorage.setItem("lista-temporada-temporada",JSON.stringify(elemento));
        window.location.href = "detalles-temporada.html"
    },

    // llama al controlador que gestiona el finalizar temporada
    onFinalizarTemporada: function(elemento){
        this.controladorFinalizarTemporada.iniciar(elemento.id)
    },

    onRemoveElementoLista: function (elemento) {
        // confimar borrado
        if(confirm("Se borrará para siempre la temporada "+elemento.nombre+"\n¿Estás seguro?")){
            this.vista.mostrarCarga();
            console.log(elemento.id)
            this.modelo.borrarElemento(elemento.id);
        }
    },
    onChangeRadioGroup: function (e) {
        if(!e.checked){
            // si se ha desmarcado quitar el filtro
            this.modelo.filtrarRadioGroup("");
        }else if(e.id.includes("finalizadas")) {
            // filtrar por finalizadas
            this.modelo.filtrarRadioGroup("finalizadas");
        }else{
            // filtrar por activas
            this.modelo.filtrarRadioGroup("activas");
        }
    },
};

