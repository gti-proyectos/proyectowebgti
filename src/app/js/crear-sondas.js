/**
 * Autor Ruben
 * Logica de carga de los MVC de crear-sondas.html
 */
let user =  JSON.parse(localStorage.getItem("user"));
// ponemos el nombre del usuario en el titulo
document.getElementById("titulo").innerText = user['nombre'];
// cogemos el cliente pasado por localstorage desde la lista de clientes
let parcela = JSON.parse(localStorage.getItem("listaSondas-sondas"));
VistaCrearSondas.preparar("errorForm","carga","latLng-form","okForm",parcela.id);
ControladorCrearSondas.iniciar();