function cargarCustomParcelasSelect() {


    var x, i, j, selElmnt, a, b, c;
    /* Look for any elements with the class "custom-select": */
    x = document.getElementsByClassName("custom-select-parcela");

    for (i = 0; i < x.length; i++) {
        selElmnt = x[i].getElementsByTagName("select")[0];
        let idSelector = selElmnt.id;
        /* For each element, create a new DIV that will act as the selected item: */
        a = document.createElement("DIV");
        a.setAttribute("class", "select-selected");
        a.setAttribute("class", "select-selected-parcela");
        a.innerHTML = `<p>${selElmnt.options[selElmnt.selectedIndex].innerHTML}<p>`;
        a.innerHTML+= `<i class="flechaDropdowns"></i>`
        x[i].appendChild(a);
        x[i].appendChild(a);
        /* For each element, create a new DIV that will contain the option list: */
        b = document.createElement("DIV");

        b.classList.add("select-items-parcela")
        b.classList.add("select-items")
        b.classList.add("select-hide")
        for (j = 1; j < selElmnt.length; j++) {
            /* For each option in the original select element,
            create a new DIV that will act as an option item: */
            c = document.createElement("DIV");
            c.innerHTML = selElmnt.options[j].innerHTML;
            let value = selElmnt.options[j].value;
            c.addEventListener("click", function(e) {

                /* When an item is clicked, update the original select box,
                and the selected item: */
                var y, i, k, s, h;
                s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                h = this.parentNode.previousSibling;
                for (i = 0; i < s.length; i++) {
                    if (s.options[i].innerHTML == this.innerHTML) {
                        s.selectedIndex = i;
                        h.innerHTML = "";
                        h.innerHTML = `<p>${this.innerHTML}</p>`;
                        h.innerHTML += `<i class="flechaDropdowns"></i>`
                        y = this.parentNode.getElementsByClassName("same-as-selected");
                        for (k = 0; k < y.length; k++) {
                            y[k].removeAttribute("class");
                        }
                        this.setAttribute("class", "same-as-selected");
                        //===========================================================================================
                        // MODIFICACION IMPORTANTE
                        // AQUI SE HACE EL ON CHANGE DEL SELECTOR
                        // LA VARIABLE VALUE ES EL ID SELECCIONADO
                        // CON LA VARIABLE IDSELECTOR SABEMOS EN QUE SELECTOR ESTAMOS
                        // ID = parcelas | tipo-medicion
                        //===========================================================================================

                        parcelaOnChange(value);
                        break;
                    }
                }
                h.click();
            });
            b.appendChild(c);
        }
        x[i].appendChild(b);
        a.addEventListener("click", function(e){
            /* When the select box is clicked, close any other select boxes,
            and open/close the current select box: */
            e.stopPropagation();
            this.nextSibling.classList.toggle("select-hide");
            this.classList.toggle("select-arrow-active");
        });
    }
}
function cargarCustomMedicionSelect(remove) {


    let element = document.getElementsByClassName("select-selected-medicion")[0];
    let parent = document.getElementById("selector-tipo-medicion");
    if(parent!==undefined && element !== undefined){
        parent.removeChild(element);
    }


    var x, i, j, selElmnt, a, b, c;
    /* Look for any elements with the class "custom-select": */
    x = document.getElementsByClassName("custom-select-medicion");

    for (i = 0; i < x.length; i++) {
        selElmnt = x[i].getElementsByTagName("select")[0];
        let idSelector = selElmnt.id;
        /* For each element, create a new DIV that will act as the selected item: */
        a = document.createElement("DIV");
        a.setAttribute("class", "select-selected");
        a.setAttribute("class", "select-selected-medicion");
        if(remove){
            a.innerHTML = "Selecciona un tipo de medida";
            a.innerHTML+= `<i class="flechaDropdowns"></i>`
        }else{
            a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
            a.innerHTML+= `<i class="flechaDropdowns"></i>`
        }

        x[i].appendChild(a);
        x[i].appendChild(a);
        /* For each element, create a new DIV that will contain the option list: */
        b = document.createElement("DIV");
        b.classList.add("select-items-medicion")
        b.classList.add("select-items")
        b.classList.add("select-hide")
        for (j = 1; j < selElmnt.length; j++) {
            /* For each option in the original select element,
            create a new DIV that will act as an option item: */
            c = document.createElement("DIV");
            c.innerHTML = selElmnt.options[j].innerHTML;
            let value = selElmnt.options[j].value;
            c.addEventListener("click", function(e) {

                /* When an item is clicked, update the original select box,
                and the selected item: */
                var y, i, k, s, h;
                s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                h = this.parentNode.previousSibling;
                for (i = 0; i < s.length; i++) {
                    if (s.options[i].innerHTML == this.innerHTML) {
                        s.selectedIndex = i;
                        h.innerHTML = "";
                        h.innerHTML = `<p>${this.innerHTML}</p>`;
                        h.innerHTML += `<i class="flechaDropdowns"></i>`
                        y = this.parentNode.getElementsByClassName("same-as-selected");
                        for (k = 0; k < y.length; k++) {
                            y[k].removeAttribute("class");
                        }
                        this.setAttribute("class", "same-as-selected");
                        //===========================================================================================
                        // MODIFICACION IMPORTANTE
                        // AQUI SE HACE EL ON CHANGE DEL SELECTOR
                        // LA VARIABLE VALUE ES EL ID SELECCIONADO
                        // CON LA VARIABLE IDSELECTOR SABEMOS EN QUE SELECTOR ESTAMOS
                        //===========================================================================================
                        tipoMedicionOnChange(value)
                        break;
                    }
                }

                h.click();

            });
            b.appendChild(c);
        }
        x[i].appendChild(b);

        a.addEventListener("click", function(e){
            /* When the select box is clicked, close any other select boxes,
            and open/close the current select box: */
            e.stopPropagation();
            this.nextSibling.classList.toggle("select-hide");
            this.classList.toggle("select-arrow-active");
        });
    }
}


// FUNCION QUE SE LLAMA AL PULSAR UNA PARCELA EN EL MAPA
// BUSCA EL OPTION Y LO SELECCIONA
function selectItemParcela(value) {
    //comprobar cual esta activo
    var x, i, j, selElmnt,selItems, a, b, c;
    /* Look for any elements with the class "custom-select": */
    x = document.getElementsByClassName("custom-select-parcela");
    for (i = 0; i < x.length; i++) {
        selElmnt = x[i].getElementsByTagName("select")[0];
        selItems = x[i].getElementsByClassName("select-items-parcela")[0];

        for( j = 0; j<selElmnt.children.length; j++){
            if(selElmnt.children[j].value == value){
                for(let k = 0; k < selItems.children.length; k++) {
                    selItems.children[k].removeAttribute("class")
                    if (selItems.children[k].textContent == selElmnt.children[j].textContent) {

                        // actualizar el selector
                        let item = selItems.children[k];
                        item.setAttribute("class", "same-as-selected")
                        let s = item.parentNode.parentNode.getElementsByTagName("select")[0];
                        let h = item.parentNode.previousSibling;
                        for (i = 0; i < s.length; i++) {
                            if (s.options[i].innerHTML == item.innerHTML) {
                                s.selectedIndex = i;
                                h.innerHTML = "";
                                h.innerHTML = `<p>${item.innerHTML}</p>`;
                                h.innerHTML += `<i class="flechaDropdowns"></i>`
                                item.setAttribute("class", "same-as-selected");
                                //===========================================================================================
                                // MODIFICACION IMPORTANTE
                                // AQUI SE HACE EL ON CHANGE DEL SELECTOR
                                // LA VARIABLE VALUE ES EL ID SELECCIONADO
                                // CON LA VARIABLE IDSELECTOR SABEMOS EN QUE SELECTOR ESTAMOS
                                // ID = parcelas | tipo-medicion
                                //===========================================================================================

                                parcelaOnChange(value)

                            }
                            
                        }
                    }
                }
            }
        }

    }
}
function closeAllSelect(elmnt) {
    /* A function that will close all select boxes in the document,
    except the current select box: */
    var x,x2, y,y2, i, arrNo = [];
    x = document.getElementsByClassName("select-items-parcela");
    x2 = document.getElementsByClassName("select-items-medicion");
    y = document.getElementsByClassName("select-selected-parcela");
    y2 = document.getElementsByClassName("select-selected-medicion");

    for (i = 0; i < y.length; i++) {
        if (elmnt === y[i]) {
            y[i].classList.remove("select-arrow-active");
        } else {
            arrNo.push(i)
        }
    }
    for (i = 0; i < x.length; i++) {
        if (arrNo.indexOf(i)) {
            x[i].classList.add("select-hide");
        }
    }
    arrNo = [];
    for (i = 0; i < y2.length; i++) {
        if (elmnt === y2[i]) {
            y2[i].classList.remove("select-arrow-active");
        } else {
            arrNo.push(i)
        }
    }
    for (i = 0; i < x2.length; i++) {
        if (arrNo.indexOf(i)) {
            x2[i].classList.add("select-hide");
        }
    }
}

cargarCustomMedicionSelect(false);
/* If the user clicks anywhere outside the select box,
then close all select boxes: */
document.addEventListener("click", closeAllSelect);