/**
 * Autor: Ruben Pardo
 * Fecha: 08/05/2020
 * Modelo Vista Controlador Editar un cliente
 *
 */

var ModeloModificarPassUsuario = {
    controlador: {},

    cambiarPass: async function (data) {

        try{
            console.log(data)
            let form = new FormData();
            form.append("data",JSON.stringify(data));
            await fetch('../../src/api/v1.0/userpass',{
                method: 'POST',
                body: form,
            })
                .then(respuesta => {
                    if (respuesta.status === 200) {
                        return respuesta.json();
                    } else {
                        this.controlador.errorUpdate();
                        throw Error(respuesta.statusText);

                    }
                })


            this.controlador.editarUsuarioOk();
        }catch (e) {
            console.error(e);
        }
    },
};

var VistaModificarPassUsuario = {

    controlador: {},
    formulario:{},
    modal:{},
    tituloModal:{},
    usuario: {},


    preparar:function (idError, idCarga,idFormulario,idModal,idCloseModal,idTituloModal) {
        this.bloqueError = document.getElementById(idError);
        this.bloqueCarga = document.getElementById(idCarga);
        this.formulario = document.getElementById(idFormulario);
        this.modal = document.getElementById(idModal);
        this.tituloModal = document.getElementById(idTituloModal);
    },
    mostrarModal: function(){
        this.bloqueError.style.display = "none";
        this.modal.style.display = "block";
    },

    esconderModal: function(){
        this.modal.style.display = "none";
        this.formulario.pass.value =  "";
        this.formulario.repetirPass.value =  "";
    },

    mostrarError:function () {
        this.bloqueError.style.display = "block"

    },

    // validacion del formulario
    // que las contraseña sean iguales y no vacias
    checkFormulario: function(){
        return (this.formulario.pass.value.trim().length > 0
            && this.formulario.repetirPass.value.trim().length > 0
            && (this.formulario.pass.value === this.formulario.repetirPass.value))
    },

    getDatosFormulario: function(){
        return {id:this.usuario.id,pass:SHA1(this.formulario.pass.value)};
    },

}

var ControladorModificarPassUsuario = {
    modelo: ModeloModificarPassUsuario,
    vista: VistaModificarPassUsuario,

    error: false,// controla si ha habido un error en la obtencio de datos

    iniciar: function () {
        this.modelo.controlador = this;
        this.vista.controlador = this;
        this.vista.mostrarModal();
    },

    representarError: function () {
        this.vista.mostrarError();
    },

    // eventos del on sumbit y on reset del formulario de los datos
    onSumbitFormulario: function(e){
        e.preventDefault();
        if(this.vista.checkFormulario()){
            this.modelo.cambiarPass(this.vista.getDatosFormulario())
        }else{
            this.vista.mostrarError();
        }

    },
    onResetFormulario: function(e){
        e.preventDefault();
        this.vista.esconderModal();
    },

    errorUpdate: function(){
        this.vista.mostrarError();
    },
    editarUsuarioOk: function(){
        // llamar al callback de la lista para actualizarlo
        this.vista.esconderModal();
    },
}
