/**
 * Autor Marc
 * Logica de carga de los MVC de parcela.html
 *
 */

let user =  JSON.parse(localStorage.getItem("user"));
// ponemos el nombre del usuario en el titulo
document.getElementById("titulo").innerText = user['nombre'];

// cogemos la parcela pasado por localstorage
let parcela = JSON.parse(localStorage.getItem("listaClienteTecnico-parcela"));
// cambiamos el titulo si se va a modifcar o crear una parcela
document.title = parcela.modificar ? "Modificar Parcela | Agroanalytics" : "Crear Parcela | Agroanalytics";
document.getElementById("btn-formulario-enviar").innerHTML = parcela.modificar ? "Modificar" : "Añadir";
document.getElementById("titulo-form").innerHTML = parcela.modificar ? "Modificar Parcela " + parcela.nombre : "Añadir Parcela";
document.getElementById("okForm").innerHTML = parcela.modificar ? "Parcela modificada correctamente " : "Parcela creada correctamente";

VistaCrearParcela.preparar("errorForm","carga","latLng-form","okForm",parcela);
ControladorCrearParcela.iniciar();