/**
 * Autor: Rubén Pardo Casanova
 * GTI GRUPO 11
 * Fecha: 09/05/2020
 *
 * Modelo vista controlador que gestiona la lista de los parcelas de un campo
 *
 */

var ModeloListaSondas = {

    controlador: {},
    idCampo: -1,
    idParcela: -1,

    todasLasSondas:{},//contendrá todos los clientes siempre
    sondasFiltradas:{},// contendrá los clientes filtrados cuando se use el buscador o el ordenar

    paginaActual: 1,

    // al principio no estan ordenados
    // true = ASC, false = DESC
    ordenNombre: false,
    ordenLat: false,
    ordenLng: false,

    obtenerDatos: async function () {
        try{
            this.todasLasSondas  = await fetch('../../src/api/v1.0/posiciones?idCampo=' + this.idCampo)
                .then(respuesta => {
                    if (respuesta.status === 200) {
                        return respuesta.json();
                    } else {
                        this.controlador.error = true;
                        throw Error(respuesta.statusText);
                    }
                })


            // devuelve las posiciones por parcela
            this.todasLasSondas = this.todasLasSondas.find(parcela =>{
                if(parcela.parcela === this.idParcela){
                    return true;
                }
            });
            this.todasLasSondas = this.todasLasSondas == null ? [] :this.todasLasSondas.posiciones ;
            this.sondasFiltradas = this.todasLasSondas;
            this.controlador.representarDatos(this.sondasFiltradas,this.paginaActual);
            this.controlador.cargarMapa(this.sondasFiltradas);


        }catch (e) {
            console.error(e);
            this.controlador.representarError();
        }
    },
    borrarElemento: async function (idSonda) {
        try{

            await fetch('../../src/api/v1.0/posiciones',{
                method:'DELETE',
                body: JSON.stringify({id:idSonda})
            })
                .then(respuesta => {
                    if (respuesta.status === 200) {
                        return respuesta.json();
                    } else {
                        this.controlador.error = true;
                        throw Error(respuesta.statusText);

                    }
                })
            this.controlador.borradoOk();


        }catch (e) {
            console.error(e);
            this.controlador.representarError();
        }
    },

    // filtrar los clientes tanto por nombre correo o telefono
    filtrarPorNombreLatLng: function (textoAFiltrar) {
        // si no hay nada escrito poner todos los clientes
        if(textoAFiltrar === ""){
            this.sondasFiltradas = this.todasLasSondas;
        }else{
            this.sondasFiltradas = this.todasLasSondas.filter(sonda =>{
                if(sonda.nombre!=null && sonda.nombre.toLowerCase().includes(textoAFiltrar.toLowerCase())){
                    return sonda;
                }else if(sonda.lat!=null &&  sonda.lat.toLowerCase().includes(textoAFiltrar.toLowerCase())){
                    return sonda;
                }else if(sonda.lng!=null &&  sonda.lng.toLowerCase().includes(textoAFiltrar.toLowerCase())){
                    return sonda;
                }
            })


        }
        // siempre que filtremos volvamos a la pagina 1
        this.paginaActual = 1;
        this.controlador.representarDatos(this.sondasFiltradas,this.paginaActual);
    },

    // idOrden -> 1 = Nombre, 2 = lat, 3 = lng, 4 = Fecha
    // ascendente -> true = ordenar asc, false = ordenar desc
    ordenarPor:function (idOrden) {
//ocultamos todas las flechas
        document.getElementById('flecha-nombre-sonda').style.display="none";
        document.getElementById('flecha-lat-sonda').style.display="none";
        document.getElementById('flecha-lng-sonda').style.display="none";
        let filtro = "";
        let ordenASC = null;

        // determinar el tipo de filtro y el orden ASC o DESC
        if(idOrden===1){
            document.getElementById('flecha-nombre-sonda').style.display="inline";

            filtro = "nombre";
            // ordenar por nombre
           // cade vez que se pulse de mas cambiarlo
            this.ordenNombre = !this.ordenNombre;
            ordenASC = this.ordenNombre;
        }else{
            this.ordenNombre = false
        }
        if(idOrden === 2){
            document.getElementById('flecha-lat-sonda').style.display="inline";

            filtro = "lat";
            // ordenar por correo
            this.ordenLat = !this.ordenLat;
            ordenASC = this.ordenLat;
        }else{
            this.ordenLat = false
        }
        if(idOrden === 3){
            document.getElementById('flecha-lng-sonda').style.display="inline";

            filtro = "lng";
            // ordenar por correo
            // cade vez que se pulse de mas cambiarlo
            this.ordenLng = !this.ordenLng;
            ordenASC = this.ordenLng;
        }else{
            this.ordenLng = false
        }
        // ordenamos siempre desde clientes filtrados
        // al inicio seran todos y si hay algun filtro en el buscador se
        // ordenará sobre este
        this.sondasFiltradas = this.sondasFiltradas.sort((campo1, campo2) =>{
            // juntar todos los nulls o arriba o abajo
            if(campo1[filtro] == null){
                return ordenASC ? 1 : -1;
            }else if(campo2[filtro] == null ){
                return ordenASC ? -1 : 1;
            }else{
                // ordenar
                if(campo1[filtro].toLowerCase() > campo2[filtro].toLowerCase()){
                    return ordenASC ? 1 : -1;
                }else if(campo1[filtro].toLowerCase() < campo2[filtro].toLowerCase()){
                    return ordenASC ? -1 : 1;
                }
            }


            return 0;
        })

        // simepre que ordenemos volvemos a la pagina 1
        this.paginaActual = 1
        this.controlador.cambiarFlechasOrden(this.ordenNombre,this.ordenLat,this.ordenLng)
        this.controlador.representarDatos(this.sondasFiltradas,this.paginaActual);
    },

    anteriorPagina:function () {
        this.paginaActual--;
        this.controlador.representarDatos(this.sondasFiltradas,this.paginaActual);
    },

    siguientePagina: function () {
        this.paginaActual++;
        this.controlador.representarDatos(this.sondasFiltradas,this.paginaActual);
    }

};

var VistaListaSondas = {

    controlador: {},
    parcela:{},

    bloqueLista:{}, // donde se representarán los elementos de la lista
    paginaActual: 1,// indice para controlar la paginacion
    maxElementosPorPagina: 5,
    bloqueError:{},
    bloqueCarga:{},
    bloqueBodyLista:{},

    // botones para el control de la paginacion de clientes
    btnNextPage:{},
    btnPreviousPage:{},

    // booleano que controla si se tiene que mostrar en primera instancia
    mostrar: false,

    preparar:function (idBloqueLista, idListaBody, idError, idCarga, idBtnNext, idBtnPrev,mostrar,parcela) {
        this.bloqueLista = document.getElementById(idBloqueLista);
        this.bloqueBodyLista = document.getElementById(idListaBody);
        this.bloqueError = document.getElementById(idError);
        this.bloqueCarga = document.getElementById(idCarga);
        this.btnPreviousPage = document.getElementById(idBtnPrev);
        this.btnNextPage = document.getElementById(idBtnNext);
        this.parcela = parcela;
        this.mostrar = mostrar;

    },

    // funciones para controlar cuando se muestran los bloques html
    esconderElementos: function(){
        this.bloqueLista.style.display = "none";
        this.bloqueCarga.style.display = "none";
        this.bloqueError.style.display = "none";
    },
    mostrarCarga: function () {
        if(this.mostrar) {
            this.esconderElementos();
            this.bloqueCarga.style.display = "flex"
        }
    },
    mostrarError:function () {
        this.esconderElementos();
        this.bloqueError.style.display = "flex"

    },
    mostrarLista:function () {
        if(this.mostrar) {
            this.esconderElementos();
            if(this.controlador.error){
                this.bloqueError.style.display = "flex"
            }else{
                this.bloqueLista.style.display = "flex"
            }
        }
    },

    // representar elementos en el html
    representarDatos:function (sondas,paginaActual) {

        //control de paginacion
        // maxPages = length/maxElementosPorPagina redondeado hacia arriba
        let maxPages = Math.ceil(sondas.length/this.maxElementosPorPagina);
        // si estamos en la pagina 1 empezamos desde el 0 (5*(1-1))
        // si estamos en la pagina 2 empezamos desde el 5 (5*(2-1))
        let elementoInicial = this.maxElementosPorPagina * (paginaActual-1);

        this.prepararBotonesPaginador(paginaActual,maxPages);

        this.bloqueBodyLista.innerHTML = "";

        // pintar elemntos acorde al maxElementosPorPagina
        // los elementos iran desde (maxElementosPorPagina* (paginaActual-1)) hasta this.maxElementosPorPagina*paginaActual
        for(let i = elementoInicial;i<(this.maxElementosPorPagina*paginaActual);i++){
            // si en una pagina no llega al maxElementosPorPagina hay que evitar que intente pintar esos
            // elementos y no de error
            if(i<sondas.length){
                let hilera = document.createElement("tr");

                let celdaNombre = document.createElement("td");
                let celdaLat = document.createElement("td");
                let celdaLng = document.createElement("td");
                let celdaOperacion = document.createElement("td");

                celdaOperacion.className = "list-opereciones";

                let textoCeldaNombre = document.createTextNode(sondas[i].nombre != null ? sondas[i].nombre : "Sin asignar");
                let textoCeldaLat = document.createTextNode(sondas[i].latitud != null ? sondas[i].latitud : "Sin asignar");
                let textoCeldaLng = document.createTextNode(sondas[i].longitud != null ? sondas[i].longitud : "Sin asignar");

                celdaNombre.appendChild(textoCeldaNombre);
                celdaLat.appendChild(textoCeldaLat);
                celdaLng.appendChild(textoCeldaLng);


                let btnEditar = document.createElement("button");
                btnEditar.className = "btn-list-element";
                btnEditar.onclick = () =>{this.controlador.onEditSonda(sondas[i])};

                let btnBorrar = document.createElement("button");
                btnBorrar.className = "btn-list-element";
                btnBorrar.onclick = () =>{this.controlador.onRemoveElementoLista(sondas[i])};

                let ul = document.createElement("ul");
                ul.className = "listaOperaciones";


                let li2 = document.createElement("li")
                btnEditar.innerHTML = `<i class="fas fa-pen-square"></i><p class="label">Modificar</p>`
                li2.appendChild(btnEditar);

                let li3 = document.createElement("li")
                btnBorrar.innerHTML = `<i class="far fa-trash-alt"></i><p class="label">Eliminar</p>`
                li3.appendChild(btnBorrar);

                ul.appendChild(li2)
                ul.appendChild(li3)

                this.createDropDown(celdaOperacion,sondas[i])
                celdaOperacion.appendChild(ul);

                // añadimos los elementos a la fila
                hilera.appendChild(celdaNombre);
                hilera.appendChild(celdaLat);
                hilera.appendChild(celdaLng);

                hilera.appendChild(celdaOperacion);


                // añadimos la fila al tbody
                this.bloqueBodyLista.appendChild(hilera);
            }


        }


    },


    createDropDown:function(celdaOperacion,sonda) {


        let btnEditar = document.createElement("button");
        btnEditar.className = "btn-list-element";
        btnEditar.onclick = () =>{this.controlador.onEditSonda(sonda)};

        let btnBorrar = document.createElement("button");
        btnBorrar.className = "btn-list-element";
        btnBorrar.onclick = () =>{this.controlador.onRemoveElementoLista(sonda)};

        let li2Drop = document.createElement("div")
        btnEditar.innerHTML = `<i class="fas fa-pen-square"></i><p>Modificar</p>`
        li2Drop.appendChild(btnEditar);
        let li3Drop = document.createElement("div")
        btnBorrar.innerHTML = `<i class="far fa-trash-alt"></i><p>Eliminar</p>`
        li3Drop.appendChild(btnBorrar);
        // añadimos el dropdown button que se mostrará en movil
        celdaOperacion.innerHTML = `<button class="btn btn-secondary dropdown-toggle tresPuntos"
                          type="button" id="dropdownMenu1" data-toggle="dropdown"
                          aria-haspopup="true" aria-expanded="false">
                   <i class="fas fa-ellipsis-v tresPuntos">
                  </button>
                  <div class="dropdown-menu" aria-labelledby="dropdownMenu1">
                    
                  </div>`
        celdaOperacion.querySelector("div.dropdown-menu").append(li2Drop)
        celdaOperacion.querySelector("div.dropdown-menu").append(li3Drop)
    },

    // habilitar y deshabilitar los botones que controlan la paginacion
    // para evitar errores
    prepararBotonesPaginador : function (paginaActual, maxPages) {
        this.btnNextPage.removeAttribute("disabled");
        this.btnPreviousPage.removeAttribute("disabled");
        document.getElementById("paginadorListaSonda").innerHTML = "Página "+paginaActual + " de " + maxPages;
        // si el maxPages es mayor que uno habilitar el next page
        if(maxPages>1){
            this.btnNextPage.removeAttribute("disabled");
        }
        // si el current page es igual al maxPages deshabilitar el next page
        if(paginaActual === maxPages){
            this.btnNextPage.setAttribute("disabled","disabled");
        }
        if(maxPages === 0){
            this.btnNextPage.setAttribute("disabled","disabled");
        }
        // si el current page es mayor que uno habilitar el previous page
        if(paginaActual > 1){
            this.btnPreviousPage.removeAttribute("disabled");
        }else{
            this.btnPreviousPage.setAttribute("disabled","disabled");
        }

    },

    cambiarFlechasOrden(ordenNombre, ordenLat, ordenLng) {
        document.getElementById("flecha-nombre-sonda").className = (!ordenNombre) ? "fas fa-chevron-down" : "fas fa-chevron-up";
        document.getElementById("flecha-lat-sonda").className = (!ordenLat) ? "fas fa-chevron-down" : "fas fa-chevron-up";
        document.getElementById("flecha-lng-sonda").className = (!ordenLng) ? "fas fa-chevron-down" : "fas fa-chevron-up";

    },

};

var ControladorListaSondas = {
    modelo: ModeloListaSondas,
    vista: VistaListaSondas,
    controladorModificarSonda: null,
    controladorMapaTecnico: null,

    error: false,// controla si ha habido un error en la obtencio de datos

    iniciar: function (parcela,controladorModificarSonda,controladorMapaTecnico) {
        this.modelo.controlador = this;
        this.modelo.idCampo = parcela.campo;
        this.modelo.idParcela = parcela.id;
        this.vista.controlador = this;
        this.vista.mostrarCarga();
        this.modelo.obtenerDatos();
        this.controladorModificarSonda = controladorModificarSonda;
        this.controladorMapaTecnico = controladorMapaTecnico;
    },

    cargarMapa:function(datos){
        this.controladorMapaTecnico.iniciar(this.vista.parcela,datos)
    },
    representarDatos: function(datos,paginaActual){
        this.vista.representarDatos(datos,paginaActual);
        this.vista.mostrarLista();
    },

    cambiarFlechasOrden(ordenNombre, ordenLat, ordenLng) {
        this.vista.cambiarFlechasOrden(ordenNombre,ordenLat,ordenLng)
    },

    representarError: function () {
        this.vista.mostrarError();
    },


    onRemoveElementoLista:function (element) {
        if(confirm("Se borrará para siempre la sonda "+element.nombre+"\n¿Estás seguro?")){
            this.vista.mostrarCarga();
            this.modelo.borrarElemento(element.id);
        }
    },
    onEditSonda:function(elemento){
        this.controladorModificarSonda.iniciar(elemento,this.modelo.idParcela)
    },
    onCrearSondas: function () {
        let elemento = {};
        elemento["id"] = this.vista.parcela.id;
        localStorage.setItem("listaSondas-sondas",JSON.stringify(elemento));
        location.href = "crear-sondas.html";
    },

    borradoOk:function () {
        window.location.reload();
    }
}