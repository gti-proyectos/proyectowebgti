/**
 * Autor: Rubén Pardo Casanova
 * GTI GRUPO 11
 * Fecha: 05/05/2020
 *
 * Modelo vista controlador que gestiona la lista de clientes
 *
 */

var ModeloListaClientes = {

    controlador: {},

    todosLosClientes:{},//contendrá todos los clientes siempre
    clientesFiltrados:{},// contendrá los clientes filtrados cuando se use el buscador o el ordenar

    paginaActual: 1,

    // al principio no estan ordenados
    // true = ASC, false = DESC
    ordenNombre: false,
    ordenApellido: false,
    ordenCorreo: false,

    obtenerDatos: async function () {

        try{
            let clientes = await fetch('../../src/api/v1.0/cliente')
                .then(respuesta =>{
                    if(respuesta.status === 200){
                        return respuesta.json();
                    }else{
                        this.controlador.error = true;
                        throw Error(respuesta.statusText);
                    }
                });

            this.todosLosClientes = clientes;
            this.clientesFiltrados = clientes;
            this.controlador.representarDatos(this.clientesFiltrados,this.paginaActual);


        }catch (e) {
            console.error(e);
            this.controlador.representarError();
        }
    },

    borrarElemento: async function (idCliente) {

        try{

            await fetch('../../src/api/v1.0/cliente', {
                method: 'DELETE', // or 'PUT'
                body: JSON.stringify({id:idCliente}) // data can be `string` or {object}!

            }).then(res => res.json())
                .catch(error => {throw Error(error)})
                .then(response => console.log('Success:', response));



            // si sale bien borramos el elemento en local y nos evitamos el tener que refrescar
            // encontramos el elemento
            let clienteABorrar = this.todosLosClientes.find(e=>{
                if(e.id === idCliente){
                    return e;
                }
            });
            if(clienteABorrar!=null){
                // lo borramos del array local
                this.todosLosClientes.splice(this.todosLosClientes.indexOf(clienteABorrar),1);
            }

            // volvemos a la pagina principal y pintamos de nuevo los datos
            this.paginaActual = 1;
            this.controlador.representarDatos(this.clientesFiltrados,this.paginaActual);

        }catch (e) {
            console.error(e);
            this.controlador.representarError();
        }
    },

    // filtrar los clientes tanto por nombre correo o telefono
    filtrarPorNombreApellidoCorreo: function (textoAFiltrar) {
        // si no hay nada escrito poner todos los clientes
        if(textoAFiltrar === ""){
            this.clientesFiltrados = this.todosLosClientes;
        }else{
            this.clientesFiltrados = this.todosLosClientes.filter(cliente =>{
                if(cliente.nombre!=null && cliente.nombre.toLowerCase().includes(textoAFiltrar.toLowerCase())){
                    return cliente;
                }else if(cliente.apellidos!=null && cliente.apellidos.toLowerCase().includes(textoAFiltrar.toLowerCase())){
                    return cliente;
                }else if(cliente.correo!=null && cliente.correo.toLowerCase().includes(textoAFiltrar.toLowerCase())){
                    return cliente;
                }
            })


        }
        // siempre que filtremos volvamos a la pagina 1
        this.paginaActual = 1;
        this.controlador.representarDatos(this.clientesFiltrados,this.paginaActual);
    },

    // idOrden -> 1 = Nombre, 2 = Apellido, 3 = Correo
    // ascendente -> true = ordenar asc, false = ordenar desc
    ordenarPor:function (idOrden) {
        //ocultamos todas las flechas
        document.getElementById('flecha-nombre-cliente').style.display="none";
        document.getElementById('flecha-apellido-cliente').style.display="none";
        document.getElementById('flecha-correo-cliente').style.display="none";
        let filtro = "";
        let ordenASC = null;

        // determinar el tipo de filtro y el orden ASC o DESC
        if(idOrden===1){
            //aparece la flecha
            document.getElementById('flecha-nombre-cliente').style.display="inline";
            filtro = "nombre"
            // ordenar por nombre
            // cade vez que se pulse de mas cambiarlo
            this.ordenNombre = !this.ordenNombre;
            ordenASC = this.ordenNombre;
        }else{
            this.ordenNombre = false;
        }
        if(idOrden === 2){
            //aparece la flecha
            document.getElementById('flecha-apellido-cliente').style.display="inline";
            filtro = "apellidos";
            // ordenar por correo

            // cade vez que se pulse de mas cambiarlo
            this.ordenApellido = !this.ordenApellido;
            ordenASC = this.ordenApellido;
        }else{
            this.ordenApellido = false;
        }
        if(idOrden===3){
            //aparece la flecha
            document.getElementById('flecha-correo-cliente').style.display="inline";
            filtro = "correo"
            // ordenar por telefono
            // cade vez que se pulse de mas cambiarlo
            this.ordenCorreo = !this.ordenCorreo;

            ordenASC = this.ordenCorreo;
        }else{
            this.ordenCorreo = false;
        }
        // ordenamos siempre desde clientes filtrados
        // al inicio seran todos y si hay algun filtro en el buscador se
        // ordenará sobre este
       this.clientesFiltrados = this.clientesFiltrados.sort((cliente1,cliente2) =>{
           // juntar todos los nulls o arriba o abajo
           if(cliente1[filtro] == null){
               return ordenASC ? 1 : -1;
           }else if(cliente2[filtro] == null ){
               return ordenASC ? -1 : 1;
           }else {
               // ordenar
               if (cliente1[filtro].toLowerCase() > cliente2[filtro].toLowerCase()) {
                   return ordenASC ? 1 : -1;
               } else if (cliente1[filtro].toLowerCase() < cliente2[filtro].toLowerCase()) {
                   return ordenASC ? -1 : 1;
               }
           }
           return 0;
       })

        // simepre que ordenemos volvemos a la pagina 1
        this.paginaActual = 1
        this.controlador.cambiarFlechasOrden(this.ordenNombre,this.ordenApellido,this.ordenCorreo);
        this.controlador.representarDatos(this.clientesFiltrados,this.paginaActual);
    },

    anteriorPagina:function () {
        this.paginaActual--;
        this.controlador.representarDatos(this.clientesFiltrados,this.paginaActual);
    },

    siguientePagina: function () {
        this.paginaActual++;
        this.controlador.representarDatos(this.clientesFiltrados,this.paginaActual);
    }

};

var VistaListaClientes = {

    controlador: {},

    bloqueLista:{}, // donde se representarán los elementos de la lista
    paginaActual: 1,// indice para controlar la paginacion
    maxElementosPorPagina: 5,
    bloqueError:{},
    bloqueCarga:{},
    bloqueBodyLista:{},

    // botones para el control de la paginacion de clientes
    btnNextPage:{},
    btnPreviousPage:{},

    // booleano que controla si se tiene que mostrar en primera instancia
    mostrar: false,


    preparar:function (idBloqueLista, idListaBody, idError, idCarga, idBtnNext, idBtnPrev,mostrar) {
        this.bloqueLista = document.getElementById(idBloqueLista);
        this.bloqueBodyLista = document.getElementById(idListaBody);
        this.bloqueError = document.getElementById(idError);
        this.bloqueCarga = document.getElementById(idCarga);
        this.btnPreviousPage = document.getElementById(idBtnPrev);
        this.btnNextPage = document.getElementById(idBtnNext);

        this.mostrar = mostrar;
    },

    // funciones para controlar cuando se muestran los bloques html
    esconderElementos: function(){
        this.bloqueLista.style.display = "none";
        this.bloqueCarga.style.display = "none";
        this.bloqueError.style.display = "none";
    },
    mostrarCarga: function () {
        if(this.mostrar) {
            this.esconderElementos();
            this.bloqueCarga.style.display = "flex"
        }
    },
    mostrarError:function () {
        this.esconderElementos();
        this.bloqueError.style.display = "flex"
    },
    mostrarLista:function () {
        if(this.mostrar) {
            this.esconderElementos();
            if(this.controlador.error){
                this.bloqueError.style.display = "block"
            }else{
                this.bloqueLista.style.display = "flex"
            }
        }else{
            this.bloqueLista.style.display = "none"
        }
    },

    // representar elementos en el html
    representarDatos:function (clientes,paginaActual) {
        //control de paginacion
        // maxPages = length/maxElementosPorPagina redondeado hacia arriba
        let maxPages = Math.ceil(clientes.length/this.maxElementosPorPagina);
        // si estamos en la pagina 1 empezamos desde el 0 (5*(1-1))
        // si estamos en la pagina 2 empezamos desde el 5 (5*(2-1))
        let elementoInicial = this.maxElementosPorPagina * (paginaActual-1);

        this.prepararBotonesPaginador(paginaActual,maxPages);

        this.bloqueBodyLista.innerHTML = "";

        // pintar elemntos acorde al maxElementosPorPagina
        // los elementos iran desde (maxElementosPorPagina* (paginaActual-1)) hasta this.maxElementosPorPagina*paginaActual
        for(let i = elementoInicial;i<(this.maxElementosPorPagina*paginaActual);i++){
            // si en una pagina no llega al maxElementosPorPagina hay que evitar que intente pintar esos
            // elementos y no de error
           if(i<clientes.length){
               let hilera = document.createElement("tr");

               let celdaNombre = document.createElement("td");
               let celdaApellido = document.createElement("td");
               let celdaCorreo = document.createElement("td");
               let celdaOperacion = document.createElement("td");
               celdaOperacion.className = "list-opereciones";

               let textoCeldaNombre = document.createTextNode(clientes[i].nombre != null ? clientes[i].nombre : "Sin asignar");
               let textoCeldaApellido = document.createTextNode(clientes[i].apellidos != null ? clientes[i].apellidos : "Sin asignar");
               let textoCeldaCorreo = document.createTextNode(clientes[i].correo != null ? clientes[i].correo : "Sin asignar");


               celdaNombre.appendChild(textoCeldaNombre);
               celdaApellido.appendChild(textoCeldaApellido);
               celdaCorreo.appendChild(textoCeldaCorreo);


               // añadimos los botones a todas las listas
               let btnRead = document.createElement("button");
               btnRead.className = "btn-list-element";
               btnRead.onclick = () =>{this.controlador.onClickElementoLista(clientes[i])};

               let btnEdit = document.createElement("button");
               btnEdit.className = "btn-list-element";
               btnEdit.onclick = () =>{this.controlador.onEditElementoLista(clientes[i])};

               let btnRemove = document.createElement("button");
               btnRemove.className = "btn-list-element";
               btnRemove.onclick = () =>{this.controlador.onRemoveElementoLista(clientes[i])};


               let ul = document.createElement("ul");
               ul.className = "listaOperaciones";


               let li = document.createElement("li")
               btnRead.innerHTML = `<i class="far fa-eye"></i><p class="label">Ver detalles</p>`
               li.appendChild(btnRead);

               let li2 = document.createElement("li")
               btnEdit.innerHTML = `<i class="fas fa-pen-square"></i><p class="label">Modificar</p>`
               li2.appendChild(btnEdit);

               let li3 = document.createElement("li")
               btnRemove.innerHTML = `<i class="far fa-trash-alt"></i><p class="label">Eliminar</p>`
               li3.appendChild(btnRemove);

               ul.appendChild(li)
               ul.appendChild(li2)
               ul.appendChild(li3)


               // añadimos el dropdown button que se mostrará en movil
               this.createDropDown(celdaOperacion,clientes[i])
               // añadimos la lista que se mostrará a partir de tablet
               celdaOperacion.appendChild(ul)


               // añadimos los elementos a la fila
               hilera.appendChild(celdaNombre);
               hilera.appendChild(celdaApellido);
               hilera.appendChild(celdaCorreo);
               hilera.appendChild(celdaOperacion)


               // añadimos la fila al tbody
               this.bloqueBodyLista.appendChild(hilera);
           }
        }
    },

    createDropDown:function(celdaOperacion,cliente) {
        let btnRead = document.createElement("button");
        btnRead.className = "btn-list-element";
        btnRead.onclick = () =>{this.controlador.onClickElementoLista(cliente)};

        let btnEdit = document.createElement("button");
        btnEdit.className = "btn-list-element";
        btnEdit.onclick = () =>{this.controlador.onEditElementoLista(cliente)};

        let btnRemove = document.createElement("button");
        btnRemove.className = "btn-list-element";
        btnRemove.onclick = () =>{this.controlador.onRemoveElementoLista(cliente)};

        let liDrop = document.createElement("div")
        btnRead.innerHTML = `<i class="fas fa-eye"></i><p>Ver detalles</p>`
        liDrop.appendChild(btnRead);
        let li2Drop = document.createElement("div")
        btnEdit.innerHTML = `<i class="fas fa-pen-square"></i><p>Modificar</p>`
        li2Drop.appendChild(btnEdit);
        let li3Drop = document.createElement("div")
        btnRemove.innerHTML = `<i class="far fa-trash-alt"></i><p>Eliminar</p>`
        li3Drop.appendChild(btnRemove);
        // añadimos el dropdown button que se mostrará en movil
        celdaOperacion.innerHTML = `<button class="btn btn-secondary dropdown-toggle tresPuntos"
                          type="button" id="dropdownMenu1" data-toggle="dropdown"
                          aria-haspopup="true" aria-expanded="false">
                   <i class="fas fa-ellipsis-v tresPuntos">
                  </button>
                  <div class="dropdown-menu" aria-labelledby="dropdownMenu1">
                    
                  </div>`
        celdaOperacion.querySelector("div.dropdown-menu").append(liDrop)
        celdaOperacion.querySelector("div.dropdown-menu").append(li2Drop)
        celdaOperacion.querySelector("div.dropdown-menu").append(li3Drop)
    },


    // habilitar y deshabilitar los botones que controlan la paginacion
    // para evitar errores
    prepararBotonesPaginador : function (paginaActual, maxPages) {
        this.btnNextPage.removeAttribute("disabled");
        this.btnPreviousPage.removeAttribute("disabled");

        document.getElementById("paginadorListaCliente").innerHTML = "Página "+paginaActual + " de " + maxPages;

        // si el maxPages es mayor que uno habilitar el next page
        if(maxPages>1){
            this.btnNextPage.removeAttribute("disabled");
        }
        // si el current page es igual al maxPages deshabilitar el next page
        if(paginaActual === maxPages){
            this.btnNextPage.setAttribute("disabled","disabled");
        }
        if(maxPages === 0){
            this.btnNextPage.setAttribute("disabled","disabled");
        }
        // si el current page es mayor que uno habilitar el previous page
        if(paginaActual > 1){
            this.btnPreviousPage.removeAttribute("disabled");
        }else{
            this.btnPreviousPage.setAttribute("disabled","disabled");
        }

    },


    cambiarFlechasOrden(ordenNombre, ordenApellido, ordenCorreo) {
        document.getElementById("flecha-nombre-cliente").className = (!ordenNombre) ? "fas fa-chevron-down" : "fas fa-chevron-up";
        document.getElementById("flecha-apellido-cliente").className = (!ordenApellido) ? "fas fa-chevron-down" : "fas fa-chevron-up";
        document.getElementById("flecha-correo-cliente").className = (!ordenCorreo) ? "fas fa-chevron-down" : "fas fa-chevron-up";
    },
};

var ControladorListaClientes = {
    modelo: ModeloListaClientes,
    vista: VistaListaClientes,
    controladorCreateCliente:  null,
    controladorModificarCliente: null,
    error: false,// controla si ha habido un error en la obtencio de datos

    iniciar: function (controladorCreateCliente, controladorModificarCliente) {
        this.modelo.controlador = this;
        this.vista.controlador = this;
        this.controladorCreateCliente = controladorCreateCliente;
        this.vista.mostrarCarga();
        this.modelo.obtenerDatos();
        this.controladorModificarCliente = controladorModificarCliente;
    },


    cambiarFlechasOrden(ordenNombre, ordenApellido, ordenCorreo) {
        this.vista.cambiarFlechasOrden(ordenNombre,ordenApellido,ordenCorreo)
    },

    representarDatos: function(datos,paginaActual){
        this.vista.representarDatos(datos,paginaActual);
        this.vista.mostrarLista();
    },
    mostrarTabla: function(){
        this.vista.mostrarLista();
    },
    representarError: function () {
        this.vista.mostrarError();
    },

    // eventos on click de los elementos de la lista
    // el on click y el on edit redirecciona a la siguiente pagina con un flag de editar
    // el on delete confirma la operacion y lanza la peticion al server en caso afirmativo
    onClickElementoLista:   function (elemento) {
        elemento['modificar'] = false;
        localStorage.setItem("listaCliente-cliente",JSON.stringify(elemento));
        location.href = "panel-admin-cliente.html";
    },
    onEditElementoLista: function (elemento) {
        this.controladorModificarCliente.iniciar(elemento);
    },
    onRemoveElementoLista: function (elemento) {
        // confimar borrado
        if(confirm("Se borrará para siempre el cliente "+elemento.nombre+"\n¿Estás seguro?")){
            this.vista.mostrarCarga();
            this.modelo.borrarElemento(elemento.id);
        }

    },

    onCreateCliente: function () {
        this.controladorCreateCliente.iniciar()
    },
}