/**
 Autor: Rubén Pardo
 Fecha: 25/05/2020
 Descripcion: lógica genrald el archivo detalles-temporada.html

 */
let temporada = JSON.parse(localStorage.getItem("lista-temporada-temporada"));

document.getElementById("header-titulo").innerHTML = temporada.nombre
document.getElementById("header-subtitulo").innerHTML = "Fecha Inicio: " + temporada.fechaInicio

vistaFacturacion.preparar("lista-facturas","lista-facturas-body","error","carga","btnNextCampos","btnPrevCampos", true);
controladorFacturacion.iniciar(temporada.id)


//Modal de finalizar temporada
VistaFinalizarTemporada.preparar("modal-error-finalizar-temporada","formularioFinalizarTemporada","modal-finalizar-temporada",)

// modal crear factura
VistaCrearFactura.preparar("modal-error-crear-factura","formularioModalCrearFactura","modal-crear-factura",temporada)

//si la temporada esta finalizada no mostrar los botones flotantes de añadir ni editar
if(temporada.terminada === 1){
    let btnsflotantes = document.getElementsByClassName("tab-fab");
    for(let i=0;i<btnsflotantes.length;i++){
        btnsflotantes[i].style.display = 'none';
    }

    // ocultar el boton de finalizar    
    document.getElementById("btn-finalizar-temporada").style.display = "none";
}

VistaModificarTemporada.preparar("btn-editar-detalles-temporada","btn-guardar-detalles-temporada",
    "btn-cancelar-detalles-temporada","botones-detalles-temporada-confirmar",
    "error-titulo",temporada,"titulo-temporada-con-text","titulo-temporada-sin-text",
    "desc-temporada-con-text","desc-temporada-sin-text","error-modifcar-temporada")

ControladorModificarTemporada.iniciar();



// =================================
// setear el valores en la info de temporada
// ====================================
document.getElementById("titulo-temporada-sin-text").innerText = temporada.nombre;
document.getElementById("desc-temporada-sin-text").innerText = temporada.descripcion !== null ? temporada.descripcion : "No hay descripción";
document.getElementById("fecha-inicio").innerText = temporada.fechaInicio;
document.getElementById("fecha-fin").innerText = temporada.fechaFin !== null ? temporada.fechaFin : "Activa";

let parcelas = JSON.parse(localStorage.getItem("parcelas-detallesCampo"))[0];
//rellenar el select de parcelas del modal de crear temporadas
let htmlSelect = "";
let isPrimero = true;
for(let i = 0;i<parcelas.length;i++){
    let isValid = false;
    temporada.parcelas.forEach(idParcela=>{
        if(idParcela === parcelas[i].id){
            isValid = true;
        }
    })
    if(isValid){
        if(isPrimero){
            htmlSelect+=parcelas[i].nombre;
            isPrimero = false;
        }else{
            htmlSelect+=", "+parcelas[i].nombre;
        }
    }
}
document.getElementById("parcelas-content").innerHTML += htmlSelect;






//-------------------------------------
// N,N -> getRandomColor() -> Color
// recibe cuantos colors diferentes hay en ese
// momento se quieren y en cual esta
// calcula colors random distintos entre sí
//-------------------------------------
function getRandomColor(colorNum, colors) {

    if (colors < 1) colors = 1;
    // defaults to one color - avoid divide by zero
    return colorNum * (360 / colors) % 360;

}

// elementoPulsado -> () ->
// callback de las graficas para cambiarlas de la disposicion
function SwapWithFirst (elementoPulsado) {
    var container = document.getElementById ("container-graficas");
    var elementoPrincipal = container.getElementsByClassName("grafica")[0];
    if (elementoPulsado != elementoPrincipal) {
        // la pulsada pasará a ser la primera
        elementoPulsado.className = "";
        elementoPulsado.classList.add("principal")
        elementoPulsado.classList.add("grafica")
        elementoPulsado.classList.add("col-12")


        // la que esta la primera pasará a ser donde la pulsada
        elementoPrincipal.className = "";
        elementoPrincipal.classList.add("segunda")
        elementoPrincipal.classList.add("grafica")
        elementoPrincipal.classList.add("col-6")
        elementoPrincipal.classList.add("col-md-5")
        elementoPrincipal.classList.add("col-lg")


        controladorFacturacion.esconderOMostrarLabelsSegunPulsado(elementoPrincipal.querySelector("canvas").id,true)
        controladorFacturacion.esconderOMostrarLabelsSegunPulsado(elementoPulsado.querySelector("canvas").id,false)

        // poner el on click a la que era la principal
        elementoPrincipal.onclick = () =>{SwapWithFirst(elementoPrincipal);}
        // quitar el on click a la que va a ser la principal para que se pueda pulsar la gráfica
        elementoPulsado.onclick = null;

        if (elementoPulsado.swapNode) {
            elementoPulsado.swapNode (elementoPrincipal);
        }
        else {
            var next = elementoPulsado.nextSibling;
            container.replaceChild (elementoPulsado, elementoPrincipal);
            container.querySelector("#secundarios").insertBefore (elementoPrincipal, next);

        }
    }
}

// callback del nav tabs
// cambia el contenido principal segun pulsado
// id de los elementos
//id="nav-item-graph", id="nav-item-detalles", id="nav-item-historial"
// id de los tabs
// tab-grafica, tab-info, tab-historial
function navPulsado(elementoPulsado) {
    // primero cambiamos el estilo de los navs
    // quitarle el active de los elementos
    document.querySelectorAll("ul.nav-tabs .nav-link").forEach(elemento=>{
        elemento.classList.remove("active")
    })
    // ponerle active al pulsado
    elementoPulsado.classList.add("active")

    // segundo cambiamos el contenido
    // escondemos todos
    document.querySelectorAll(".contendor-principal div.tab-item-contenido ").forEach(elemento=>{
        // comprobamos cual se ha pulsado y mostramos su contenido
        if(elementoPulsado.id === "nav-item-graph" && elemento.id === "tab-grafica"){
            elemento.style.display = "block"
        }else if(elementoPulsado.id === "nav-item-detalles" && elemento.id === "tab-info"){
            elemento.style.display = "block"
        }else if(elementoPulsado.id === "nav-item-historial" && elemento.id === "tab-historial"){
            elemento.style.display = "block"
        }else{
            // si no es el que se ha pulsado lo escondemos
            elemento.style.display = "none"
        }

    })



}

// String -> () -> String
// Se recibe del tipo 01,02
// Y devuelve Enero, Febrero,..
function getStringMesFromNumber(mes){
    switch (mes) {
        case '01':
            return "Enero";
        case '02':
            return "Febrero";
        case '03':
            return "Marzo";
        case '04':
            return "Abril";
        case '05':
            return "Mayo";
        case '06':
            return "Junio";
        case '07':
            return "Julio";
        case '08':
            return "Agosto";
        case '09':
            return "Septiembre";
        case '10':
            return "Octubre";
        case '11':
            return "Noviembre";
        case '12':
            return "Diciembre";
    }
}

//Añadimos el servicio para que se pueda meter texto dentro del donut chart
Chart.pluginService.register({
    beforeDraw: function(chart) {
        if (chart.config.options.elements.center) {
            // Get ctx from string
            var ctx = chart.chart.ctx;

            // Get options from the center object in options
            var centerConfig = chart.config.options.elements.center;
            var fontStyle = centerConfig.fontStyle || 'Arial';
            var txt = centerConfig.text;
            var color = centerConfig.color || '#000';
            var maxFontSize = centerConfig.maxFontSize || 75;
            var sidePadding = centerConfig.sidePadding || 20;
            var sidePaddingCalculated = (sidePadding / 100) * (chart.innerRadius * 2)
            // Start with a base font of 30px
            ctx.font = "30px " + fontStyle;

            // Get the width of the string and also the width of the element minus 10 to give it 5px side padding
            var stringWidth = ctx.measureText(txt).width;
            var elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;

            // Find out how much the font can grow in width.
            var widthRatio = elementWidth / stringWidth;
            var newFontSize = Math.floor(30 * widthRatio);
            var elementHeight = (chart.innerRadius * 2);

            // Pick a new font size so it will not be larger than the height of label.
            var fontSizeToUse = Math.min(newFontSize, elementHeight, maxFontSize);
            var minFontSize = centerConfig.minFontSize;
            var lineHeight = centerConfig.lineHeight || 25;
            var wrapText = false;

            if (minFontSize === undefined) {
                minFontSize = 20;
            }

            if (minFontSize && fontSizeToUse < minFontSize) {
                fontSizeToUse = minFontSize;
                wrapText = true;
            }

            // Set font settings to draw it correctly.
            ctx.textAlign = 'center';
            ctx.textBaseline = 'middle';
            var centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
            var centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
            ctx.font = fontSizeToUse + "px " + fontStyle;
            ctx.fillStyle = color;

            if (!wrapText) {
                ctx.fillText(txt, centerX, centerY);
                return;
            }

            var words = txt.split(' ');
            var line = '';
            var lines = [];

            // Break words up into multiple lines if necessary
            for (var n = 0; n < words.length; n++) {
                var testLine = line + words[n] + ' ';
                var metrics = ctx.measureText(testLine);
                var testWidth = metrics.width;
                if (testWidth > elementWidth && n > 0) {
                    lines.push(line);
                    line = words[n] + ' ';
                } else {
                    line = testLine;
                }
            }

            // Move the center up depending on line height and number of lines
            centerY -= (lines.length / 2) * lineHeight;

            for (var n = 0; n < lines.length; n++) {
                ctx.fillText(lines[n], centerX, centerY);
                centerY += lineHeight;
            }
            //Draw text in center
            ctx.fillText(line, centerX, centerY);
        }
    }
});

// DATE -> () -> YYYY-MM-DD
function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [year, month, day].join('-');
}

