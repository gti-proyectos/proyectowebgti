/**
 * Autor: Rubén Pardo Casanova
 * GTI GRUPO 11
 * Fecha: 06/05/2020
 *
 * Modelo vista controlador que gestiona la lista de los usuarios de la emrpesa admin y tecnicos
 *
 */

var ModeloListaUsuarios = {

    controlador: {},
    cliente: null,

    todosLosUsuarios:{},//contendrá todos los clientes siempre
    usuariosFiltrados:{},// contendrá los clientes filtrados cuando se use el buscador o el ordenar

    paginaActual: 1,

    // al principio no estan ordenados
    // true = ASC, false = DESC
    ordenNombre: false,
    ordenApellido: false,
    ordenCorreo: false,

    obtenerDatos: async function () {


        try{
            let admins = await fetch('../../src/api/v1.0/usuario?rol=1')
                .then(respuesta =>{
                    if(respuesta.status === 200){
                        return respuesta.json();
                    }else{
                        this.controlador.error = true;
                        throw Error(respuesta.statusText);
                    }
                });

            let tecnicos = await fetch('../../src/api/v1.0/usuario?rol=2')
                .then(respuesta =>{
                    if(respuesta.status === 200){
                        return respuesta.json();
                    }else{
                        this.controlador.error = true;
                        throw Error(respuesta.statusText);
                    }
                });

            this.todosLosUsuarios = admins.concat(tecnicos);// juntamos los dos arrays en uno
            this.usuariosFiltrados = this.todosLosUsuarios;
            this.controlador.representarDatos(this.usuariosFiltrados,this.paginaActual);


        }catch (e) {
            console.error(e);
            this.controlador.representarError();
        }
    },

    borrarElemento: async function (id) {

        try{

            await fetch('../../src/api/v1.0/usuario', {
                method: 'DELETE', // or 'PUT'
                body: JSON.stringify({id:id}) // data can be `string` or {object}!

            }).then(res => res.json())
                .catch(error => {throw Error(error)})
                .then(response => console.log('Success:', response));



            // si sale bien borramos el elemento en local y nos evitamos el tener que refrescar
            // encontramos el elemento
            let clienteABorrar = this.todosLosUsuarios.find(e=>{
                if(e.id === id){
                    return e;
                }
            });
            if(clienteABorrar!=null){
                // lo borramos del array local
                this.todosLosUsuarios.splice(this.todosLosUsuarios.indexOf(clienteABorrar),1);
            }

            // volvemos a la pagina principal y pintamos de nuevo los datos
            this.paginaActual = 1;
            this.controlador.representarDatos(this.usuariosFiltrados,this.paginaActual);

        }catch (e) {
            console.error(e);
            this.controlador.representarError();
        }
    },

    // filtrar los clientes tanto por nombre correo o telefono
    filtrarPorNombreApellidoCorreo: function (textoAFiltrar) {
        // si no hay nada escrito poner todos los clientes
        if(textoAFiltrar === ""){
            this.usuariosFiltrados = this.todosLosUsuarios;
        }else{
            this.usuariosFiltrados = this.todosLosUsuarios.filter(usuario =>{
                if(usuario.nombre!=null && usuario.nombre.toLowerCase().includes(textoAFiltrar.toLowerCase())){
                    return usuario;
                }else if(usuario.apellidos!=null &&  usuario.apellidos.toLowerCase().includes(textoAFiltrar.toLowerCase())){
                    return usuario;
                }else if(usuario.correo!=null && usuario.correo.toLowerCase().includes(textoAFiltrar.toLowerCase())){
                    return usuario;
                }
            })


        }
        // siempre que filtremos volvamos a la pagina 1
        this.paginaActual = 1;
        this.controlador.representarDatos(this.usuariosFiltrados,this.paginaActual);
    },

    // idOrden -> 1 = Nombre, 2 = Correo, 3 = telefono
    // ascendente -> true = ordenar asc, false = ordenar desc
    ordenarPor:function (idOrden) {
        //ocultamos todas las flechas
        document.getElementById('flecha-nombre-usuario').style.display="none";
        document.getElementById('flecha-apellido-usuario').style.display="none";
        document.getElementById('flecha-correo-usuario').style.display="none";
        let filtro = "";
        let ordenASC = null;

        // determinar el tipo de filtro y el orden ASC o DESC
        if(idOrden===1){
            //aparece la flecha
            document.getElementById('flecha-nombre-usuario').style.display="inline";
            filtro = "nombre"
            // ordenar por nombre
            if(this.ordenNombre == null){
                // la primera vez ordenar ascendentemente
                this.ordenNombre = true;
            }else{
                // cade vez que se pulse de mas cambiarlo
                this.ordenNombre = !this.ordenNombre;
            }
            ordenASC = this.ordenNombre;
        }else{
            this.ordenNombre = false;
        }
        if(idOrden === 2){
            //aparece la flecha
            document.getElementById('flecha-apellido-usuario').style.display="inline";

            filtro = "apellidos";
            // ordenar por apellido
            if(this.ordenApellido == null){
                // la primera vez ordenar ascendentemente
                this.ordenApellido = true;
            }else{
                // cade vez que se pulse de mas cambiarlo
                this.ordenApellido = !this.ordenApellido;
            }
            ordenASC = this.ordenApellido;
        }else{
            this.ordenApellido = false;
        }
        if(idOrden === 3){
            //aparece la flecha
            document.getElementById('flecha-correo-usuario').style.display="inline";

            filtro = "correo"
            // ordenar por correo
            if(this.ordenCorreo == null){
                // la primera vez ordenar ascendentemente
                this.ordenCorreo = true;
            }else{
                // cade vez que se pulse de mas cambiarlo
                this.ordenCorreo = !this.ordenCorreo;
            }
            ordenASC = this.ordenCorreo;
        }else{
            this.ordenCorreo = false;
        }
        // ordenamos siempre desde clientes filtrados
        // al inicio seran todos y si hay algun filtro en el buscador se
        // ordenará sobre este
        this.usuariosFiltrados = this.usuariosFiltrados.sort((usuario1, usuario2) =>{
            // juntar todos los nulls o arriba o abajo
            if(usuario1[filtro] == null){
                return ordenASC ? 1 : -1;
            }else if(usuario2[filtro] == null ){
                return ordenASC ? -1 : 1;
            }else {
                // ordenar
                if (usuario1[filtro].toLowerCase() > usuario2[filtro].toLowerCase()) {
                    return ordenASC ? 1 : -1;
                } else if (usuario1[filtro].toLowerCase() < usuario2[filtro].toLowerCase()) {
                    return ordenASC ? -1 : 1;
                }
            }
            return 0;
        })

        // simepre que ordenemos volvemos a la pagina 1
        this.paginaActual = 1
        this.controlador.cambiarFlechasOrden(this.ordenNombre,this.ordenApellido,this.ordenCorreo);
        this.controlador.representarDatos(this.usuariosFiltrados,this.paginaActual);
    },

    anteriorPagina:function () {
        this.paginaActual--;
        this.controlador.representarDatos(this.usuariosFiltrados,this.paginaActual);
    },

    siguientePagina: function () {
        this.paginaActual++;
        this.controlador.representarDatos(this.usuariosFiltrados,this.paginaActual);
    }

};

var VistaListaUsuarios = {

    controlador: {},

    bloqueLista:{}, // donde se representarán los elementos de la lista
    paginaActual: 1,// indice para controlar la paginacion
    maxElementosPorPagina: 5,
    bloqueError:{},
    bloqueCarga:{},
    bloqueBodyLista:{},

    // botones para el control de la paginacion de clientes
    btnNextPage:{},
    btnPreviousPage:{},

    // booleano que controla si se tiene que mostrar en primera instancia
    mostrar: false,


    preparar:function (idBloqueLista, idListaBody, idError, idCarga, idBtnNext, idBtnPrev,mostrar) {
        this.bloqueLista = document.getElementById(idBloqueLista);
        this.bloqueBodyLista = document.getElementById(idListaBody);
        this.bloqueError = document.getElementById(idError);
        this.bloqueCarga = document.getElementById(idCarga);
        this.btnPreviousPage = document.getElementById(idBtnPrev);
        this.btnNextPage = document.getElementById(idBtnNext);

        this.mostrar = mostrar;
    },

    // funciones para controlar cuando se muestran los bloques html
    esconderElementos: function(){

        this.bloqueLista.style.display = "none";
        this.bloqueCarga.style.display = "none";
        this.bloqueError.style.display = "none";
    },
    mostrarCarga: function () {
        if(this.mostrar) {
            this.esconderElementos();
            this.bloqueCarga.style.display = "flex"
        }
    },
    mostrarError:function () {
        this.esconderElementos();
        this.bloqueError.style.display = "flex"

    },
    mostrarLista:function () {
        if(this.mostrar) {
            this.esconderElementos();
            if(this.controlador.error){
                this.bloqueError.style.display = "flex"
            }else{
                this.bloqueLista.style.display = "flex"
            }
        }
    },

    // representar elementos en el html
    representarDatos:function (usuarios,paginaActual) {
        //control de paginacion
        // maxPages = length/maxElementosPorPagina redondeado hacia arriba
        let maxPages = Math.ceil(usuarios.length/this.maxElementosPorPagina);
        // si estamos en la pagina 1 empezamos desde el 0 (5*(1-1))
        // si estamos en la pagina 2 empezamos desde el 5 (5*(2-1))
        let elementoInicial = this.maxElementosPorPagina * (paginaActual-1);

        this.prepararBotonesPaginador(paginaActual,maxPages);

        this.bloqueBodyLista.innerHTML = "";

        // pintar elemntos acorde al maxElementosPorPagina
        // los elementos iran desde (maxElementosPorPagina* (paginaActual-1)) hasta this.maxElementosPorPagina*paginaActual
        for(let i = elementoInicial;i<(this.maxElementosPorPagina*paginaActual);i++){
            // si en una pagina no llega al maxElementosPorPagina hay que evitar que intente pintar esos
            // elementos y no de error
            if(i<usuarios.length){
                let hilera = document.createElement("tr");

                let celdaNombre = document.createElement("td");
                let celdaApellido = document.createElement("td");
                let celdaCorreo = document.createElement("td");
                let celdaOperacion = document.createElement("td");
                celdaOperacion.className = "list-opereciones";

                let textoCeldaNombre = document.createTextNode(usuarios[i].nombre != null ? usuarios[i].nombre : "Sin asignar");
                let textoCeldaApellido = document.createTextNode(usuarios[i].apellidos != null ? usuarios[i].apellidos : "Sin asignar");
                let textoCeldaCorreo = document.createTextNode(usuarios[i].correo != null ? usuarios[i].correo : "Sin asignar");


                celdaNombre.appendChild(textoCeldaNombre);
                celdaApellido.appendChild(textoCeldaApellido);
                celdaCorreo.appendChild(textoCeldaCorreo);



                let btnEdit = document.createElement("button");
                btnEdit.className = "btn-list-element";
                btnEdit.onclick = () =>{this.controlador.onEditElementoLista(usuarios[i])};

                let btnRemove = document.createElement("button");
                btnRemove.className = "btn-list-element";
                btnRemove.onclick = () =>{this.controlador.onRemoveElementoLista(usuarios[i])};

                let ul = document.createElement("ul");
                ul.className = "listaOperaciones";


                let li2 = document.createElement("li")
                btnEdit.innerHTML = `<i class="fas fa-pen-square"></i><p class="label">Modificar</p>`
                li2.appendChild(btnEdit);

                let li3 = document.createElement("li")
                btnRemove.innerHTML = `<i class="far fa-trash-alt"></i><p class="label">Eliminar</p>`
                li3.appendChild(btnRemove);

                ul.appendChild(li2)
                ul.appendChild(li3)

                this.createDropDown(celdaOperacion,usuarios[i])
                celdaOperacion.appendChild(ul)


                // añadimos los elementos a la fila
                hilera.appendChild(celdaNombre);
                hilera.appendChild(celdaApellido);
                hilera.appendChild(celdaCorreo);
                hilera.appendChild(celdaOperacion)


                // añadimos la fila al tbody
                this.bloqueBodyLista.appendChild(hilera);
            }


        }


    },

    createDropDown:function(celdaOperacion,usuario) {


        let btnEdit = document.createElement("button");
        btnEdit.className = "btn-list-element";
        btnEdit.onclick = () =>{this.controlador.onEditElementoLista(usuario)};

        let btnRemove = document.createElement("button");
        btnRemove.className = "btn-list-element";
        btnRemove.onclick = () =>{this.controlador.onRemoveElementoLista(usuario)};

        let li2Drop = document.createElement("div")
        btnEdit.innerHTML = `<i class="fas fa-pen-square"></i><p>Modificar</p>`
        li2Drop.appendChild(btnEdit);
        let li3Drop = document.createElement("div")
        btnRemove.innerHTML = `<i class="far fa-trash-alt"></i><p>Eliminar</p>`
        li3Drop.appendChild(btnRemove);
        // añadimos el dropdown button que se mostrará en movil
        celdaOperacion.innerHTML = `<button class="btn btn-secondary dropdown-toggle tresPuntos"
                          type="button" id="dropdownMenu1" data-toggle="dropdown"
                          aria-haspopup="true" aria-expanded="false">
                   <i class="fas fa-ellipsis-v tresPuntos">
                  </button>
                  <div class="dropdown-menu" aria-labelledby="dropdownMenu1">
                    
                  </div>`
        celdaOperacion.querySelector("div.dropdown-menu").append(li2Drop)
        celdaOperacion.querySelector("div.dropdown-menu").append(li3Drop)
    },


    // habilitar y deshabilitar los botones que controlan la paginacion
    // para evitar errores
    prepararBotonesPaginador : function (paginaActual, maxPages) {
        this.btnNextPage.removeAttribute("disabled");
        this.btnPreviousPage.removeAttribute("disabled");
        document.getElementById("paginadorListaUsuario").innerHTML = "Página "+paginaActual + " de " + maxPages;
        // si el maxPages es mayor que uno habilitar el next page
        if(maxPages>1){
            this.btnNextPage.removeAttribute("disabled");
        }
        // si el current page es igual al maxPages deshabilitar el next page
        if(paginaActual === maxPages){
            this.btnNextPage.setAttribute("disabled","disabled");
        }
        if(maxPages === 0){
            this.btnNextPage.setAttribute("disabled","disabled");
        }
        // si el current page es mayor que uno habilitar el previous page
        if(paginaActual > 1){
            this.btnPreviousPage.removeAttribute("disabled");
        }else{
            this.btnPreviousPage.setAttribute("disabled","disabled");
        }

    },

    cambiarFlechasOrden(ordenNombre, ordenApellido, ordenCorreo) {
        document.getElementById("flecha-nombre-usuario").className = (!ordenNombre) ? "fas fa-chevron-down" : "fas fa-chevron-up";
        document.getElementById("flecha-apellido-usuario").className = (!ordenApellido) ? "fas fa-chevron-down" : "fas fa-chevron-up";
        document.getElementById("flecha-correo-usuario").className = (!ordenCorreo) ? "fas fa-chevron-down" : "fas fa-chevron-up";
    },

};

var ControladorListaUsuarios = {
    modelo: ModeloListaUsuarios,
    vista: VistaListaUsuarios,
    controladorUpdateUsuario: null,
    controladorUpdatePassUsuario: null,
    controladorCreateUsuario: null,

    error: false,// controla si ha habido un error en la obtencio de datos

    iniciar: function (cliente,controladorUpdateUsuario,controladorCreateUsuario,controladorUpdatePassUsuario) {
        this.modelo.controlador = this;
        this.modelo.cliente = cliente;
        this.vista.controlador = this;
        this.controladorUpdateUsuario = controladorUpdateUsuario;
        this.controladorCreateUsuario = controladorCreateUsuario;
        this.controladorUpdatePassUsuario = controladorUpdatePassUsuario;
        this.vista.mostrarCarga();
        this.modelo.obtenerDatos();
    },

    cambiarFlechasOrden(ordenNombre, ordenApellido, ordenCorreo) {
        this.vista.cambiarFlechasOrden(ordenNombre,ordenApellido,ordenCorreo)
    },

    representarDatos: function(datos,paginaActual){
        this.vista.representarDatos(datos,paginaActual);
        this.vista.mostrarLista();
    },
    mostrarTabla: function(){
        this.vista.mostrarLista();
    },

    representarError: function () {
        this.vista.mostrarError();
    },

    // eventos on click de los elementos de la lista
    // el on click y el on edit redirecciona a la siguiente pagina con un flag de editar
    // el on delete confirma la operacion y lanza la peticion al server en caso afirmativo
    onEditElementoLista: function (elemento) {
        this.controladorUpdateUsuario.iniciar(elemento,null)
        this.controladorUpdatePassUsuario.vista.usuario = elemento;
    },
    onCreateUsuario: function () {
        this.controladorCreateUsuario.iniciar(this.modelo.idCliente)
    },
    onRemoveElementoLista: function (elemento) {

        if(elemento.id !== this.modelo.cliente.id){
            // confimar borrado
            if(confirm("Se borrará para siempre el usuario "+elemento.nombre+"\n¿Estás seguro?")){
                this.vista.mostrarCarga();
                this.modelo.borrarElemento(elemento.id);
            }
        }else{
            alert("No puedes borrarte a tí mismo")
        }



    },



}