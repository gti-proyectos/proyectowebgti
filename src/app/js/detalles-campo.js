/**
 * Autor: Rubén Pardo, Aitor
 * Fecha: 17/03/2020
 * Funcionalidad js del fichero detalles-campo.html
 */



/*FUNCION QUE SE LLAMARÁ Al cargar el mapa*/
function iniciar(){
    VistaParcelas.preparar("idMapa","error", "contenido",
        "carga","graphs","no-sensor","no-parcela","idTitulo","idDesc");

    VistaGraficas.preparar("graph-carga","graph-info","graphs-item","graph-error");

    ControladorParcelas.iniciar();

}

//Coge todas las parcelas del campo y las pone en local Storage
// para ser recogidas en contabilidad
function irAContabilidad() {
    localStorage.setItem("parcelas-detallesCampo",JSON.stringify(ModeloParcelas.parcelasJSON));
    localStorage.setItem("parcelas-idCampo",JSON.stringify(VistaParcelas.idCampo));
    window.location='listado-temporadas.html';
}

//------------------------------------
// Funcion para volver atras
//------------------------------------
function volver() {
    // debemos echar uno atras y no ir al login porque hay usuarios que vienen del login
    // y otros del listado de campos

    if(this.VistaParcelas.unCampo == 1){
        cerrarSesion();
    }else{
    // si tine mas de un campo hay que volver atras que es el listado
    if (window.location.href.indexOf('#') > -1){
        // si tenemos anchor tenemos que echar atras 2 veces para quitar el anchor
        history.go(-2)
    }else{
        // sin anchor basta con -1
        history.go(-1);
    }
    }


}// ()

// SOBRESCRIBIMOS EL METODO ON LOAD PARA QUE CUANDO REFRESQUE
// SI TEIEN EL ANCHOR BAJARÁ AL FINAL PORQUE NO EXISTE, FORZAMOS
// QUE ESTE SIEMPRE ARRIBA AL RECARGAR
window.onbeforeunload = function () {
    window.scrollTo(0, 0);
}


//-------------------------------------
// N,N -> getRandomColor() -> Color
// recibe cuantos colors diferentes hay en ese
// momento se quieren y en cual esta
// calcula colors random distintos entre sí
//-------------------------------------
function getRandomColor(colorNum, colors) {

    if (colors < 1) colors = 1;
    // defaults to one color - avoid divide by zero
    return colorNum * (360 / colors) % 360;

}

function limpiarMapa() {
    //deseleccionar todas las parcelas y eleminar las parcelas para evitar duplicidad
    ModeloParcelas.parcelas.forEach(parcela=>{

        parcela.sondas.forEach(sonda=>{
            sonda.setSelected(false)
            if(sonda.getMarker() !== null){
                sonda.getMarker().setMap(null);
                sonda.setMarker(null);
            }
        })


        //clear polygon, still finding more elegant way
        parcela.getPolygon().setMap(null);
        parcela.setPolygon(null);
        parcela.setSelected(false);


    })


}

//-------------------------------------------
// listener del on click de los polygonos
// recibe el objeto json de la parcela
// Cuando se pulsa una parcela se le hace zoom
// y la selecciona en el select
//-------------------------------------------
function parcelaPulsada(parcela) {
    if(!parcela.isSeleccionado()){
        //clear mapa
        limpiarMapa();
        //seleccionamos la pulsada
        parcela.setSelected(true);

        //repintamos las parcelas y las sondas
        VistaParcelas.pintarParcelasEnMapa(ModeloParcelas.parcelas);
        VistaParcelas.pintarPosicionesEnMapa( ModeloParcelas.parcelas);
        VistaParcelas.comprobarNingunaMarcaPulsada();

        selectItemParcela(parcela.id);
        // cogemos el selector de parcelas
        let selectorParcelas = document.getElementById("parcelas");
        if(selectorParcelas.value !== parcela.id){
            // seleccionamos la parcela pulsada
            selectorParcelas.value = parcela.id;
            cambiarTextoBtnSeleccionar(false)
            console.log("hola")
        }

        // hacemos zoom a la parcela
        zoomParcela(parcela.id)
        if(!this.VistaParcelas.mostrarInfo){
            this.VistaParcelas.mostrarInfo = true
            this.VistaParcelas.openInfoWindowAyuda(parcela)

        }

    }

}//()

//-----------------------------------
// N->parcelaOnChange()
// Recibe el id del option seleccionada cada
// vez que el selector parcelas cambie
//-----------------------------------
function parcelaOnChange(idSeleccionado) {

    //clear mapa
    limpiarMapa();
    //seleccionamos la pulsada
    let parcela;
    ModeloParcelas.parcelas.forEach(e=>{
        if(e.id == idSeleccionado){
            e.setSelected(true);
            parcela = e;
        }
    });

    //repintamos las parcelas y las sondas
    VistaParcelas.pintarParcelasEnMapa(ModeloParcelas.parcelas);
    VistaParcelas.pintarPosicionesEnMapa( ModeloParcelas.parcelas);
    VistaParcelas.comprobarNingunaMarcaPulsada();
    cambiarTextoBtnSeleccionar(false)
    zoomParcela(idSeleccionado)
    if(!this.VistaParcelas.mostrarInfo){
        this.VistaParcelas.mostrarInfo = true
        this.VistaParcelas.openInfoWindowAyuda(parcela)
    }
}//()

//---------------------------------
// Coge los bounds de la parcela y hace zoom en el mapa
//---------------------------------
function zoomParcela(idParcela) {
    let bounds = new google.maps.LatLngBounds();
    let polygon;
    //cogemos el polygon con el id asociado
    this.VistaParcelas.boundsIdParcelas.forEach(e=>{
        if(idParcela == e['id']){
            polygon = e['polygon']
        }
    });
    // rellenamos los bounds
    polygon.getPath().getArray().forEach(function (v) {
        bounds.extend(v);
    });

    /*Hacemos que el mapa se adecue a esos limites*/
    this.VistaParcelas.mapa.fitBounds(bounds);
}// ()

//---------------------------------
// N -> getPosicionesSeleccionadas() -> Array de posiciones
// devuelve un array de id de los que estan seleccionados en el
// dropdown de posiciones
//---------------------------------
function getPosicionesSeleccionadas () {
    let posiciones = [];
    let sesnores = document.getElementById('sensores');

    for(let i = 0;i<sesnores.options.length;i++){
        if(sesnores.options[i].selected){
            posiciones.push({id:sesnores.options[i].value});
        }
    }
    return posiciones;
}// ()


//CUANDO SE PULSA EL BOTON SALIR MODO SELECCION
// DESELECCIONAR TODAS LAS SONDAS DE LA PARCELA Y ESCONDER LOS BOTONES
function salirModoSeleccion(){
    VistaParcelas.isModoSeleccion = false;

    let parcelaSeleccionada =  ModeloParcelas.parcelas.find(parcela =>{
        if(parcela.isSeleccionado()){
            return parcela;
        }
    });
    limpiarMapa();
    parcelaSeleccionada.setSelected(true)
    VistaParcelas.pintarPosicionesEnMapa(ModeloParcelas.parcelas)
    VistaParcelas.pintarParcelasEnMapa(ModeloParcelas.parcelas)



    document.getElementById("btn-modoSeleccion").style.display = "none";
    document.getElementById("btn-verHistorial").style.display = "none";
    document.getElementById("btn-seleccionar-todo").style.display = "none";
}

//CUANDO SE PULSA EL BOTON SELEECIONAR SONDAS
// O LA LLAMAMOS DESDE EL CODIGO (SE PASRARÁ EL BOOLEANO)
// SELECCIONAR TODAS O DESELECCIONARLAS
// seleccionar = true, texto = "SELECCIONAR TODOS"
function seleccionarTodo(seleccionar){

    // se llame de donde se llama si es true es que se tiene que seleccionar todo
    let isSeleccionadoTodo = cambiarTextoBtnSeleccionar(seleccionar);
    if(isSeleccionadoTodo){
        // seleccionamos todas las sondas de la parcela seleccionada
        let parcelaSeleccionada =  ModeloParcelas.parcelas.find(parcela =>{
            if(parcela.isSeleccionado()){
                return parcela;
            }
        });
        limpiarMapa();
        parcelaSeleccionada.setSelected(true)
        this.ModeloParcelas.parcelas.forEach(parcela =>{
            if(parcela.isSeleccionado()){
                parcela.sondas.forEach(sonda=>{
                    sonda.setSelected(true);
                })

            }
        });

        //repintamos las parcelas y las sondas
        VistaParcelas.pintarParcelasEnMapa(ModeloParcelas.parcelas);
        VistaParcelas.pintarPosicionesEnMapa( ModeloParcelas.parcelas);

    }
    else{
        let parcelaSeleccionada =  ModeloParcelas.parcelas.find(parcela =>{
            if(parcela.isSeleccionado()){
                return parcela;
            }
        });
        limpiarMapa();
        parcelaSeleccionada.setSelected(true)
        VistaParcelas.pintarPosicionesEnMapa(ModeloParcelas.parcelas)
        VistaParcelas.pintarParcelasEnMapa(ModeloParcelas.parcelas)
    }
    VistaParcelas.comprobarNingunaMarcaPulsada();

}

//--------------------------------------
// VACIO | T/F -> cambiarTextoBtnSeleccionar() -> T/F
// El valor de entrada es para indicar si se ha llamado desde
// el html o desde el codigo
// si es vacio es desde el html y cambia a la otra opcion
// si es tiene un booleano
// true es que estaba en seleccionar y el boton pasa a quitar selccion, es decir se selecciona todo
// false es que estaba en quitar y el boton pasa a seleccionar, es decir se deselecciona todo
//
// el valor de salida indica si se tiene que seleccionar todo o no
//--------------------------------------
function cambiarTextoBtnSeleccionar(seleccionar) {
    // cambiamos el texto
    let elemento = document.getElementById("btn-seleccionar-todo");
    if(seleccionar!==undefined){
        if(seleccionar){
            elemento.textContent = "QUITAR SELECCIÓN"
            return  true;
        }else{

            elemento.textContent = "SELECCIONAR TODAS"
            return false;
        }
    }
    else{

        if(elemento.textContent === "QUITAR SELECCIÓN"){
            elemento.textContent = "SELECCIONAR TODAS"
            return false;
        }else{
            elemento.textContent = "QUITAR SELECCIÓN"
            return true;
        }
    }
} // ()

// callback del boton Generar graficas
function generarGrafica() {

    // cogemos las sondas seleccionadas para pasarsela al controlador de graficas

    let sondasSeleccionadas = [];

    // recorremos para ver las sondas seleccionadas
    // y la añadimos a un array
    ModeloParcelas.parcelas.forEach(parcela =>{
        if(parcela.isSeleccionado()){
            parcela.sondas.forEach(sonda =>{
                if(sonda.isSeleccionado()){
                    sondasSeleccionadas.push(sonda);
                }
            })
        }
    });

    // ponemos los nombres de las sondas en el tiutlo de la seccion
    let tituloElmt = document.getElementById("titulo-graphs")
    // reseteamos el texto explicativo
    tituloElmt.innerHTML = "Seleccione el tipo de medicion y el rango de tiempo para generar los datos de ";

    for(let i = 0; i< sondasSeleccionadas.length;i++){
            // si es la ultima no añadir coma al texto
            if(i === sondasSeleccionadas.length-1){
                tituloElmt.innerHTML+= sondasSeleccionadas[i].label;
            }else if(i === sondasSeleccionadas.length-2){
                // si es la penultima poner la y
                tituloElmt.innerHTML+= sondasSeleccionadas[i].label+" y ";
            }else{
                tituloElmt.innerHTML+= sondasSeleccionadas[i].label+", ";
            }

    }


    // mostramos el bloque de graficas y salimos del modo edicion para evitar errores
    ControladorParcelas.vista.contenedorGraficas.style.display = "block";
    // salirModoSeleccion();
    location = '#graphs';

    document.querySelector('footer').className = "footerGraph";

    ControladroGraficas.iniciar(sondasSeleccionadas);

}


//=============================================================
// MODELO-VISTA-CONTROLADOR
//=============================================================
var ModeloParcelas = {
    controlador: {},
    datosUser: {},
    parcelas:{},
    parcelasJSON:{},

    //-----------------------------------
    // N->obtenerCampos()
    // obtiene las parcelas y las posicioens por el id del Campo
    //-----------------------------------
    obtenerCampos: async function (id) {
        let peticionError = false;
        fetch('../../src/api/v1.0/sesion').then((respuesta) => {
            if (respuesta.status !== 200) {
                location.href = this.controlador.vista.urlLogin;//vuelve a app, se redireccionará al login
            } else {
                return respuesta.json();
            }
        }).then(async(json)=> {
            //guardamos los datos del user
            this.datosUser = json;
            //despues de obtener los datos del user, cogemos sus campos
          let parcelas = await  fetch('../../src/api/v1.0/parcela?idCampo=' + id).then(function (respuesta) {
                return respuesta;
            }).catch((e) =>{
              console.log("ERROR EN GET PARCELA:"+e.message);
              this.controlador.representarError();
            })
              .then((res) => {
                if (res.status !== 200) {
                    // ha habido un error mostrar error:
                    this.controlador.representarError();
                } else {
                    // han llegado los datos, devolverlos en formato json
                    return res.json();
                }
            });

            // obtenemos las posiciones de las parcelas por el id del campo
          let posiciones = await  fetch('../../src/api/v1.0/posiciones?idCampo='+id).then(function (respuesta) {
                return respuesta;
            }).catch((e) =>{
              console.log("ERROR EN GET POSICIONES:"+e.message);
              this.controlador.representarError();
            }).then((res) => {
                if (res.status !== 200) {
                    // ha habido un error mostrar error:
                    this.controlador.representarError();
                } else {
                    // han llegado los datos, devolverlos en formato json
                    return res.json();
                }
            });


            // de los json montamos la estructura de objetos
            this.parcelasJSON = parcelas;
            console.log(posiciones)
            this.parcelas = getParcelasFromJson(parcelas,posiciones);


            //-------------- PETICIONES HECHAS
            //pintamos el titulo
            this.controlador.vista.pintarTitulo(parcelas)


            // comprobar que no haya nada vacio
            // pintamos las parcelas y las sondas
            if (parcelas[0].length === 0) {
                this.controlador.vista.mostrarError();
            }else{
                // si esta  pintamos el resultado
                this.controlador.vista.mostrarDatos(this.parcelas);
            }
        }).catch((e) =>{
            console.log("ERROR: "+e);
            this.controlador.representarError();
        })

    },//()
    //-----------------------------------------------
    // Sonda -> () -> V/F, Sonda
    // Obtiene las ultimas mesuras de la sonda pasada
    //-----------------------------------------------
    getUltimasMesuras: async function(sonda) {
        let error = false;
        let src = '../../src/api/v1.0/mediciones?idPos='+sonda.id;
        let mediciones = await fetch(src)
            .then(respuesta => {
                if (respuesta.status === 200) {
                    return respuesta.json()
                } else{
                    error = true;
                }
            }).catch(e=>{
                error = true;
            })

        if(!error){
            if(mediciones.length !== 0){
                let medicion = mediciones[0];
                sonda.ultimaTemp = medicion.temperatura;
                sonda.ultimaHum = medicion.humedad;
                sonda.ultimaIlu = medicion.iluminacion;
                sonda.ultimaSal = medicion.salinidad;
                sonda.ultimaFecha = medicion.fecha;
            }else{
                sonda.ultimaTemp = -1;
            }

        }

        return error;
    }
};

var VistaParcelas = {

    unCampo: 0,
    mapa:{},
    controlador:{},
    boundsIdParcelas:[],
    idCampo:-1,
    tituloCampo:{},
    descCampo:{},

    infoWindow:{},

    //bloques que se muestran o se esconden a relacion de los procesos
    bloqueError: {},
    bloqueCarga: {},
    bloqueMapa: {},
    contenedorParcelas: {},
    contenedorGraficas: {},
    contenedorNoSensores: {},
    contenedorNoParcelas: {},
    mostrarInfo: false,

    // ponemos la grafica como atributo para no ir creando muchos charts y evitar posibles solapamientos de anteriores
    // graficas que no se borran
    grafica: null,

    // devuelve el icono de la marca con el estilo adecuado
    getIconoMarca: function(sonda,marcado){
      if(marcado){
          return {
              path: google.maps.SymbolPath.CIRCLE,
              scale: 10,
              strokeColor: "#FF0000",
              fillColor: "#FF0000",
              fillOpacity: 1.0,
          }
      } else{
          return {
              path: google.maps.SymbolPath.CIRCLE,
              scale: 10,
              strokeColor: "#FFF",
              fillColor: "#FFF",
              fillOpacity: 1.0,
          }
      }
    },

    cargaInfoWindow:function(marker,sonda){
        this.infoWindow.setContent(`<div class="infowindow">
                 <h3>Sonda ${sonda.label}</h3>
                   <div style="margin: 0 auto;height: 42px;">
                    <img src="img/carga.svg" alt="Sonda cargando" 
                            style="width: 24px;
                            align-self: center;
                             margin: 0 auto;">
                    </div>
                </div>`)
        this.infoWindow.open(this.mapa, marker)
    },
    // Abre el info windo en la marka pasada
    openInfoWindow: function (marker,sonda){
        if(sonda.ultimaTemp !== -1){
            this.infoWindow.setContent(`<div class="infowindow">
             <h3>Sonda ${sonda.label}</h3>
            <div class="infoWindow-content">
                <div><span class="circle" style="background: ${sonda.getColorWarningTemp()}; 
                        height: 20px;
                        width: 20px;
                        border-radius: 50%;
                        margin-right: 1rem;
                        border: 1px solid;"></span><h4>Temperatura:</h4><span class="mesura">${sonda.ultimaTemp} ºC</span></div>
                <div><span class="circle" style="background: ${sonda.getColorWarningHum()}; 
                        height: 20px;
                        width: 20px;
                        border-radius: 50%;
                        margin-right: 1rem;
                        border: 1px solid;"></span><h4>Humedad:</h4><span class="mesura">${sonda.ultimaHum} %</span></div>
                <div><span class="circle" style="background: ${sonda.getColorWarningIlu()}; 
                        height: 20px;
                        width: 20px;
                        border-radius: 50%;
                        margin-right: 1rem;
                        border: 1px solid;"></span><h4>Iluminación: </h4><span class="mesura">${sonda.ultimaIlu} %</span></div>
                <div><span class="circle" style="background: ${sonda.getColorWarningSal()}; 
                        height: 20px;
                        width: 20px;
                        border-radius: 50%;
                        margin-right: 1rem;
                        border: 1px solid;"></span><h4>Salinidad:</h4><span class="mesura">${sonda.ultimaSal}%</span></div>
            </div>
            <div class="infowindow-fecha"><h4>Última actualización:</h4><span class="mesura">${sonda.ultimaFecha}</span></div>
            </div>`)
        }else{
            this.infoWindow.setContent(`<div class="infowindow">
             <h3>Sonda ${sonda.label}</h3>
            <div class="infoWindow-content">
                <p>Esta sonda no ha tomado aun mediciones</p>
            </div>
            </div>`)
        }


        this.infoWindow.open(this.mapa, marker)
    },
    errorInfoWindow(marker,sonda){
        this.infoWindow.setContent(`<div class="infowindow">
                 <h3>Sonda ${sonda.label}</h3>
                <div class="infoWindow-content">
                    <p>Hubo un problema. Vuelve ha intentarlo más tarde</p>
                </div>
                </div>`)
        this.infoWindow.open(this.mapa, marker)
    },
    openInfoWindowAyuda: function (parcela){
        this.infoWindow.setPosition(this.getPolygonCenter(parcela.getPolygon()))
        this.infoWindow.setContent(`<div class="infowindow">
         <h3>Mantener pulsado una sonda para entrar a modo selección</h3>
        </div>`)
        this.infoWindow.open(this.mapa)
    },
    getPolygonCenter: function(polygon) {
        let a = polygon.getPath().getArray();
        let b = new google.maps.LatLngBounds();
        for (let i = 0; i < a.length; i++) {
            b.extend(a[i]);
        }
        return b.getCenter();
    },
    //LOGICA AL PULSAR UNA MARCA
    // SI NO SE ESTA EN MODO SELECCION MOSTRAR EL INFO WINDOW
    // EN MODO SELECCION SELECCIONARLA O DESLECCIONARLA
    pulsarMarker: function(marker,sonda){


        if(!this.isModoSeleccion){
            this.controlador.openInfoWindow(marker,sonda)
        }else {
            //si no seleccionarla o deseleccionarla
            // si esta deseleccionada, seleccionarla
            if (!sonda.isSeleccionado()) {
                sonda.setSelected(true)
                // marcar
                marker.setIcon(this.getIconoMarca(sonda,true))
                marker.label.color = "#FFF"
            } else {
                sonda.setSelected(false)
                // comprobar si esta seleccionada, cambiar a por lo del objeto
                // desmarcar
                marker.setIcon(this.getIconoMarca(sonda,false))
                marker.label.color = "#000"
            }

            this.comprobarTodasLasMarcasPulsadas();
            this.comprobarNingunaMarcaPulsada();
        }

    },

    //LOGICA AL HACER LONG PRESS EN UNA MARCA
    // ENTRAR EN MODO SELECCION SI NO SE ESTA
    longPressMarker:function(marker,sonda){

        document.getElementById("btn-modoSeleccion").style.display = "flex";
        document.getElementById("btn-verHistorial").style.display = "flex";
        document.getElementById("btn-seleccionar-todo").style.display = "block";
        this.isModoSeleccion = true;
        //marcar la sonda
        sonda.setSelected(true);
        marker.setIcon(this.getIconoMarca(sonda,true))
        marker.label.color = "#FFF"
        this.comprobarTodasLasMarcasPulsadas();
        this.comprobarNingunaMarcaPulsada();
    },

    // CUNADO SE HACE UN LONG PRESS O UN CLCIK EN UNA MARCA
    // COMPROBAMOS SI ESTAN TODAS LAS MARCAS PULSADAS O NO
    // PARA CAMBIAR EL BOTON DE SELECCIONARLAS TODAS O QUITARLAS
    comprobarTodasLasMarcasPulsadas: function(){
        //comprobar si estan todas las sondas seleccionadas
        let isTodasSeleccionadas = true;
        let isNingunaSeleccionada = true;
        for(let i = 0; i< ModeloParcelas.parcelas.length; i++){
            for(let j = 0; j< ModeloParcelas.parcelas[i].sondas.length; j++){
                if(ModeloParcelas.parcelas[i].isSeleccionado()){
                    if(!ModeloParcelas.parcelas[i].sondas[j].isSeleccionado()){
                        // hay una seleccionada, no estan todas seleccionadas
                        isTodasSeleccionadas = false;
                    }else{
                        // esta seleccionada, no estan todas deseleccionadas
                        isNingunaSeleccionada = false;
                    }
                }
            }
        }

        //si estan todas las seleccionadas cambiar el boton de seleccionar
        // a deseleccionarlas
        if(isTodasSeleccionadas){
            cambiarTextoBtnSeleccionar(true);
        }else{
            cambiarTextoBtnSeleccionar(false);
        }

        //si estan todas deseleccionadas no se puede pulsar ver historial
        // evitamos que se genere un historial sin sondas
        if(!isNingunaSeleccionada){
            this.esconderVerHistorial();
        }else{
            // mostrar boton ver historial para que vuelva a aparecer
            this.mostrarVerHistorial();
        }
    },

    // COMPROBAR SI NO HAY NINGUNA MARCAS PULSADAS
    // SE USA PARA VER SI MOSTRAR O ESCONDER EL BOTON DEL HISTORIAL
    // PARA EVITAR QUE SE GENERE UNO SIN SONDAS SELECCIONADAS
    comprobarNingunaMarcaPulsada: function(){
        //comprobar si estan todas las sondas seleccionadas
        let isNingunaSeleccionada = true;
        for(let i = 0; i< ModeloParcelas.parcelas.length; i++){
            for(let j = 0; j< ModeloParcelas.parcelas[i].sondas.length; j++){
                if(ModeloParcelas.parcelas[i].isSeleccionado()){
                    if(ModeloParcelas.parcelas[i].sondas[j].isSeleccionado()){
                        // hay una seleccionada, no estan todas seleccionadas
                        isNingunaSeleccionada = false;
                    }
                }
            }
        }


        //si estan todas deseleccionadas no se puede pulsar ver historial
        // evitamos que se genere un historial sin sondas
        if(!isNingunaSeleccionada){
            this.esconderVerHistorial();
        }else{
            // mostrar boton ver historial para que vuelva a aparecer
            this.mostrarVerHistorial();
        }
    },


    // coge el parametro de la url, si no existe redirecciona a la login
    cogerIdCampoDeUrl: function(){
        //Obtenemos el parametro de la url
        var paramstr = window.location.search.substr(1);
        var paramarr = paramstr.split("&");
        var params = {};
        var id = 0;

        for (var i = 0; i < paramarr.length; i++) {
            var tmparr = paramarr[i].split("=");
            params[tmparr[0]] = tmparr[1];
        }
        if (params['id']) {
            this.idCampo = params['id'];
        } else {
            location.href = "login.html";//vuelve a app, se redireccionará al login
        }

    },

    checkUnCampo:function(){
        var paramstr = window.location.search.substr(1);
        var paramarr = paramstr.split("&");
        var params = {};

        for (var i = 0; i < paramarr.length; i++) {
            var tmparr = paramarr[i].split("=");
            params[tmparr[0]] = tmparr[1];
        }

        // esto indica si puede vuelve atras. Si solo tiene un campo tiene que cerrar sesion
        this.unCampo = params['uc'];
        if(this.unCampo!=null){
            if(this.unCampo == 0){
                //cambiar el atras a salir
                document.getElementById("btn-atras").style.display = "flex"

            }else{
                document.getElementById("selectors-parcela").classList.add("selector-parcela-sin-atras");
                document.getElementById("selectors-parcela").classList.remove("selector-parcela-con-atras");
            }

        }else {
            location.href = "login.html";//vuelve a app, se redireccionará al login
        }




    },
    //Prepara el mapa y los bloques que se mostrarán
    preparar: function (idMapa,idBloqueError, idContenedorParcelas, idBloqueCarga,idGraficas,idNoSensores,idNoParcela,idTitulo,idDesc) {

        this.infoWindow = new google.maps.InfoWindow();

        this.cogerIdCampoDeUrl();
        this.checkUnCampo();
        this.tituloCampo = document.getElementById(idTitulo);
        this.descCampo = document.getElementById(idDesc);

        this.bloqueError = document.getElementById(idBloqueError);
        this.bloqueCarga = document.getElementById(idBloqueCarga);
        this.contenedorParcelas = document.getElementById(idContenedorParcelas);
        this.mapa = document.getElementById(idMapa);
        this.contenedorGraficas = document.getElementById(idGraficas);
        this.contenedorNoSensores = document.getElementById(idNoSensores);
        this.contenedorNoParcelas = document.getElementById(idNoParcela);


        //Preparamos el mapa
        this.mapa = new google.maps.Map(document.getElementById(idMapa), {
            center: {lat: 38.9965055, lng: -0.1674364},/*Donde se centra*/
            zoom: 20,/*que nivel de zoom*/
            mapTypeId: 'hybrid',/*Tipo de mapa que se va a dibujar, hybird mapa satelite con carreteras y calles*/
            styles: [/*Permite personalizar estilos del mapa*/
                {
                    featureType: 'poi',/*Ocultar los points of interests, negocios, edificios...*/
                    stylers: [{visibility: 'off'}]
                },
                {
                    featureType: 'transit',/*Ocultar paradas de autobus, taxi..*/
                    stylers: [{visibility: 'off'}]
                }
            ],
            mapTypeControl: false,/*Denegar el cambio de tipo de mapa*/
            streetViewControl: false,/*Denegar el street view*/
            rotateControl: false,/*Denegar el rotar el mapa */
            fullscreenControl: false
        });
        google.maps.Polygon.prototype.my_bounds=function(){
            var bounds = new google.maps.LatLngBounds()
            this.getPath().forEach(function(element,index){bounds.extend(element)})
            return bounds
        }
        this.mapa.setTilt(0);


    },

    esconderElementos: function () {
        this.bloqueError.style.display = "none";
        this.bloqueCarga.style.display = "none";
        this.contenedorParcelas.style.display = "none";
    },

    //Muestra el <div> donde se muestra el mensaje de error.
    mostrarError: function () {
        this.esconderElementos();
        this.bloqueError.style.display = "block"
    },
    //Muestra el <div> donde se muestra el mensaje de error.
    mostrarCarga: function () {
        this.esconderElementos();
        this.bloqueCarga.style.display = "block"
    },

    mostrarVerHistorial: function(){
        document.getElementById("btn-verHistorial").style.display = "none"
    },
    esconderVerHistorial: function(){
        document.getElementById("btn-verHistorial").style.display = "flex"
    },

    //-------------------------------
    // pintar las posiciones en el mapa
    //-------------------------------
    pintarPosicionesEnMapa: function(parcelas){
        parcelas.forEach((parcela) =>{
            if(parcela.isSeleccionado()){
                parcela.sondas.forEach(sonda=>{
                    let marker = new google.maps.Marker({
                        position: sonda.posicion,
                        label: {
                            text: sonda.label,
                            color: sonda.isSeleccionado() ? "#FFF": "#000",
                            fontFamily: 'Roboto, Arial, sans-serif',
                            fontSize: '10px',
                        },
                        map: this.mapa,/*en que mapa queremos que se dibuje*/
                    });
                    marker.setIcon(this.getIconoMarca(sonda,sonda.isSeleccionado()))
                    //asignamos el marker al objeto sonda
                    sonda.setMarker(marker);

                    // ON CLICK MARKS====================================================
                    var longPress = false;
                    marker.addListener('click', () => {
                        if(!longPress){
                            this.pulsarMarker(marker,sonda);
                        }
                        longPress = false;


                    })

                    // LONG PRESS MARKS====================================================
                    var mousedUp = false;
                    google.maps.event.addListener(marker, 'mousedown', (event)=>{
                        mousedUp = false;
                        clearTimeout(marker.pressButtonTimer);
                        marker.pressButtonTimer = setTimeout( () =>{
                            if(mousedUp === false){
                                //do something if the mouse was still down
                                //after 500ms
                                this.longPressMarker(marker,sonda);
                                longPress = true;
                            }
                        }, 500);
                    });
                    google.maps.event.addListener(marker, 'mouseup', (event)=>{
                        mousedUp = true;
                    });
                    google.maps.event.addListener(marker, 'dragstart', (event)=>{
                        mousedUp = true;
                    });
                })
            }

        });
    },

    //-------------------------------
    // pintar las parcelas en el mapa
    //-------------------------------
    pintarParcelasEnMapa: function(parcelas) {


        let bounds = new google.maps.LatLngBounds();

        parcelas.forEach((parcela) =>{
            let color = parcela.isSeleccionado() ? parcela.getColor() : "#555555";
            let opacity = parcela.isSeleccionado() ? 0.35: 0.7;

            let polygon = new google.maps.Polygon({
                paths: parcela.posiciones,
                strokeColor: color,
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: color,
                fillOpacity: opacity,
                map: this.mapa
            });

            parcela.setPolygon(polygon);

            // añadimos el id y el array de los path del poligono
            this.boundsIdParcelas.push({'id':parcela.id,'polygon':polygon});

            /*Cogemos los path de cada parcela, por cada limites extiende el bounds hasta que lo incluya*/
            polygon.getPath().getArray().forEach(function (v) {
                bounds.extend(v);
            });

            // le añadimos un click listener
            polygon.addListener("click",()=>{
                parcelaPulsada(parcela)
            });

        });

        /*Hacemos que el mapa se adecue a esos limites*/
        this.mapa.fitBounds(bounds)

    },

    mostrarDatos: function (parcelas) {

        this.esconderElementos();

        this.contenedorParcelas.style.display = "flex";

        this.pintarPosicionesEnMapa(parcelas);
        this.pintarParcelasEnMapa(parcelas);

        //Ponemos contenido en las listas
        let parcelasElement = document.getElementById("parcelas");



        // añadimos las parcelas
        parcelas.forEach((parcela) => {
            parcelasElement.innerHTML += `<option value="${parcela.id}">${parcela.nombre}</option>`;
        });

        cargarCustomParcelasSelect();



    },


    pintarTitulo: function (datos) {
        // si no hay parcelas se lo indicamos tambien en el titulo
        if(datos['nombreCampo'] == null){
            // PONER TITULO Y DESC DEL CAMPO
            this.tituloCampo.innerHTML = "Sin parcelas";
            this.descCampo.innerHTML = datos['descCampo'];
        }else{
            // PONER TITULO Y DESC DEL CAMPO
            this.tituloCampo.innerHTML = datos['nombreCampo'];
            this.descCampo.innerHTML = datos['descCampo'];
        }

    },


};

var ControladorParcelas = {
    modelo: ModeloParcelas,
    vista: VistaParcelas,
    iniciar: function () {
        this.modelo.controlador = this;
        this.vista.controlador = this;
        //Asegurar que el <div> de error no se está mostrando.
        this.vista.mostrarCarga();
        this.modelo.obtenerCampos(this.vista.idCampo);
    },

    representarError: function () {
        this.vista.mostrarError();
    },

    openInfoWindow: async function (marker, sonda) {
        this.vista.cargaInfoWindow(marker,sonda);
        let error = await this.modelo.getUltimasMesuras(sonda);
        if(!error) {
            this.vista.openInfoWindow(marker,sonda)
        }else{
            this.vista.errorInfoWindow(marker,sonda)
        }
    }
};
