/**
 * Autor Ruben
 * Fecha: 13/05/2020
 * Logica de carga de los MVC de panel-admin-cliente
 */
function onClickBtnGroup(usuarios){

    if(usuarios){
        VistaListaUsuariosCliente.mostrar = true;
        VistaListaCampos.mostrar = false;
        document.getElementById("btn-cliente").classList.add("active");
        document.getElementById("btn-usuario").classList.remove("active");
        document.getElementById("buscar-secundario").style.display = "none";
        document.getElementById("buscar-principal").style.display = "flex";

    }else{
        VistaListaUsuariosCliente.mostrar = false;
        VistaListaCampos.mostrar = true;
        document.getElementById("btn-cliente").classList.remove("active");
        document.getElementById("btn-usuario").classList.add("active");
        document.getElementById("buscar-secundario").style.display = "flex";
        document.getElementById("buscar-principal").style.display = "none";
    }

    VistaListaCampos.esconderElementos();
    VistaListaUsuariosCliente.esconderElementos();
    ControladorListaUsuariosCliente.mostrarTabla();
    ControladorListaCampos.mostrarTabla();

}

// cogemos el cliente pasado por localstorage desde la lista de clientes
let cliente = JSON.parse(localStorage.getItem("listaCliente-cliente"));
let user =  JSON.parse(localStorage.getItem("user"));

if(cliente.id === user.cliente){
    // esta logeado el  usuario admin
    let liAdmin = document.getElementById("li-admin-jerarquia")
    document.getElementById("jq-nombre-cliente").classList.add("active")
    let ol = document.getElementById("ol-jerarquia")
    ol.removeChild(liAdmin);
}

// vistas de los dos modales con los formularios de crear y modificar usuarios
VistaCrearUsuario.preparar("modal-error-crear-usuarios","carga","formularioModalCrearUsuarios","modal-crear-usuario","close-crear-usuario");
VistaCrearCampo.preparar("modal-error-crear-campo","carga","formularioModalCrearCampo","modal-crear-campo","close-crear-campo");
VistaModifcarCampo.preparar("modal-error-editar-campo","carga","formularioModalEditarCampo","modal-editar-campo","close-editar-campo");
VistaModificarCliente.preparar("modal-error-modificar-usuario","carga","formularioModalModificarUsuario","modal-modificar-usuario","");
VistaModificarUsuario.preparar("modal-error-modificar-usuario","carga","formularioModalModificarUsuario","modal-modificar-usuario","close-editar-usuario");
VistaModificarPassUsuario.preparar("modal-error-pass","carga","formularioModalPass","modal-editar-pass","close-editar-pass");



VistaListaCampos.preparar("lista-campos","lista-campos-body","error","carga","btnNextCampos","btnPrevCampos",false,cliente);
// le pasamos al controlador de la lista los controladores de crear y modicar de campos
ControladorListaCampos.iniciar(cliente.id,
    ControladorCrearCampo,
    ControladorModificarCampo,
);

VistaListaUsuariosCliente.preparar("lista-usuarios","lista-usuarios-body","error","carga","btnNextUsuarios","btnPrevUsuarios",true,cliente);
// le pasamos al controlador de la lista los controladores de crear y modicar de usuarios
ControladorListaUsuariosCliente.iniciar(
    user,
    cliente.id,
    ControladorCrearUsuario,
    ControladorModificarUsuario,
    ControladorModificarPassUsuario
);


// ponemos el nombre del usuario en el titulo
document.getElementById("titulo").innerText = user.nombre;
document.getElementById("jq-nombre-cliente").innerText = cliente.nombre

// para que cuando refresque no entre en modo editar
let clienteTemp = cliente;
clienteTemp.modificar = false;
localStorage.setItem("listaCliente-cliente",JSON.stringify(clienteTemp))