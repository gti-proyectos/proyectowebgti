/**
 * Autor: Aitor Benítez Estruch
 * Fecha: 15/06/2020
 * Modelo Vista Controlador de las gráficas y tabla para PDF
 *
 */


var modeloPDF = {

    controlador: {},
    facturas: [],


    cargar: async function (idTemporada) {
        try {
            //Importamos la información de las facturas desde mcv-facturacion.js
            this.facturas = JSON.parse(localStorage.getItem("lista-facturas"))

            let facturasHistorial = [...this.facturas];// hacemos una copia del array para evitar problemas de race conditions

            this.facturasFiltrado = this.facturas;
            await this.controlador.representarGraficas(facturasHistorial);
            await this.controlador.tablaFacturasTemporada(this.facturas);

        } catch (e) {
            console.log(e);
        }


    },
}

var vistaPDF = {
    bloqueCarga:{},
    graficaBalancePDF:null,
    graficaBeneficiosPDF:null,
    graficaGastosPDF:null,

    preparar:function ( idCarga) {
        this.bloqueCarga = document.getElementById(idCarga);
    },
    //representar graficas de las temporada
    representarGraficas:function(facturaciones){
        if(facturaciones.length !== 0){
            this.representarGraficasCirculares(facturaciones)
            this.representarBalanceNetoPorMes(facturaciones);

        }

    },//()

    mostrarCarga: function () {
        this.bloqueCarga.style.display = "flex"
    },

    representarGraficasCirculares(facturacion){
        //elementos graficos
        let arrayLabelBen = [];
        let arrayLabelGas = [];
        let arrayBackgroundColors = [];
        let arrayBackgroundColorsPerdida = [];

        let perdidas = [];
        let beneficios = [];
        let parcelaAux = "";// parcela actual, empieza en la posicion cero
        //objeto solo con idParcela y cantidad

        let arrayResult = []; //
        let contParcela = -1;//contador parcela
        //ordenamos las facturas por parcela
        facturacion.sort(function (a,b) {
            if (a.parcela < b.parcela) return -1;
            else if (a.parcela > b.parcela) return 1;
            return 0;
        });

        facturacion.forEach(factura=>{
            if(factura.parcela !== parcelaAux) {


                arrayLabelBen.push(factura.parcela);
                arrayLabelGas.push(factura.parcela);

                contParcela++;
                parcelaAux = factura.parcela
                beneficios[contParcela] = 0;
                perdidas[contParcela] = 0;
                if (factura.perdida === 1) {
                    perdidas[contParcela] += parseInt(factura.cantidad);
                }
                else {
                    beneficios[contParcela] += parseInt(factura.cantidad);
                }

            }
            else {
                if (factura.perdida === 1) {
                    perdidas[contParcela] += parseInt(factura.cantidad);
                }
                else {
                    beneficios[contParcela] += parseInt(factura.cantidad);
                }
            }
        })

        // añadimos las cantidades a los labels
        let cont = 0;
        arrayLabelBen = arrayLabelBen.map(label=>{
            // aprovechamos para generar los colors, este bucle tiene las mismas iteraciones que parcelas
            arrayBackgroundColors.push("hsl( " + getRandomColor(cont, arrayLabelBen.length) + ", 100%, 50% )")
            return label +": "+ beneficios[cont++] + " €";
        })
        cont = 0;
        arrayLabelGas = arrayLabelGas.map(label=>{
            arrayBackgroundColorsPerdida.push("hsl( " + getRandomColor(cont, arrayLabelGas.length) + ", 100%, 50% )")
            return label +": "+ perdidas[cont++] + " €";
        })

        let options2 = {

            legend: {
                display:true,
                position: 'top',
                align: 'center',
                labels: {
                    fontSize:13,
                    width:5
                }
            },

            title: {
                display: true,
                position: 'top',
                align: 'start',
                fontSize: 14,
                fontStyle: 'bold',
                fontcolor: '#d7d7d7',
                padding: 8,
            },
            elements: {
                center: {
                    text: '',
                    color: '#FF6384', // Default is #000000
                    fontStyle: 'sans-serif', // Default is Arial
                    sidePadding: 30, // Default is 20 (as a percentage)
                    minFontSize: 8, // Default is 20 (in px), set to false and text will not wrap.
                    lineHeight: 25 // Default is 25 (in px), used for when text wraps
                }
            },
            tooltips: {
                // mostrar el porcentaje en el tooltip
                callbacks: {
                    label: function(tooltipItem, data) {
                        var allData = data.datasets[tooltipItem.datasetIndex].data;
                        var tooltipLabel = data.labels[tooltipItem.index];
                        var tooltipData = allData[tooltipItem.index];
                        var total = 0;
                        for (var i in allData) {
                            total += allData[i];
                        }
                        var tooltipPercentage = Math.round((tooltipData / total) * 100);
                        return ' (' + tooltipPercentage + '%) ' + tooltipLabel;
                    }
                }
            },
            animation:{
                duration: 1
            },
            maintainAspectRatio: false,
            scaleBeginAtZero: false,
            responsive: true,
            cutoutPercentage: 70,

        };

        // graficas beneficios PDF
        let ctx2 = document.getElementById('grafica-beneficios-pdf');
        this.graficaBeneficiosPDF = new Chart(ctx2, {
            type: 'doughnut',
            data: {
                datasets: [{data: beneficios,backgroundColor: arrayBackgroundColors,}],
                // These labels appear in the legend and in the tooltips when hovering different arcs
                labels: arrayLabelBen
            },
            options: options2
        });
        // calculo del total para ponerlo en el texto central
        let total2 = 0;
        beneficios.forEach(e=>{total2+=e})
        this.graficaBeneficiosPDF.options.title.text = "Beneficios por parcela";
        this.graficaBeneficiosPDF.options.elements.center.text = total2+" €";
        this.graficaBeneficiosPDF.options.elements.center.color = "#46b63c";


        // grafica gastos PDF
        let ct2 = document.getElementById('grafica-gastos-pdf');
        this.graficaGastosPDF = new Chart(ct2, {
            type: 'doughnut',
            data: {
                datasets: [{data: perdidas,backgroundColor: arrayBackgroundColorsPerdida,}],
                // These labels appear in the legend and in the tooltips when hovering different arcs
                labels: arrayLabelGas
            },
            options: options2
        });

        total2 = 0;
        perdidas.forEach(e=>{total2+=e;})
        this.graficaGastosPDF.options.title.text = "Gastos por parcela";
        this.graficaGastosPDF.options.elements.center.text = total2+" €";
        this.graficaGastosPDF.options.elements.center.color = "#b73a3a";



    },//()

    // temporadas -> ()
    // pinta las gráfica temporadas en el canvas id = "grafica-balance"
    representarBalanceNetoPorMes: function(facturaciones){

        // juntar las facturas por mes
        // y por mes tener un balance total

        // primero ordenar por mes
        facturaciones = facturaciones.sort((f1,f2) =>{
            if(f1.fecha > f2.fecha){
                return 1;
            }else if(f1.fecha < f2.fecha){
                return -1;
            }
            return 0;
        });

        // coger el primer mes para luego pivotar sobre él
        // cuando cambie el mes habrá que crear una nueva posición en el
        // array resultado
        let mesAux = facturaciones[0].fecha.split("-")[1];
        let contMes = 0;
        // añadimos el balance neto del mes
        let data = [];
        // añadimos el mes
        let labels = [];
        labels.push(getStringMesFromNumber(mesAux))
        data[contMes] = 0;

        let dataBackgroundColors = [];// array que contendrá los colores de cada barra del mes

        facturaciones.forEach(facturacion=>{
            // coger el mes
            let mes = facturacion.fecha.split("-")[1];

            if(mesAux !== mes){
                // aumentar el contador del mes
                mesAux = mes;
                contMes++
                // si es diferente
                labels.push(getStringMesFromNumber(mes))

                let cantidad = 0;
                if(facturacion.perdida === 1){
                    // es perdida
                    cantidad = parseInt("-"+facturacion.cantidad)
                }else{
                    // es beneficio
                    cantidad = parseInt(facturacion.cantidad)
                }
                data[contMes] = 0;
                data[contMes] += cantidad;


            }else{
                let cantidad;
                if(facturacion.perdida === 1){
                    // es perdida
                    cantidad = parseInt("-"+facturacion.cantidad)
                }else{
                    // es beneficio
                    cantidad = parseInt(facturacion.cantidad)
                }
                data[contMes] += cantidad;
            }

            // antes de cambiar de mes
            // comprobamos si el anterior era beneficio o gasto
            // y añadimos el color de la barra del mes
            dataBackgroundColors[contMes] = (data[contMes] > 0 ? '#46B63C' : '#B63C3C');

        })

        let datosGrafica = {
            labels: labels,
            datasets: [{
                data: data,
                backgroundColor: dataBackgroundColors,
                barPercentage: 0.4
            }]
        };

        //Para PDF
        let ctx2 = document.getElementById('grafica-balance-pdf');
        ctx2.fillStyle = "#FFF"
        let options2 = {
            bezierCurve: false,
            connectNullData: true,
            spanGaps: true,
            scales: {
                yAxes: [{
                    ticks: {
                    },
                    scaleLabel: {
                        display: true,
                        labelString: "Balance neto (€)"
                    },
                    gridLines: {
                        color: [],
                        zeroLineWidth: 1,
                    }
                }],
            },
            animation:{
                duration: 0
            },
            maintainAspectRatio: false,
            legend: null,
            title: {
                display: true,
                position: 'top',
                align: 'start',
                fontSize: 16,
                fontStyle: 'bold',
                fontcolor: '#d7d7d7',
                padding: 8,
                text:"Balance neto mensual"
            },
            tooltips: {
                backgroundColor: '#fff',
                titleFontColor: '#000',
                titleAlign: 'center',
                bodyFontColor: '#333',
                borderColor: '#666',
                borderWidth: 1,
            },
            scaleBeginAtZero: true,
            responsive: true,
        };
        this.graficaBalancePDF = new Chart(ctx2, {
            type: 'bar',
            data: datosGrafica,
            options: options2,
        });

    },//()


    // Preparar la vista del pdf con los datos de las facturas de la temporada
    tablaFacturasTemporada: function(facturas){

        let balanceTotal = 0;

        for(let i = 0;i<(facturas.length);i++) {
            let tabla = document.getElementById("cuerpoPDF");
            let fila = document.createElement("tr");

            let concepto = document.createElement("td");
            let parcela = document.createElement("td");
            let fecha = document.createElement("td");
            let cantidad = document.createElement("td");

            let textoCeldaConcepto = document.createTextNode(facturas[i].concepto)
            let textoCeldaParcela = document.createTextNode(facturas[i].parcela != null ? facturas[i].parcela : "Sin asignar");
            let textoCeldaFecha = document.createTextNode(facturas[i].fecha != null ? facturas[i].fecha : "Sin asignar");

            let textoCeldaCantidad;
            if (facturas[i].perdida === 1) {
                textoCeldaCantidad = document.createTextNode(facturas[i].cantidad != null ? -facturas[i].cantidad + " €" : "Sin ingresos");
            }
            else {
                textoCeldaCantidad = document.createTextNode(facturas[i].cantidad != null ? facturas[i].cantidad + " €" : "Sin ingresos");
            }

            if (facturas[i].cantidad != null) {
                // sumamos el balance al total
                if (facturas[i].perdida === 1) {
                    balanceTotal -= parseInt(facturas[i].cantidad)
                } else {
                    balanceTotal += parseInt(facturas[i].cantidad)
                }
            }

            concepto.appendChild(textoCeldaConcepto)
            parcela.appendChild(textoCeldaParcela);
            fecha.appendChild(textoCeldaFecha);
            cantidad.appendChild(textoCeldaCantidad);

            fila.appendChild(concepto)
            fila.appendChild(parcela)
            fila.appendChild(fecha)
            fila.appendChild(cantidad)

            tabla.appendChild(fila)
        }
        document.getElementById("total").innerHTML = "TOTAL: " + balanceTotal + " €"

    }

}


var controladorPDF = {
    modelo: modeloPDF,
    vista: vistaPDF,

    iniciar: function (idTemporada) {
        this.modelo.controlador = this;
        this.vista.controlador = this;
        this.vista.mostrarCarga();
        this.modelo.cargar(idTemporada);

    },

    tablaFacturasTemporada: function (facturas) {
        this.vista.tablaFacturasTemporada(facturas)
    },

    representarGraficas : function(facturaciones){
        this.vista.representarGraficas(facturaciones);
    },

}

