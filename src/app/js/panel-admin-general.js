/**
 * Autor Carlos
 * Logica de carga de los MVC de panel-admin-general.html
 *
 */


function onClickBtnGroup(clientes){

    if(clientes){
        VistaListaClientes.mostrar = true;
        VistaListaUsuarios.mostrar = false;
        document.getElementById("btn-cliente").classList.add("active");
        document.getElementById("btn-usuario").classList.remove("active");
        document.getElementById("buscar-secundario").style.display = "none";
        document.getElementById("buscar-principal").style.display = "flex";

    }else{
        VistaListaClientes.mostrar = false;
        VistaListaUsuarios.mostrar = true;
        document.getElementById("btn-cliente").classList.remove("active");
        document.getElementById("btn-usuario").classList.add("active");
        document.getElementById("buscar-secundario").style.display = "flex";
        document.getElementById("buscar-principal").style.display = "none";
    }

    VistaListaUsuarios.esconderElementos();
    VistaListaClientes.esconderElementos();
    ControladorListaClientes.mostrarTabla();
    ControladorListaUsuarios.mostrarTabla();

}

let user =  JSON.parse(localStorage.getItem("user"));
// ponemos el nombre del usuario en el titulo
document.getElementById("titulo").innerText = user['nombre'];

VistaModificarUsuario.preparar("modal-error-usuarios","carga","formularioModalUsuarios","modal-editar-usuario","close-editar-usuario");
VistaModificarPassUsuario.preparar("modal-error-pass","carga","formularioModalPass","modal-editar-pass","close-editar-pass");
VistaCrearUsuario.preparar("modal-error-crear-usuarios","carga","formularioModalCrearUsuarios","modal-crear-usuario","close-crear-usuario");
VistaCrearCliente.preparar("modal-error-crear-cliente","carga","formularioModalCrearCliente","modal-crear-cliente","close-crear-cliente");
VistaModificarCliente.preparar("modal-error-modificar-cliente","cargar","formularioDetallesCliente","modal-modificar-cliente","close-modificar-cliente");

VistaListaClientes.preparar("lista-clientes","lista-clientes-body","error","carga","btnNextClientes","btnPrevClientes",true);
ControladorListaClientes.iniciar(
    ControladorCrearCliente,
    ControladorModificarCliente
);

VistaListaUsuarios.preparar("lista-usuarios","lista-usuarios-body","error","carga","btnNextUsuarios","btnPrevUsuarios",false);
ControladorListaUsuarios.iniciar(
    user,
    ControladorModificarUsuario,
    ControladorCrearUsuario,
    ControladorModificarPassUsuario
);

