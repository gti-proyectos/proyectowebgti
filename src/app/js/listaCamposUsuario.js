/**
 * Autor: Rubén Pardo Casanova
 * GTI GRUPO 11
 * Fecha: 07/05/2020
 *
 * Modelo vista controlador que gestiona la lista de los campos asignados de un usuario
 *
 */

var ModeloListaCamposUsuarios = {

    controlador: {},
    usuario: {},

    todosLosCampos:{},//contendrá todos los clientes siempre
    camposAsignados:{},//contendrá todos los clientes siempre
    camposFiltrados:{},// contendrá los clientes filtrados cuando se use el buscador o el ordenar

    paginaActual: 1,

    // al principio no estan ordenados
    // true = ASC, false = DESC
    ordenNombre: false,
    ordenDescripcion: false,
    ordenFecha: false,

    obtenerDatos: async function () {

        try{

            let camposCLiente = await fetch('../../src/api/v1.0/campo?idCliente=' + this.usuario.cliente)
                .then(respuesta => {
                    if (respuesta.status === 200) {
                        return respuesta.json();
                    } else {
                        this.controlador.error = true;
                        throw Error(respuesta.statusText);
                    }
                })// juntamos los dos arrays en uno

            let camposAsignados = await fetch('../../src/api/v1.0/campo?id=' + this.usuario.id)
                .then(respuesta => {
                    if (respuesta.status === 200) {
                        return respuesta.json();
                    } else {
                        this.controlador.error = true;
                        throw Error(respuesta.statusText);
                    }
                })// juntamos los dos arrays en uno

            // los items del array de los campos seran objetos del tipo:
            // {campo:campo,asignado:T/F}
            this.todosLosCampos = camposCLiente.map(campo => {
                let asignado = false;
                // buscamos el campo en los campos asignados
                camposAsignados.forEach(campoA =>{
                    if(campo.id === campoA.id){
                        asignado = true;
                        return;
                    }
                });
                return {campo:campo,asignado:asignado}
            });

            this.camposFiltrados = this.todosLosCampos;
            this.controlador.representarDatos(this.camposFiltrados,this.paginaActual);


        }catch (e) {
            console.error(e);
            this.controlador.representarError();
        }
    },

    desasignarCampo: async function (idCampo) {

        try{

            await fetch('../../src/api/v1.0/campoUsuario', {
                method: 'DELETE', // or 'PUT'
                body: JSON.stringify({idUsuario:this.usuario.id,idCampo:idCampo}) // data can be `string` or {object}!

            }).then(res => res.json())
                .catch(error => {throw Error(error)})
                .then(response => console.log('Success:', response));



            // si sale bien acutalizamos el elemento en local y nos evitamos el tener que refrescar
            // encontramos el elemento
            this.todosLosCampos = this.todosLosCampos.map(campo =>{
                console.log(campo)
                if(campo.campo.id === idCampo){
                    campo.asignado = false;
                    return campo;
                }else{
                    return campo;
                }
            })

            // volvemos a la pagina principal y pintamos de nuevo los datos
            this.paginaActual = 1;
            this.controlador.representarDatos(this.camposFiltrados,this.paginaActual);

        }catch (e) {
            console.error(e);
            this.controlador.representarError();
        }
    },
    asignarCampo: async function (idCampo) {

        try{
            let form = new FormData();
            form.append("modificar","false");
            form.append("data",JSON.stringify({idUsuario:this.usuario.id,idCampo:idCampo}));
            console.log(JSON.stringify({idUsuario:this.usuario.id,idCampo:idCampo}))
            await fetch('../../src/api/v1.0/campoUsuario', {
                method: 'POST', // or 'PUT'
                body: form // data can be `string` or {object}!
            }).then(res => res.json())
                .catch(error => {throw Error(error)})



            // si sale bien acutalizamos el elemento en local y nos evitamos el tener que refrescar
            // encontramos el elemento
            this.todosLosCampos = this.todosLosCampos.map(campo =>{
                if(campo.campo.id === idCampo){
                    campo.asignado = true;
                    return campo;
                }else{
                    return campo;
                }
            })


            // volvemos a la pagina principal y pintamos de nuevo los datos
            this.paginaActual = 1;
            this.controlador.representarDatos(this.camposFiltrados,this.paginaActual);

        }catch (e) {
            console.error(e);
            this.controlador.representarError();
        }
    },

    // filtrar los clientes tanto por nombre correo o telefono
    filtrarPorNombreDescripcion: function (textoAFiltrar) {
        // si no hay nada escrito poner todos los clientes
        if(textoAFiltrar.trim() === ""){
            this.camposFiltrados = this.todosLosCampos;
        }else if(textoAFiltrar.toLowerCase() === "asignado"){
            // que siempre salan los asignados arriba
            this.camposFiltrados =  this.todosLosCampos.filter((campo)=>{
                return campo.asignado;
            });

        }else if(textoAFiltrar.toLowerCase() === "desasignado"){
            // que siempre salan los asignados arriba
            this.camposFiltrados =  this.todosLosCampos.filter((campo)=>{
                return !campo.asignado;
            });

        }else{
            this.camposFiltrados = this.todosLosCampos.filter(campo =>{
                if(campo.campo.nombre!=null && campo.campo.nombre.toLowerCase().includes(textoAFiltrar.toLowerCase())){
                    return campo;
                }else if(campo.campo.descripcion!=null &&  campo.campo.descripcion.toLowerCase().includes(textoAFiltrar.toLowerCase())){
                    return campo;
                }
            })
        }
        // siempre que filtremos volvamos a la pagina 1
        this.paginaActual = 1;
        this.controlador.representarDatos(this.camposFiltrados,this.paginaActual);
    },

    // idOrden -> 1 = Nombre, 2 = Descripcion,
    // ascendente -> true = ordenar asc, false = ordenar desc
    ordenarPor:function (idOrden) {
        //ocultamos todas las flechas
        document.getElementById('flecha-nombre-campo').style.display="none";
        document.getElementById('flecha-desc-campo').style.display="none";
        document.getElementById('flecha-fecha-campo').style.display="none";

        let filtro = "";
        let ordenASC = null;

        // determinar el tipo de filtro y el orden ASC o DESC
        if(idOrden===1){
            //aparece la flecha
            document.getElementById('flecha-nombre-campo').style.display="inline";
            filtro = "nombre"
            // ordenar por nombre
            if(this.ordenNombre == null){
                // la primera vez ordenar ascendentemente
                this.ordenNombre = true;
            }else{
                // cade vez que se pulse de mas cambiarlo
                this.ordenNombre = !this.ordenNombre;
            }
            ordenASC = this.ordenNombre;
        }else{
            this.ordenNombre = false;
        }
        if(idOrden === 2){
            //aparece la flecha
            document.getElementById('flecha-desc-campo').style.display="inline";
            filtro = "descripcion";
            // ordenar por correo
            if(this.ordenDescripcion == null){
                // la primera vez ordenar ascendentemente
                this.ordenDescripcion = true;
            }else{
                // cade vez que se pulse de mas cambiarlo
                this.ordenDescripcion = !this.ordenDescripcion;
            }
            ordenASC = this.ordenDescripcion;
        }else{
            this.ordenDescripcion = false;
        }
        if(idOrden === 3){
            //aparece la flecha
            document.getElementById('flecha-fecha-campo').style.display="inline";
            filtro = "fechaCreacion";
            // ordenar por correo
            if(this.ordenFecha == null){
                // la primera vez ordenar ascendentemente
                this.ordenFecha = true;
            }else{
                // cade vez que se pulse de mas cambiarlo
                this.ordenFecha = !this.ordenFecha;
            }
            ordenASC = this.ordenFecha;
        }else{
            this.ordenFecha = false;
        }
        // ordenamos siempre desde clientes filtrados
        // al inicio seran todos y si hay algun filtro en el buscador se
        // ordenará sobre este
        this.camposFiltrados = this.camposFiltrados.sort((campo1, campo2) =>{
            // juntar todos los nulls o arriba o abajo
            if(campo1.campo[filtro] == null){
                return ordenASC ? 1 : -1;
            }else if(campo2.campo[filtro] == null ){
                return ordenASC ? -1 : 1;
            }else{
                // ordenar
                if(campo1.campo[filtro].toLowerCase() > campo2.campo[filtro].toLowerCase()){
                    return ordenASC ? 1 : -1;
                }else if(campo1.campo[filtro].toLowerCase() < campo2.campo[filtro].toLowerCase()){
                    return ordenASC ? -1 : 1;
                }
            }


            return 0;
        })

        // simepre que ordenemos volvemos a la pagina 1
        this.paginaActual = 1
        this.controlador.cambiarFlechasOrden(this.ordenNombre,this.ordenDescripcion, this.ordenFecha);
        this.controlador.representarDatos(this.camposFiltrados,this.paginaActual);
    },

    anteriorPagina:function () {
        this.paginaActual--;
        this.controlador.representarDatos(this.camposFiltrados,this.paginaActual);
    },

    siguientePagina: function () {
        this.paginaActual++;
        this.controlador.representarDatos(this.camposFiltrados,this.paginaActual);
    }

};

var VistaListaCamposUsuario = {

    controlador: {},
    usuario:{},

    bloqueLista:{}, // donde se representarán los elementos de la lista
    paginaActual: 1,// indice para controlar la paginacion
    maxElementosPorPagina: 5,
    bloqueError:{},
    bloqueCarga:{},
    bloqueBodyLista:{},

    radioGroup: null,

    // botones para el control de la paginacion de clientes
    btnNextPage:{},
    btnPreviousPage:{},

    preparar:function (idBloqueLista, idListaBody, idError, idCarga, idBtnNext, idBtnPrev,usuario,claseRadioGroup) {
        this.bloqueLista = document.getElementById(idBloqueLista);
        this.bloqueBodyLista = document.getElementById(idListaBody);
        this.bloqueError = document.getElementById(idError);
        this.bloqueCarga = document.getElementById(idCarga);
        this.btnPreviousPage = document.getElementById(idBtnPrev);
        this.btnNextPage = document.getElementById(idBtnNext);
        this.radioGroup =  document.getElementsByName(claseRadioGroup);
        this.usuario = usuario;
        this.prepararRadioGroup();
    },

    // on click a los radio group para que siempre que se pulse uno se desmarquen todos
    // asi se puede desmarcar el radio group
    prepararRadioGroup:function(){
        let booRadio;
        let x = 0;
        for(x = 0; x < this.radioGroup.length; x++) {

            this.radioGroup[x].onclick = function () {
                if (booRadio == this) {
                    this.checked = false;
                    booRadio = null;
                } else {
                    booRadio = this;
                }
                ControladorListaCamposUsuario.onChangeRadioGroup(this)
            };
        }
    },
    // funciones para controlar cuando se muestran los bloques html
    esconderElementos: function(){
        this.bloqueLista.style.display = "none";
        this.bloqueCarga.style.display = "none";
        this.bloqueError.style.display = "none";
    },
    mostrarCarga: function () {
        this.esconderElementos();
        this.bloqueCarga.style.display = "flex"

    },
    mostrarError:function () {
        this.esconderElementos();
        this.bloqueError.style.display = "flex"

    },
    mostrarLista:function () {
        this.esconderElementos();
        this.bloqueLista.style.display = "flex"
    },

    // representar elementos en el html
    representarDatos:function (campos,paginaActual) {
        // que siempre salan los asignados arriba
        campos = campos.sort((campo1,campo2)=>{
            if(campo1.asignado && !campo2.asignado){
                return -1;
            }else if(!campo1.asignado && campo2.asignado){
                return 1;
            }
            return 0;
        });


        //control de paginacion
        // maxPages = length/maxElementosPorPagina redondeado hacia arriba
        let maxPages = Math.ceil(campos.length/this.maxElementosPorPagina);
        // si estamos en la pagina 1 empezamos desde el 0 (5*(1-1))
        // si estamos en la pagina 2 empezamos desde el 5 (5*(2-1))
        let elementoInicial = this.maxElementosPorPagina * (paginaActual-1);

        this.prepararBotonesPaginador(paginaActual,maxPages);

        this.bloqueBodyLista.innerHTML = "";

        // pintar elemntos acorde al maxElementosPorPagina
        // los elementos iran desde (maxElementosPorPagina* (paginaActual-1)) hasta this.maxElementosPorPagina*paginaActual
        for(let i = elementoInicial;i<(this.maxElementosPorPagina*paginaActual);i++){
            // si en una pagina no llega al maxElementosPorPagina hay que evitar que intente pintar esos
            // elementos y no de error
            if(i<campos.length){
                let hilera = document.createElement("tr");

                let celdaNombre = document.createElement("td");
                let celdaDescripcion = document.createElement("td");
                let celdaFecha = document.createElement("td");
                let celdaOperacion = document.createElement("td");

                celdaOperacion.className = "list-opereciones-un-elemento";

                let textoCeldaNombre = document.createTextNode(campos[i].campo.nombre != null ? campos[i].campo.nombre : "Sin asignar");
                let textoCeldaDesc = document.createTextNode(campos[i].campo.descripcion != null ? campos[i].campo.descripcion : "Sin asignar");
                let textoCeldaFecha = document.createTextNode(campos[i].campo.fechaCreacion != null ? campos[i].campo.fechaCreacion : "Sin asignar");
                celdaNombre.appendChild(textoCeldaNombre);
                celdaDescripcion.appendChild(textoCeldaDesc);
                celdaFecha.appendChild(textoCeldaFecha);


                let btnAsignar = document.createElement("button");
                btnAsignar.className = "btn-list-element";
                btnAsignar.onclick = () =>{this.controlador.onCampoAsignar(campos[i])};
                btnAsignar.innerHTML = `<i class="fas fa-plus"></i><p class="label">Asignar</p>`

                let btnDesasignar = document.createElement("button");
                btnDesasignar.className = "btn-list-element";
                btnDesasignar.onclick = () =>{this.controlador.onCampoDesasignar(campos[i])};
                btnDesasignar.innerHTML = `<i class="fas fa-minus"></i><p class="label">Desasignar</p>`

                // si esta asignado le añadimos la clase campo-asignado
                // y le añadimos el boton desasignar sino el boton asignar
                if(campos[i].asignado){
                    celdaOperacion.appendChild(btnDesasignar);
                }else{
                    celdaOperacion.appendChild(btnAsignar);
                    hilera.className = "campo-no-asignado"
                }

                // añadimos los elementos a la fila
                hilera.appendChild(celdaNombre);
                hilera.appendChild(celdaDescripcion);
                hilera.appendChild(celdaFecha);

                hilera.appendChild(celdaOperacion)


                // añadimos la fila al tbody
                this.bloqueBodyLista.appendChild(hilera);
            }


        }


    },


    // habilitar y deshabilitar los botones que controlan la paginacion
    // para evitar errores
    prepararBotonesPaginador : function (paginaActual, maxPages) {
        this.btnNextPage.removeAttribute("disabled");
        this.btnPreviousPage.removeAttribute("disabled");

        document.getElementById("paginadorListaCampo").innerHTML = "Página "+paginaActual + " de " + maxPages;

        // si el maxPages es mayor que uno habilitar el next page
        if(maxPages>1){
            this.btnNextPage.removeAttribute("disabled");
        }
        // si el current page es igual al maxPages deshabilitar el next page
        if(paginaActual === maxPages){
            this.btnNextPage.setAttribute("disabled","disabled");
        }
        if(maxPages === 0){
            this.btnNextPage.setAttribute("disabled","disabled");
        }
        // si el current page es mayor que uno habilitar el previous page
        if(paginaActual > 1){
            this.btnPreviousPage.removeAttribute("disabled");
        }else{
            this.btnPreviousPage.setAttribute("disabled","disabled");
        }

    },

    cambiarFlechasOrden(ordenNombre, ordenDescripcion, ordenFecha) {
        document.getElementById("flecha-nombre-campo").className = (!ordenNombre) ? "fas fa-chevron-down" : "fas fa-chevron-up";
        document.getElementById("flecha-desc-campo").className = (!ordenDescripcion) ? "fas fa-chevron-down" : "fas fa-chevron-up";
        document.getElementById("flecha-fecha-campo").className = (!ordenFecha) ? "fas fa-chevron-down" : "fas fa-chevron-up";

    },

};

var ControladorListaCamposUsuario = {
    modelo: ModeloListaCamposUsuarios,
    vista: VistaListaCamposUsuario,
    controladorCreateUsuario: null,
    controladorModificarCampo: null,

    error: false,// controla si ha habido un error en la obtencio de datos

    iniciar: function (usuario) {
        this.modelo.controlador = this;
        this.modelo.usuario = usuario;
        this.vista.controlador = this;
        this.modelo.obtenerDatos();
    },

    cambiarFlechasOrden(ordenNombre, ordenDesc, ordenFecha) {
        this.vista.cambiarFlechasOrden(ordenNombre,ordenDesc, ordenFecha)
    },
    representarDatos: function(datos,paginaActual){
        this.vista.representarDatos(datos,paginaActual);
        this.vista.mostrarLista();
    },

    representarError: function () {
        this.vista.mostrarError();
    },

    onCampoAsignar: function (elemento) {
        this.vista.mostrarCarga();
        this.modelo.asignarCampo(elemento.campo.id);
    },

    onCampoDesasignar: function (elemento) {

        this.vista.mostrarCarga();
        this.modelo.desasignarCampo(elemento.campo.id);

    },

    onChangeRadioGroup: function (e) {
       if(!e.checked){
           // si se ha desmarcado quitar el filtro
           this.modelo.filtrarPorNombreDescripcion("");
       }else if(e.id.includes("desasignado")){
           this.modelo.filtrarPorNombreDescripcion("desasignado");
       }else{
           this.modelo.filtrarPorNombreDescripcion("asignado");
       }
    },

}