/**
 Autor: Rubén Pardo
 Fecha: 24/05/2020
 Descripción: Logica general del archivo lista-temporadas-html
 */



let idCliente = JSON.parse(localStorage.getItem("user")).cliente;
VistaCrearTemporada.preparar("modal-error-crear-temporada","formularioModalCrearTemporada",
    "modal-crear-temporada",idCliente);

//Modal de finalizar temporada
VistaFinalizarTemporada.preparar("modal-error-finalizar-temporada","formularioFinalizarTemporada","modal-finalizar-temporada",)

vistaTemporadas.preparar("lista-temporadas","lista-temporadas-body","error","carga","btnNextCampos","btnPrevCampos", "radio-group-temporada", true);
controladorTemporadas.iniciar(ControladorFinalizarTemporada);




// String -> () -> Date
// la fecha de hoy en formato YYYY-MM-DD
function getToday() {
    let today = new Date();
    return formatDate(today);
}

// DATE -> () -> YYYY-MM-DD
function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [year, month, day].join('-');
}