/**
 * Autor: Ruben Pardo
 * Fecha: 10/05/2020
 * Modelo Vista Controlador Editar una sonda
 *
 */

var ModeloModificarSonda = {
    controlador: {},

    editarSonda: async function (usuario) {

        try{
            let form = new FormData();
            form.append("modificar","true");
            form.append("data",JSON.stringify(usuario));

            await fetch('../../src/api/v1.0/posiciones',{
                method: 'POST',
                body: form,
            })
                .then(respuesta => {
                    if (respuesta.status === 200) {
                        return respuesta.json();
                    } else {
                        this.controlador.errorUpdate();
                        throw Error(respuesta.statusText);

                    }
                })


            this.controlador.editarUsuarioOk(usuario);


        }catch (e) {
            console.error(e);
        }
    },
};

var VistaModificarSonda = {

    controlador: {},
    formulario:{},
    modal:{},
    idParcela: -1,
    sonda:{},


    preparar:function (idError, idCarga,idFormulario,idModal,idCloseModal) {
        this.bloqueError = document.getElementById(idError);
        this.bloqueCarga = document.getElementById(idCarga);
        this.formulario = document.getElementById(idFormulario);
        this.modal = document.getElementById(idModal);
    },
    mostrarModal: function(){
        this.bloqueError.style.display = "none";
        this.modal.style.display = "block";
    },

    esconderModal: function(){
        this.modal.style.display = "none";
        this.formulario.lat.value =  "";
        this.formulario.lng.value =  "";
        this.formulario.nombre.value =  "";
    },

    mostrarError:function () {
        this.bloqueError.style.display = "block"

    },

    // funciones para el controlar del formulario
    prepararFormulario: function(){
        // cogemos todos los inputs del formulario y le asignamos los valores del cliente
        this.formulario.nombre.value =  this.sonda.nombre == null ? "" : this.sonda.nombre;
        this.formulario.lat.value =  this.sonda.latitud == null ? "" : this.sonda.latitud;
        this.formulario.lng.value =  this.sonda.longitud == null ? "" : this.sonda.longitud;
    },

    getSondaFormulario: function(){
        let sonda = {};

        // cogemos todos los inputs del formulario y le asignamos los valores del cliente
        sonda["id"] = this.sonda.id;
        sonda["parcela"] = this.idParcela;
        sonda["nombre"] = this.formulario.nombre.value;
        sonda["latitud"] = this.formulario.lat.value;
        sonda["longitud"] = this.formulario.lng.value;

        return sonda;
    },

}

var ControladorModificarSonda = {
    modelo: ModeloModificarSonda,
    vista: VistaModificarSonda,

    error: false,// controla si ha habido un error en la obtencio de datos

    iniciar: function (sonda,idParcela) {
        this.modelo.controlador = this;
        this.vista.idParcela = idParcela;
        this.vista.sonda = sonda;
        this.vista.controlador = this;
        this.vista.prepararFormulario();
        this.vista.mostrarModal();
    },

    representarError: function () {
        this.vista.mostrarError();
    },

    // eventos del on sumbit y on reset del formulario de los datos
    onSumbitFormulario: function(e){
        e.preventDefault();
        this.modelo.editarSonda(this.vista.getSondaFormulario())
    },
    onResetFormulario: function(e){
        e.preventDefault();
        this.vista.esconderModal();
    },

    errorUpdate: function(){
        this.vista.mostrarError();
    },
    editarUsuarioOk: function(){
        // llamar al callback de la lista para actualizarlo
        this.vista.esconderModal();
        window.location.reload();
    },

}
