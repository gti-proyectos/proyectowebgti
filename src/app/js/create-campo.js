/**
 * Autor: Ruben Pardo
 * Fecha: 08/05/2020
 * Modelo Vista Controlador Crear un usuario
 *
 */

var ModeloCrearCampo = {
    controlador: {},

    crearUsuario: async function (usuario) {

        try{
            let form = new FormData();
            form.append("modificar","false");
            form.append("data",JSON.stringify(usuario));

            await fetch('../../src/api/v1.0/campo',{
                method: 'POST',
                body: form,
            })
                .then(respuesta => {
                    if (respuesta.status === 200) {
                        return respuesta.json();
                    } else {
                        this.controlador.errorUpdate();
                        throw Error(respuesta.statusText);

                    }
                })


            this.controlador.crearUsuarioOk();


        }catch (e) {
            console.error(e);
        }
    },
};

var VistaCrearCampo = {

    controlador: {},
    formulario:{},
    modal:{},
    idCliente: -1,
    idUsuario: -1,


    preparar:function (idError, idCarga,idFormulario,idModal,idCloseModal) {
        this.bloqueError = document.getElementById(idError);
        this.bloqueCarga = document.getElementById(idCarga);
        this.formulario = document.getElementById(idFormulario);
        this.modal = document.getElementById(idModal);

    },
    mostrarModal: function(){
        this.bloqueError.style.display = "none";
        this.modal.style.display = "block";
    },

    esconderModal: function(){
        this.modal.style.display = "none";
        this.formulario.nombre.value =  "";
        this.formulario.descripcion.value =  "";
    },

    mostrarError:function () {
        this.bloqueError.style.display = "block"

    },


    getUsuarioFormulario: function(){
        let usuario = {};

        // cogemos todos los inputs del formulario y le asignamos los valores del cliente
        usuario["id"] = this.idUsuario;
        usuario["cliente"] = this.idCliente;
        usuario["nombre"] = this.formulario.nombre.value;
        usuario["descripcion"] = this.formulario.descripcion.value;

        return usuario;
    },

}

var ControladorCrearCampo = {
    modelo: ModeloCrearCampo,
    vista: VistaCrearCampo,

    error: false,// controla si ha habido un error en la obtencio de datos

    iniciar: function (idCliente) {
        this.modelo.controlador = this;
        this.vista.idCliente = idCliente;
        this.vista.controlador = this;
        this.vista.mostrarModal();
    },

    representarError: function () {
        this.vista.mostrarError();
    },

    // eventos del on sumbit y on reset del formulario de los datos
    onSumbitFormulario: function(e){
        e.preventDefault();
        this.modelo.crearUsuario(this.vista.getUsuarioFormulario())


    },
    onResetFormulario: function(e){
        e.preventDefault();
        this.vista.esconderModal();
    },

    errorUpdate: function(){
        this.vista.mostrarError();
    },
    crearUsuarioOk: function(){
        // llamar al callback de la lista para actualizarlo
        this.vista.esconderModal();
        window.location.reload();
    },
}
