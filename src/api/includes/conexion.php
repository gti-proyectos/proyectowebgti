<?php

/**
 * Autor: Rubén Pardo Casanova
 * GTI GRUPO 11
 * Fecha: 26/02/2020
 *
 * conexion.php
 * Establecemos la conexion a la base datos
 */

$serverNombre = "localhost";
$userNombre = "root";
$password = "";
$dbNombre = "rparcas_proyecto_web_g11";

// Crear la conexion
$conn = mysqli_connect($serverNombre, $userNombre,$password,$dbNombre);
// garantizar que lo que devuelve la bd esta codificado en utf8
mysqli_query($conn,'SET NAMES utf8');