<?php
/**
 * Autor: Rubén Pardo Casanova
 * GTI GRUPO 11
 * Fecha: 26/02/2020
 *
 * Vista JSON
 * Se define las cabeceras de la respuesta http
 * y se indica que se muestre en json
 *
 */

// indicamos la respuesta http
http_response_code($http_code);

//desde cualquier dominio se pueda hacer una peticion a esta api
header("Access-Control-Allow-Origin: *");//desde cualquier dominio se pueda hacer una peticion a esta api
//que metodos http estan permitidos
header("Access-Control-Allow-Methods: PUT, GET, POST, DELETE");//que metodos http estan permitidos
// la respuesta será en json
header('Content-type: application/json; charset=utf-8');
// enviamos la respuesta
echo json_encode($salida,JSON_PRETTY_PRINT);