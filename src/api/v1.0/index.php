<?php
/**
 * Autor: Rubén Pardo Casanova
 * GTI GRUPO 11
 * Fecha: 26/02/2020
 *
 * Index.php
 * Donde ese redireccionan todas las peticiones de la api
 * aqui se gestiona a que fichero se tiene que llamar
 *
 */
//incluimos la conexion de la base de datos
require_once '../includes/conexion.php';
//definimos la version de la api
define('API_VERSION','v1.0');
// cogemos el recurso y los parametros
$uri = explode(API_VERSION.'/',parse_url($_SERVER['REQUEST_URI'],PHP_URL_PATH))[1];
// convertimos $uri en un array donde tendremos el recurso y los parametros
$uri_array = explode('/',$uri);
// en el primer elemento tenemos el recurso
$recurso = array_shift($uri_array);

// obtenemos el metodo http
$operacion = strtolower($_SERVER['REQUEST_METHOD']);

// definimos la vista en la que queremos obtener los datos
$vista = 'json';

// en este array se guardaran los datos finales
$salida = array();

// por defecto definimos a 404
$http_code = 404;
@include "modelos/$operacion-$recurso.php";
@include "vistas/$vista.php";




