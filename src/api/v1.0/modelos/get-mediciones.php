<?php

/**
 * Autor: Aitor Benítez Estruch
 * GTI GRUPO 11
 * Fecha: 28/03/2020
 *
 * get-mediciones.php obtiene los datos de un tipo de medida (temperatura, humedad...),
 * de la/s posicion/es que se quieren consultar.
 * Parámetros a pasar:
 *  - tipo de medida (0 = temperatura, 1 = salinidad, 2 = iluminación, 3 = humedad).
 *  - id de la posición.
 *  - Rango de tiempo (fecha de inicio y fecha de fin)
 * */

session_start();

if (isset($_SESSION['registrado']) && $_SESSION['registrado'] == 'ok' && isset($_GET['idPos'])){

    //En caso de pasar también el tipo de medida, y fechas de inicio y fin de un periodo, estos
    //datos serán para representarse en un gráfico
    if(isset($_GET['tipoMedida']) && $_GET['tipoMedida'] >= 0 && $_GET['tipoMedida']<=3){

        switch ($_GET['tipoMedida']){
            case 0:
                $tipoMedicion = 'temperatura';
                $medida = 'm.temperatura as medida';
                break;
            case 1:
                $tipoMedicion = 'salinidad';
                $medida = 'm.salinidad as medida';
                break;
            case 2:
                $tipoMedicion = 'iluminacion';
                $medida = 'm.iluminacion as medida';
                break;
            case 3:
                $tipoMedicion = 'humedad';
                $medida = 'm.humedad as medida';
                break;
        }

        //Pasar los valores almacenados en $_GET['idPos'] a un array
        $a =array();
        $a = explode(",",$_GET['idPos']);
        //Pasar los valores almacenados en el array a un string separados por una coma
        $b= implode(",", $a);

        $sql = "SELECT m.id as id, m.fecha as fecha, $medida, m.idPosicion as idPosicion, pos.nombre as nombrePos FROM   
            posiciones pos inner join mediciones m on pos.id = m.idPosicion where pos.id in (".$b.")";


        $filtros = array();
        if(isset($_GET['fechaIni'])) array_push($filtros, "m.fecha >= '". $_GET['fechaIni']."'");
        if(isset($_GET['fechaFin'])) array_push($filtros, "m.fecha <= '". $_GET['fechaFin']."'");

        if(count($filtros)>0) $sql.= ' AND ' . join(' AND ',$filtros);


        $stmt = $conn->prepare($sql);

        $stmt -> execute();

        $resultSet = $stmt->get_result();

        //Conversión de las fechas a tiempo en segundos
        $fechaIni = strtotime($_GET['fechaIni']);
        $fechaFin = strtotime($_GET['fechaFin']);

        $tiempo = $fechaFin - $fechaIni;

        //array donde se guardarán las id de las posiciones
        $posiciones = array();

        //Si tiempo > a 15 dias, cojer medidas cada hora (1 de cada 3 medidas)
        if($tiempo >= 1296000 && $tiempo < 7776000){
            //Contador
            $cont = 0;
            while ($fila = mysqli_fetch_assoc($resultSet)){
                if($cont == 2){
                    //Variable booleana
                    $is = 0;

                    //saber si el id de la posición está incliudo en el array posiciones
                    for ($i = 0; $i<count($posiciones); $i++){
                        if($posiciones[$i] == $fila['idPosicion']){
                            $is = 1;
                        }
                    }
                    //definición de un objeto con los valores del id de la medida, valor de la medida i fecha/hora
                    $obj['id'] = $fila['id'];
                    $obj['medida'] = $fila['medida'];
                    $obj['fecha'] = $fila['fecha'];


                    // objeto aux que contiene el id de la posición, nombre, tipo de medida i los datos recogidos da cada sonda
                    $aux['idPosicion'] = '';
                    $aux['nombrePosicion'] = '';
                    $aux['medida'] = '';
                    $aux['mediciones'] = [];

                    //Si el id de la posición no está incluido en el array posiciones
                    if ($is==0){
                        $aux['idPosicion'] = $fila['idPosicion'];
                        $aux['nombrePosicion'] = $fila['nombrePos'];
                        $aux['medida'] = $tipoMedicion;
                        array_push($aux['mediciones'], $obj);
                        array_push($salida, $aux);
                        array_push($posiciones,$fila['idPosicion']);
                    }else{
                        //En caso de que el id de la parcela ya esté incluido en el array
                        for ($i = 0; $i<count($posiciones); $i++){
                            if($fila['idPosicion'] == $salida[$i]['idPosicion']){
                                array_push($salida[$i]['mediciones'], $obj);
                            }
                        }
                    }
                    $cont = 0;
                }else{
                    $cont ++;
                }
            }
        }
        // Si el tiempo es mayor a 90 dias cojer 1 de cada 15 medidas (1 medida cada 5 horas)
        else if($tiempo >= 7776000){
            //Contador
            $cont = 0;
            while ($fila = mysqli_fetch_assoc($resultSet)){
                if($cont == 14){
                    //Variable booleana
                    $is = 0;

                    //saber si el id de la posición está incliudo en el array posiciones
                    for ($i = 0; $i<count($posiciones); $i++){
                        if($posiciones[$i] == $fila['idPosicion']){
                            $is = 1;
                        }
                    }
                    //definición de un objeto con los valores del id de la medida, valor de la medida i fecha/hora
                    $obj['id'] = $fila['id'];
                    $obj['medida'] = $fila['medida'];
                    $obj['fecha'] = $fila['fecha'];


                    // objeto aux que contiene el id de la posición, nombre, tipo de medida i los datos recogidos da cada sonda
                    $aux['idPosicion'] = '';
                    $aux['nombrePosicion'] = '';
                    $aux['medida'] = '';
                    $aux['mediciones'] = [];

                    //Si el id de la posición no está incluido en el array posiciones
                    if ($is==0){
                        $aux['idPosicion'] = $fila['idPosicion'];
                        $aux['nombrePosicion'] = $fila['nombrePos'];
                        $aux['medida'] = $tipoMedicion;
                        array_push($aux['mediciones'], $obj);
                        array_push($salida, $aux);
                        array_push($posiciones,$fila['idPosicion']);
                    }else{
                        //En caso de que el id de la parcela ya esté incluido en el array
                        for ($i = 0; $i<count($posiciones); $i++){
                            if($fila['idPosicion'] == $salida[$i]['idPosicion']){
                                array_push($salida[$i]['mediciones'], $obj);
                            }
                        }
                    }
                    $cont = 0;
                }else{
                    $cont ++;
                }
            }

            //definición de un objeto con los valores del id de la medida, valor de la medida i fecha/hora
            $obj['id'] = $fila['id'];
            $obj['medida'] = $fila['medida'];
            $obj['fecha'] = $fila['fecha'];


            // objeto aux que contiene el id de la posición, nombre, tipo de medida i los datos recogidos da cada sonda
            $aux['idPosicion'] = '';
            $aux['nombrePosicion'] = '';
            $aux['medida'] = '';
            $aux['mediciones'] = [];

            //Si el id de la posición no está incluido en el array posiciones
            if ($is==0){
                $aux['idPosicion'] = $fila['idPosicion'];
                $aux['nombrePosicion'] = $fila['nombrePos'];
                $aux['medida'] = $tipoMedicion;
                array_push($aux['mediciones'], $obj);
                array_push($salida, $aux);
                array_push($posiciones,$fila['idPosicion']);
            }else{
                //En caso de que el id de la parcela ya esté incluido en el array
                for ($i = 0; $i<count($posiciones); $i++){
                    if($fila['idPosicion'] == $salida[$i]['idPosicion']){
                        array_push($salida[$i]['mediciones'], $obj);
                    }
                }
            }
        }
        //En caso de que el tiempo no llegue a los 15 dias, mostrar datos recojidos cada cada 20 minutos
        else{
            while ($fila = mysqli_fetch_assoc($resultSet)){
                //Variable booleana
                $is = 0;

                //saber si el id de la posición está incliudo en el array posiciones
                for ($i = 0; $i<count($posiciones); $i++){
                    if($posiciones[$i] == $fila['idPosicion']){
                        $is = 1;
                    }
                }

                //definición de un objeto con los valores del id de la medida, valor de la medida i fecha/hora
                $obj['id'] = $fila['id'];
                $obj['medida'] = $fila['medida'];
                $obj['fecha'] = $fila['fecha'];


                // objeto aux que contiene el id de la posición, nombre, tipo de medida i los datos recogidos da cada sonda
                $aux['idPosicion'] = '';
                $aux['nombrePosicion'] = '';
                $aux['medida'] = '';
                $aux['mediciones'] = [];

                //Si el id de la posición no está incluido en el array posiciones
                if ($is==0){
                    $aux['idPosicion'] = $fila['idPosicion'];
                    $aux['nombrePosicion'] = $fila['nombrePos'];
                    $aux['medida'] = $tipoMedicion;
                    array_push($aux['mediciones'], $obj);
                    array_push($salida, $aux);
                    array_push($posiciones,$fila['idPosicion']);
                }else{
                    //En caso de que el id de la parcela ya esté incluido en el array
                    for ($i = 0; $i<count($posiciones); $i++){
                        if($fila['idPosicion'] == $salida[$i]['idPosicion']){
                            array_push($salida[$i]['mediciones'], $obj);
                        }
                    }
                }
            }
        }
        $http_code = 200;
    }else{
        // Si no se pasa datos de tipo de medida ni de tiempo, se obtiene la última medición realizada de esa sonda
        $sql = 'SELECT * FROM mediciones m WHERE m.idPosicion = ? ORDER BY fecha DESC LIMIT 1';
        $stmt = $conn->prepare($sql);

        $stmt->bind_param("s",$_GET['idPos']);

        $stmt -> execute();

        $resultSet = $stmt->get_result();

        if($fila = mysqli_fetch_assoc($resultSet)){
            array_push($salida, $fila);
        }
        $http_code = 200;
    }


}else{
    $http_code = 401;
}