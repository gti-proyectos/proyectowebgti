<?php

session_start();
// comprobamos la sesion
if(isset($_SESSION['registrado']) && $_SESSION['registrado'] == 'ok'){
    // modificar campo obligatorio
    if(isset($_POST['idTemporada']) && isset($_POST['datos'])){


        // cogemos los datos
        $idTemporada = $_POST['idTemporada'];

        $data = json_decode($_POST['datos'],true);// convertimos la cadena de texto JSON a un array asociativo
        
        $parcela = $data['idParcela'];
        $concepto = $data['concepto'];
        $cantidad = $data['cantidad'];
        $fecha = $data['fecha'];
        $perdida = $data['tipoFactura'];


        // comprobamos los parametros obligatorios ( nombre )
        if($parcela != null && $concepto !=null && $cantidad !=null && $fecha !=null){
                $sql = "INSERT INTO facturacion (idTemporada, idParcela, concepto, cantidad, fecha, perdida) values(?, ?, ?, ?,?,?);";

                // creamos una prepared statment
                $stmt = $conn->prepare($sql);
                // por cada ? definimos que parametro será y el valor que tendrá:
                $stmt->bind_param("ssssss",$idTemporada,$parcela, $concepto, $cantidad, $fecha, $perdida );
                $stmt->execute();
                // comprobar que se ha modificado correctamente
                if(mysqli_affected_rows($conn)>0){
                    array_push($salida,"Factura creada correctamente");
                    $http_code = 200;
                }else{
                    array_push($salida,"Error sql. No se ha podido crear el usuario ");
                    $http_code = 400;
                }
                $stmt->close();

        }
        else{
            array_push($salida,"Faltan parametros obligatorios(parcela, concepto, cantidad");
            // falta el parametro nombre
            $http_code = 400;
        }
    }
    else{
        array_push($salida,"Faltan parametros (modifcar)");
        $http_code = 400;
    }

}else{
     //no ha iniciado sesion
    array_push($salida,"Ninguna sesion activada");
    $http_code = 401;
}
