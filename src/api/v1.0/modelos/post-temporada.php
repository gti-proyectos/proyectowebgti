<?php
/**
 * Autor: Carlos Ramirez Dorado
 * GTI GRUPO 11
 * Fecha: 21/05/2020
 *
 * post-temporada
 * Crear y modificar temporada
 *
 */
session_start();
// comprobamos la sesion
if (isset($_SESSION['registrado']) && $_SESSION['registrado'] == 'ok') {
    // modificar campo obligatorio
    if (isset($_POST['modificar']) && isset($_POST['data'])) {
        // cogemos los datos
        $modificar = $_POST['modificar'];
        $data = json_decode($_POST['data'], true);// convertimos la cadena de texto JSON a un array asociativo
        // cogemos todos los datos posibles
        $id = $data['id'];
        $cliente = $data['idCliente'];
        $nombre = $data['nombre'];
        $descripcion = $data['descripcion'];
        $fecha = $data['fechaInicio'];
        $parcelas = $data['parcela'];
        // modificar parcela
        if ($modificar == 'true') {
            // editamos
            // campos obligatorios
            if ($nombre != null && $id != null) {


                    //update de la temporada
                    $sql = 'UPDATE temporada set nombre = ?, descripcion = ? where id = ?';

                    $stmt = $conn->prepare($sql);
                    // por cada ? definimos que parametro será y el valor que tendrá:
                    $stmt->bind_param("sss",$nombre,$descripcion, $id );
                    $stmt->execute();
                    // comprobar que se ha modificado correctamente
                    if(mysqli_affected_rows($conn)>0){
                        array_push($salida,"Temporada modificada correctamente");
                        $http_code = 200;
                    }else{
                        array_push($salida,"Error sql. No se ha podido modificar la temporada ");
                        $http_code = 400;
                    }
                    $stmt->close();


            }
            else {
                // falta parametros
                array_push($salida, "Faltan parametros obligatorios (id, nombre)");
                $http_code = 400;
            }


        } // crear temporada
        else {
            // comprobamos los parametros obligatorios ( nombre )
            if ($nombre != null && $cliente != null && $parcelas != null && $fecha != null ) {

                $http_code= 200;
                // transaccion, se va a lanzar muchas sentencias, se requiere que se hagan todas o ninguna
                $conn->autocommit(false);
                try {
                    //create de la temporada
                    $query = $conn->query("INSERT into  temporada (nombre, idCliente, descripcion, fechaInicio) values('".$nombre."','".$cliente."','".$descripcion."','".$fecha."')");
                    // si falla lanzar error
                    if ($query === FALSE) {
                        throw new Exception("Insertando temporada,".$conn->error);
                    }
                    $id = $conn->insert_id;
                    // update por cada parcela
                    foreach ($parcelas as $parcela){
                        $query = $conn->query("INSERT into  `temporadas-parcelas` (idParcela,idTemporada) values('".$parcela."','".$id."')");
                        if ($query === FALSE) {
                            throw new Exception("Insertando parcela, ".$conn->error);
                        }
                    }
                    $conn->commit();
                    $http_code = 200;
                    array_push($salida, 'Temporada creada correctamente');

                } catch (Exception $e) {
                    $conn->rollback();
                    $conn->autocommit(true);
                    $http_code = 400;
                    array_push($salida, 'Error al crear temporada. '.$e->getMessage());
                }
                $conn->autocommit(true);

            } else {
                array_push($salida, "Faltan parametros obligatorios (nombre, cliente, parcelas, fechaInicio");
                $http_code = 400;
            }
        }
    }
    else {
        array_push($salida, "Falta parametros (modifcar)");
        $http_code = 400;
    }
}
else {
    // no ha iniciado sesion
    array_push($salida, "Ninguna sesion activada");
    $http_code = 401;
}