<?php
/**
 * Autor: Carlos Ramirez Dorado
 * GTI GRUPO 11
 * Fecha: 21/05/2020
 *
 * get-temporada
 * Comprueba que existe una temporada y la devuelve
 * en el caso de existir
 *
 */
session_start();
if(isset($_SESSION['registrado']) && $_SESSION['registrado'] == 'ok') {
    if(isset($_GET['idCliente']) && isset($_GET['idCampo']) && isset($_GET['idUsuario'])
        && $_GET['idCliente'] !=null && $_GET['idCampo'] !=null && ($_GET['idUsuario'])!=null){
        // definimos la sentencia sql con los ? para el prepared statment

        //SELECT TEMPORADAS  CON SUS PARCELAS
        $sql = "SELECT t.id, t.idCliente, t.nombre, t.descripcion, t.fechaInicio,t.fechaFin, t.terminada, tp.idParcela as parcela,
            (SELECT SUM(CASE WHEN perdida = 0 THEN cantidad ELSE -cantidad END) FROM facturacion WHERE idTemporada = t.id) as balance FROM temporada t INNER JOIN cliente c on c.id = t.idCliente 
            INNER JOIN `temporadas-parcelas` tp on tp.idTemporada = t.id WHERE c.id = ?";
        // creamos una prepared statment
        $stmt = $conn->prepare($sql);

        // por cada ? definimos que parametro será y el valor que tendrá:
        $stmt->bind_param("s", $_GET['idCliente']); // la s indica que el parametro es un string
        // ejecutamos la query
        $stmt->execute();

        // cogemos el resultado sql
        $resultSet = $stmt->get_result(); // get the mysqli result
        $resultadoGetTemporada = array();
        // recorremos el array
        while ($fila = mysqli_fetch_assoc($resultSet)) {
            // creamos el array asociativo final
            array_push($resultadoGetTemporada, $fila);
        }

        // del sql devuelve con el inner join como temporada1 - parcela 1 // temporada1 - parcela2
        // juntarlo de la forma temporada 1 - parcela1,parcela2
        // juntamos los resultados repetidos
        $idAux = $resultadoGetTemporada[0]['id'];
        $cont = 0;
        $temporadas = array();
        // ponemos los datos del primer elemento
        $temporadas[$cont]['id'] = $resultadoGetTemporada[0]['id'];
        $temporadas[$cont]['idCliente'] = $resultadoGetTemporada[0]['idCliente'];
        $temporadas[$cont]['nombre'] = $resultadoGetTemporada[0]['nombre'];
        $temporadas[$cont]['descripcion'] = $resultadoGetTemporada[0]['descripcion'];
        $temporadas[$cont]['fechaInicio'] = $resultadoGetTemporada[0]['fechaInicio'];
        $temporadas[$cont]['fechaFin'] = $resultadoGetTemporada[0]['fechaFin'];
        $temporadas[$cont]['terminada'] = $resultadoGetTemporada[0]['terminada'];
        $temporadas[$cont]['balance'] = $resultadoGetTemporada[0]['balance'];
        $contParcela = 0;
        foreach($resultadoGetTemporada as $resultado){
            if($idAux != $resultado["id"]){
                $idAux = $resultado["id"];
                $contParcela = 0;
                // ha cambiado de temporada
                $cont++;
                $temporadas[$cont]['id'] = $resultado['id'];
                $temporadas[$cont]['idCliente'] = $resultado['idCliente'];
                $temporadas[$cont]['nombre'] = $resultado['nombre'];
                $temporadas[$cont]['descripcion'] = $resultado['descripcion'];
                $temporadas[$cont]['fechaInicio'] = $resultado['fechaInicio'];
                $temporadas[$cont]['fechaFin'] = $resultado['fechaFin'];
                $temporadas[$cont]['terminada'] = $resultado['terminada'];
                $temporadas[$cont]['balance'] = $resultado['balance'];
            }

            $temporadas[$cont]['parcelas'][$contParcela++] = $resultado['parcela'];

        }
        // fin de juntar los temporadas repetidas


        //SELECT DE LAS PARCELAS DEL CAMPO SELECCIONADO
        $sql = "SELECT id FROM parcela WHERE idCampo = ?";
        // creamos una prepared statment
        $stmt = $conn->prepare($sql);

        // por cada ? definimos que parametro será y el valor que tendrá:
        $stmt->bind_param("s", $_GET['idCampo']); // la s indica que el parametro es un string
        // ejecutamos la query
        $stmt->execute();

        $parcelasUsuario = array();
        // cogemos el resultado sql
        $resultSet = $stmt->get_result(); // get the mysqli result
        // recorremos el array
        while ($fila = mysqli_fetch_assoc($resultSet)) {
            // creamos el array asociativo final
            array_push($parcelasUsuario, $fila);
        }

        // ahora comprobamos que temporadas tiene acceso el usuario
        $resultado = array();
        foreach ($temporadas as $temporada){
            $valid = false;
            // las parcelas son sus id

            foreach ($temporada['parcelas'] as $parcelaTemporada){
                // si una de las parcelas de la temporada esta en las parcelas del usuario
                // es que tiene acceso
                foreach ($parcelasUsuario as $parcelaUsuario){
                    if($parcelaTemporada == $parcelaUsuario["id"]){
                        $valid = true;
                    }
                }
            }
            if($valid){
                array_push($salida, $temporada);
            }
        }
        // se ha encontrado el usuario y se va a devolver correctamente
        $http_code = 200;
    }
    else{
        //faltan parametros
        array_push($array1, "Error en get-temporadas. Faltan parametros obligatorios (idCliente, idUsuario)");
        $http_code = 401;
    }
}
else {
    //no ha iniciado sesion
    array_push($array1, "Error en get-temporadas. Ninguna sesion encontrada");
    $http_code = 401;
}
