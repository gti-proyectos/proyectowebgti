<?php
/**
 * Autor: Rubén Pardo
 * Fecha: 29/04/2020
 * Descripción: Crear o modificar clientes
 *
 */
session_start();
// comprobamos la sesion
if(isset($_SESSION['registrado']) && $_SESSION['registrado'] == 'ok'){
    // modificar campo obligatorio
    if(isset($_POST['modificar']) && isset($_POST['data'])){
        // cogemos los datos
        $modificar = $_POST['modificar'];
        $data = json_decode($_POST['data'],true);// convertimos la cadena de texto JSON a un array asociativo

        // modificar cliente
        if($modificar == 'true'){
            // editamos el cliente
            $id = $data['id'];
            $nombre = $data['nombre'];
            $apellidos = $data['apellidos'];
            $direccion = $data['direccion'];
            $correo = $data['correo'];
            $telefono = $data['telefono'];

            // id y nombre obligatorios
            if($nombre != null && $id != null){

                $sql = "UPDATE cliente set nombre = ?, apellidos=?, direccion = ?,correo = ?, telefono = ? where id = ?;";

                // creamos una prepared statment
                $stmt = $conn->prepare($sql);
                // por cada ? definimos que parametro será y el valor que tendrá:
                $stmt->bind_param("ssssss",$nombre,$apellidos,$direccion, $correo, $telefono, $id);

                $stmt->execute();
                // comprobar que se ha modificado correctamente
                if(mysqli_affected_rows($conn) > 0 ){
                    array_push($salida,"Cliente modificado");
                    $http_code = 200;
                }else{
                    array_push($salida,"No se ha modifcado nada. O no existe el usuario o son los mismos datos");
                    $http_code = 400;
                }
                $stmt->close();
            }else{
                // falta el parametro nombre o id
                array_push($salida,"Faltan parametros obligatorios (id, nombre");
                $http_code = 400;
            }



        }
        // crear cliente
        else{
            $nombre = $data['nombre'];
            $direccion = $data['direccion'];
            $apellidos = $data['apellidos'];
            $correo = $data['correo'];
            $telefono = $data['telefono'];
            // comprobamos los parametros obligatorios ( nombre )
            if($nombre != null){
                $sql = "INSERT INTO cliente (nombre, apellidos, direccion,correo, telefono) values(?,?, ?, ?, ?);";

                // creamos una prepared statment
                $stmt = $conn->prepare($sql);
                // por cada ? definimos que parametro será y el valor que tendrá:
                $stmt->bind_param("sssss",$nombre,$apellidos,$direccion, $correo, $telefono);
                $stmt->execute();
                // comprobar que se ha modificado correctamente
                if(mysqli_affected_rows($conn)>0){
                    array_push($salida,"Cliente creado");
                    $http_code = 200;
                }else{
                    array_push($salida,"Error sql. No se ha podido crear el cliente ");
                    $http_code = 400;
                }
                $stmt->close();


            }else{
                // falta el parametro nombre
                array_push($salida,"Faltan parametros obligatorios (nombre)");
                $http_code = 400;
            }

        }


    }
    else{
        array_push($salida,"Faltan parametros obligatorios (modificar)");
        $http_code = 400;
    }


}else{
    // no ha iniciado sesion
    array_push($salida,"Ninguna sesion activada");
    $http_code = 401;
}