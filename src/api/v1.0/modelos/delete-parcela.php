<?php
/**
 * Autor: Carlos Ramirez
 * Fecha: 03/05/2020
 * Descripción: Borrar campo
 *
 */


session_start();

// comprobamos la sesion
if(isset($_SESSION['registrado']) && $_SESSION['registrado'] == 'ok'){


    // cogemos los datos pasados en el body
    $datosRecibidos = json_decode(file_get_contents('php://input'),true);

    if(isset($datosRecibidos['id'])){
        $sql = "DELETE FROM parcela where id = ?;";

        $id = $datosRecibidos['id'];

        // creamos una prepared statment
        $stmt = $conn->prepare($sql);
        // por cada ? definimos que parametro será y el valor que tendrá:
        $stmt->bind_param("s", $id);

        $stmt->execute();
        // comprobar que se ha modificado correctamente
        if(mysqli_affected_rows($conn) > 0 ){
            array_push($salida,"Parcela borrada correctamente");
            $http_code = 200;
        }
        else{
            // no se ha borrado
            array_push($salida,"No se ha podido borrar la parcela. No existe");
            $http_code = 400;
        }
        $stmt->close();
    }
    else{
        array_push($salida,"Falta parametro obligatorio (id)");
        $http_code = 400;
    }

}
else{
    // no ha iniciado sesion
    array_push($salida,"Ninguna sesion activada");
    $http_code = 401;
}