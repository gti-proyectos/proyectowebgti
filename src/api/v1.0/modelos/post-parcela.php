<?php
/**
 * Autor: Marc Castelló Martí y Rubén Pardo
 * GTI GRUPO 11
 * Fecha: 03/05/2020
 *
 * post-parcela
 * Crear y modificar parcelas
 *
 */
session_start();
// comprobamos la sesion
if (isset($_SESSION['registrado']) && $_SESSION['registrado'] == 'ok') {
    // modificar campo obligatorio
    if (isset($_POST['modificar']) && isset($_POST['data'])) {
        // cogemos los datos
        $modificar = $_POST['modificar'];
        $data = json_decode($_POST['data'], true);// convertimos la cadena de texto JSON a un array asociativo
        // cogemos todos los datos posibles
        $idParcela = $data['id'];
        $nombre = $data['nombre'];
        $descripcion = $data['descripcion'];
        $campo = $data['campo'];
        $vertices = $data['vertices'];
        // modificar parcela
        if ($modificar == 'true') {
            // editamos
            // campos obligatorios
            if ($nombre != null && $idParcela != null && $campo != null && $vertices != null) {


                // transaccion, se va a lanzar muchas sentencias, se requiere que se hagan todas o ninguna
                $conn->autocommit(false);
                try {
                    //update de la parcela
                    $query = $conn->query('UPDATE parcela set nombre ="'.$nombre.'",descripcion = "'.$descripcion.'",idCampo = "'.$campo.'" where id ="'.$idParcela.'"');
                    // si falla lanzar error
                    if ($query === FALSE) {
                        throw new Exception("No existe el campo");
                    }

                    // borramos todos los vertices que habian
                    $query = $conn->query('DELETE from vertices where idParcela = '.$idParcela);

                    // creamos los nuevos vertices
                    foreach ($vertices as $vertice){
                        if($vertice['lat']!=null && $vertice['lng'] != null){
                            $query = $conn->query("INSERT into  vertices (latitud, longitud, idParcela) values('".$vertice['lat']."','".$vertice['lng']."','".$idParcela."')");
                            if ($query === FALSE) {
                                throw new Exception("Insertando vertice, ".$conn->error);
                            }
                        }else{
                            throw new Exception("No se han pasado los parametros obligatorios de un vertice(lat, lng)");
                        }
                    }
                    $conn->commit();
                    $http_code = 200;
                    array_push($salida, 'Parcela modificada correctamente');

                } catch (Exception $e) {
                    $conn->rollback();
                    $conn->autocommit(true);
                    $http_code = 400;
                    array_push($salida, 'Error al modificar parcela. '.$e->getMessage());
                }
                $conn->autocommit(true);

            }
            else {
                // falta el parametro nombre e id
                array_push($salida, "Faltan parametros obligatorios (id, nombre, vertices, campo)");
                $http_code = 400;
            }


        } // crear parcela
        else {
            // comprobamos los parametros obligatorios ( nombre )
            if ($nombre != null && $campo != null && $vertices != null) {

            // transaccion, se va a lanzar muchas sentencias, se requiere que se hagan todas o ninguna
            $conn->autocommit(false);
            try {
                //fecha actual
                $fecha = strval(date("Y-m-d"));
                //update de la parcela
                $query = $conn->query("INSERT into  parcela (nombre, descripcion, idCampo, fechaCreacion) values('".$nombre."','".$descripcion."','".$campo."','".$fecha."')");
                // si falla lanzar error
                if ($query === FALSE) {
                    throw new Exception("Insertando parcela,".$conn->error);
                }
                $idParcela = $conn->insert_id;
                // update por cada vertice
                foreach ($vertices as $vertice){
                    if($vertice['lat']!=null && $vertice['lng'] != null){
                        $query = $conn->query("INSERT into  vertices (latitud, longitud, idParcela) values('".$vertice['lat']."','".$vertice['lng']."','".$idParcela."')");
                        if ($query === FALSE) {
                            throw new Exception("Insertando vertice, ".$conn->error);
                        }
                    }else{
                        throw new Exception("No se han pasado los parametros obligatorios de un vertice(lat, lng,id)");
                    }
                }
                $conn->commit();
                $http_code = 200;
                array_push($salida, 'Parcela creada correctamente');

            } catch (Exception $e) {
                $conn->rollback();
                $conn->autocommit(true);
                $http_code = 400;
                array_push($salida, 'Error al modificar parcela. '.$e->getMessage());
            }
            $conn->autocommit(true);

            } else {
                array_push($salida, "Faltan parametros obligatorios (nombre, vertices, campo");
                // falta el parametro nombre
                $http_code = 400;
            }

        }


    } else {
        array_push($salida, "Faltan parametros (modifcar)");
        $http_code = 400;
    }


} else {
    // no ha iniciado sesion
    array_push($salida, "Ninguna sesion activada");
    $http_code = 401;
}