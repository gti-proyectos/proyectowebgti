<?php
/**
 * Autor: Carlos Ramirez Dorado
 * GTI GRUPO 11
 * Fecha: 24/05/2020
 *
 * get-facturacion
 * Comprueba que existe una factura y la devuelve
 * en el caso de existir
 */

session_start();
if(isset($_SESSION['registrado']) && $_SESSION['registrado'] == 'ok') {
    if (isset($_GET['idTemporada'])&& $_GET['idTemporada'] != null) {
        // definimos la sentencia sql con los ? para el prepared statment
        $sql = "SELECT f.id, (SELECT nombre from parcela WHERE id = f.idParcela) as parcela, f.cantidad, f.fecha, f.perdida, f.concepto FROM facturacion f WHERE idTemporada = ?";
        // creamos una prepared statment
        $stmt = $conn->prepare($sql);

        // por cada ? definimos que parametro será y el valor que tendrá:
        $stmt->bind_param("s", $_GET['idTemporada']); // la s indica que el parametro es un string
        // ejecutamos la query
        $stmt->execute();

        // cogemos el resultado sql
        $resultSet = $stmt->get_result(); // get the mysqli result
        // recorremos el array
        while ($fila = mysqli_fetch_assoc($resultSet)) {
            // creamos el array asociativo final
            array_push($salida, $fila);
        }
        // se ha encontrado la factura y se va a devolver correctamente
        $http_code = 200;
    }
    else{
        //faltan parametros
        array_push($array1, "Error en get-facturacion. Faltan parametros obligatorios (idTemporadas)");
        $http_code = 401;
    }
}
else {
    //no ha iniciado sesion
    array_push($array1, "Error en get-facturacion. Ninguna sesion encontrada");
    $http_code = 401;
}
