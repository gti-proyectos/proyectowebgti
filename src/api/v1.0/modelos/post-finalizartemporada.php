<?php
/**
 * Autor: Carlos Ramirez Dorado
 * GTI GRUPO 11
 * Fecha: 21/05/2020
 *
 * post-temporada
 * Crear y modificar temporada
 *
 */
session_start();
if (isset($_SESSION['registrado']) && $_SESSION['registrado'] == 'ok') {
    if ((isset($_POST['id'])&&isset($_POST['fechaFin']))&&($_POST['id'] != '' && $_POST['fechaFin'] !='')) {

        // cogemos todos los datos posibles
        $id = $_POST['id'];
        $fecha = $_POST['fechaFin'];

        //terminamos la temporada
        $sql = 'UPDATE temporada set terminada = 1, fechaFin = ? WHERE id = ?';
        // si falla lanzar error
        // creamos una prepared statment
        $stmt = $conn->prepare($sql);
        // por cada ? definimos que parametro será y el valor que tendrá:
        $stmt->bind_param("ss",$fecha, $id);

        $stmt->execute();
        // comprobar que se ha modificado correctamente
        if(mysqli_affected_rows($conn) > 0 ){
            array_push($salida,"Temporada finalizada");
            $http_code = 200;
        }else{
            array_push($salida,"No se ha terminado la temporada. O no existe el usuario o ya está finalizada");
            $http_code = 400;
        }
        $stmt->close();

    }else{
        // no ha iniciado sesion
        array_push($salida, "Faltan parametros obligatorio. id, fechaFin");
        $http_code = 401;
    }
}
else {
    // no ha iniciado sesion
    array_push($salida, "Ninguna sesion activada");
    $http_code = 401;
}