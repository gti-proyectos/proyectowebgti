<?php
/**
 * Autor: Marc Castelló
 * Fecha: 03/05/2020
 * Descripción: Borrar usuarios-campos
 *
 */


session_start();

// comprobamos la sesion
if(isset($_SESSION['registrado']) && $_SESSION['registrado'] == 'ok'){

    // cogemos los datos pasados en el body
    $datosRecibidos = json_decode(file_get_contents('php://input'),true);


    if(isset($datosRecibidos['idUsuario']) && isset($datosRecibidos['idCampo'])){
        $sql = "DELETE FROM `usuarios-campos` where idUsuario = ? and idCampo = ?;";

        $idU = $datosRecibidos['idUsuario'];
        $idC = $datosRecibidos['idCampo'];

        // creamos una prepared statment
        $stmt = $conn->prepare($sql);
        // por cada ? definimos que parametro será y el valor que tendrá:
        $stmt->bind_param("ss", $idU,$idC);

        $stmt->execute();
        // comprobar que se ha modificado correctamente
        if(mysqli_affected_rows($conn) > 0 ){
            array_push($salida,"Usuario-campo borrado correctamente");
            $http_code = 200;
        }else{
            // no se ha borrado
            array_push($salida,"No se ha podido borrar el usuario-campo. No existe");
            $http_code = 400;
        }
        $stmt->close();
    }else{
        array_push($salida,"Falan parametros obligatorios (id)");
        $http_code = 400;
    }

}else{
    // no ha iniciado sesion
    array_push($salida,"Ninguna sesion activada");
    $http_code = 401;
}
