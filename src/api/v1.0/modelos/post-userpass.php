<?php

/**
 * Autor: Rubén Pardo
 * Fecha: 09/04/2020
 * Descripción: modificar la contraseña de un usuario
 *
 */
session_start();
// comprobamos la sesion
if (isset($_SESSION['registrado']) && $_SESSION['registrado'] == 'ok') {

    // modificar campo obligatorio
    if (isset($_POST['data'])) {
        $data = json_decode($_POST['data'],true);// convertimos la cadena de texto JSON a un array asociativo
        // cogemos los datos
        $id = $data['id'];
        $pass = $data['pass'];
        if($id!=null && $pass!=null){
            $sql = "UPDATE usuario set contrasenya = ? where id = ?;";

            // creamos una prepared statment
            $stmt = $conn->prepare($sql);
            // por cada ? definimos que parametro será y el valor que tendrá:
            $stmt->bind_param("ss", $pass,  $id);

            $stmt->execute();
            // comprobar que se ha modificado correctamente
            array_push($salida, "Contraseña modificado");
            $http_code = 200;
            $stmt->close();

        }else{
            array_push($salida, "Faltan parametros dentro de data (id o pass)");
            $http_code = 400;
        }

    } else {
        array_push($salida, "Faltan parametros (data)");
        $http_code = 400;
    }


} else {
    // no ha iniciado sesion
    array_push($salida, "Ninguna sesion activada");
    $http_code = 401;
}
