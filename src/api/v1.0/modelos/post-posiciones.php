<?php
/**
 * Autor: Carlos Ramirez Dorado
 * GTI GRUPO 11
 * Fecha: 03/05/2020
 *
 * post-posicion
 * Se crea una sesion si los credenciales recibidos
 * concuerdan en la base de datos
 *
 */

session_start();
// comprobamos la sesion
if(isset($_SESSION['registrado']) && $_SESSION['registrado'] == 'ok'){
    // modificar campo obligatorio
    if(isset($_POST['modificar']) && isset($_POST['data'])){
        // cogemos los datos
        $modificar = $_POST['modificar'];
        $data = json_decode($_POST['data'],true);// convertimos la cadena de texto JSON a un array asociativo

        // modificar campo
        if($modificar == 'true'){
            // cogemos todos los datos posibles
            $id = $data['id'];
            $latitud = $data['latitud'];
            $longitud = $data['longitud'];
            $parcela = $data['parcela'];
            $nombre = $data['nombre'];
            // editamos la posicion
            // parametros obligatorios (todos)
            if($id !=null && $latitud !=null && $longitud !=null && $parcela !=null && $nombre !=null){

                $sql = "UPDATE posiciones set latitud = ?,longitud = ?, idParcela = ?, nombre = ? where id = ?;";

                // creamos una prepared statment
                $stmt = $conn->prepare($sql);
                // por cada ? definimos que parametro será y el valor que tendrá:
                $stmt->bind_param("sssss",$latitud, $longitud, $parcela, $nombre, $id);

                $stmt->execute();
                // comprobar que se ha modificado correctamente
                if(mysqli_affected_rows($conn) > 0 ){
                    array_push($salida,"posicion modificada");
                    $http_code = 200;
                }else{
                    array_push($salida,"No se ha modifcado nada. No existe la posicion o la parcela, o son los mismos datos");
                    $http_code = 400;
                }
                $stmt->close();
            }else{
                // faltan parametros
                array_push($salida,"Faltan parametros obligatorios (id,latitud, longitud, idParcela nombre)");
                $http_code = 400;
            }



        }
        // crear posicion
        else{

            $posiciones = $data['posiciones'];
            $parcela = $data['parcela'];
            // comprobamos los parametros obligatorios (todos)
            if($posiciones !=null && $parcela !=null){

                // transaccion, se va a lanzar muchas sentencias, se requiere que se hagan todas o ninguna
                $conn->autocommit(false);
                try {
                    // creamos las posiciones
                    foreach ($posiciones as $posicion){
                        //fecha actual
                        $fecha = strval(date("Y-m-d"));
                        if($posicion['lat']!=null && $posicion['lng'] != null && $posicion['nombre'] != null){
                            $query = $conn->query("INSERT into  posiciones (latitud, longitud, idParcela, nombre, fechaCreacion) values('".$posicion['lat']."','".$posicion['lng']."','".$parcela."','".$posicion['nombre']."','".$fecha."')");

                            if ($query === FALSE) {
                                throw new Exception("Insertando posicion, ".$conn->error);
                            }
                        }else{
                            throw new Exception("No se han pasado los parametros obligatorios de un vertice(lat, lng,nombre, id de parcela)");
                        }
                    }
                    $conn->commit();
                    $http_code = 200;
                    array_push($salida, 'Posiciones creadas correctamente');

                } catch (Exception $e) {
                    $conn->rollback();
                    $conn->autocommit(true);
                    $http_code = 400;
                    array_push($salida, 'Error al crear posiciones. '.$e->getMessage());
                }
                $conn->autocommit(true);

            }
            else{
                array_push($salida,"Faltan parametros obligatorios(latitud, longitud, idParcela, nombre");
                // falta el parametros obligatorios
                $http_code = 400;
            }

        }


    }
    else{
        array_push($salida,"Falta parametro (modifcar)");
        $http_code = 400;
    }

}
else{
    // no ha iniciado sesion
    array_push($salida,"Ninguna sesion activada");
    $http_code = 401;
}
