<?php
/**
 * Autor: Rubén Pardo
 * Fecha: 30/04/2020
 * Descripción: Crear o modificar usuarios
 *
 */
session_start();
// comprobamos la sesion
if(isset($_SESSION['registrado']) && $_SESSION['registrado'] == 'ok'){
    // modificar campo obligatorio
    if(isset($_POST['modificar']) && isset($_POST['data'])){
        // cogemos los datos
        $modificar = $_POST['modificar'];
        $data = json_decode($_POST['data'],true);// convertimos la cadena de texto JSON a un array asociativo
        // cogemos todos los datos posibles
        $id = $data['id'];
        $nombre = $data['nombre'];
        $contrasenya = $data['contrasenya'];
        $apellidos = $data['apellidos'];
        $cliente = $data['cliente'];
        $rol = $data['rol'];
        $correo = $data['correo'];

        // modificar cliente
        if($modificar == 'true'){
            // editamos el cliente
            // id y nombre obligatorios
            if($nombre != null && $id != null && $rol !=null){

                $sql = "UPDATE usuario set nombre = ?, correo = ?, cliente = ?,apellidos=?, rol = ?  where id = ?;";

                // creamos una prepared statment
                $stmt = $conn->prepare($sql);
                // por cada ? definimos que parametro será y el valor que tendrá:
                $stmt->bind_param("ssssss",$nombre,$correo, $cliente, $apellidos,$rol, $id);

                $stmt->execute();
                // comprobar que se ha modificado correctamente
                if(mysqli_affected_rows($conn) > 0 ){
                    array_push($salida,"Usuario modificado");
                    $http_code = 200;
                }else{
                    array_push($salida,"No se ha modifcado nada. O no existe el usuario o son los mismos datos");
                    $http_code = 400;
                }
                $stmt->close();
            }else{
                // falta el parametro nombre e id
                array_push($salida,"Faltan parametros obligatorios (id, nombre, cliente, rol, contrasenya");
                $http_code = 400;
            }



        }
        // crear cliente
        else{
            // comprobamos los parametros obligatorios ( nombre )
            if($nombre != null && $rol !=null && $contrasenya !=null){
                $sql = "INSERT INTO usuario (nombre,apellidos, contrasenya, correo, cliente, rol) values(?, ?, ?, ?, ?,?);";

                // creamos una prepared statment
                $stmt = $conn->prepare($sql);
                // por cada ? definimos que parametro será y el valor que tendrá:
                $stmt->bind_param("ssssss",$nombre,$apellidos,$contrasenya, $correo, $cliente,$rol);
                $stmt->execute();
                // comprobar que se ha modificado correctamente
                if(mysqli_affected_rows($conn)>0){
                    array_push($salida,"Usuario creado");
                    $http_code = 200;
                }else{
                    array_push($salida,"Error sql. No se ha podido crear el usuario ");
                    $http_code = 400;
                }
                $stmt->close();


            }else{
                array_push($salida,"Faltan parametros obligatorios(nombre, rol, contrasenya");
                // falta el parametro nombre
                $http_code = 400;
            }

        }


    }
    else{
        array_push($salida,"Faltan parametros (modifcar)");
        $http_code = 400;
    }


}else{
    // no ha iniciado sesion
    array_push($salida,"Ninguna sesion activada");
    $http_code = 401;
}