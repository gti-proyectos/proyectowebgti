<?php
/**
 * Autor: Marc Castelló Martí
 * GTI GRUPO 11
 * Fecha: 09/03/2020
 *
 * get-sesion
 * Comprueba que existe un campo y lo devuelve
 * en el caso de existir
 *
 */
session_start();
if(isset($_SESSION['registrado']) && $_SESSION['registrado'] == 'ok' && isset($_GET['idCampo'])){

    // definimos la sentencia sql con los ? para el prepared statment
    //SELECT PARCELAS VERTICES
    $sql = "SELECT c.nombre as cNombre, c.descripcion as cDesc,p.id, p.nombre,p.descripcion, p.fechaCreacion, latitud, longitud, pos.id as verticeId FROM campos c inner join parcela p on c.id = p.idCampo inner join vertices pos on p.id = pos.idParcela WHERE c.id = ?";
    $objVertices = array();

    // creamos una prepared statment
    $stmt = $conn->prepare($sql);

    // por cada ? definimos que parametro será y el valor que tendrá:
    // la contraseña la ciframos a sha1, así es como esta en la bd
    $stmt->bind_param("s",$_GET['idCampo']); // la s indica que el parametro es un string

    // ejecutamos la query
    $stmt->execute();
    // cogemos el resultado sql
    $resultSet = $stmt->get_result(); // get the mysqli result



    // separar los vertices por parcela
    $arrayAux = array();

    while($fila = mysqli_fetch_assoc($resultSet)){
        array_push($arrayAux,$fila);
    }
    // Añadimos el nombre y la desc del campo
    $salida['nombreCampo'] = $arrayAux[0]['cNombre'];
    $salida['descCampo'] = $arrayAux[0]['cDesc'];

    $contador = 0;//contador para marcar las parcelas
    $contadorSecundario = 0;//contador para marcar los vertices de las parcelas
    $verticesPorParcela = array();
    $idParcela = $arrayAux[0]['id'];
    $aux = $idParcela;

    foreach ($arrayAux as $tupla ){
        $idParcela = $tupla['id'];

        if($idParcela != $aux){
            $contador++;// hemos cambiado de parcela
            $contadorSecundario =0;//resteamos el cont de vertices a cero
        }
        // añadimos el nombre y la desc
        $verticesPorParcela[$contador]['id'] = $tupla['id'];
        $verticesPorParcela[$contador]['nombre'] = $tupla['nombre'];
        $verticesPorParcela[$contador]['descripcion'] = $tupla['descripcion'];
        $verticesPorParcela[$contador]['fechaCreacion'] = $tupla['fechaCreacion'];


        // en el campo vertices añadimos la lat y lng
        $verticesPorParcela[$contador]['vertices'][$contadorSecundario]['lat'] = $tupla['latitud'];
        $verticesPorParcela[$contador]['vertices'][$contadorSecundario]['lng'] = $tupla['longitud'];
        $verticesPorParcela[$contador]['vertices'][$contadorSecundario]['verticeId'] = $tupla['verticeId'];
        $contadorSecundario++;// sumamos al cont de vertices

        $aux = $idParcela;//

    }
    array_push($salida,$verticesPorParcela);
    $http_code=200;
}else{
    $http_code = 401;
}