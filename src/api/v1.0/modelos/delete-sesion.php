<?php
/**
 * Autor: Rubén Pardo Casanova
 * GTI GRUPO 11
 * Fecha: 27/02/2020
 *
 * delete-sesion
 * Elimina la sesión con usuario identificado.
 *
 */
// iniciamos la sesion para evitar errores
session_start();

if(isset($_SESSION['registrado']) && $_SESSION['registrado'] == 'ok'){
    // si existe una sesion
    // borramos todos los parametros
    unset($_SESSION['registrado']);
    unset($_SESSION['id']);
    // destruimos la sesion
    session_destroy();
    $http_code = 200;
}else{
    // si no existe una sesion devolvemos 401
    $http_code = 401;
}
