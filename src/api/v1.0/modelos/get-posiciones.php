<?php

/**
 ** Autor: Aitor Benítez Estruch
 * GTI GRUPO 11
 * Fecha: 26/03/2020
 *
 *
 * get-posiciones.php obtiene todas las posiciones de las sondas que se encuentran dentro de un campo
 * Parámetros:
 *  - Id del campo
 * */


session_start();

if(isset($_SESSION['registrado']) && $_SESSION['registrado'] == 'ok' && isset($_GET['idCampo'])){

    $sql = "SELECT pos.id as id, pos.nombre as nombre, pos.latitud as latitud, pos.longitud as longitud, pos.idParcela as parcela, pos.fechaCreacion as fechaCreacion FROM campos c inner join parcela p on c.id = p.idCampo inner join posiciones pos 
            on p.id = pos.idParcela where  c.id = ?";

    //Conexión a la bbdd
    $stmt = $conn->prepare($sql);

    // por cada ? definimos que parametro será y el valor que tendrá:
    $stmt->bind_param("i",$_GET['idCampo']); // la i indica que el parametro es un entero

    $stmt -> execute();

    $resultSet = $stmt->get_result();

    //array donde se guardarán las id de las parcelas
    $parcelas = array();

    while ($fila = mysqli_fetch_assoc($resultSet)){
        //Variable booleana
        $is = 0;

        //saber si el id de la parcela está incliudo en el array parcelas
        for ($i = 0; $i<count($parcelas); $i++){
            if($parcelas[$i] == $fila['parcela']){
                $is =1;
            }
        }

        //definición de un objeto con los valores del id de posición, latitud i longitud
        $obj['id'] = $fila['id'];
        $obj['nombre'] = $fila['nombre'];
        $obj['latitud'] = $fila['latitud'];
        $obj['longitud'] = $fila['longitud'];
        $obj['parcela'] =$fila['parcela'];
        $obj['fechaCreacion'] =$fila['fechaCreacion'];

        // atributos auxiliares parcela i posiciones
        $aux['parcela'] = '';
        $aux['posiciones'] = [];

        //Si el id de la parcela no está incluido en el array parcelas
        if ($is==0){
            $aux['parcela'] = $fila['parcela'];
            array_push($aux['posiciones'], $obj);
            array_push($salida, $aux);
            array_push($parcelas,$fila['parcela']);
        }else{
            //En caso de que el id de la parcela ya esté incluido en el array
            for ($i = 0; $i<count($parcelas); $i++){
                if($fila['parcela'] == $salida[$i]['parcela']){
                    array_push($salida[$i]['posiciones'], $obj);
                }
            }
        }
    }
    $http_code = 200;
}else{
    $http_code = 401;
}