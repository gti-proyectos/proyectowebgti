<?php
/**
 * Autor: Carlos Ramirez Dorado
 * GTI GRUPO 11
 * Fecha: 03/05/2020
 *
 * post-campo
 * Se crea una sesion si los credenciales recibidos
 * concuerdan en la base de datos
 *
 */

session_start();
// comprobamos la sesion
if(isset($_SESSION['registrado']) && $_SESSION['registrado'] == 'ok'){
    // modificar campo obligatorio
    if(isset($_POST['modificar']) && isset($_POST['data'])){
        // cogemos los datos
        $modificar = $_POST['modificar'];
        $data = json_decode($_POST['data'],true);// convertimos la cadena de texto JSON a un array asociativo
        // cogemos todos los datos posibles
        $id = $data['id'];
        $nombre = $data['nombre'];
        $descripcion = $data['descripcion'];
        $cliente = $data['cliente'];

        // modificar campo
        if($modificar == 'true'){
            // editamos el campo
            // id, nombre y cliente obligatorios
            if($nombre != null && $id != null && $cliente !=null){

                $sql = "UPDATE campos set nombre = ?,descripcion = ?, idCliente = ? where id = ?;";

                // creamos una prepared statment
                $stmt = $conn->prepare($sql);
                // por cada ? definimos que parametro será y el valor que tendrá:
                $stmt->bind_param("ssss",$nombre, $descripcion, $cliente, $id);

                $stmt->execute();
                // comprobar que se ha modificado correctamente
                if(mysqli_affected_rows($conn) > 0 ){
                    array_push($salida,"Campo modificado");
                    $http_code = 200;
                }else{
                    array_push($salida,"No se ha modifcado nada. No existe el campo o el cliente, o son los mismos datos");
                    $http_code = 400;
                }
                $stmt->close();
            }else{
                // falta el parametro nombre e id
                array_push($salida,"Faltan parametros obligatorios (id, nombre, cliente)");
                $http_code = 400;
            }



        }
        // crear campo
        else{
            // comprobamos los parametros obligatorios ( nombre, cliente )
            if($nombre != null && $cliente !=null){
                //fecha actual
                $fecha = strval(date("Y-m-d"));
                $sql = "INSERT INTO campos (nombre, descripcion, idCliente, fechaCreacion) values(?, ?, ?, '".$fecha."');";

                // creamos una prepared statment
                $stmt = $conn->prepare($sql);
                // por cada ? definimos que parametro será y el valor que tendrá:
                $stmt->bind_param("sss",$nombre,$descripcion, $cliente);
                $stmt->execute();
                // comprobar que se ha modificado correctamente
                if(mysqli_affected_rows($conn)>0){
                    array_push($salida,"Campo creado");
                    $http_code = 200;
                }else{
                    array_push($salida,"Error sql. No se ha podido crear el campo ");
                    $http_code = 400;
                }
                $stmt->close();


            }
            else{
                array_push($salida,"Faltan parametros obligatorios(nombre, cliente");
                // falta el parametros obligatorios
                $http_code = 400;
            }

        }


    }
    else{
        array_push($salida,"Falta parametro (modificar)");
        $http_code = 400;
    }


}
else{
    // no ha iniciado sesion
    array_push($salida,"Ninguna sesion activada");
    $http_code = 401;
}