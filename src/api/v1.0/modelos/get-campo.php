<?php
/**
 * Autor: Carlos Ramirez Dorado
 * GTI GRUPO 11
 * Fecha: 29/04/2020
 *
 * get-campos
 * Comprueba que existe un campo y lo devuelve
 * en el caso de existir
 *
 */
session_start();
//comprobamos la sesion
if(isset($_SESSION['registrado']) && $_SESSION['registrado'] == 'ok'){
    // id es el idUsuario. Existe uno de los dos id
    if(isset($_GET['id']) || isset($_GET['idCliente'])) {
        //GET campo del usuario
        if (isset($_GET['id'])) {
            // definimos la sentencia sql con los ? para el prepared statment
            $sql = "SELECT c.id, c.nombre, c.descripcion, c.fechaCreacion FROM usuario u INNER JOIN `usuarios-campos` uc on uc.idUsuario = u.id INNER JOIN campos c on uc.idCampo = c.id WHERE u.id = ?";

            // creamos una prepared statment
            $stmt = $conn->prepare($sql);

            // por cada ? definimos que parametro será y el valor que tendrá:
            $stmt->bind_param("s", $_GET['id']); // la s indica que el parametro es un string

            // ejecutamos la query
            $stmt->execute();
            // cogemos el resultado sql
            $resultSet = $stmt->get_result(); // get the mysqli result

            // recorremos el array
            while ($fila = mysqli_fetch_assoc($resultSet)) {
                // creamos el array asociativo final
                array_push($salida, $fila);
            }
            // se ha encontrado el usuario y se va a devolver correctamente
            $http_code = 200;
        }
        elseif (isset($_GET['idCliente'])) {
            $sql = 'SELECT id, nombre, descripcion, fechaCreacion FROM campos c WHERE c.idCliente = ?';
            // creamos una prepared statment
            $stmt = $conn->prepare($sql);

            // por cada ? definimos que parametro será y el valor que tendrá:
            $stmt->bind_param("s", $_GET['idCliente']); // la s indica que el parametro es un string

            // ejecutamos la query
            $stmt->execute();
            // cogemos el resultado sql
            $resultSet = $stmt->get_result(); // get the mysqli result

            // recorremos el array
            while ($fila = mysqli_fetch_assoc($resultSet)) {
                // creamos el array asociativo final
                array_push($salida, $fila);
            }
            // se ha encontrado el cliente y se va a devolver correctamente
            $http_code = 200;
        }
    }
    else{
        //faltan parametros
        $http_code = 400;
    }
}
else {
    //no ha iniciado sesion
    $http_code = 401;
}
