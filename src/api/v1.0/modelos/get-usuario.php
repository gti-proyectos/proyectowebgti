<?php
/**
 * Autor: Rubén Pardo
 * Fecha: 29/04/2020
 * Descripción: Obtener todos los usuarios
 * si se pasa un id de cliente solo se pasaran de ese cliente [idCliente]
 * si se pasa un tipo de usuario solo se pasarn ese tipo de usuarios [rol]
 *
 */
session_start();

// comprobamos la sesion
if(isset($_SESSION['registrado']) && $_SESSION['registrado'] == 'ok'){
    $rol = $_GET['rol'];

    // filtrar por cliente
    if(isset($_GET['idCliente'])){
        // usuarios por cliente
        $idCliente = $_GET['idCliente'];

        // no filtrar por rol
        if($rol == null){
            //no filtramos por rol
            $sql = 'SELECT u.id,u.nombre,u.correo,u.apellidos,u.contrasenya,u.rol as rolId, rol.nombre as rol FROM usuario u INNER join rol on u.rol = rol.id WHERE cliente = ?';
            // creamos una prepared statment
            $stmt = $conn->prepare($sql);
            // por cada ? definimos que parametro será y el valor que tendrá:
            $stmt->bind_param("s",$idCliente); // la s indica que el parametro es un string

        }
        // filtrar por rol
        else{
            // filtramos por rol
            $sql = 'SELECT u.id,u.nombre,u.correo,u.apellidos,u.contrasenya,u.rol as rolId, rol.nombre as rol FROM usuario u INNER join rol on u.rol = rol.id WHERE cliente = ? AND u.rol = ? ';
            // creamos una prepared statment
            $stmt = $conn->prepare($sql);
            // por cada ? definimos que parametro será y el valor que tendrá:
            $stmt->bind_param("ss",$idCliente,$rol); // la s indica que el parametro es un string
        }

        // ejecutamos la query
        $stmt->execute();
        // cogemos el resultado sql
        $resultSet = $stmt->get_result(); // get the mysqli result


        // recorremos el array
        while($fila = mysqli_fetch_assoc($resultSet)){
            // creamos el array asociativo final
            array_push($salida, $fila);
        }
        // se ha encontrado el usuario y se va a devolver correctamente
        $http_code = 200;


    }
    //no filtrar por cliente
    else{
       // todos los usuarios
        if($rol == null){
            // sin filtrar por rol
            $sql = 'SELECT u.id,u.nombre,u.correo,u.apellidos,u.contrasenya,u.rol as rolId, rol.nombre as rol FROM usuario u INNER join rol on u.rol = rol.id';
            // creamos una prepared statment
            $stmt = $conn->prepare($sql);
        }
        // todos los usuarios filtrados por rol
        else{
            // filtramos por rol
            $sql = 'SELECT u.id,u.nombre,u.correo,u.apellidos,u.contrasenya,u.rol as rolId, rol.nombre as rol FROM usuario u INNER join rol on u.rol = rol.id AND u.rol = ? ';
            // creamos una prepared statment
            $stmt = $conn->prepare($sql);
            // por cada ? definimos que parametro será y el valor que tendrá:
            $stmt->bind_param("s",$rol); // la s indica que el parametro es un string
        }

        // ejecutamos la query
        $stmt->execute();
        // cogemos el resultado sql
        $resultSet = $stmt->get_result(); // get the mysqli result


        // recorremos el array
        while($fila = mysqli_fetch_assoc($resultSet)){
            // creamos el array asociativo final
            array_push($salida, $fila);
        }
        // se ha encontrado el usuario y se va a devolver correctamente
        $http_code = 200;
    }


}else{
    // no ha iniciado sesion
    $http_code = 401;
}