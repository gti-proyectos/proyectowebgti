<?php
/**
 * Autor: Rubén Pardo Casanova
 * GTI GRUPO 11
 * Fecha: 26/02/2020
 *
 * get-sesion
 * Comprueba que existe una sesion y la devuelve
 * en el caso de existir
 *
 */
session_start();

if(isset($_SESSION['registrado']) && $_SESSION['registrado'] == 'ok' && isset($_SESSION['id'])){

    // definimos la sentencia sql con los ? para el prepared statment
    // el if de la query indica si tiene solo un campo devolver el id si no devolverá null
    $sql = "SELECT usuario.id as id, usuario.nombre as nombre, rol.id as rolid, rol.nombre as rol, usuario.cliente as cliente,c.nombre as nombreCliente,
            IF((SELECT count(*) FROM `usuarios-campos` WHERE idUsuario = usuario.id)=1, (SELECT idCampo FROM `usuarios-campos` WHERE idUsuario = usuario.id) , null) as idCampo 
            FROM usuario 
            inner join rol on usuario.rol = rol.id 
            inner join cliente c on c.id = usuario.cliente 
            where usuario.id = ?";

    // creamos una prepared statment
    $stmt = $conn->prepare($sql);

    // por cada ? definimos que parametro será y el valor que tendrá:
    // la contraseña la ciframos a sha1, así es como esta en la bd
    $stmt->bind_param("s",$_SESSION['id']); // la ss indica que el parametro es un string

    // ejecutamos la query
    $stmt->execute();
    // cogemos el resultado sql
    $resultSet = $stmt->get_result(); // get the mysqli result

    // si es 0 puede que se el admin general
    if (mysqli_affected_rows($conn) == 0) {
        $sql = "SELECT usuario.id as id, usuario.nombre as nombre, rol.id as rolid, rol.nombre as rol, usuario.cliente as cliente, (SELECT count(*) FROM `usuarios-campos` WHERE idUsuario = usuario.id) as cuantosCampos FROM usuario inner join rol on usuario.rol = rol.id where usuario.id = ?";
        // creamos una prepared statment
        $stmt = $conn->prepare($sql);

        // por cada ? definimos que parametro será y el valor que tendrá:
        // la contraseña la ciframos a sha1, así es como esta en la bd
        $stmt->bind_param("s",$_SESSION['id']); // la ss indica que el parametro es un string

        // ejecutamos la query
        $stmt->execute();
        // cogemos el resultado sql
        $resultSet = $stmt->get_result(); // get the mysqli result
    }

    $rol =[];
    // como maximo solo tenemos una fila
    if($fila = mysqli_fetch_assoc($resultSet)){
        // creamos el array asociativo de rol
        $rol['id'] = $fila['rolid'];
        $rol['nombre'] = $fila['rol'];
        // creamos el array asociativo final
        $salida['id'] = $fila['id'];
        $salida['nombre'] = $fila['nombre'];
        $salida['rol'] = $rol;
        $salida['cuantosCampos'] = $fila['cuantosCampos'];
        $salida['cliente'] = $fila['cliente'];
        $salida['nombreCliente'] = $fila['nombreCliente'];
        $salida['idCampo'] = $fila['idCampo'];
        // se ha encontrado el usuario y se va a davolver correctamente
        $http_code = 200;
    }else{
        $http_code = 401;
    }

}else{
    $http_code = 401;
}
