<?php
/**
 * Autor: Rubén Pardo Casanova
 * GTI GRUPO 11
 * Fecha: 26/02/2020
 *
 * post-sesion
 * Se crea una sesion si los credenciales recibidos
 * concuerdan en la base de datos
 *
 */


// las claves para acceder a las propiedades recibidas
$kUsuario = "user";
$kPass = "pass";

// comprobamos que recibimos todos los parametros y que no esten vacios
if(isset($_POST[$kPass]) && isset($_POST[$kUsuario]) && ($_POST[$kPass] !='' && $_POST[$kUsuario]!='')){


    // definimos la sentencia sql con los ? para el prepared statment
    $sql = "SELECT rol,id FROM usuario where nombre = ? AND contrasenya = ? ";

    // creamos una prepared statment
    $stmt = $conn->prepare($sql);

    // por cada ? definimos que parametro será y el valor que tendrá:
    // la contraseña la ciframos a sha1, así es como esta en la bd
    $stmt->bind_param("ss",$_POST[$kUsuario],$_POST[$kPass]); // la ss indica que el parametro es un string

    // ejecutamos la query
    $stmt->execute();
    // cogemos el resultado sql
    $resultSet = $stmt->get_result(); // get the mysqli result
    //$res = $resultSet->fetch_assoc(); // fetch data

    $res = [];

    // como maximo habrá una fila
    if($fila = mysqli_fetch_assoc($resultSet)){

        session_start();//crea una sesion si no esta creada ya
        $_SESSION['registrado'] = 'ok';
        // guardamos el id del usuario en sesion para poder hacer el get-sesion
        $_SESSION['id'] = $fila['id'];

        // existe el usuario http_code ok
        $http_code = 200;
    }else{
        // no ha encontrado ningun usuario con esos credenciales
        $http_code = 401;
    }

}else{
    $http_code = 400;
}
