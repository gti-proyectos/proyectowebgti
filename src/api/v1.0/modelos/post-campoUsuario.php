<?php

/**
 * Autor: Marc Castelló
 * Fecha: 03/05/2020
 * Descripción: Crear o modificar usuario-campo
 *
 */
session_start();
// comprobamos la sesion
if (isset($_SESSION['registrado']) && $_SESSION['registrado'] == 'ok') {
    // modificar campo obligatorio
    if (isset($_POST['modificar']) && isset($_POST['data'])) {
        // cogemos los datos
        $modificar = $_POST['modificar'];
        $data = json_decode($_POST['data'], true);// convertimos la cadena de texto JSON a un array asociativo
        // cogemos todos los datos posibles
        $id = $data['id'];
        $idCampo = $data['idCampo'];
        $idUsuario = $data['idUsuario'];

        // modificar linia
        if ($modificar == 'true') {
            // editamos la linia
            // id y nombre obligatorios
            if ($id != null && $idCampo != null && $idUsuario != null ) {

                $sql = "UPDATE `usuarios-campos` set idCampo = ?, idUsuario = ? where id = ?;";

                // creamos una prepared statment
                $stmt = $conn->prepare($sql);
                // por cada ? definimos que parametro será y el valor que tendrá:
                $stmt->bind_param("sss", $idCampo, $idUsuario, $id);

                $stmt->execute();
                // comprobar que se ha modificado correctamente el usuario-campo
                if (mysqli_affected_rows($conn) > 0) {
                    array_push($salida, "Usuario modificado correctamente en el usuario-campo");
                    $http_code = 200;
                } else {
                    array_push($salida, "Error SQL. O no existe el usuario-campo o son los mismos datos");
                    $http_code = 400;
                }
                $stmt->close();
            } else {
                // falta el parametro id, idCampo o idUsuario
                array_push($salida, "Faltan parametros obligatorios (id, idCampo, idUsuario");
                $http_code = 400;
            }


        } // crear usuario-campo
        else {
            // comprobamos los parametros obligatorios ( idCampo, idUsuario )
            if ($idCampo != null && $idUsuario != null) {
                $sql = "INSERT INTO `usuarios-campos` (idCampo, idUsuario) values(?, ?);";

                // creamos una prepared statment
                $stmt = $conn->prepare($sql);
                // por cada ? definimos que parametro será y el valor que tendrá:
                $stmt->bind_param("ss", $idCampo, $idUsuario);
                $stmt->execute();
                // comprobar que se ha modificado correctamente
                if (mysqli_affected_rows($conn) > 0) {
                    array_push($salida, "Usuario-campo creado");
                    $http_code = 200;
                } else {
                    array_push($salida, "Error sql. No se ha podido crear el usuario-campo. No existe el usuario o campo ");
                    $http_code = 400;
                }
                $stmt->close();


            } else {
                array_push($salida, "Faltan parametros obligatorios(idCampo, idUsuario");
                // falta el parametro nombre
                $http_code = 400;
            }

        }


    } else {
        array_push($salida, "Faltan parametros (modifcar)");
        $http_code = 400;
    }


} else {
    // no ha iniciado sesion
    array_push($salida, "Ninguna sesion activada");
    $http_code = 401;
}