





 async function nuevaFactura () {

    try{
        let datos = {
            idParcela: 2,
            concepto: 'Mano de obra',
            cantidad: '300',
            fecha: '2019-05-22',
            tipoFactura: 'gasto'

        }

        let form = new FormData();
        form.append("idTemporada","3");
        form.append("datos",JSON.stringify(datos));

        await fetch('../../src/api/v1.0/facturacion',{
            method: 'POST',
            body: form,
        })
            .then(respuesta => {
                if (respuesta.status === 200) {
                    console.log("TOT OK")
                    return respuesta.json();
                } else {
                    this.controlador.errorUpdate();
                    throw Error(respuesta.statusText);

                }
            })



    }catch (e) {
        console.error(e);
    }
}
